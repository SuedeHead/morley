<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Transaction fees and storage cost

Transaction cost consists of 2 components:
* Fees that go to the baker
* Storage cost that gets burned

Tx `SOURCE` always pays both of the components (see the description of the `SOURCE` instruction here: http://tezos.gitlab.io/whitedoc/michelson.html, also confirmed during tests on alphanet).

The whitepaper features the following passage which is a source of confusion:
>Since storage imposes a cost on the network, a minimum fee of ꜩ 1 is assessed
for each byte increase in the storage. For instance, if after the execution of a
transaction, an integer has been added to the storage and ten characters have
been appended to an existing string in the storage, then ꜩ 18 will be withdrawn
from the contract’s balance and destroyed.

I could not find any confirmation that this is true. Note that generally the [whitepaper](https://tezos.com/static/white_paper-2dc8c02267a8fb86bd67a108199441bf.pdf) is **outdated and must be ignored.** See protocol descriptions in the [whitedoc](https://tezos.gitlab.io/mainnet/index.html) instead.

The whitepaper also says that contracts must have positive balance to _exist,_ which is also [not true](https://tezos.stackexchange.com/questions/886/what-are-the-new-rules-for-when-a-kt1-gets-destroyed-or-when-burn-cap-must) — contracts may have zero balance and it is perfectly fine.

`AMOUNT` excludes fees and burned tez, so it is perfectly fine to make transactions with `amount = 0`.

## Fees (that go to the baker)

Bakers choose the transactions from the mempool and include them into blocks. Bakers are economically incentivized to take the most profitable transactions first. Starting from [003_PsddFKi3](https://gitlab.com/tezos/tezos/-/blob/767de2b6665ec2cc21e41e6348f8a0b369d26450/docs/protocols/003_PsddFKi3.rst)
```
fees >= minimal_fees +
        minimal_nanotez_per_byte * size +
        minimal_nanotez_per_gas_unit * gas
```

I could not find any clarifications on how this formula is enforced on the protocol level, i.e., I don't understand why bakers are _obliged_ to use this formula and not any other `f(size, gas)` apart from the fact that this formula is the one used in the reference implementation.

In this formula, `size` — the number of bytes of the complete serialized operation, including header and signature; `gas` — **the announced gas limit** (not the actual gas used). Bakers are free to configure `minimal_*` values as they see fit. Currently by default:
```
minimal_fees = 0.000 1ꜩ (100µꜩ)
minimal_nanotez_per_gas_unit = 100nꜩ/gu (0.000 000 1ꜩ/gu)
minimal_nanotez_per_byte = 1000nꜩ/B (0.000 001ꜩ/B)
```

Testnet nodes seem to stick to the default values. **This may possibly not be the case for the real network,** where the expected values configured by the administrator of `tezos-client` may diverge from economically-justified parameters. In this case, txs may pass client validation but get stuck in mempool forever. (I could not find the definition of "forever" in the official documentation but [this answer](https://tezos.stackexchange.com/questions/1856/operation-injected-to-node-but-not-added-to-blockchain-wallet-counter-error) suggests Tezos has some limit for stuck transactions to prevent mempool bloat).

Since transaction execution fees depend on the announced gas limit, you might be interested in how to predict and optimize gas consumption for your contract. There is a separate [document](./gasConsumption.md) that describes Tezos gas accounting model in detail.

## Storage cost (that gets burned)

If as a result of an operation a new address is added to the blockchain state, the burn cost is determined by the following formula:

```
storage_cost = 0.257 + cost_per_byte * (contract_size + storage_size) ꜩ
cost_per_byte = 0.001 ꜩ/B (by default)
```

`contract_size` and `storage_size` are 0 for implicit accounts. For contract accounts, these are equal to contract bytecode size and initial storage size (in bytes) correspondingly.

For other operations:

```
storage_cost = cost_per_byte * storage_size_diff ꜩ, if storage_size_diff > 0
storage_cost = 0 ꜩ, otherwise

storage_size_diff = sum( (for each contract) storage_size - max_storage_size)
cost_per_byte = 0.001 ꜩ/B (by default)
```
where `storage_size` is the new storage size, `max_storage_size` is the maximum of all historical storage sizes for the target contract. Note that:
1. There are no refunds
2. The new storage size is compared against the historical maximum value and not the most recent one

Thus you don't pay for the same storage twice: it is free to remove `X` bytes of data, and then add `X` bytes of data in a separate transaction.

Users of `tezos-client` are required to limit the storage cost by passing a `--burn-cap` command line argument.  Burn cap specifies an upper bound on storage cost. If the actual storage cost computed using the formula above is lower than the burn cap, the actual storage cost gets burned, not the burn cap. If the storage cost is higher than the specified cap, the transaction fails.

## Cost of failed operations

> What happens if contract execution fails? In Ethereum you'll still pay some fees and transaction will be recorded. Is it the same in Tezos?

Most probably it will be rejected by the client and the client would not broadcast it. However, it is possible to emit an operation that will be accepted by the client but rejected by other nodes (e.g., you can make a mutex contract and emit two `acquireOrFail` operations simultaneously — both will be accepted by the client but one of them would be rejected by the baker).

You pay _execution fees_ (min + gas + opsize) but you don't burn tz for _storage_. An example of failed operation: https://better-call.dev/carthagenet/opg/onwnMiceCEw9GrjLSRQPR3if34ExdTmNuafTdEYF1SC2kdE9sqN/contents.
