# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

<<: !include "../hpack/module.yaml"

<<: *meta
name:                cleveland
version:             0.1.0
synopsis:            Testing framework for Morley.
author:              Serokell, Tocqueville Group
copyright:           2020 Tocqueville Group
description:
  This package provides an eDSL for testing contracts written in Michelson, Morley or Lorentz.
  These tests can be run on an emulated environment or on a real network.
category:            Blockchain
extra-source-files:
  - README.md

library:
  <<: *lib-common

  generated-other-modules:
    - Paths_cleveland

  dependencies:
    - HUnit
    - caps
    - constraints >= 0.11
    - containers
    - criterion
    - cryptonite
    - data-default
    - directory
    - fmt
    - hedgehog >= 1.0.3
    - hspec
    - hspec-expectations
    - lens
    - lorentz
    - morley
    - morley-client
    - morley-prelude
    - mtl
    - named
    - o-clock
    - optparse-applicative
    - servant-client-core
    - singletons
    - statistics
    - tagged
    - tasty
    - tasty-ant-xml
    - tasty-hedgehog
    - tasty-hunit-compat
    - text
    - time
    - with-utf8

tests:
  cleveland-test:
    <<: *test-common
    source-dirs:
      - test

    dependencies:
      - cleveland
      - containers
      - fmt
      - hspec-expectations
      - lorentz
      - morley
      - morley-client
      - morley-prelude
      - o-clock
      - raw-strings-qq
      - servant-client
      - tasty
      - tasty-hunit-compat
      - text
      - time

  morley-test:
    <<: *test-common
    source-dirs:
      - "morley-test"
      - test-common

    default-extensions:
      - DerivingStrategies

    dependencies:
      - aeson
      - bytestring
      - cleveland
      - containers
      - data-default
      - directory
      - filepath
      - fmt
      - hedgehog >= 1.0.3
      - hex-text
      - hspec
      - hspec-expectations
      - HUnit
      - lens
      - megaparsec >= 7.0.0
      - morley
      - morley-client
      - morley-prelude
      - silently
      - syb
      - tasty
      - tasty-hedgehog
      - tasty-hspec
      - tasty-hunit-compat
      - text
      - unordered-containers
      - vinyl
      - with-utf8

  lorentz-test:
    <<: *test-common
    source-dirs:
      - "lorentz-test"

    default-extensions:
      - DerivingStrategies

    dependencies:
      - bimap
      - bytestring
      - constraints
      - containers
      - filepath
      - first-class-families
      - fmt
      - hedgehog >= 1.0.3
      - HUnit
      - lorentz
      - morley
      - cleveland
      - morley-prelude
      - singletons
      - spoon
      - tasty
      - tasty-hunit-compat
      - tasty-hedgehog
      - text
      - type-spec
      - unordered-containers


benchmarks:
  morley-bench:
    <<: *bench-common
    source-dirs: ["morley-bench"]

    dependencies:
      - gauge
      - megaparsec >= 7.0.0
      - morley
      - cleveland
      - morley-prelude
      - with-utf8
