-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Testing utility functions used by testing framework itself or
-- intended to be used by test writers.

module Cleveland.Util
  ( leftToShowPanic
  , leftToPrettyPanic
  , failedTest
  , succeededTest
  , eitherIsLeft
  , eitherIsRight
  , meanTimeUpperBoundProp
  , meanTimeUpperBoundPropNF
  , genTuple2
  , assertGoesBefore
  , goesBefore
  , partialParse
  , runGen
  , roundtripTree
  , formatValue
  , formatSomeValue
  , ShowWith(..)
  , Showing(..)
  , ceilingUnit
  , timeToFixed
  , timeToNominalDiffTime

  -- * Re-exports
  --
  -- | These functions from "Time" are re-exported here to make it convenient to call
  -- 'meanTimeUpperBoundProp' and 'meanTimeUpperBoundPropNF'.
  , mcs, ms, sec, minute
  ) where

import Criterion (Benchmarkable, benchmarkWith', nf, whnf)
import Criterion.Main (defaultConfig)
import Criterion.Types (SampleAnalysis(anMean), Verbosity(Quiet), reportAnalysis, verbosity)
import Data.Fixed (Fixed, HasResolution)
import Data.Singletons (demote)
import Data.Time (NominalDiffTime, secondsToNominalDiffTime)
import Data.Typeable (typeRep)
import Fmt (Buildable, Builder, build, pretty, (+|), (+||), (|+), (||+))
import Hedgehog
  (Gen, MonadGen, MonadTest, Property, annotate, evalIO, failure, forAll, property, success,
  tripping, withTests)
import Hedgehog.Internal.Gen (runGenT)
import qualified Hedgehog.Internal.Seed as Seed
import Hedgehog.Internal.Tree (TreeT(runTreeT), nodeValue)
import qualified Hedgehog.Range as Range
import Michelson.Doc (DocItem, docItemPosition)
import Michelson.Typed (OpPresence(OpAbsent, OpPresent), SingI, SomeValue, SomeValue'(..), sing)
import qualified Michelson.Typed as T
import Statistics.Types (Estimate(estPoint))
import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import Test.Tasty.HUnit (testCase)
import Text.Printf (printf)
import qualified Text.Show
import Time
  (KnownDivRat, KnownUnitName, Microsecond, Millisecond, Minute, Nanosecond, Picosecond, Rat,
  RatioNat, Second, Time, mcs, minute, ms, ns, sec, time, timeout, toUnit, unTime, unitNameVal)

leftToShowPanic :: (Show e, HasCallStack) => Either e a -> a
leftToShowPanic = either (error . show) id

leftToPrettyPanic :: (Buildable e, HasCallStack) => Either e a -> a
leftToPrettyPanic = either (error . pretty) id

----------------------------------------------------------------------------
-- Property
----------------------------------------------------------------------------

-- | A 'Property' that always fails with given message.
failedTest :: (HasCallStack, MonadTest m) => Text -> m ()
failedTest r = withFrozenCallStack $ annotate (toString r) >> failure

-- | A 'Property' that always succeeds.
succeededTest :: MonadTest m => m ()
succeededTest = success

-- | The 'Property' holds on `Left a`.
eitherIsLeft :: (Show b, MonadTest m, HasCallStack) => Either a b -> m ()
eitherIsLeft = \case
  Left _ -> succeededTest
  Right x -> withFrozenCallStack $ failedTest $ "expected Left, got Right (" <> show x <> ")"

-- | The 'Property' holds on `Right b`.
eitherIsRight :: (Show a, MonadTest m, HasCallStack) => Either a b -> m ()
eitherIsRight = \case
  Right _ -> succeededTest
  Left x -> withFrozenCallStack $ failedTest $ "expected Right, got Left (" <> show x <> ")"

-- | Benchmarks the given function and checks that the mean time to evaluate to weak head
-- normal form is under the given amount of time.
--
-- This test fails if the benchmark takes longer than 30 seconds to run.
meanTimeUpperBoundProp
  :: (KnownDivRat unit Second, KnownUnitName unit, HasCallStack)
  => Time unit -> (a -> b) -> a -> Property
meanTimeUpperBoundProp upperBound run arg =
  withFrozenCallStack $
    checkReport upperBound $ whnf run arg

-- | Benchmarks the given function and checks that the mean time to evaluate to
-- normal form is under the given amount of time.
--
-- This test aborts and fails if the benchmark takes longer than 120 seconds to run.
meanTimeUpperBoundPropNF
  :: (KnownDivRat unit Second, KnownUnitName unit, HasCallStack, NFData b)
  => Time unit -> (a -> b) -> a -> Property
meanTimeUpperBoundPropNF upperBound run arg =
  withFrozenCallStack $
    checkReport upperBound $ nf run arg

checkReport
  :: (KnownDivRat unit Second, KnownUnitName unit)
  => HasCallStack => Time unit -> Benchmarkable -> Property
checkReport upperBound benchmarkable =
  withTests 1 $ property $
    evalIO runBench >>= \case
      Nothing -> failedTest "Expected benchmark to complete within 120 seconds."
      Just report ->
        let mean = sec . realToFrac @Double @RatioNat . estPoint . anMean $ reportAnalysis report
        in  if mean < toUnit @Second upperBound
              then succeededTest
              else failedTest $
                "Expected mean estimate to be under "
                <> show upperBound
                <> ", but was "
                <> display mean
  where
    runBench = timeout (minute 2) $
      benchmarkWith' (defaultConfig { verbosity = Quiet }) benchmarkable

    display :: Time Second -> Text
    display n = case n of
      (toUnit @Minute -> x) | x > minute 1 -> format x
      (toUnit @Second -> x) | x > sec 1 -> format x
      (toUnit @Millisecond -> x) | x > ms 1 -> format x
      (toUnit @Microsecond -> x) | x > mcs 1 -> format x
      (toUnit @Nanosecond -> x) | x > ns 1 -> format x
      _ -> format (toUnit @Picosecond n)

    format :: forall unit. KnownUnitName unit => Time unit -> Text
    format n =
      toText @String $ printf "%.4f%s"
        (realToFrac @RatioNat @Double $ unTime n)
        (unitNameVal @unit)

----------------------------------------------------------------------------
-- Generator
----------------------------------------------------------------------------

-- | Generates an @a@ and a @b@ and wraps them in a tuple.
genTuple2 :: MonadGen m => m a -> m b -> m (a, b)
genTuple2 = liftA2 (,)

-- | Run the given generator deterministically, by fixing its size and seed.
runGen :: HasCallStack => Range.Size -> Word64 -> Gen a -> a
runGen size seed genT =
  let tree = runGenT size (Seed.from seed) genT
      node = fromMaybe discardedErr $ runIdentity $ runMaybeT $ runTreeT tree
      discardedErr = error $
        "Generator could not produce a value for size "
        <> show size <> " and seed " <> show seed
  in  nodeValue node


----------------------------------------------------------------------------
-- Roundtrip
----------------------------------------------------------------------------

-- | This 'TestTree' contains a property based test for conversion from
-- some @x@ to some @y@ and back to @x@ (it should successfully return
-- the initial @x@).
roundtripTree
  :: forall x y err.
     ( Show x
     , Show y
     , Show err
     , Typeable x
     , Eq x
     , Eq err
     )
  => Gen x
  -> (x -> y)
  -> (y -> Either err x)
  -> TestTree
roundtripTree genX xToY yToX = testProperty typeNameX prop
  where
    typeNameX = show $ typeRep (Proxy @x)
    prop :: Property
    prop = property $ do
      x <- forAll genX
      tripping x xToY yToX

-- | Test that one doc item goes before another doc item in generated
-- documentation.
assertGoesBefore
  :: forall d1 d2.
      (DocItem d1, DocItem d2)
  => Proxy d1 -> Proxy d2 -> Assertion
assertGoesBefore dp1 dp2 =
  unless (p1 < p2) $
    assertFailure $
      "Doc item " <> show (typeRep dp1) <> " with position " <> pretty p1 <> " \
      \goes before doc item " <> show (typeRep dp2) <> " with position " <> pretty p2
  where
    p1 = docItemPosition @d1
    p2 = docItemPosition @d2

-- | Test that one doc item goes before another doc item in generated
-- documentation.
goesBefore
  :: forall d1 d2.
      (DocItem d1, DocItem d2)
  => Proxy d1 -> Proxy d2 -> TestTree
goesBefore dp1 dp2 = testCase testName (assertGoesBefore dp1 dp2)
  where
  testName = "`" +|| typeRep dp1 ||+ "` should come before `" +|| typeRep dp2 ||+ "`"

-- | Helper function for running parser in test environment
partialParse :: (HasCallStack, Buildable b) => (a -> Either b c) -> a -> c
partialParse f = either (error . pretty) id . f

----------------------------------------------------------------------------
-- Pretty-printing
----------------------------------------------------------------------------

formatValue :: forall t. SingI t => T.Value t -> Builder
formatValue v =
  case T.checkOpPresence (sing @t) of
    OpPresent -> show v
    OpAbsent -> "" +| build v |+ " of type " +| demote @t |+ ""

formatSomeValue :: SomeValue -> Builder
formatSomeValue (SomeValue v) = formatValue v

-- | Derive a 'Show' instance for a type using a custom "show" function.
data ShowWith a = ShowWith (a -> String) a

instance Show (ShowWith a) where
  show (ShowWith f a) = f a

-- | Derive a 'Buildable' instance for a type using 'show'.
newtype Showing a = Showing a
  deriving stock Eq
  deriving newtype Show

instance Show a => Buildable (Showing a) where
  build (Showing a) = build (show @Text a)

----------------------------------------------------------------------------
-- Time
----------------------------------------------------------------------------

-- | Round the given time to the nearest whole number of the given unit,
-- not smaller than the given time.
--
-- @
-- ceilingUnit (sec 2.0) == sec 2
-- ceilingUnit (sec 2.1) == sec 3
-- ceilingUnit (sec 2.9) == sec 3
-- @
ceilingUnit :: forall (unit :: Rat) . Time unit -> Time unit
ceilingUnit = time . fromIntegral @Natural . ceiling . unTime

-- | Converts the given time to a number with fixed-precision (in the given time unit).
--
-- @
-- timeToFixed (sec 1.234) == (1.2            :: Deci)
-- timeToFixed (sec 1.234) == (1.234          :: Milli)
-- timeToFixed (sec 1.234) == (1.234000000000 :: Pico)
-- @
timeToFixed
  :: forall precision unit
   . HasResolution precision
  => Time unit -> Fixed precision
timeToFixed = fromRational @(Fixed precision) . toRational . unTime

-- | Converts the given time to a 'NominalDiffTime'.
timeToNominalDiffTime :: KnownDivRat unit Second => Time unit -> NominalDiffTime
timeToNominalDiffTime =
  secondsToNominalDiffTime . timeToFixed . toUnit @Second
