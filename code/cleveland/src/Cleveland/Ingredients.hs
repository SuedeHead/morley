-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Ingredients that we use in our test suites.

module Cleveland.Ingredients
  ( ourIngredients
  ) where

import Test.Tasty.Ingredients (Ingredient, composeReporters)
import Test.Tasty.Ingredients.Basic (consoleTestReporter, listingTests)
import Test.Tasty.Runners.AntXML (antXMLRunner)

-- | This is the default set of ingredients extended with the
-- 'antXMLRunner' which is used to generate xml reports for CI.
ourIngredients :: [Ingredient]
ourIngredients = [listingTests, antXMLRunner `composeReporters` consoleTestReporter]
