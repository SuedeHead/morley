-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Dummy data to be used in tests where it's not essential.

module Michelson.Test.Dummy
  ( dummyNow
  , dummyMaxSteps
  , dummyContractEnv
  , dummyOrigination
  , dummyChainId
  ) where

import Michelson.Runtime.Dummy
import Tezos.Core
