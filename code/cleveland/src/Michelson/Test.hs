-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module containing some utilities for testing Michelson contracts using
-- Haskell testing frameworks.
-- It's Morley testing EDSL.
-- We focus on @tasty@ and @hedgehog@ because that's what we mostly use in
-- our tests, but we also provide helpers for @hspec@, and defining helpers
-- for other libraries (e. g. @QuickCheck@) shouldn't be hard.
-- We don't provide helpers for other libraries to have less dependencies.

module Michelson.Test
  ( -- * Importing a contract
    specWithContract
  , specWithTypedContract
  , specWithUntypedContract
  , testTreesWithContract
  , testTreesWithUntypedContract
  , testTreesWithTypedContract
  , concatTestTrees
  , importContract
  , importSomeContract
  , importUntypedContract

  -- * Unit testing
  , ContractReturn
  , ContractPropValidator
  , contractProp
  , contractPropVal
  , validateSuccess
  , validateStorageIs
  , validateMichelsonFailsWith

  -- * Integrational testing
  -- ** Testing engine
  , IntegrationalScenario
  , IntegrationalScenarioM
  , integrationalTestExpectation
  , integrationalTestProp
  , originate
  , tOriginate
  , transfer
  , tTransfer
  , integrationalFail
  , unexpectedInterpreterError
  , setMaxSteps
  , setNow
  , rewindTime
  , withSender
  , setChainId
  , branchout
  , (?-)

  -- ** Validators
  , expectNoStorageUpdates
  , expectStorageUpdate
  , expectStorageUpdateConst
  , expectBalance
  , expectStorage
  , expectStorageConst
  , tExpectStorageConst
  , expectAddressLogs
  , expectScenarioLogs
  , getScenarioLogs

  -- ** Errors
  , attempt
  , expectError
  , catchExpectedError
  , expectGasExhaustion
  , expectMichelsonFailed

  -- ** Various
  , TxData (..)
  , TxParam (..)
  , genesisAddress

  -- * General utilities
  , failedTest
  , succeededTest
  , eitherIsLeft
  , eitherIsRight
  , meanTimeUpperBoundProp
  , meanTimeUpperBoundPropNF

  -- * Re-exports
  --
  -- | These functions from "Time" are re-exported here to make it convenient to call
  -- 'meanTimeUpperBoundProp' and 'meanTimeUpperBoundPropNF'.
  , mcs, ms, sec, minute

  -- * Autodoc testing
  , runDocTests
  , testDocBasic
  , excludeDocTests

  -- * Dummy values
  , dummyContractEnv
  ) where

import Cleveland.Util
import Michelson.Doc.Test
import Michelson.Test.Dummy
import Michelson.Test.Import
import Michelson.Test.Integrational
import Michelson.Test.Unit
