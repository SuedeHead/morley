-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module Hedgehog.Gen.Michelson.Typed
  ( genBigMap
  , genEpAddress
  , genValueKeyHash
  , genValueMutez
  , genValueInt
  , genValueList
  , genValueUnit
  , genValuePair
  , genValueTimestamp

  , genValue
  , genValue'

  , genSimpleInstr
  ) where

import Data.Singletons (Sing)
import Hedgehog (GenBase, MonadGen)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Michelson.Text (mkMTextUnsafe)
import Michelson.Typed
  (Comparable, Instr(DIP, DROP, DUP, FAILWITH, Seq), KnownT, SingT (..), T (..), Value' (..),
  WellTyped)
import Michelson.Typed.Entrypoints (EpAddress (..), sepcCallRootUnsafe)
import Michelson.Typed.Haskell.Value (BigMap (..), ToT, WellTypedToT)
import Michelson.Typed.Scope
import Michelson.Typed.Value (RemFail (..))

import Hedgehog.Gen.Michelson.Untyped (genEpName)
import Hedgehog.Gen.Tezos.Address (genAddress)
import Hedgehog.Gen.Tezos.Core (genChainId, genMutez, genTimestamp)
import Hedgehog.Gen.Tezos.Crypto (genKeyHash, genPublicKey, genSignature)

genBigMap
  :: forall k v m.
     (MonadGen m, Ord k, WellTypedToT k, WellTypedToT v, Comparable (ToT k))
  => m k -> m v -> m (BigMap k v)
genBigMap genK genV = BigMap <$> Gen.map (Range.linear 0 100) (liftA2 (,) genK genV)

genEpAddress :: (MonadGen m, GenBase m ~ Identity) => m EpAddress
genEpAddress = EpAddress <$> genAddress <*> genEpName

genValueKeyHash :: MonadGen m => m (Value' instr 'TKeyHash)
genValueKeyHash = VKeyHash <$> genKeyHash

genValueMutez :: MonadGen m => m (Value' instr 'TMutez)
genValueMutez = VMutez <$> genMutez

genValueInt :: MonadGen m => m (Value' instr 'TInt)
genValueInt = VInt <$> Gen.integral (Range.linearFrom 0 -1000 1000)

genValueList :: (MonadGen m, KnownT a) => m (Value' instr a) -> m (Value' instr ('TList a))
genValueList genA = VList <$> Gen.list (Range.linear 0 100) genA

genValueUnit :: Applicative m => m (Value' instr 'TUnit)
genValueUnit = pure VUnit

genValuePair :: MonadGen m => m (Value' instr a) -> m (Value' instr b) -> m (Value' instr ('TPair a b))
genValuePair genA genB = VPair ... (,) <$> genA <*> genB

genValueTimestamp :: MonadGen m => m (Value' instr 'TTimestamp)
genValueTimestamp = VTimestamp <$> genTimestamp

genValue
  :: forall t m.
      (MonadGen m, GenBase m ~ Identity, HasNoOp t, WellTyped t)
  => m (Value' Instr t)
genValue = genValue' (sing @t)

-- | Generate a simple instruction.
-- Ideally instruction generator should produce instructions containing
-- all possible primitive instructions.
-- In our case we consider only a few primitive instructions and
-- pick one from a hardcoded list. Hence we call it "simple".
-- Another limitation is that input stack and output stack types must be
-- identical and non-empty.
genSimpleInstr :: (MonadGen m, inp ~ (x ': xs), KnownT x) => m (Instr inp inp)
genSimpleInstr = Gen.element [ FAILWITH, Seq DUP DROP, Seq DUP $ DIP DROP ]

genValue'
  :: (MonadGen m, GenBase m ~ Identity, HasNoOp t, WellTyped t)
  => Sing t -> m (Value' Instr t)
genValue' = \case
  STKey -> VKey <$> genPublicKey
  STUnit -> genValueUnit
  STSignature -> VSignature <$> genSignature
  STChainId -> VChainId <$> genChainId
  STOption st -> Gen.choice
    [ pure $ VOption Nothing
    , VOption . Just <$> genValue' st
    ]
  STList st -> VList <$> Gen.list (Range.linear 0 100) (genValue' st)
  STSet st -> VSet <$> Gen.set (Range.linear 0 100) (genValue' st)
  STContract (s :: SingT p) -> case (checkOpPresence s, checkNestedBigMapsPresence s) of
    (OpAbsent, NestedBigMapsAbsent) -> VContract <$> genAddress <*> pure sepcCallRootUnsafe
    _ -> Gen.discard
  STPair l r -> VPair <$> ((,) <$> genNoOpValue l <*> genNoOpValue r)
  STOr l r -> VOr <$> Gen.choice
    [ Left <$> genNoOpValue l
    , Right <$> genNoOpValue r
    ]
  -- It's quite hard to generate proper lambda of given type, so it always returns FAILWITH.
  -- Such implementation is sufficient for now.
  STLambda{} -> pure $ VLam $ RfAlwaysFails FAILWITH
  STMap k v ->
    VMap <$> Gen.map (Range.linear 0 100) (liftA2 (,) (genNoOpValue k) (genNoOpValue v))
  STBigMap k v ->
    VBigMap <$> Gen.map (Range.linear 0 100) (liftA2 (,) (genNoOpValue k) (genNoOpValue v))
  STInt -> genValueInt
  STNat -> VNat <$> Gen.integral (Range.linearFrom 0 0 1000)
  STString -> VString . mkMTextUnsafe . fromString <$> Gen.string (Range.linear 0 100) Gen.alphaNum
  STBytes -> VBytes <$> Gen.bytes (Range.linear 0 100)
  STMutez -> genValueMutez
  STKeyHash -> genValueKeyHash
  STTimestamp -> genValueTimestamp
  STAddress -> VAddress <$> genEpAddress
  STBool -> VBool <$> Gen.bool
  where
    genNoOpValue
      :: (MonadGen m, GenBase m ~ Identity, SingI t', WellTyped t')
      => Sing t' -> m (Value' Instr t')
    genNoOpValue st = case checkOpPresence st of
      OpAbsent -> genValue' st
      _ -> Gen.discard
