-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Hedgehog.Gen.Tezos.Crypto.Secp256k1
  ( genPublicKey
  , genSecretKey
  , genSignature
  ) where

import Crypto.Random (drgNewSeed, seedFromInteger, withDRG)
import Hedgehog (MonadGen)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Tezos.Crypto.Secp256k1
  (PublicKey, SecretKey, Signature, detSecretKey, detSecretKeyDo, sign, toPublic)

genPublicKey :: MonadGen m => m PublicKey
genPublicKey = toPublic <$> genSecretKey

genSecretKey :: MonadGen m => m SecretKey
genSecretKey = detSecretKey <$> Gen.bytes (Range.singleton 32)

genSignature :: MonadGen m => m Signature
genSignature = do
  seed <- drgNewSeed . seedFromInteger <$> Gen.integral (Range.linearFrom 0 -1000 1000)
  byteToSign <- Gen.word8 Range.linearBounded
  return $ fst $ withDRG seed $ do
    sk <- detSecretKeyDo
    sign sk (one byteToSign)
