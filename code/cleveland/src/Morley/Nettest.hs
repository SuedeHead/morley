-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Michelson contracts testing on a real Tezos network.
--
-- It defines an interface for writing network testing scenarios and provides
-- two implementations: one of them talks to reference Tezos software and
-- submits operations to real network, the other one converts scenario to
-- the existing integrational testing eDSL.
--
-- Expected usage is the following:
-- 1. Write a scenario using abstract nettest interface.
-- 2. Make an executable (i. e. something that can be executed:
-- executable or test-suite in .cabal file, normally the latter) which
-- runs this scenario via pure integrational testing engine first (as
-- a quick check for correctness) and then via @Client@ implementation.
-- This executable is supposed to run periodically, but not in each
-- MR on each change.
-- 3. Also run this scenario in existing test-suite using pure implementation.
-- So pure implementation will be used twice: in normal test-suite that
-- runs on each change (which proves that it is /likely/ correct) and in
-- real network test to prevent that test from starting if the scenario
-- is /most likely/ __not__ correct.
-- 4. Note that in order to run it on a real network you should have
-- an address with @nettest@ alias and it should have sufficient
-- balance to run the scenario.
--
-- TODO:
-- ★ [#50] Collect gas and other statistics in nettest.
-- ★ [#53] Maybe somehow merge with testing eDSL.
-- ★ [#55] Add command line options parsing.

module Morley.Nettest
  ( AliasHint
  , aliasesSeries
  , Alias
  , AddressOrAlias (..)
  , addressResolved
  , OriginateData (..)
  , UntypedOriginateData (..)
  , TransferData (..)
  , CallData (..)
  , NettestImpl (..)
  , EmulatedImpl (..)
  , NettestScenario
  , EmulatedScenario

  -- * Constant address
  , nettestAddress

  -- * Actions
  , niResolveNettestAddress
  , niNewAddress
  , niOriginateUntypedSimple
  , niOriginate
  , niOriginateSimple
  , niCallFrom
  , niImportUntypedContract
  -- * Helpers
  , ep
  , (?-)
  , mkCallData

  -- * Validation
  , NettestFailure (..)

  -- * Real network implementation based on @tezos-client@ and RPC.
  , runNettestClient
  , nettestImplClient

  -- * @caps@-based commands
  , MonadNettest
  , MonadEmulated
  , NettestT
  , EmulatedT
  , runIO
  , resolveAddress
  , resolveNettestAddress
  , getAlias
  , newAddress
  , newFreshAddress
  , signBytes
  , signBinary
  , originate
  , originateSimple
  , originateUntyped
  , originateUntypedSimple
  , transfer
  , transferBatch
  , call
  , callBatch
  , callFrom
  , callBatchFrom
  , importUntypedContract
  , comment
  , expectFailure
  , expectCustomError
  , expectCustomError_
  , getBalance
  , getMorleyLogs
  , checkBalance
  , getStorage
  , getStorageExpr
  , checkStorage
  , checkStorage'
  , getPublicKey
  , getChainId
  , advanceTime
  , getNow
  , branchout
  , offshoot
  , uncapsNettest
  , uncapsNettestEmulated

  -- * Assertions
  , failure
  , assert
  , (@==)
  , (@/=)
  , checkCompares
  , checkComparesWith
  , Showing(..)

  -- * Integration with @hedgehog@
  , nettestTestProp

  -- * Config (reexports)
  , TezosClientEnv(..)
  , MorleyClientEnv
  , MorleyClientEnv'(..)
  , NettestEnv (..)
  , MorleyLogs
  ) where

import Cleveland.Util (Showing(..))
import Michelson.Interpret (MorleyLogs)
import Morley.Client (MorleyClientEnv, MorleyClientEnv'(..), addressResolved, aliasesSeries)
import Morley.Client.TezosClient.Types (Alias, TezosClientEnv(..))
import Morley.Nettest.Abstract
import Morley.Nettest.Caps
import Morley.Nettest.Client
import Morley.Nettest.Parser
import Morley.Nettest.Pure
