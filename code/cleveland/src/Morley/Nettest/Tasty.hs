-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
{-# LANGUAGE InstanceSigs #-}

-- | This module allows the use of "Morley.Nettest" tests in @tasty@.
--
-- These tests can be run on:
--
-- * the "Michelson.Runtime" emulator using
--   'nettestScenarioOnEmulator'/'nettestScenarioOnEmulatorCaps'
-- * both on the emulator and a real Tezos network using
--   'nettestScenario'/'nettestScenarioCaps'.
--
-- 'whenNetworkEnabled' can be used to write tests that need
-- to run a Tezos network, but are not necessarily written using "Morley.Nettest".
--
-- If a 'TestTree' contains many tests scheduled to run on a real Tezos network,
-- those tests will be run sequentially.
--
-- Example:
--
-- > import Morley.Nettest
-- > import Morley.Nettest.Tasty
-- >
-- > main :: IO ()
-- > main = nettestMain test
-- >
-- > test :: TestTree
-- > test = nettestScenarioCaps "storage is 1" $ do
-- >   addr <- addressResolved <$> originate OriginateData {..}
-- >   checkStorage addr (toVal @Natural 1)
--
-- By default, network tests will only be run if the @CI@ environment variable
-- is set to @true@ (case-insensitive) or @1@. Most CI systems will automatically
-- enable this environment variable.
--
-- This behaviour can be overriden:
--
-- * To run network tests outside of a CI environment,
--   use @--nettest-run-network@/@TASTY_NETTEST_RUN_NETWORK@.
-- * To disable network tests in a CI environment,
--   use @--nettest-no-run-network@/@TASTY_NETTEST_NO_RUN_NETWORK@.
--
-- The following decision table details how the 3 variables interact with each other:
--
-- +-----------+-------------------------+----------------------------+--------------------+
-- |   @CI@    | @--nettest-run-network@ | @--nettest-no-run-network@ | run network tests? |
-- +===========+=========================+============================+====================+
-- | unset     | unset                   | unset                      | no                 |
-- +-----------+-------------------------+----------------------------+--------------------+
-- | set       | unset                   | unset                      | yes                |
-- +-----------+-------------------------+----------------------------+--------------------+
-- | unset     | set                     | unset                      | yes                |
-- +-----------+-------------------------+----------------------------+--------------------+
-- | set       | set                     | unset                      | yes                |
-- +-----------+-------------------------+----------------------------+--------------------+
-- | set/unset | set/unset               | set                        | no                 |
-- +-----------+-------------------------+----------------------------+--------------------+
module Morley.Nettest.Tasty
  (
  -- * Main
    nettestMain
  , nettestMainWithIngredients
  , nettestIngredients
  , loadTastyEnv

  -- * Test cases
  , nettestScenario
  , nettestScenarioOnEmulator

  , nettestScenarioCaps
  , nettestScenarioOnEmulatorCaps

  , whenNetworkEnabled

  -- * Reading/setting options
  , modifyNettestEnv
  , setAliasPrefix

  -- * Internals
  , RunOnNetwork(..)
  , RunOnEmulator(..)
  , tastyEnvFromOpts
  , shouldRunNetwork
  , onNetworkTag
  , TastyEnvOpt(..)
  , Skipped
  , testSkipped
  , TastyEnv(..)
  , mkTastyEnv
  , mapTastyEnv
  , withTastyEnv
  , memoize
  ) where

import Control.Concurrent (modifyMVar, withMVar)
import qualified Control.Exception as Ex (assert)
import qualified Data.Char as C
import Data.Tagged (Tagged(Tagged))
import Fmt (pretty)
import GHC.Stack (SrcLoc(srcLocModule))
import Servant.Client.Core (BaseUrl(..))
import System.Environment (lookupEnv)
import qualified System.IO.Unsafe as Unsafe
import Test.Tasty
  (TestName, defaultIngredients, defaultMainWithIngredients, includingOptions, localOption,
  testGroup)
import Test.Tasty.Ingredients (Ingredient)
import Test.Tasty.Options (IsOption(..), OptionSet, lookupOption)
import Test.Tasty.Providers (IsTest(..), Progress, singleTest, testFailed, testPassed)
import Test.Tasty.Runners (Result(..), TestTree(AskOptions))

import Morley.Client (MorleyClientConfig(..), mceTezosClientL, mkMorleyClientEnv)
import Morley.Client.TezosClient.Types (tceAliasPrefixL)

import Morley.Nettest
import Morley.Nettest.Exceptions (WithCallStack(WithCallStack))
import Morley.Nettest.Parser (neMorleyClientEnvL)
import Morley.Nettest.Pure (PureM, emulatedImplIntegrational, scenarioToIO)
import Morley.Nettest.Tasty.Options
  ( AddressOpt(..), AliasPrefixOpt(..), ContextLinesOpt(..), DataDirOpt(..), EndpointOpt(..)
  , NoRunNetworkOpt(..), PathOpt(..), PortOpt(..), RunNetworkOpt(..), SecretKeyOpt(..)
  , Switch(Set, Unset), UseHttpsOpt(..), VerboseOpt(..), nettestOptions)
import Morley.Nettest.Tasty.Report (formatError)

-- | A name that we use to tag all tests that run on the network.
--
-- We use this in a tasty @--pattern@ in .gitlab-ci.yml to run only network tests.
onNetworkTag :: TestName
onNetworkTag = "On network"

-- | Create a tasty test case from a 'NettestScenario'.
--
-- This will create a test tree with 2 tests:
-- one that runs the 'NettestScenario' on the "Michelson.Runtime" emulator,
-- and another that runs it on a real Tezos network.
--
-- The network config is read from the command line/environment variables.
-- Use @--help@ to see the available options.
--
-- If a 'TestTree' contains many tests scheduled to run on a real Tezos network,
-- those tests will be run sequentially.
nettestScenario :: TestName -> (forall m. NettestScenario m) -> TestTree
nettestScenario testName scenario =
  testGroup testName
    [ singleTest "On emulator" (RunOnEmulator (\_ -> scenario))
    , singleTest "On network" (RunOnNetwork scenario)
    ]

-- | Create a tasty test case from a 'NettestScenario'.
--
-- This will create a test tree with 1 test,
-- which will run the 'NettestScenario' on the "Michelson.Runtime" emulator.
nettestScenarioOnEmulator :: TestName -> EmulatedScenario PureM -> TestTree
nettestScenarioOnEmulator testName scenario =
  singleTest testName (RunOnEmulator scenario)

-- | Similar to 'nettestScenario', but adapted for tests written using
-- the capabilities interface ("Morley.Nettest.Caps").
nettestScenarioCaps :: TestName -> (forall m. Monad m => NettestT m ()) -> TestTree
nettestScenarioCaps testName scenario =
  nettestScenario testName (uncapsNettest scenario)

-- | Similar to 'nettestScenarioOnEmulator', but adapted for tests written using
-- the capabilities interface ("Morley.Nettest.Caps").
nettestScenarioOnEmulatorCaps :: TestName -> EmulatedT PureM () -> TestTree
nettestScenarioOnEmulatorCaps testName scenario =
  nettestScenarioOnEmulator testName (uncapsNettestEmulated scenario)

newtype RunOnEmulator = RunOnEmulator (EmulatedScenario PureM)

instance IsTest RunOnEmulator where
  run :: OptionSet -> RunOnEmulator -> (Progress -> IO ()) -> IO Result
  run opts (RunOnEmulator scenario) _ =
    (scenarioToIO (scenario emulatedImplIntegrational) $> testPassed "") `catch` printFormattedException opts

  testOptions = Tagged []

newtype RunOnNetwork = RunOnNetwork (NettestScenario IO)

instance IsTest RunOnNetwork where
  run :: OptionSet -> RunOnNetwork -> (Progress -> IO ()) -> IO Result
  run opts (RunOnNetwork scenario) _ = do
    case tastyEnvFromOpts opts of
      Nothing -> pure testSkipped
      Just tastyEnv ->
        useNettestEnv tastyEnv $ \nettestEnv ->
          (runNettestClient nettestEnv scenario $> testPassed "") `catch` printFormattedException opts

  -- If a 'RunOnNetwork' test is created somewhere in a suite,
  -- these options will be automatically added to tasty's @--help@.
  testOptions = Tagged nettestOptions

printFormattedException :: OptionSet -> SomeException -> IO Result
printFormattedException opts se =
  case fromException @WithCallStack se of
    Nothing ->
      shouldContainCallStack $
        pure (testFailed $ displayException se)

    Just (WithCallStack cs ex) ->
      shouldNotContainImplementationDetails cs $ do
        msg <- formatError contextLines cs (displayException ex)
        pure (testFailed (pretty msg))
  where
    ContextLinesOpt contextLines = lookupOption opts

    -- Sanity check: All the functions in "Morley.Nettest.Caps" should use 'withFrozenCallStack'
    -- to guarantee that implementation details (i.e. any functions called internally)
    -- do not show up in the callstack.
    shouldNotContainImplementationDetails :: CallStack -> a -> a
    shouldNotContainImplementationDetails cs =
      -- These assertions are erased when compiled with any '-O' flag or with '-fignore-asserts'.
      Ex.assert $ and $ getCallStack cs <&> \(_, loc) ->
        srcLocModule loc /= "Morley.Nettest.Client" &&
        srcLocModule loc /= "Morley.Nettest.Pure"

    -- Sanity check: All the functions in "Morley.Nettest.Client" and "Morley.Nettest.Pure"
    -- should wrap all exceptions in 'WithCallStack', which means this branch
    -- should never be hit.
    shouldContainCallStack :: a -> a
    shouldContainCallStack a =
      -- These assertions are erased when compiled with any '-O' flag or with '-fignore-asserts'.
      Ex.assert False a

-- | A global mutex to ensure only one nettest is executed at a time.
--
-- TODO: Remove this when #399 is done.
-- https://gitlab.com/morley-framework/morley/-/issues/399
--
-- See also: https://wiki.haskell.org/Top_level_mutable_state
lock :: MVar ()
lock = Unsafe.unsafePerformIO (newMVar ())
{-# NOINLINE lock #-}

-- | Result of a test that was not run.
testSkipped :: Result
testSkipped = (testPassed "") { resultShortDescription = "SKIPPED" }

----------------------------------------------------------------------------
-- Main
----------------------------------------------------------------------------

-- | Similar to 'defaultMain', but also preloads 'TastyEnv' and
-- registers the necessary command line options/environment variables to configure
-- "Morley.Nettest".
nettestMain :: TestTree -> IO ()
nettestMain = nettestMainWithIngredients defaultIngredients . loadTastyEnv

-- | Similar to 'defaultMainWithIngredients', but also preloads 'TastyEnv' and
-- registers the necessary command line options/environment variables to configure
-- "Morley.Nettest".
nettestMainWithIngredients :: [Ingredient] -> TestTree -> IO ()
nettestMainWithIngredients ingredients tree =
  defaultMainWithIngredients
    (nettestIngredients <> ingredients)
    (loadTastyEnv tree)

-- | A list with all the ingredients necessary to configure "Morley.Nettest".
--
-- Note: If a test suite uses 'nettestScenario'/'nettestScenarioCaps',
-- the relevant command line options will be automatically added to tasty's @--help@.
--
-- However, if a test suite intends to not use those functions, and use 'whenNetworkEnabled'
-- only, then the CLI options need to be registered manually by using this ingredient
-- (or 'nettestMain'/'nettestMainWithIngredients').
nettestIngredients :: [Ingredient]
nettestIngredients =
  [ includingOptions nettestOptions
  ]

----------------------------------------------------------------------------
-- Reading/setting options
----------------------------------------------------------------------------

-- | Creates a 'TastyEnv' from the passed command line/environment options.
--
-- Returns 'Nothing' if network tests are disabled.
tastyEnvFromOpts :: OptionSet -> Maybe TastyEnv
tastyEnvFromOpts optionSet =
  let
    AliasPrefixOpt aliasPrefix = lookupOption optionSet
    EndpointOpt endpoint = lookupOption optionSet
    AddressOpt addr = lookupOption optionSet
    PortOpt port = lookupOption optionSet
    PathOpt path = lookupOption optionSet
    DataDirOpt dataDir = lookupOption optionSet
    UseHttpsOpt scheme = lookupOption optionSet
    VerboseOpt verbosity = lookupOption optionSet
    SecretKeyOpt sk = lookupOption optionSet
  in
    case shouldRunNetwork withinCI optionSet of
      False -> Nothing
      True ->
        case lookupOption @TastyEnvOpt optionSet of
          -- If 'TastyEnv' has been already loaded and cached, use it.
          TastyEnvOpt (Just tastyEnv) -> Just tastyEnv
          -- Otherwise, load it.
          TastyEnvOpt Nothing -> Just $ mkTastyEnv $ do
            morleyClientEnv <- mkMorleyClientEnv MorleyClientConfig
              { mccAliasPrefix = aliasPrefix
              , mccEndpointUrl = endpoint <|>
                  BaseUrl <$> Just scheme <*> addr <*> port <*> Just ""
              , mccTezosClientPath = path
              , mccMbTezosClientDataDir = dataDir
              , mccVerbosity = verbosity
              , mccSecretKey = Nothing
              }
            pure NettestEnv
              { neMorleyClientEnv = morleyClientEnv
              , neSecretKey = sk
              }

-- | Heuristics to check whether we are running within CI.
-- Check the respective env variable which is usually set in all CIs.
withinCI :: Switch
withinCI = Unsafe.unsafePerformIO $ lookupEnv "CI" <&> \case
  Just "1"                       -> Set
  Just (map C.toLower -> "true") -> Set
  _                              -> Unset
{-# NOINLINE withinCI #-}

-- | Decide whether to run network tests
--
-- Note: see the decision table in the haddocks for this module.
shouldRunNetwork :: Switch -> OptionSet -> Bool
shouldRunNetwork ci optionSet = do
  case (ci, lookupOption @RunNetworkOpt optionSet, lookupOption @NoRunNetworkOpt optionSet) of
    (_, _, NoRunNetworkOpt Set) -> False
    (_, RunNetworkOpt Set, _) -> True
    (Set, _, _) -> True
    _ -> False

-- | Creates a 'TastyEnv' from the passed command line/environment options
-- and uses it to create a 'TestTree'.
--
-- 'Nothing' will be passed to the given function if
-- `--nettest-skip-network`/`TASTY_NETTEST_SKIP_NETWORK` are enabled.
withTastyEnv :: (Maybe TastyEnv -> TestTree) -> TestTree
withTastyEnv mkTestTree =
  AskOptions $ \optionSet ->
    mkTestTree $ tastyEnvFromOpts optionSet

-- | Runs some tests only when network tests are enabled
-- (i.e., when running in the CI or when @--nettest-run-network@ is enabled)
--
-- If network tests are disabled, then an empty testGroup will be created instead.
--
-- Example usage:
--
-- > test :: TestTree
-- > test =
-- >   whenNetworkEnabled $ \withEnv ->
-- >     testCase "a test name" $
-- >       withEnv $ \env ->
-- >         runMorleyClientM (neMorleyClientEnv env) $ do
-- >           ...
whenNetworkEnabled :: ((forall a. (NettestEnv -> IO a) -> IO a) -> TestTree) -> TestTree
whenNetworkEnabled mkTestTree =
  withTastyEnv $ \case
    Just (TastyEnv useNettestEnv) -> testGroup onNetworkTag [ mkTestTree useNettestEnv ]
    Nothing -> singleTest onNetworkTag Skipped

-- | A test that always reports as having been "SKIPPED".
data Skipped = Skipped
instance IsTest Skipped where
  run _ _ _ = pure testSkipped
  testOptions = Tagged []

-- | Modifies the 'NettestEnv' for all the tests in the given test tree.
modifyNettestEnv :: (NettestEnv -> NettestEnv) -> TestTree -> TestTree
modifyNettestEnv f tree =
  withTastyEnv $ \tastyEnvMb ->
    localOption (TastyEnvOpt (mapTastyEnv f <$> tastyEnvMb)) $
      tree

-- | Overrides the alias prefix (parsed from @--nettest-alias-prefix@ or @TASTY_NETTEST_ALIAS_PREFIX@)
-- for all the tests in the given test tree.
setAliasPrefix :: Text -> TestTree -> TestTree
setAliasPrefix aliasPrefix tree =
  modifyNettestEnv (neMorleyClientEnvL.mceTezosClientL.tceAliasPrefixL .~ Just aliasPrefix) $
    tree

----------------------------------------------------------------------------
-- Preload TastyEnv
----------------------------------------------------------------------------

-- | A pre-loaded 'TastyEnv'.
--
-- It's not an actual command line option, we use it
-- so we can load a 'TastyEnv' once, and then cache it
-- alongside the other options in tasty's 'OptionSet'.
--
-- Kiiiind of a hack, but it works :D
--
-- It is purposefully never registered as a CLI option
-- (e.g. using 'Test.Tasty.Providers.testOptions' or 'Test.Tasty.includingOptions')
-- to make sure it doesn't appear in tasty's @--help@.
newtype TastyEnvOpt = TastyEnvOpt (Maybe TastyEnv)

instance IsOption TastyEnvOpt where
  defaultValue = TastyEnvOpt Nothing
  optionName = ""
  optionHelp = ""
  parseValue = \_ -> Nothing

-- | Pre-load 'TastyEnv' from the passed command line/environment options,
-- and store it in tasty's 'OptionSet' to make it available to
-- all tests within the test tree.
--
-- Creating a 'NettestEnv' is a relatively expensive operation, when executed hundreds of times.
-- This function guarantees that only one 'TastyEnv' is created for this test tree, and
-- 'TastyEnv' will, in turn, guarantee that only one 'NettestEnv' is created while the tests are running.
loadTastyEnv :: TestTree -> TestTree
loadTastyEnv tree =
  withTastyEnv $ \tastyEnvMb ->
    localOption (TastyEnvOpt tastyEnvMb)
      tree

----------------------------------------------------------------------------
-- TastyEnv
----------------------------------------------------------------------------

-- | This action will:
--
-- 1. Enter a critical section
-- 2. Either:
--
--     * Create a 'NettestEnv' and cache it, if it's the first time being evaluated.
--     * Or reuse an existing cached 'NettestEnv' otherwise.
--
-- 3. Pass it to the given @NettestEnv -> IO a@ function.
-- 4. Exit the critical section
--
-- This ensures:
--
-- * 'NettestEnv' is only created once (it's a relatively expensive operation).
-- * tests that use 'NettestEnv' are run sequentially
--    (see #399, https://gitlab.com/morley-framework/morley/-/issues/399)
newtype TastyEnv = TastyEnv
  { useNettestEnv :: forall a. (NettestEnv -> IO a) -> IO a
  }

mkTastyEnv :: IO NettestEnv -> TastyEnv
mkTastyEnv mkEnv =
  TastyEnv
    { useNettestEnv = \cont ->
        withMVar lock $ \() -> do
          env <- memoMkEnv
          cont env
    }
  where
    -- Note: Using `unsafePerformIO` here is the recommended workaround for
    -- not being able to use IO inside `askOption`/`AskOptions`.
    -- https://github.com/feuerbach/tasty/issues/228
    --
    -- We're only using it here to initialize an 'MVar', so it /should/ be safe 🤞
    memoMkEnv = Unsafe.unsafePerformIO $ memoize mkEnv

mapTastyEnv :: (NettestEnv -> NettestEnv) -> (TastyEnv -> TastyEnv)
mapTastyEnv g (TastyEnv f) =
  TastyEnv $ \cont ->
    f (\nettestEnv -> cont (g nettestEnv))

-- | A thread-safe, lazy, write-once cache.
--
-- >>> action <- memoize (putStrLn "hello" $> 3)
-- >>> action
-- hello
-- 3
-- >>> action
-- 3
memoize :: forall a. IO a -> IO (IO a)
memoize action = do
  -- The implementation is very similar to 'once' from @io-memoize@.
  -- https://hackage.haskell.org/package/io-memoize-1.1.1.0/docs/System-IO-Memoize.html
  cache <- newMVar Nothing

  let
    readCache :: IO a
    readCache =
      readMVar cache >>= \case
        Just a -> pure a
        Nothing ->
          modifyMVar cache $ \case
            Just a -> pure (Just a, a)
            Nothing -> do
              a <- action
              pure (Just a, a)

  pure readCache
