-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Implementation that works with real Tezos network, it
-- talks to a Tezos node and uses @tezos-client@.

module Morley.Nettest.Client
  ( runNettestClient
  , nettestImplClient
  , revealKeyUnlessRevealed
  , TestError(..)
  , SecretKeyException (..)
  ) where

import Data.Constraint ((\\))
import Data.Time (NominalDiffTime, UTCTime, diffUTCTime, secondsToNominalDiffTime)
import Fmt (Buildable(build), pretty, unlinesF, (+|), (|+))
import Named (arg)
import System.IO (hFlush)
import Time (KnownDivRat, Second, Time, sec, threadDelay, toNum, toUnit)
import Type.Reflection (SomeTypeRep(..), someTypeRep)

import Lorentz
  (NiceUnpackedValue, ToAddress(toAddress), errorToVal, errorToValNumeric, niceParameterEvi,
  niceUnpackedValueEvi)
import Michelson.Typed (SomeValue, SomeValue'(SomeValue), ToT, UnpackedValScope, toVal)
import qualified Michelson.Typed as T
import Morley.Client (Alias, MorleyClientEnv, disableAlphanetWarning, runMorleyClientM)
import qualified Morley.Client as Client
import qualified Morley.Client.Action.Transaction as AT
  (TD(..), TransactionData(..), runTransactions)
import Morley.Client.Logging (logDebug)
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types (BlockConstants(bcHeader), BlockHeader(bhTimestamp))
import Morley.Micheline (Expression, FromExpression(fromExpression), FromExpressionError)
import Morley.Nettest.Parser (NettestEnv(..))
import Tezos.Address (Address, mkKeyAddress)
import Tezos.Core as Tezos (Mutez, Timestamp, timestampFromUTCTime)
import Tezos.Crypto
import Util.Named ((:!), (.!))

import Cleveland.Util (ceilingUnit, formatSomeValue)
import Morley.Nettest.Abstract
import Morley.Nettest.Exceptions (addCallStack, throwWithCallStack, tryWithCallStack)

data SecretKeyException
  = TwoKeysException SecretKey
  | NoKeyException
  deriving stock(Generic, Show, Eq)

instance Exception SecretKeyException where
  displayException (TwoKeysException ek)= "Cannot deduce which key to use\n" ++
    "Passed key:" ++ toString (formatSecretKey ek) ++ "\n"
  displayException NoKeyException =
    "No secret key is passed, neither `nettest` key is found in key storage"

-- | Run 'NettestScenario' using implementation for real network.
runNettestClient :: NettestEnv -> NettestScenario IO -> IO ()
runNettestClient (NettestEnv env envKey) scenario = do
  disableAlphanetWarning
  storageAddress <- runMorleyClientM env (Client.resolveAddressMaybe nettestAddress)
  case (envKey, storageAddress) of
    (Nothing, Just _) -> pure ()
    (Nothing, Nothing) -> throwM NoKeyException
    (Just ek, Just sa) -> unless (mkKeyAddress (toPublic ek) == sa) (throwM $ TwoKeysException ek)
    (Just ek, Nothing) -> runMorleyClientM env (Client.importKey False "nettest" ek)
  scenario $ nettestImplClient env

-- | Implementation that works with real network and uses `tezos-node`
-- RPC and `tezos-client`.
nettestImplClient :: MorleyClientEnv -> NettestImpl IO
nettestImplClient env =
  NettestImpl
    { niRunIO = addCallStack

    , niOriginateUntyped = \UntypedOriginateData {..} -> addCallStack do
        originatorAlias <- niGetAlias uodFrom
        let
          originationScenario =
            Client.originateUntypedContract True uodName uodFrom uodBalance uodContract uodStorage Nothing
        -- Note that tezos key reveal operation cost an additional fee
        -- so that's why we reveal keys in origination and transaction
        -- rather than doing it before scenario execution
        revealKeyUnlessRevealed env originatorAlias
        (_, res) <- runMorleyClientM env originationScenario
        niComment $
          "Originated smart contract " +| uodName |+
          " with address " <> pretty res
        pure res

    , niSignBytes = \hash signer -> addCallStack $ runMorleyClientM env $
        Client.signBytes (AddressAlias signer) hash

    , niGenKey =
        addCallStack . runMorleyClientM env . Client.genKey

    , niGenFreshKey =
        addCallStack . runMorleyClientM env . Client.genFreshKey

    , niExpectFailure = withExpectedFailure
    , niGetBalance = addCallStack . getBalanceHelper
    , niCheckBalance = \addrOrAlias expected -> addCallStack do
        balance <- getBalanceHelper addrOrAlias
        when (balance /= expected) $
          throwM $
            UnexpectedBalance addrOrAlias (#expected .! expected) (#actual .! balance)
    , niCheckStorage = \addrOrAlias expected -> addCallStack do
        actual <- niGetStorage addrOrAlias
        when (actual /= expected) $
          throwM $
            UnexpectedStorage addrOrAlias (#expected .! SomeValue expected) (#actual .! SomeValue actual)
    , niGetChainId = addCallStack $ runMorleyClientM env Client.getChainId
    , niFailure = \msg ->
        throwWithCallStack callStack $ CustomTestError (pretty msg)
    , ..
    }
  where
    niGetStorage :: forall t. (HasCallStack, UnpackedValScope t) => AddressOrAlias -> IO (T.Value t)
    niGetStorage addrOrAlias = addCallStack do
      expr <- niGetStorageExpr addrOrAlias
      either throwM pure (fromExpression @(T.Value t) expr)

    niGetStorageExpr :: HasCallStack => AddressOrAlias -> IO Expression
    niGetStorageExpr addrOrAlias = addCallStack do
      addr <- niResolveAddress addrOrAlias
      runMorleyClientM env $ Client.getContractStorage addr

    niComment :: HasCallStack => Text -> IO ()
    niComment msg = addCallStack $ putTextLn msg >> hFlush stdout

    niResolveAddress :: HasCallStack => AddressOrAlias -> IO Address
    niResolveAddress = addCallStack . runMorleyClientM env . Client.resolveAddress

    niGetAlias :: HasCallStack => AddressOrAlias -> IO Alias
    niGetAlias = addCallStack . runMorleyClientM env . Client.getAlias

    niGetPublicKey :: HasCallStack => AddressOrAlias -> IO PublicKey
    niGetPublicKey = addCallStack . runMorleyClientM env . Client.getPublicKey

    getBalanceHelper addrOrAlias = do
      addr <- niResolveAddress addrOrAlias
      runMorleyClientM env $ Client.getBalance addr

    niGetNow :: HasCallStack => IO Tezos.Timestamp
    niGetNow = addCallStack $ timestampFromUTCTime <$> getLastBlockTimestamp

    niAdvanceTime :: (HasCallStack, KnownDivRat unit Second) => Time unit -> IO ()
    niAdvanceTime delta = addCallStack do
      let
        -- Round 'delta' to the nearest second, not smaller than 'delta'.
        -- A chain's time resolution is never smaller than a second,
        -- so if 'delta' is 0.1s, we actually need to wait at least 1s.
        deltaSec :: Time Second
        deltaSec = ceilingUnit $ toUnit @Second delta

        deltaSec' :: NominalDiffTime
        deltaSec' = secondsToNominalDiffTime $ toNum @Second deltaSec
      t0 <- getLastBlockTimestamp
      threadDelay deltaSec
      let
        go :: IO ()
        go = do
          now <- getLastBlockTimestamp
          if (now `diffUTCTime` t0) >= deltaSec'
            then pass
            else threadDelay (sec 1) >> go
      go

    getLastBlockTimestamp :: IO UTCTime
    getLastBlockTimestamp = bhTimestamp . bcHeader <$> runMorleyClientM env (Client.getBlockConstants "head")

    niTransferBatch :: HasCallStack => AddressOrAlias -> [TransferData] -> IO ()
    niTransferBatch sender tdList = addCallStack do
      senderAlias <- niGetAlias sender
      revealKeyUnlessRevealed env senderAlias
      transactions <- forM tdList $ \(TransferData _ to amount epName (param :: t)) -> do
        toAddr <- niResolveAddress to
        return $ T.withDict (niceParameterEvi @t) $
          AT.TransactionData AT.TD
            { tdReceiver = toAddr
            , tdAmount = amount
            , tdEpName = epName
            , tdParam = toVal param
            , tdMbFee = Nothing
            }
      senderAddr <- niResolveAddress sender
      runMorleyClientM env (AT.runTransactions senderAddr transactions)
      pure ()

----------------------------------------------------------------------------
-- Helpers
----------------------------------------------------------------------------

revealKeyUnlessRevealed :: HasCallStack => MorleyClientEnv -> Alias -> IO ()
revealKeyUnlessRevealed env alias = runMorleyClientM env $ do
  catch (Client.revealKey alias) $ \case
    (Client.AlreadyRevealed _) ->
      logDebug $ "<nettest> " +| alias |+ " alias has already revealed key"
    err -> throwM err

----------------------------------------------------------------------------
-- Validation
----------------------------------------------------------------------------

-- | Signals an assertion failure during the execution of a 'NettestScenario' action.
data TestError
  = UnexpectedBalance AddressOrAlias ("expected" :! Mutez) ("actual" :! Mutez)
  | UnexpectedStorage AddressOrAlias ("expected" :! SomeValue) ("actual" :! SomeValue)
  | UnexpectedAddress ("expected" :! Address) ("actual" :! Address)
  | UnexpectedFailWithValue ("expected" :! SomeValue) ("actual" :! SomeValue)
  | ParseExpressionError SomeTypeRep Expression FromExpressionError
  | UnexpectedClientSuccess
  | UnexpectedException Text SomeException
  | CustomTestError Text
  deriving stock Show

instance Exception TestError where
  displayException = pretty

instance Buildable TestError where
  build = \case
    UnexpectedBalance addrOrAlias (arg #expected -> expected) (arg #actual -> actual) ->
      "Account " +| addrOrAlias |+ " is expected to have balance " +| expected |+
      ". But its actual balance is " +| actual |+ ""
    UnexpectedStorage addrOrAlias (arg #expected -> expected) (arg #actual -> actual) ->
      "Account " +| addrOrAlias |+ " is expected to have storage " +| formatSomeValue expected |+
      ". But its actual storage is " +| formatSomeValue actual |+ "."
    UnexpectedAddress (arg #expected -> expected) (arg #actual -> actual) ->
      "Expected address: " +| expected |+ ", but found: " +| actual |+ ""
    UnexpectedFailWithValue (arg #expected -> expected) (arg #actual -> actual) ->
      "Expected interpretation to fail with value: " +| formatSomeValue expected |+
      ", but found: " +| formatSomeValue actual |+ ""
    ParseExpressionError (SomeTypeRep expectedType) expr err ->
      "Failed to parse expression: " +| expr |+ " to value of type: " +| show @String expectedType |+
      " due to: " +| err |+ ""
    UnexpectedClientSuccess ->
      "Nettest failed because tezos-client unexpectedly exited with 0 error code"
    UnexpectedException reason ex -> unlinesF
      [ "Expected exception due to: " <> build reason
      , "But got:"
      , build $ displayException ex
      ]
    CustomTestError msg -> build msg

-- | Checks that a failure occurs (if one is expected) and that it matches the
-- expected one.
withExpectedFailure :: HasCallStack => IO a -> NettestFailure -> IO ()
withExpectedFailure sc expectedErr =
  tryWithCallStack @ClientRpcError sc >>= \case
    Right _ -> throwWithCallStack callStack UnexpectedClientSuccess
    Left (_, actualErr) ->
      addCallStack $ checkFailure actualErr
  where
    checkFailure :: ClientRpcError -> IO ()
    checkFailure actualErr =
      case expectedErr of
        NettestFailedWith expectedAddr expectedVal
          | ContractFailed actualAddr actualExpr <- actualErr -> do
              checkAddr expectedAddr actualAddr
              checkExpression' expectedVal actualExpr
          | otherwise -> throwM $ UnexpectedException "FAILWITH" (toException actualErr)

        NettestFailedWithError expectedErr'
          | ContractFailed _ actualExpr <- actualErr -> do
              errorToVal expectedErr' $ \expectedVal ->
                checkExpression expectedVal actualExpr
          | otherwise -> throwM $ UnexpectedException "FAILWITH" (toException actualErr)

        NettestFailedWithNumericError errorTagMap expectedErr'
          | ContractFailed _ actualExpr <- actualErr -> do
              errorToValNumeric errorTagMap expectedErr' $ \expectedVal ->
                checkExpression expectedVal actualExpr
          | otherwise -> throwM $ UnexpectedException "FAILWITH" (toException actualErr)

        NettestShiftOverflow expectedAddr
          | ShiftOverflow actualAddr <- actualErr -> checkAddr expectedAddr actualAddr
          | otherwise -> throwM $ UnexpectedException "shift overflow" (toException actualErr)

        NettestEmptyTransaction expectedAddr
          | EmptyTransaction actualAddr <- actualErr -> checkAddr expectedAddr actualAddr
          | otherwise -> throwM $ UnexpectedException "empty transaction" (toException actualErr)

        NettestBadParameter
          | BadParameter _ _ <- actualErr -> pass
          | otherwise -> throwM $ UnexpectedException "ill-typed parameter" (toException actualErr)

    -- | Asserts that two addresses are equal.
    checkAddr :: ToAddress addr => addr -> Address -> IO ()
    checkAddr (toAddress -> expectedAddr) actualAddr
      | expectedAddr /= actualAddr =
          throwM $ UnexpectedAddress (#expected .! expectedAddr) (#actual .! actualAddr)
      | otherwise = pass

    -- | Tries to parse a Micheline expression to a value of type @t@ and,
    -- if successful, asserts that it is equal to the given expected value.
    checkExpression :: forall t. (UnpackedValScope t, Typeable t) => T.Value t -> Expression -> IO ()
    checkExpression expectedVal actualExpr =
      case fromExpression @(T.Value t) actualExpr of
        Left err -> throwM $ ParseExpressionError (someTypeRep $ Proxy @(T.Value t)) actualExpr err
        Right actualVal
          | expectedVal /= actualVal ->
              throwM $ UnexpectedFailWithValue
                (#expected .! SomeValue expectedVal)
                (#actual .! SomeValue actualVal)
          | otherwise -> pass

    checkExpression' :: forall t. NiceUnpackedValue t => t -> Expression -> IO ()
    checkExpression' expectedVal actualExpr =
      -- Note: we use `niceUnpackedValueEvi` here to build an `UnpackedValScope` dict
      -- (necessary for `fromExpression`) from a `NiceUnpackedValue` dict.
      checkExpression @(ToT t) (toVal expectedVal) actualExpr \\ niceUnpackedValueEvi @t
