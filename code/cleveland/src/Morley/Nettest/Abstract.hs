-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Abstract nettest interface not bound to a particular
-- implementation.
--
-- The interface may look a bit untyped and unsafe in some places.
-- For example, in order to call a contract one should supply a
-- simple address rather than a contract ref, so it is easy to pass
-- a value of wrong type. Also it is easy to call a non-existing entrypoint.
--
-- Subjectively, it makes writing test scenarios easier because you
-- have to prove less to the compiler. It also makes implementation of
-- nettest engine a bit easier. Of course, it also makes it easier
-- to make certain mistakes. However, we expect users of this interface
-- to also use the functionality of the 'Morley.Nettest.Pure' module
-- and convert nettest scenarios to purely testable scenarios for
-- integrational testing engine. In this case errors should be detected
-- almost as quickly as they would reported by the compiler, at least
-- before trying to run scenario on a live network.
--
-- Also this interface uses 'Address' rather than 'EpAddress'.
-- I (\@gromak) concluded that 'EpAddress' can not be passed to @tezos-client@.
-- For key addresses it just does not make sense and for contract addresses
-- I get such errors:
--
-- @
--   bad contract notation
--   Invalid contract notation "KT1VrFpBPwBTm3hsK7DB7SPmY8fTHJ3vY6sJ%mint"
-- @

module Morley.Nettest.Abstract
  ( AddressOrAlias (..)
  , CallData (..)
  , OriginateData (..)
  , TransferData (..)
  , NettestScenario
  , EmulatedScenario
  , UntypedOriginateData (..)

  -- * Constant address
  , nettestAddress
  , nettestAddressAlias

  -- * Actions
  , NettestImpl (..)
  , niResolveNettestAddress
  , niNewAddress
  , niTransfer
  , niOriginateUntypedSimple
  , niOriginate
  , niOriginateSimple
  , niCallFrom
  , niCallBatchFrom
  , niImportUntypedContract
  , EmulatedImpl(..)

  -- * Validation
  , NettestFailure (..)

  -- * Helpers
  , ep
  , (?-)

  -- * Morley client re-exports
  , AliasHint
  , mkCallData
  ) where

import Data.Constraint ((\\))
import Fmt (Builder)
import Time (KnownDivRat, Second, Time)

import Lorentz
  (ErrorTagMap, HasEntrypointArg, IsError, TAddress(..), ToAddress, ToTAddress(toTAddress),
  useHasEntrypointArg)
import Lorentz.Constraints
import Lorentz.Run (Contract, compileLorentzContract)
import Michelson.Interpret (MorleyLogs)
import Michelson.Typed (Dict(Dict), IsoValue(..), StorageScope, UnpackedValScope)
import qualified Michelson.Typed as T
import Michelson.Typed.Convert
import Michelson.Typed.Entrypoints
import qualified Michelson.Untyped as U
import Morley.Client
import Morley.Micheline (Expression)
import Tezos.Address
import Tezos.Core (ChainId, Mutez, Timestamp, toMutez, unsafeMkMutez)
import qualified Tezos.Crypto as Crypto

import Michelson.Test (importUntypedContract, (?-))

data OriginateData param =
  forall st.
  (NiceParameterFull param, NiceStorage st) => OriginateData
  { odFrom :: AddressOrAlias
  -- ^ The address from which contract will be originated (must have some XTZ).
  , odName :: AliasHint
  -- ^ Alias for the originated contract.
  , odBalance :: Mutez
  -- ^ Initial balance.
  , odStorage :: st
  -- ^ Initial storage.
  , odContract :: Contract param st
  -- ^ The contract itself.
  --
  -- We are using Lorentz version here which is convenient. However, keep in
  -- mind that if someone wants to test a contract from @.tz@ file, they should use
  -- 'UntypedOriginateData'.
  }

-- | Untyped version of OriginateData. It can be used for interaction with raw
-- Michelson contracts
data UntypedOriginateData = UntypedOriginateData
  { uodFrom :: AddressOrAlias
  -- ^ The address from which contract will be originated (must have some XTZ).
  , uodName :: AliasHint
  -- ^ Alias for the originated contract.
  , uodBalance :: Mutez
  -- ^ Initial balance.
  , uodStorage :: U.Value
  -- ^ Initial storage.
  , uodContract :: U.Contract
  -- ^ The contract itself.
  }

-- | Information about transfer operation.
data TransferData =
  forall v. NiceParameter v => TransferData
  { tdFrom :: AddressOrAlias
  -- ^ Sender address for this transaction.
  , tdTo :: AddressOrAlias
  -- ^ Receiver address for this transaction.
  , tdAmount :: Mutez
  -- ^ Amount to be transferred.
  , tdEntrypoint :: EpName
  -- ^ An entrypoint to be called. Consider using 'ep' in testing
  -- scenarios.
  , tdParameter :: v
  -- ^ Parameter that will be used for a contract call. Set to @()@
  -- for transfers to key addresses.
  }

-- | Datatype, which contains all parameters of call function
data CallData =
  forall v addr epRef epArg.
    (ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => CallData
  { cdProxyType :: Proxy v
  , cdTo :: addr
  -- ^ Address to be called via this transaction
  , cdEntrypoint :: epRef
  -- ^ An entrypoint to be called
  , cdEntrypointArgs :: epArg
  -- ^ Entrypoint arguments
  }

-- | A record data type with all base methods one can use during nettest.
data NettestImpl m = NettestImpl
  { niRunIO :: forall res. HasCallStack => IO res -> m res
  -- ^ Runs 'IO' Action
  , niResolveAddress :: HasCallStack => AddressOrAlias -> m Address
  -- ^ Unwrap an 'Address' or get an 'Address' corresponding to an alias.
  , niGetAlias :: HasCallStack => AddressOrAlias -> m Alias
  -- ^ Get an alias for the address provided to it.
  , niGenKey :: HasCallStack => AliasHint -> m Address
  -- ^ Generate a secret key and store it with given alias.
  -- If a key with this alias already exists, the corresponding address
  -- will be returned and no state will be changed.
  , niGenFreshKey :: HasCallStack => AliasHint -> m Address
  -- ^ Generate a secret key and store it with given alias.
  -- Unlike 'niGenKey' this function overwrites the existing key when
  -- given alias is already stored.
  , niSignBytes :: HasCallStack => ByteString -> Alias -> m Crypto.Signature
  -- ^ Get signature for preapplied operation.
  , niOriginateUntyped :: HasCallStack => UntypedOriginateData -> m Address
  -- ^ Originate a new raw Michelson contract with given data.
  , niTransferBatch :: HasCallStack => AddressOrAlias -> [TransferData] -> m ()
  -- ^ Send a batch of multiple transactions.
  , niComment :: HasCallStack => Text -> m ()
  -- ^ Print given string verbatim as a comment.
  , niExpectFailure :: forall a. HasCallStack => m a -> NettestFailure -> m ()
  -- ^ Expect a failure while running another action.
  , niGetBalance :: HasCallStack => AddressOrAlias -> m Mutez
  -- ^ Get balance for given address or alias.
  , niCheckBalance :: HasCallStack => AddressOrAlias -> Mutez -> m ()
  -- ^ Get balance for given address or alias and check whether it's
  -- equal to given one.
  , niGetStorage
      :: forall t. (HasCallStack, UnpackedValScope t, Typeable t)
      => AddressOrAlias -> m (T.Value t)
  -- ^ Get the storage for given address or alias.
  -- Note: the 'UnpackedValScope' constraint is used here because some implementations
  -- need it to parse a Micheline 'Expression's to 'T.Value'.
  --
  -- The 'UnpackedValScope' constraint also forbids storages with @big_map@s from being
  -- retrieved (due to a limitation in the tezos RPC).
  -- If a contract's storage contains @big_map@s, usage of that contract's getters is preferred.
  , niGetStorageExpr :: HasCallStack => AddressOrAlias -> m Expression
  -- ^ The same as 'niGetStorage' but doesn't require to know the type in advance.
  -- Returns a Micheline 'Expression' instead of a 'Value'.
  , niCheckStorage
      :: forall t. (HasCallStack, UnpackedValScope t, StorageScope t)
      => AddressOrAlias -> T.Value t -> m ()
  -- ^ Get storage for given address or alias and check whether it's equal to given one.
  -- Note: the 'UnpackedValScope' constraint is used here because some implementations
  -- need it to parse a Micheline 'Expression's to 'T.Value', while others need
  -- 'StorageScope' to convert the given 'T.Value' to 'U.Value'.
  --
  -- The 'UnpackedValScope' constraint also forbids storages with @big_map@s from being
  -- retrieved (due to a limitation in the tezos RPC).
  -- If a contract's storage contains @big_map@s, usage of that contract's getters is preferred.
  , niGetPublicKey :: HasCallStack => AddressOrAlias -> m Crypto.PublicKey
  -- ^ Get public key assosiated with given address or alias.
  -- Fail if given address or alias is not an implicit account.
  , niGetChainId :: HasCallStack => m ChainId
  -- ^ Get current @ChainId@.
  , niAdvanceTime :: forall unit. (HasCallStack, KnownDivRat unit Second) => Time unit -> m ()
  -- ^ Advance at least the given amount of time, or until a new block is baked,
  -- whichever happens last.
  --
  -- On a real network, this is implemented using 'threadDelay', so it's advisable
  -- to use small amounts of time only.
  , niGetNow :: HasCallStack => m Timestamp
  -- ^ Get the timestamp observed by the last block to be baked.
  , niFailure :: forall a. HasCallStack => Builder -> m a
  -- ^ Fails the test with the given error message.
  }

-- | Any monadic computation that can only access nettest methods.
type NettestScenario m = Monad m => NettestImpl m -> m ()

-- | A record data type with all base methods one can use during nettest, but which are available
-- only when running on an emulated environment (e.g. `Michelson.Runtime`) and not on a real network.
data EmulatedImpl m = EmulatedImpl
  { eiBranchout :: [(Text, m ())] -> m ()
  -- ^ Execute multiple testing scenarios independently, basing
  -- them on scenario built till this point.
  --
  -- The following property holds for this function:
  --
  -- @ pre >> branchout [a, b, c] = branchout [pre >> a, pre >> b, pre >> c] @.
  --
  -- In case of property failure in one of the branches no following branch is
  -- executed.
  --
  -- Providing empty list of scenarios to this function causes error;
  -- we do not require 'NonEmpty' here though for convenience.
  , eiOffshoot :: Text -> m () -> m ()
  -- ^ Test given scenario with the state gathered till this moment;
  -- if this scenario passes, go on as if it never happened.
  , eiCheckStorage
      :: forall t. (HasCallStack, StorageScope t)
      => AddressOrAlias -> T.Value t -> m ()
  -- ^ Get the storage for given address or alias and check whether it's equal to given one.
  --
  -- Unlike 'niCheckStorage', this function does not have the 'UnpackedValScope' constraint,
  -- and thus is able to retrieve storages that contain @big_map@s.
  , eiGetMorleyLogs :: m [MorleyLogs]
  -- ^ returns the logs of an entire scenario
  }

-- | Any monadic computation that can only access nettest methods
-- and will be run in an emulated environment.
type EmulatedScenario m = Monad m => EmulatedImpl m -> NettestImpl m -> m ()

----------------------------------------------------------------------------
-- Constant addresses
----------------------------------------------------------------------------

-- | This address will be used as source by default. Some
-- functionality in this module assumes that this address has
-- /sufficient/ amount of XTZ in advance (required amount depends on scenario).
nettestAddress :: AddressOrAlias
nettestAddress = AddressAlias nettestAddressAlias

-- | Alias for nettest used in 'nettestAddress'.
nettestAddressAlias :: Alias
nettestAddressAlias = Alias "nettest"

--------------------------------------------------------------------------
-- Higher-level actions
--
-- These functions are built upon the functions provided by 'NettestImpl' and
-- offer a more convenient experience.
--
-- They take 'NettestImpl' as an explicit argument
-- and do not have extra constraints. It makes it easier to adapt them for
-- any reasonable interface.
--
-- Adapted to @caps@ versions are provided in 'Morley.Nettest.Caps'.
--------------------------------------------------------------------------

-- | Get 'Address' corresponding to 'nettestAddress'.
niResolveNettestAddress :: HasCallStack => NettestImpl m -> m Address
niResolveNettestAddress = flip niResolveAddress nettestAddress

-- | Generate a new secret key and record it with given alias. Also
-- transfer small amount of XTZ to it from 'nettestAddress'. Does not
-- override an existing address.
--
-- Note, that when we generate a new address we must be aware of
-- prefix appended to it, so that when you create address alias @foo@,
-- you cannot call it directly by `AddressAlias "foo"`, but with the
-- corresponding prefix `AddressAlias "prefix.foo"`.
niNewAddress :: (HasCallStack, Monad m) => NettestImpl m -> AliasHint -> m Address
niNewAddress impl name = do
  addr <- niGenKey impl name
  let addr' = AddressResolved addr
  -- The address may exist from previous scenarios runs and have sufficient
  -- balance for the sake of testing; if so, we can save some time
  balance <- niGetBalance impl addr'
  when (balance < toMutez 0.5_e6) $  -- < 0.5 XTZ
    niTransfer impl TransferData
      { tdFrom = nettestAddress
      , tdTo = addr'
      , tdAmount = toMutez $ 0.9_e6 -- 0.9 XTZ
      , tdEntrypoint = DefEpName
      , tdParameter = ()
      }
  pure addr

-- | A simplified version of the originateUntyped command. Uses default
-- address ('nettestAddress'). The contract will have 0 balance.
niOriginateUntypedSimple
  :: HasCallStack => NettestImpl m -> AliasHint -> U.Value -> U.Contract -> m Address
niOriginateUntypedSimple impl name storage contract =
  niOriginateUntyped impl $
  UntypedOriginateData
    { uodFrom = nettestAddress
    , uodName = name
    , uodBalance = toMutez 0
    , uodStorage = storage
    , uodContract = contract
    }

-- | Lorentz version for origination.
niOriginate :: (HasCallStack, Functor m) => NettestImpl m -> OriginateData param -> m (TAddress param)
niOriginate impl OriginateData{..} =
  fmap TAddress . niOriginateUntyped impl $
  UntypedOriginateData
  { uodFrom = odFrom
  , uodName = odName
  , uodBalance = odBalance
  , uodStorage = untypeHelper odStorage
  , uodContract = convertContract $ compileLorentzContract odContract
  }
  where
    untypeHelper :: forall st. NiceStorage st => st -> U.Value
    untypeHelper = untypeValue . toVal \\ niceStorageEvi @st

niTransfer :: HasCallStack => NettestImpl m -> TransferData -> m ()
niTransfer impl td = (niTransferBatch impl) (tdFrom td) [td]

-- | A simplified version of the originate command. Uses default
-- address ('nettestAddress'). The contract will have 0 balance.
niOriginateSimple ::
     (HasCallStack, NiceParameterFull param, NiceStorage st, Functor m)
  => NettestImpl m
  -> AliasHint
  -> st
  -> Contract param st
  -> m (TAddress param)
niOriginateSimple impl name storage contract =
  niOriginate impl $
  OriginateData
    { odFrom = nettestAddress
    , odName = name
    , odBalance = toMutez 0
    , odStorage = storage
    , odContract = contract
    }

-- | Call a certain entrypoint of a contract referred to by the second
-- typed address from the first address.
niCallFrom ::
     forall v addr m epRef epArg.
     (HasCallStack, ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => NettestImpl m
  -> AddressOrAlias
  -> addr
  -> epRef
  -> epArg
  -> m ()
niCallFrom impl from (toTAddress @v @addr -> TAddress to) epRef param =
  case useHasEntrypointArg @v @epRef @epArg epRef of
    (Dict, epName) ->
      niTransfer impl $
      TransferData
        { tdFrom = from
        , tdTo = AddressResolved to
        , tdAmount = toMutez 0
        , tdEntrypoint = epName
        , tdParameter = param
        }

niCallBatchFrom
  :: forall m. HasCallStack
  => NettestImpl m
  -> AddressOrAlias
  -> [CallData]
  -> m ()
niCallBatchFrom impl sender callData = do
  let tds = createTransferData <$> callData
  niTransferBatch impl sender tds
  where
    createTransferData :: CallData -> TransferData
    createTransferData (CallData (_ :: Proxy v) (to :: addr) (epRef :: epRef) (param :: param)) = do
      case useHasEntrypointArg @v @epRef @param epRef of
        (Dict, epName) ->
          TransferData
            { tdFrom = sender
            , tdTo = addressResolved (toTAddress @v @addr to)
            , tdAmount = unsafeMkMutez 0
            , tdEntrypoint = epName
            , tdParameter = param
            }

niImportUntypedContract :: HasCallStack => NettestImpl m -> FilePath -> m U.Contract
niImportUntypedContract impl path = niRunIO impl (importUntypedContract path)

----------------------------------------------------------------------------
-- Validation
----------------------------------------------------------------------------

-- | Failures that could be expected in the execution of a 'NettestScenario' action.
-- These can be caught and handled with 'Morley.Nettest.expectFailure'.
data NettestFailure
  = forall t addr. (ToAddress addr, NiceUnpackedValue t) => NettestFailedWith addr t
  -- ^ Expect that interpretation of contract with the given address ended
  -- with [FAILED].
  | forall err. (IsError err, Eq err) => NettestFailedWithError err
  -- ^ Expect contract to fail with the given error, e.g. @NettestFailedWithError (VoidResult False)@.
  | forall err. (IsError err, Eq err) => NettestFailedWithNumericError ErrorTagMap err
  -- ^ Expect contract to fail with the given error (when using numeric representation of errors).
  | forall addr. ToAddress addr => NettestShiftOverflow addr
  -- ^ Expect that interpretatino of contract with the given address ended
  -- with a shift overflow error.
  | forall addr. ToAddress addr => NettestEmptyTransaction addr
  -- ^ Expect failure due to an attempt to transfer 0tz towards a simple address.
  | NettestBadParameter
  -- ^ Expect failure due to an attempt to call a contract with an invalid parameter.

  -- TODO: [#284] add more errors here!
  --  | NettestMutezArithError ArithErrorType
  --  | NettestGasExhaustion

----------------------------------------------------------------------------
-- Other helpers
----------------------------------------------------------------------------

-- | A short partial constructor for 'EpName'. It is supposed to be
-- applied to string constants, so programmer is responsible for
-- validity. And this code is for tests anyway, so each failure is a
-- programmer mistake.
--
-- It is intentionally here and not in some deeper module because the
-- name is really short and more suitable for writing scenarios.
ep :: HasCallStack => Text -> EpName
ep = U.unsafeBuildEpName


-- | Constructor for 'CallData'
mkCallData ::
     forall v addr epRef epArg.
     (ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => addr
  -> epRef
  -> epArg
  -> CallData
mkCallData = CallData (Proxy @v)
