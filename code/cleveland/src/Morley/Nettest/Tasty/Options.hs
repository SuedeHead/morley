-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | This module declares all options/flags that @tasty@ will
-- parse from the command line/environment variables.
--
-- They're used to configure 'Morley.Nettest'.
module Morley.Nettest.Tasty.Options
  ( nettestOptions
  , AliasPrefixOpt(..)
  , AddressOpt(..)
  , EndpointOpt(..)
  , PortOpt(..)
  , PathOpt(..)
  , DataDirOpt(..)
  , UseHttpsOpt(..)
  , VerboseOpt(..)
  , SecretKeyOpt(..)
  , RunNetworkOpt(..)
  , NoRunNetworkOpt(..)
  , ContextLinesOpt(..)
  , Switch(..)
  ) where

import Control.Monad.Except (runExcept)
import Data.Tagged (untag)
import qualified Options.Applicative as Opt
import Options.Applicative.Types (Parser, ReadM(..))
import Servant.Client.Core (BaseUrl(..), Scheme(..))
import Test.Tasty.Options as Tasty (IsOption(..), OptionDescription(Option), safeRead, safeReadBool)

import Morley.Client.Parser (baseUrlReader)
import qualified Tezos.Crypto as Crypto
import Util.CLI (HasCLReader(getMetavar, getReader))

-- $defaultValues
--
-- Tasty will use 'optionCLParser' to attempt to parse options from the command line.
-- If it fails, it will try to parse them from environment variables instead (using 'parseValue').
--
-- For this reason, it's important that 'optionCLParser' is NOT a parser
-- that /always/ succeeds. If it is, tasty will not attempt to read environment variables.
--
-- From the [docs](http://hackage.haskell.org/package/tasty-1.3.1/docs/Test-Tasty-Options.html#v:optionCLParser):
--
-- > Do not supply a default value (e.g., with the value function) here for this parser!
-- > This is because if no value was provided on the command line we may lookup the option e.g.
-- > in the environment. But if the parser always succeeds, we have no way to tell
-- > whether the user really provided the option on the command line.


-- | A list with all the options needed to configure 'Morley.Nettest'.
nettestOptions :: [OptionDescription]
nettestOptions =
  [ Tasty.Option (Proxy @AliasPrefixOpt)
  , Tasty.Option (Proxy @EndpointOpt)
  , Tasty.Option (Proxy @AddressOpt)
  , Tasty.Option (Proxy @PortOpt)
  , Tasty.Option (Proxy @PathOpt)
  , Tasty.Option (Proxy @DataDirOpt)
  , Tasty.Option (Proxy @UseHttpsOpt)
  , Tasty.Option (Proxy @VerboseOpt)
  , Tasty.Option (Proxy @SecretKeyOpt)
  , Tasty.Option (Proxy @RunNetworkOpt)
  , Tasty.Option (Proxy @NoRunNetworkOpt)
  , Tasty.Option (Proxy @ContextLinesOpt)
  ]

----------------------------------------------------------------------------
-- Morley Client options
----------------------------------------------------------------------------

newtype AliasPrefixOpt = AliasPrefixOpt (Maybe Text)
  deriving stock (Show, Eq)

instance IsOption AliasPrefixOpt where
  defaultValue = AliasPrefixOpt Nothing
  optionName = "nettest-alias-prefix"
  optionHelp = "[Morley.Nettest] A prefix to prepend to every alias created " <>
               "with 'newAddress' or 'newFreshAddress'."
  parseValue = mkParseValue (AliasPrefixOpt . Just)
  optionCLParser = mkOptionParser (AliasPrefixOpt . Just) Nothing

-- | Morley Client option specifying endpoint URL of the Tezos node.
newtype EndpointOpt = EndpointOpt (Maybe BaseUrl)
  deriving stock (Show, Eq)

instance IsOption EndpointOpt where
  defaultValue = EndpointOpt Nothing
  optionName = "nettest-node-endpoint"
  optionHelp = "[Morley.Nettest] Remote node endpoint URL"
  parseValue = mkParseValueWithReadM baseUrlReader (EndpointOpt . Just)
  optionCLParser =
    mkOptionParserWithReadM baseUrlReader "URL" (EndpointOpt . Just) (Just 'E')

newtype AddressOpt = AddressOpt (Maybe String)
  deriving stock (Show, Eq)

instance IsOption AddressOpt where
  defaultValue = AddressOpt Nothing
  optionName = "nettest-node-addr"
  optionHelp = "[Morley.Nettest] Remote node host "
            <> "[DEPRECATED: use nettest-node-endpoint instead]"
  parseValue = mkParseValue (AddressOpt . Just)
  optionCLParser = mkOptionParser (AddressOpt . Just) (Just 'A')

newtype PortOpt = PortOpt (Maybe Int)
  deriving stock (Show, Eq)

instance IsOption PortOpt where
  defaultValue = PortOpt Nothing
  optionName = "nettest-node-port"
  optionHelp = "[Morley.Nettest] Remote node port "
            <> "[DEPRECATED: use nettest-node-endpoint instead]"
  parseValue = mkParseValue (PortOpt . Just)
  optionCLParser = mkOptionParser (PortOpt . Just) (Just 'P')

newtype PathOpt = PathOpt FilePath
  deriving stock (Show, Eq)

instance IsOption PathOpt where
  defaultValue = PathOpt "tezos-client"
  optionName = "nettest-client-path"
  optionHelp = "[Morley.Nettest] Path to tezos-client binary"
  parseValue = mkParseValue PathOpt
  optionCLParser = mkOptionParser PathOpt (Just 'I')

newtype DataDirOpt = DataDirOpt (Maybe FilePath)
  deriving stock (Show, Eq)

instance IsOption DataDirOpt where
  defaultValue = DataDirOpt Nothing
  optionName = "nettest-data-dir"
  optionHelp = "[Morley.Nettest] Path to tezos-client data directory"
  parseValue = mkParseValue (DataDirOpt . Just)
  optionCLParser = mkOptionParser (DataDirOpt . Just) (Just 'd')

newtype UseHttpsOpt = UseHttpsOpt Scheme
  deriving stock (Show, Eq)

instance IsOption UseHttpsOpt where
  defaultValue = UseHttpsOpt Http
  optionName = "nettest-use-https"
  optionHelp = "[Morley.Nettest] Use HTTPS to communicate with the remote node "
            <> "[DEPRECATED: use nettest-node-endpoint instead]"
  parseValue = \str -> UseHttpsOpt . switchToScheme <$> safeReadSwitch str
  optionCLParser = mkSwitchParser (UseHttpsOpt . switchToScheme) (Just 'S')

-- | To increase verbosity, pass @-V@ several times on the command line (e.g. @-VVV@),
-- or set @TASTY_NETTEST_VERBOSE=3@ as an environment variable.
newtype VerboseOpt = VerboseOpt Word
  deriving stock (Show, Eq)

instance IsOption VerboseOpt where
  defaultValue = VerboseOpt 0
  optionName = "nettest-verbose"
  optionHelp = "[Morley.Nettest] Increase verbosity (pass several times to increase further)"
  parseValue = \str -> VerboseOpt <$> safeRead str
  optionCLParser =
    -- See: $defaultValues
    --
    -- Therefore, we use `some` instead of `many` here.
    -- `some` will fail if "-V" is absent, and then tasty will attempt
    -- to parse the "TASTY_NETTEST_VERBOSE" env variable instead.
    VerboseOpt . genericLength <$> some flag
    where
      flag =
        Opt.flag' () . mconcat $
          [ Opt.short 'V'
          , Opt.long (untag (optionName @VerboseOpt))
          , Opt.help (untag (optionHelp @VerboseOpt))
          ]

----------------------------------------------------------------------------
-- Nettest options
----------------------------------------------------------------------------

newtype SecretKeyOpt = SecretKeyOpt (Maybe Crypto.SecretKey)
  deriving stock (Show, Eq)

instance IsOption SecretKeyOpt where
  defaultValue = SecretKeyOpt Nothing
  optionName = "nettest-import-secret-key"
  optionHelp = "[Morley.Nettest] Saves input secret key with a `nettest` alias. This key" <>
               " will be used when performing nettest actions"
  parseValue = mkParseValue (SecretKeyOpt . Just)
  optionCLParser = mkOptionParser (SecretKeyOpt . Just) Nothing

newtype RunNetworkOpt = RunNetworkOpt Switch
  deriving stock (Show, Eq)

instance IsOption RunNetworkOpt where
  defaultValue = RunNetworkOpt Unset
  optionName = "nettest-run-network"
  optionHelp = "[Morley.Nettest] Run the tests on a Tezos network" <>
               " in addition to the Morley.Michelson emulator."
  parseValue = \str -> RunNetworkOpt <$> safeReadSwitch str
  optionCLParser = mkSwitchParser RunNetworkOpt Nothing

newtype NoRunNetworkOpt = NoRunNetworkOpt Switch
  deriving stock (Show, Eq)

instance IsOption NoRunNetworkOpt where
  defaultValue = NoRunNetworkOpt Unset
  optionName = "nettest-no-run-network"
  optionHelp = "[Morley.Nettest] Run only emulator tests and skip network tests." <>
               " This flag takes precedence over '--nettest-run-network'."
  parseValue = \str -> NoRunNetworkOpt <$> safeReadSwitch str
  optionCLParser = mkSwitchParser NoRunNetworkOpt Nothing

newtype ContextLinesOpt = ContextLinesOpt Natural
  deriving stock (Show, Eq)

instance IsOption ContextLinesOpt where
  defaultValue = ContextLinesOpt 5
  optionName = "nettest-context-lines"
  optionHelp = "[Morley.Nettest] Number of source code lines to show around an error when a test fails"
  parseValue = mkParseValue ContextLinesOpt
  optionCLParser = mkOptionParser ContextLinesOpt Nothing

----------------------------------------------------------------------------
-- Helpers
----------------------------------------------------------------------------

data Switch = Unset | Set
  deriving stock (Show, Eq)

safeReadSwitch :: String -> Maybe Switch
safeReadSwitch str = safeReadBool str <&> \case
  True -> Set
  False -> Unset

-- | Reads a 'Switch' into 'Scheme'; switch 'Set' equals to 'Https'.
switchToScheme :: Switch -> Scheme
switchToScheme = \case
  Set   -> Https
  Unset -> Http

-- | Build a command line option parser by reusing an instance of `IsOption`.
-- The parser fails if the flag is not found.
mkSwitchParser
  :: forall v
   . IsOption v
   => (Switch -> v)
   -> Maybe Char
   -> Parser v
mkSwitchParser wrap shortMb =
  -- See: $defaultValues
  --
  -- 'Opt.flag' and 'Opt.switch' always succeed, so we can't use them here.
  -- We must use 'Opt.flag'' instead, which /can/ fail.
  wrap <$> Opt.flag' Set mods
  where
    mods = mconcat
      [ maybe mempty Opt.short shortMb
      , Opt.long (untag (optionName @v))
      , Opt.help (untag (optionHelp @v))
      ]

-- Create an environment variable parser for types that don't have a
-- 'HasCLReader' instance to avoid adding an unnecessary dependency.
mkParseValueWithReadM :: forall v a. ReadM a -> (a -> v) -> String -> Maybe v
mkParseValueWithReadM readm wrap =
  fmap wrap . rightToMaybe . runExcept . runReaderT (unReadM readm)

-- | Create an environment variable parser by reusing an instance of `HasCLReader`.
mkParseValue
  :: forall v a
  .  HasCLReader a
  => (a -> v)
  -> String
  -> Maybe v
mkParseValue = mkParseValueWithReadM (getReader @a)

-- | Build a command line option parser by reusing an instance of `IsOption`,
-- for types that don't have an instance of 'HasCLReader'.
mkOptionParserWithReadM
  :: forall v a
  .  IsOption v
  => ReadM a
  -> String -- ^ metavar
  -> (a -> v)
  -> Maybe Char
  -> Parser v
mkOptionParserWithReadM readm metav wrap shortMb =
  wrap <$> Opt.option readm mods
  where
    -- See: $defaultValues
    --
    -- Therefore, we never specify a default value here.
    mods = mconcat
      [ Opt.metavar metav
      , maybe mempty Opt.short shortMb
      , Opt.long (untag (optionName @v))
      , Opt.help (untag (optionHelp @v))
      ]

-- | Build a command line option parser by reusing instances of `HasCLReader` and `IsOption`
mkOptionParser
  :: forall v a
  .  (IsOption v, HasCLReader a)
  => (a -> v)
  -> Maybe Char
  -> Parser v
mkOptionParser = mkOptionParserWithReadM (getReader @a) (getMetavar @a)
