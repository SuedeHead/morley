-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
{-# OPTIONS_GHC -Wno-deprecations #-}

-- | Integration with integrational testing engine (pun intended).
module Morley.Nettest.Pure
  ( PureM(..)
  , runEmulated
  , nettestTestProp
  , nettestImplIntegrational
  , emulatedImplIntegrational
  , BadScenario(..)

  -- * Initial environment for Emulated tests
  , initEnv

  -- * Support functions
  , scenarioToIO
  , askState
  , askAliases
  , askInternalState
  ) where

import qualified Data.Map as Map
import Data.Singletons (SingI, demote)
import Data.Typeable (gcast)
import Fmt (pretty)
import Hedgehog (MonadTest, evalIO)
import Time (Second, toNum, toUnit)

import Lorentz (ToAddress, toAddress, zeroMutez)
import Lorentz.Entrypoints (TrustEpName(..))
import Lorentz.Test
import Michelson.Interpret (MichelsonFailed(..))
import Michelson.Runtime (AddressState(..), ContractState(..), ExecutorError, ExecutorError'(..))
import Michelson.Runtime.GState (GState(..), asBalance, genesisSecretKey, gsChainIdL)
import Michelson.Test.Integrational
  (IntegrationalScenarioM(..), InternalState, ScenarioError(..), addrNameToAddr, addrToAddrName,
  appendScenarioBranch, expectBalance, expectMichelsonFailed, getScenarioLogs, initIS, isGState,
  isNow, originate, tExpectStorageConst)
import Michelson.Typed (ShiftArithErrorType(..), SomeValue'(SomeValue), toVal)
import qualified Michelson.Typed as T
import Michelson.Typed.Arith (ArithError(..))
import Morley.Client (Alias(..), AliasHint(..))
import Morley.Micheline (Expression, toExpression)
import Tezos.Address
import Tezos.Crypto (SecretKey(..), detSecretKey, sign, toPublic)
import Util.Named

import Cleveland.Util (ceilingUnit)
import Morley.Nettest.Abstract
import Morley.Nettest.Exceptions
  (addCallStack, catchWithCallStack, throwWithCallStack, tryWithCallStack)

{-# ANN nettestImplIntegrational ("HLint: ignore Use tuple-section" :: Text) #-}

data BadScenario = BadScenario
  deriving stock (Show, Eq)

instance Exception BadScenario

-- In this implementation we do not prefix aliases, so 'Alias' and 'AliasHint'
-- are identical and conversions between them are safe.
hintToAlias :: AliasHint -> Alias
hintToAlias (AliasHint a) = Alias a

scenarioToIO :: NettestScenario PureM -> IO ()
scenarioToIO scenario = do
  env <- newIORef initEnv
  runReaderT (unPureM (scenario nettestImplIntegrational)) env

-- | Run a 'NettestScenario' via the "Morley.Runtime" emulator,
-- inside an @hedgehog@ property.
nettestTestProp :: (MonadIO m, MonadTest m) => NettestScenario PureM -> m ()
nettestTestProp = evalIO . scenarioToIO

-- | Run an 'EmulatedScenario' via 'Michelson.Test.Integrational'.
runEmulated :: EmulatedScenario PureM -> NettestScenario PureM
runEmulated scenario = scenario emulatedImplIntegrational

newtype PureM a = PureM
  { unPureM :: ReaderT (IORef (Aliases, InternalState)) IO a
  }
  deriving newtype (Functor, Applicative, Monad, MonadIO,
                    MonadThrow, MonadCatch, MonadReader (IORef (Aliases, InternalState)))

type Aliases = Map Alias AliasData

-- | Datatype to store alias data, we store optional 'SecretKey' in addition
-- to 'Address' in order to support bytes signing.
data AliasData = AliasData
  { adAddress :: Address
  , adMbSecretKey :: Maybe SecretKey
  }

emulatedImplIntegrational :: EmulatedImpl PureM
emulatedImplIntegrational =
  EmulatedImpl
    { eiBranchout = supportBranchout
    , eiOffshoot = \name scenario -> supportBranchout [(name, scenario)]
    , eiCheckStorage = \addrOrAlias expectedStorage -> addCallStack do
        addr <- resolve addrOrAlias
        liftIntegrational $ tExpectStorageConst addr expectedStorage
    , eiGetMorleyLogs = liftIntegrational getScenarioLogs
    }
  where
    supportBranchout :: [(Text, PureM ())] -> PureM ()
    supportBranchout = \(scenarios :: [(Text, PureM ())]) ->
      forM_ scenarios $ \(name, scenario) -> do
        aliasesState <- askState
        newRef <- newIORef aliasesState
        local (\_ -> newRef) scenario `catchWithCallStack` \originalCallStackMb (ScenarioError branch err) ->
          maybe throwM throwWithCallStack originalCallStackMb $
            (ScenarioError (appendScenarioBranch name branch) err)

nettestImplIntegrational :: NettestImpl PureM
nettestImplIntegrational =
  NettestImpl
    { niRunIO = \action -> addCallStack $ liftIO action
    , niResolveAddress = addCallStack . resolve

    , niSignBytes = \bs alias -> addCallStack do
        aliases <- askAliases
        let mbMbSk = Map.lookup alias aliases
        mbSk <- maybe (unknownAlias alias) (pure . adMbSecretKey) mbMbSk
        case mbSk of
          Nothing ->
            failWith . CustomTestError .
            mappend "Given address doesn't have known associated secret key: " . show $ alias
          Just sk -> liftIO $ sign sk bs

    , niGenKey = addCallStack . smartGenKey Nothing

    , niGenFreshKey =
        \aliasHint -> addCallStack do
          aliases <- askAliases
          let mbSk = Map.lookup (hintToAlias aliasHint) aliases
          smartGenKey (adAddress <$> mbSk) aliasHint

    , niOriginateUntyped = \UntypedOriginateData {..} -> addCallStack do
        ref <-
          liftIntegrational $ originate uodContract (pretty uodName) uodStorage uodBalance
        saveAlias uodName (toAddress ref) Nothing

    -- Comments are not supported by integrational testing engine (yet).
    , niComment = addCallStack . const pass

    , niExpectFailure = \operation expectedErr -> do
        tryWithCallStack @ScenarioError operation >>= \case
          Right _ -> throwWithCallStack callStack ExpectingInterpreterToFail
          Left (_, (ScenarioError _ (InterpreterError actualErr))) ->
            addCallStack $
              liftIntegrational (toExpectation expectedErr (addrNameToAddr <$> actualErr))
          Left (originalCallStackMb, err) ->
            maybe throwM throwWithCallStack originalCallStackMb $
              err

    , niGetBalance = \addrOrAlias -> addCallStack do
        addr <- resolve addrOrAlias
        GState{..} <- liftIntegrational $ use isGState
        return $ maybe zeroMutez asBalance $ Map.lookup addr gsAddresses
    , niCheckBalance = \addrOrAlias expected -> addCallStack do
        addr <- resolve addrOrAlias
        liftIntegrational $ expectBalance addr expected
    , niCheckStorage = \addrOrAlias expectedStorage -> addCallStack do
        addr <- resolve addrOrAlias
        liftIntegrational $ tExpectStorageConst addr expectedStorage
    , niGetPublicKey = \addrOrAlias -> addCallStack do
        aliases <- askAliases
        mbSk <- case addrOrAlias of
          AddressAlias name -> do
            let mbSk = Map.lookup name aliases
            maybe (unknownAlias name) (pure . adMbSecretKey) mbSk
          AddressResolved addr -> do
            let mbSk = (fmap (adMbSecretKey . snd) . find
                  (\(_, AliasData addr' _) -> addr == addr') . Map.toList) aliases
            maybe (unknownAddress addr) pure mbSk
        case mbSk of
          Nothing ->
            failWith . CustomTestError .
            mappend "Given address doesn't have known associated public key: " . show $ addrOrAlias
          Just sk -> pure $ toPublic sk
    , niGetChainId = addCallStack do
        is <- askInternalState
        pure $ is^.isGState.gsChainIdL
    , niAdvanceTime = \time -> addCallStack do
        liftIntegrational $
          rewindTime $
            toNum @Second @Integer $ ceilingUnit $ toUnit @Second time

    , niGetNow = addCallStack . liftIntegrational $ use isNow
    , niFailure = \msg ->
        addCallStack $ failWith $ CustomTestError (pretty msg)
    , ..
    }
  where
    niGetStorage
      :: forall expectedT. (HasCallStack, Typeable expectedT, SingI expectedT)
      => AddressOrAlias -> PureM (T.Value expectedT)
    niGetStorage addrOrAlias = addCallStack do
      ContractState _ _ (storage :: T.Value actualT) <- contractStorage addrOrAlias
      case gcast @actualT @expectedT storage of
        Nothing -> failWith $ UnexpectedStorageType (demote @expectedT) (demote @actualT)
        Just val -> pure val

    niGetStorageExpr :: HasCallStack => AddressOrAlias -> PureM Expression
    niGetStorageExpr addrOrAlias = addCallStack do
      ContractState _ _ storage <- contractStorage addrOrAlias
      pure $ toExpression storage

    -- | Transfer function applied to one argument of 'TransferData' type
    niTransferHelper :: TransferData -> PureM ()
    niTransferHelper TransferData {..} = do
        fromAddr <- #from <.!> resolve tdFrom
        toAddr <- #to <.!> resolve tdTo
        -- Here @toAddr@ is 'Address', so we can not check anything
        -- about it and assume that entrypoint is correct. We pass
        -- unit as contract parameter because it won't be checked
        -- anyway.
        liftIntegrational $ lTransfer @() fromAddr toAddr tdAmount
          (TrustEpName tdEntrypoint) tdParameter

    niTransferBatch :: HasCallStack => AddressOrAlias -> [TransferData] -> PureM ()
    niTransferBatch sender tdList = addCallStack $ do
      case tdList of
        [] -> pure ()
        TransferData{..} : restList -> do
          niTransferHelper
            (TransferData sender tdTo tdAmount tdEntrypoint tdParameter)
          niTransferBatch sender restList

    niGetAlias :: HasCallStack => AddressOrAlias -> PureM Alias
    niGetAlias = addCallStack ... \case
      AddressAlias alias -> pure alias
      AddressResolved addr -> do
        aliases <- askAliases
        let maybeAlias = (fmap fst . find (\(_, AliasData addr' _) -> addr == addr') . Map.toList) aliases
        maybe (unknownAddress addr) pure maybeAlias

    -- Attempt to retrieve a ContractState given an AddressOrAlias. Fails if the
    -- address is unknown or the address is a simple address (contract without
    -- code and storage).
    contractStorage :: AddressOrAlias -> PureM ContractState
    contractStorage addrOrAlias = do
      addr <- resolve addrOrAlias
      GState{..} <- liftIntegrational $ use isGState
      case Map.lookup addr gsAddresses of
        Just (ASContract contractState) -> pure contractState
        Just (ASSimple {}) -> failWith . CustomTestError $
          "Expected address to be contract with storage, but it's a simple address: " <> show addr
        Nothing -> unknownAddress addr

    -- Generate a fresh address which was never generated for given alias.
    -- If the address is not saved, we use the alias as its seed.
    -- Otherwise we concatenate the alias with the saved address.
    smartGenKey :: Maybe Address -> AliasHint -> PureM Address
    smartGenKey existingAddr aliasHint@(AliasHint aliasTxt) =
      let
        seed = maybe aliasTxt (mappend aliasTxt . pretty) existingAddr
        sk = detSecretKey (encodeUtf8 seed)
        addr = detGenKeyAddress (encodeUtf8 seed)
       in saveAlias aliasHint addr $ Just sk

    unknownAddress :: Address -> PureM whatever
    unknownAddress =
      failWith . CustomTestError .
      mappend "Unknown address provided: " . pretty

    saveAlias :: AliasHint -> Address -> Maybe SecretKey -> PureM Address
    saveAlias name addr mbSk = do
      ref <- ask
      modifyIORef ref (first (Map.insert (hintToAlias name) (AliasData addr mbSk)))
      pure addr

resolve :: AddressOrAlias -> PureM Address
resolve = \case
  AddressResolved addr -> pure addr
  AddressAlias name -> do
    aliases <- askAliases
    let maybeAddress = Map.lookup name aliases
    maybe (unknownAlias name) (pure . adAddress) maybeAddress

unknownAlias :: Alias -> PureM whatever
unknownAlias =
  failWith . CustomTestError .
  mappend "Unknown address alias: " . pretty

failWith :: TestError -> PureM a
failWith = liftIntegrational . integrationalFail

liftIntegrational :: IntegrationalScenarioM a -> PureM a
liftIntegrational scenario = do
  st <- askInternalState
  updateAndReturn $ runState (runExceptT (unIntegrationalScenarioM scenario)) st
  where
    updateAndReturn (res, curState) = do
      ref <- ask
      modifyIORef ref (\(aliases, _) -> (aliases, curState))
      either throwM return res


-- | Converts a 'NettestFailure' to an 'IntegrationalScenario' expectation.
toExpectation :: NettestFailure -> ExecutorError -> IntegrationalScenario
toExpectation expectedErr actualErr =
  case expectedErr of
    NettestFailedWith (toAddress -> expectedAddr) (toVal -> expectedVal) ->
      expectMichelsonFailed expectedAddr actualErr >>= \case
        MichelsonFailedWith actualVal
          | SomeValue expectedVal == SomeValue actualVal -> pass
          | otherwise -> integrationalFail $ UnexpectedFailWithValue
              (SomeValue expectedVal)
              (SomeValue actualVal)
        _ -> unexpectedInterpreterError actualErr "expected FAILWITH"

    NettestFailedWithError expectedErr' ->
      lExpectError (== expectedErr') actualErr

    NettestFailedWithNumericError errorTagMap expectedErr' ->
      lExpectErrorNumeric errorTagMap (== expectedErr') actualErr

    NettestShiftOverflow expectedAddr ->
      expectMichelsonFailed (toAddress expectedAddr) actualErr >>= \case
        MichelsonArithError (ShiftArithError errType _ _) | isOverflow errType -> pass
        _ -> unexpectedInterpreterError actualErr "expected shift overflow error"

    NettestEmptyTransaction expectedAddr
      | EEZeroTransaction actualAddr <- actualErr -> checkAddr expectedAddr actualAddr
      | otherwise -> unexpectedInterpreterError actualErr "expected empty transaction"

    NettestBadParameter
      | EEIllTypedParameter _ <- actualErr -> pass
      | EEUnexpectedParameterType _ _ <- actualErr -> pass
      | otherwise -> unexpectedInterpreterError actualErr "expected ill-typed parameter"

    -- TODO: [#284](https://gitlab.com/morley-framework/morley/-/issues/284)
    -- NettestMutezArithError _ -> pass
    -- NettestGasExhaustion -> pass

  where
    checkAddr :: ToAddress addr => addr -> Address -> IntegrationalScenario
    checkAddr (toAddress -> expectedAddr) actualAddr
      | expectedAddr /= actualAddr = do
          iState <- get
          unexpectedInterpreterError actualErr $
            "expected runtime failure for contract with address: "
            <> pretty (addrToAddrName expectedAddr iState)
            <> ", but found address: "
            <> pretty (addrToAddrName actualAddr iState)
      | otherwise = pass

    isOverflow :: ShiftArithErrorType -> Bool
    isOverflow = \case
      LslOverflow -> True
      LsrUnderflow -> False

----------------------------------------------------------------------------
-- Support functions
----------------------------------------------------------------------------

initAliases :: Aliases
initAliases = one (nettestAddressAlias
                  , AliasData genesisAddress $
                    Just $ genesisSecretKey
                  )

initEnv :: (Aliases, InternalState)
initEnv = (initAliases, initIS)

askState :: PureM (Aliases, InternalState)
askState = ask >>= readIORef

askAliases :: PureM Aliases
askAliases = fst <$> askState

askInternalState :: PureM InternalState
askInternalState = snd <$> askState
