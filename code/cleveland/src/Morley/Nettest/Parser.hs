-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
module Morley.Nettest.Parser
  ( NettestConfig (..)
  , NettestEnv (..)
  , mkNettestEnv
  , nettestConfigParser

  -- * Lens
  , neMorleyClientEnvL
  , neSecretKeyL
  ) where

import qualified Options.Applicative as Opt
import Util.Lens (makeLensesWith, postfixLFields)

import Morley.CLI
import Morley.Client
import Tezos.Crypto as Crypto
import Util.Named

{-# DEPRECATED NettestConfig "This is being deprecated in favour of 'Morley.Nettest.Tasty'." #-}
data NettestConfig = NettestConfig
  { ncMorleyClientConfig :: MorleyClientConfig
  , ncSecretKey :: Maybe Crypto.SecretKey
  }

data NettestEnv = NettestEnv
  { neMorleyClientEnv :: MorleyClientEnv
  , neSecretKey :: Maybe Crypto.SecretKey
  }

makeLensesWith postfixLFields ''NettestEnv

{-# DEPRECATED nettestConfigParser "This is being deprecated in favour of 'Morley.Nettest.Tasty'." #-}
nettestConfigParser :: (Opt.Parser (Maybe Text)) -> Opt.Parser NettestConfig
nettestConfigParser prefixParser = do
    ncMorleyClientConfig <- clientConfigParser prefixParser
    ncSecretKey <- optional
      (secretKeyOption Nothing (#name .! "import-secret-key")
        (#help .!  ("Saves input secret key with a `nettest` alias. This key" <>
          " will be used when performing nettest actions")))
    pure NettestConfig {..}

{-# DEPRECATED mkNettestEnv "This is being deprecated in favour of 'Morley.Nettest.Tasty'." #-}
mkNettestEnv :: MonadIO m => NettestConfig -> m NettestEnv
mkNettestEnv NettestConfig {..} = do
  neMorleyClientEnv <- liftIO $ mkMorleyClientEnv ncMorleyClientConfig
  let neSecretKey = ncSecretKey
  return NettestEnv {..}
