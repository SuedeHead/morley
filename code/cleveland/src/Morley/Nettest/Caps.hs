-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE InstanceSigs #-}

-- | @caps@-based interface for nettest.
module Morley.Nettest.Caps
  ( MonadNettest
  , MonadEmulated
  , NettestT
  , EmulatedT
  , runIO
  , resolveAddress
  , resolveNettestAddress
  , getAlias
  , newAddress
  , newFreshAddress
  , signBytes
  , signBinary
  , originate
  , originateSimple
  , originateUntyped
  , originateUntypedSimple
  , transfer
  , transferBatch
  , call
  , callBatch
  , callFrom
  , callBatchFrom
  , importUntypedContract
  , comment
  , expectFailure
  , expectCustomError
  , expectCustomError_
  , getBalance
  , checkBalance
  , getStorage
  , getStorageExpr
  , getMorleyLogs
  , checkStorage
  , checkStorage'
  , getPublicKey
  , getChainId
  , advanceTime
  , getNow
  , branchout
  , offshoot

  -- * Assertions
  , failure
  , assert
  , (@==)
  , (@/=)
  , checkCompares
  , checkComparesWith

  -- * Capabilities
  , uncapsNettest
  , uncapsNettestEmulated
  , nettestCapImpl
  , emulatedCapImpl

  -- * Internal helpers
  , ActionToCaps(..)
  ) where

import Fmt (Buildable, Builder, build, pretty, unlinesF)
import qualified Monad.Capabilities as Caps
import Time (KnownDivRat, Second, Time)

import Lorentz
  (CustomError(..), ErrorArg, HasEntrypointArg, IsError, IsoValue, Label, TAddress, ToTAddress)
import Lorentz.Bytes
import Lorentz.Constraints
import Lorentz.Run (Contract)
import Michelson.Interpret (MorleyLogs)
import Michelson.Typed (StorageScope, UnpackedValScope)
import qualified Michelson.Typed as T
import qualified Michelson.Untyped as U
import Morley.Client (Alias)
import Morley.Micheline (Expression)
import Morley.Nettest.Abstract
import Tezos.Address (Address)
import Tezos.Core (ChainId, Mutez, Timestamp)
import Tezos.Crypto (PublicKey, Signature)

-- | Constraint for a monad in which we can do nettest actions.
-- It requires the 'NettestImpl' capability.
type MonadNettest caps base m =
  (Monad base, m ~ Caps.CapsT caps base, Caps.HasCap NettestImpl caps)

-- | Constraint for a monad in which we can do nettest actions that can't be run on a real network.
-- It requires the 'EmulatedImpl' capability.
type MonadEmulated caps base m =
  (MonadNettest caps base m, Caps.HasCap EmulatedImpl caps)

-- | Monad transformer that adds only 'NettestImpl' capability.
type NettestT m = Caps.CapsT '[ NettestImpl] m

-- | Monad transformer that adds both 'NettestImpl' and 'EmulatedImpl' capabilities.
type EmulatedT m = Caps.CapsT '[ NettestImpl, EmulatedImpl ] m

-- | 'niRunIO' adapted to @caps@
runIO :: (HasCallStack, MonadNettest caps base m) => IO res -> m res
runIO = withFrozenCallStack $ actionToCaps niRunIO

-- | 'niResolveAddress' adapted to @caps@.
resolveAddress :: (HasCallStack, MonadNettest caps base m) => AddressOrAlias -> m Address
resolveAddress = withFrozenCallStack $ actionToCaps niResolveAddress

getAlias :: (HasCallStack, MonadNettest caps base m) => AddressOrAlias -> m Alias
getAlias = withFrozenCallStack $ actionToCaps niGetAlias

-- | 'niResolveNettestAddress' adapted to @caps@.
resolveNettestAddress :: (HasCallStack, MonadNettest caps base m) => m Address
resolveNettestAddress = withFrozenCallStack $ actionToCaps niResolveNettestAddress

-- | 'niNewAddress' adapted to @caps@.
newAddress :: (HasCallStack, MonadNettest caps base m) => AliasHint -> m Address
newAddress = withFrozenCallStack $ actionToCaps niNewAddress

-- | Generate a new secret key and record it with given alias. If the
-- alias is already known, the key will be overwritten. The address is
-- guaranteed to be fresh, i. e. no operations on it have been made.
newFreshAddress :: (HasCallStack, MonadNettest caps base m) => AliasHint -> m Address
newFreshAddress = withFrozenCallStack $ actionToCaps niGenFreshKey

-- | 'niSignBytes' adapted to @caps@.
signBytes :: (HasCallStack, MonadNettest caps base m) => ByteString -> Alias -> m Signature
signBytes = withFrozenCallStack $ actionToCaps niSignBytes

-- | Type-safer version of 'signBytes'.
signBinary :: (HasCallStack, BytesLike bs, MonadNettest caps base m) => bs -> Alias -> m (TSignature bs)
signBinary bs alias = withFrozenCallStack $ TSignature <$> signBytes (toBytes bs) alias

-- | 'niOriginateUntyped' adapted to @caps@
originateUntyped :: (HasCallStack, MonadNettest caps base m) => UntypedOriginateData -> m Address
originateUntyped = withFrozenCallStack $ actionToCaps niOriginateUntyped

-- | 'niOriginateUntypedSimple' adapted to @caps@
originateUntypedSimple
  :: (HasCallStack, MonadNettest caps base m) => AliasHint -> U.Value -> U.Contract -> m Address
originateUntypedSimple = withFrozenCallStack $ actionToCaps niOriginateUntypedSimple

-- | 'niOriginate' adapted to @caps@.
originate :: (HasCallStack, MonadNettest caps base m) => OriginateData param -> m (TAddress param)
originate = withFrozenCallStack $ actionToCaps niOriginate

-- | 'niOriginateSimple' adapted to @caps@.
originateSimple
  :: ( HasCallStack
     , MonadNettest caps base m
     , NiceParameterFull param, NiceStorage st
     )
  => AliasHint
  -> st
  -> Contract param st
  -> m (TAddress param)
originateSimple = withFrozenCallStack $ actionToCaps niOriginateSimple

-- | 'niTransfer' adapted to @caps@.
transfer :: (HasCallStack, MonadNettest caps base m) => TransferData -> m ()
transfer = withFrozenCallStack $ actionToCaps niTransfer

-- | 'niTransferBatch' adapted to @caps@.
transferBatch :: (HasCallStack, MonadNettest caps base m) => AddressOrAlias -> [TransferData] -> m ()
transferBatch = withFrozenCallStack $ actionToCaps niTransferBatch

-- | Call a certain entrypoint of a contract referred to by some
-- typed address. The sender is 'nettestAddress', so it should have sufficient
-- XTZ to pay tx fee.
call
  :: forall v addr m epRef epArg caps base.
     (HasCallStack, MonadNettest caps base m, ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => addr
  -> epRef
  -> epArg
  -> m ()
call = withFrozenCallStack $ callFrom @v nettestAddress

-- | For each entrypoint in list, calls the entrypoint of a contract referred to by the second
-- typed address from the 'nettestAddress', so it should have sufficient
-- XTZ to pay tx fee.
callBatch :: (HasCallStack, MonadNettest caps base m) => [CallData] -> m ()
callBatch cfData = withFrozenCallStack $ callBatchFrom nettestAddress cfData

-- | Call a certain entrypoint of a contract referred to by the second
-- typed address from the first address.
callFrom
  :: forall v addr m epRef epArg caps base.
     (HasCallStack, MonadNettest caps base m, ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => AddressOrAlias
  -> addr
  -> epRef
  -> epArg
  -> m ()
callFrom = withFrozenCallStack $ actionToCaps (niCallFrom @v)

-- | 'niImportUntypedContract' adapted to @caps@.
importUntypedContract :: (HasCallStack, MonadNettest caps base m) => FilePath -> m U.Contract
importUntypedContract = withFrozenCallStack $ actionToCaps niImportUntypedContract

-- | 'niCallBatch' adapted to @caps@.
callBatchFrom :: (HasCallStack, MonadNettest caps base m) => AddressOrAlias -> [CallData] -> m ()
callBatchFrom = withFrozenCallStack $ actionToCaps niCallBatchFrom

-- | 'niComment' adapted to @caps@.
comment :: (HasCallStack, MonadNettest caps base m) => Text -> m ()
comment = withFrozenCallStack $ actionToCaps niComment

-- | 'niExpectFailure' adapted to @caps@.
expectFailure :: (HasCallStack, MonadNettest caps base m) => m a -> NettestFailure -> m ()
expectFailure = withFrozenCallStack $ actionToCaps niExpectFailure

-- | Version of 'expectFailure' specialized for expecting @CustomError@s.
expectCustomError
  :: (HasCallStack, MonadNettest caps base m, IsError (CustomError tag), ErrorArg tag ~ arg, Eq arg)
  => Label tag -> arg -> m a -> m ()
expectCustomError tag arg act =
  withFrozenCallStack $
    expectFailure act $ NettestFailedWithError $ CustomError tag arg

-- | Version of 'expectCustomError' for error with @unit@ argument.
expectCustomError_
  :: (HasCallStack, MonadNettest caps base m, IsError (CustomError tag), ErrorArg tag ~ ())
  => Label tag -> m a -> m ()
expectCustomError_ tag = withFrozenCallStack $ expectCustomError tag ()

-- | 'niGetBalance' adapted to @caps@.
getBalance :: (HasCallStack, MonadNettest caps base m) => AddressOrAlias -> m Mutez
getBalance = withFrozenCallStack $ actionToCaps niGetBalance

-- | 'niCheckBalance' adapted to @caps@.
checkBalance :: (HasCallStack, MonadNettest caps base m) => AddressOrAlias -> Mutez -> m ()
checkBalance = withFrozenCallStack $ actionToCaps niCheckBalance

-- | 'niGetStorage' adapted to @caps@
getStorage
  :: forall t caps base m. (HasCallStack, MonadNettest caps base m, UnpackedValScope t, Typeable t)
  => AddressOrAlias
  -> m (T.Value t)
getStorage = withFrozenCallStack $ actionToCaps niGetStorage

-- | 'niGetStorageExpr' adapted to @caps@
getStorageExpr
  :: forall caps base m. (HasCallStack, MonadNettest caps base m)
  => AddressOrAlias
  -> m Expression
getStorageExpr = withFrozenCallStack $ actionToCaps niGetStorageExpr

-- | 'niCheckStorage' adapted to @caps@
checkStorage
  :: forall t caps base m. (HasCallStack, MonadNettest caps base m, UnpackedValScope t, StorageScope t)
  => AddressOrAlias
  -> T.Value t
  -> m ()
checkStorage = withFrozenCallStack $ actionToCaps niCheckStorage

-- | Get the storage for given address or alias and check whether it's equal to given one.
--
-- Unlike 'checkStorage', this function does not have the 'UnpackedValScope' constraint,
-- and thus is able to retrieve storages that contain @big_map@s.
checkStorage'
  :: forall t caps base m. (HasCallStack, MonadEmulated caps base m, StorageScope t)
  => AddressOrAlias
  -> T.Value t
  -> m ()
checkStorage' = withFrozenCallStack $ actionToCaps eiCheckStorage

-- | 'niGetPublicKey' adapted to @caps@.
getPublicKey :: (HasCallStack, MonadNettest caps base m) => AddressOrAlias -> m PublicKey
getPublicKey = withFrozenCallStack $ actionToCaps niGetPublicKey

-- | 'niGetChainId' adapted to @caps@.
getChainId :: (HasCallStack, MonadNettest caps base m) => m ChainId
getChainId = withFrozenCallStack $ actionToCaps niGetChainId

-- | Advance at least the given amount of time, or until a new block is baked,
-- whichever happens last.
--
-- On a real network, this is implemented using 'threadDelay', so it's advisable
-- to use small amounts of time only.
advanceTime
  :: forall unit caps base m
  . (HasCallStack, MonadNettest caps base m, KnownDivRat unit Second)
  => Time unit -> m ()
advanceTime = withFrozenCallStack $ actionToCaps niAdvanceTime

-- | Get the timestamp observed by the last block to be baked.
getNow :: (HasCallStack, MonadNettest caps base m) => m Timestamp
getNow = withFrozenCallStack $ actionToCaps niGetNow

-- | Execute multiple testing scenarios independently, basing
-- them on scenario built till this point.
--
-- The following property holds for this function:
--
-- @ pre >> branchout [a, b, c] = branchout [pre >> a, pre >> b, pre >> c] @.
--
-- In case of property failure in one of the branches no following branch is
-- executed.
--
-- Providing empty list of scenarios to this function causes error;
-- we do not require 'NonEmpty' here though for convenience.
branchout :: (MonadEmulated caps base m) => [(Text, m ())] -> m ()
branchout = actionToCaps eiBranchout

-- | Test given scenario with the state gathered till this moment;
-- if this scenario passes, go on as if it never happened.
offshoot :: MonadEmulated caps base m => Text -> m () -> m ()
offshoot = actionToCaps eiOffshoot

-- | Returns a list of logs of all operations performed so far. Logs are messages of morley instruction
-- @PRINT@ - https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/language/morleyInstructions.md#print
getMorleyLogs :: MonadEmulated caps base m => m [MorleyLogs]
getMorleyLogs = actionToCaps eiGetMorleyLogs

----------------------------------------------------------------------------
-- Assertions
----------------------------------------------------------------------------

-- | Fails the test with the given error message.
failure :: (HasCallStack, MonadNettest caps base m) => Builder -> m a
failure = withFrozenCallStack $ actionToCaps niFailure

-- | Fails the test with the given error message if the given condition is false.
assert :: (HasCallStack, MonadNettest caps base m) => Bool -> Builder -> m ()
assert b errMsg =
  withFrozenCallStack $
    unless b $ failure errMsg

-- | Fails the test if the two given values are not equal.
(@==)
  :: (HasCallStack, MonadNettest caps base m, Eq a, Buildable a)
  => a -> a -> m ()
a @== b =
  withFrozenCallStack $ checkCompares a (==) b

infix 1 @==

-- | Fails the test if the two given values are equal.
(@/=)
  :: (HasCallStack, MonadNettest caps base m, Eq a, Buildable a)
  => a -> a -> m ()
a @/= b =
  withFrozenCallStack $
    assert (a /= b) $
      unlinesF
        [ "The two values are equal:"
        , build a
        ]

infix 1 @/=

-- | Fails the test if the comparison operator fails when applied to the given arguments.
-- Prints an error message with both arguments.
--
-- Example:
--
-- > checkCompares 2 (>) 1
checkCompares
  :: forall a b caps base m
   . (HasCallStack, MonadNettest caps base m, Buildable a, Buildable b)
  => a
  -> (a -> b -> Bool)
  -> b
  -> m ()
checkCompares a f b =
  withFrozenCallStack $ checkComparesWith pretty a f pretty b

-- | Like 'checkCompares', but with an explicit show function.
-- This function does not have any constraint on the type parameters @a@ and @b@.
--
-- For example, to print with 'Fmt.pretty':
--
-- > checkComparesWith pretty a (<) pretty b
checkComparesWith
  :: forall a b caps base m
   . (HasCallStack, MonadNettest caps base m)
  => (a -> Text)
  -> a
  -> (a -> b -> Bool)
  -> (b -> Text)
  -> b
  -> m ()
checkComparesWith showA a f showB b =
  withFrozenCallStack $
    assert (f a b) $
      unlinesF
        [ "Failed"
        , "━━ lhs ━━"
        , showA a
        , "━━ rhs ━━"
        , showB b
        ]

----------------------------------------------------------------------------
-- Capabilities
----------------------------------------------------------------------------

-- | Adapt @caps@-based interface back to argument passing style.
uncapsNettest
  :: forall m a. Monad m
  => NettestT m a
  -> NettestImpl m
  -> m a
uncapsNettest action impl =
  runReaderT action $
  Caps.buildCaps $
    Caps.AddCap (nettestCapImpl impl) $
    Caps.BaseCaps Caps.emptyCaps

nettestCapImpl :: forall m. Monad m => NettestImpl m -> Caps.CapImpl NettestImpl '[] m
nettestCapImpl impl = Caps.CapImpl $ NettestImpl
  { niRunIO = lift . niRunIO impl
  , niResolveAddress = lift . niResolveAddress impl
  , niGetAlias = lift . niGetAlias impl
  , niSignBytes = lift ... niSignBytes impl
  , niGenKey = lift . niGenKey impl
  , niGenFreshKey = lift . niGenFreshKey impl
  , niOriginateUntyped = lift . niOriginateUntyped impl
  , niTransferBatch = lift ... niTransferBatch impl
  , niComment = lift . niComment impl
  , niExpectFailure = \op nettestFailure -> do
      ReaderT $ \ r -> niExpectFailure impl (runReaderT op r) nettestFailure
  , niGetBalance = lift . niGetBalance impl
  , niCheckBalance = lift ... niCheckBalance impl
  , niGetStorage = lift . niGetStorage impl
  , niGetStorageExpr = lift . niGetStorageExpr impl
  , niCheckStorage = lift ... niCheckStorage impl
  , niGetPublicKey = lift . niGetPublicKey impl
  , niGetChainId = lift $ niGetChainId impl
  , niAdvanceTime = lift ... niAdvanceTime impl
  , niGetNow = lift ... niGetNow impl
  , niFailure = lift ... niFailure impl
  }

uncapsNettestEmulated
  :: forall m a. Monad m
  => EmulatedT m a
  -> EmulatedImpl m
  -> NettestImpl m
  -> m a
uncapsNettestEmulated action implEmulated implNettest =
  runReaderT action $
  Caps.buildCaps $
    Caps.AddCap (nettestCapImpl implNettest) $
    Caps.AddCap (emulatedCapImpl implEmulated) $
    Caps.BaseCaps Caps.emptyCaps

emulatedCapImpl :: Monad m => EmulatedImpl m -> Caps.CapImpl EmulatedImpl '[] m
emulatedCapImpl impl = Caps.CapImpl $ EmulatedImpl
  { eiBranchout =
      \(scenarios :: [(Text, Caps.CapsT caps m ())]) ->
        withFrozenCallStack $
          ReaderT $ \caps ->
            let scenarios' :: [(Text, m ())] = second (flip runReaderT caps) <$> scenarios
            in  eiBranchout impl scenarios'
  , eiOffshoot =
      \name (scenario :: Caps.CapsT caps m ()) ->
        ReaderT $ \caps ->
          let scenario' :: m () = runReaderT scenario caps
          in  eiOffshoot impl name scenario'
  , eiCheckStorage = lift ... eiCheckStorage impl
  , eiGetMorleyLogs = lift ... eiGetMorleyLogs impl
  }

----------------------------------------------------------------------------
-- Internal helpers
----------------------------------------------------------------------------

class ActionToCaps impl m x where
  actionToCaps :: (impl m -> x) -> x

instance (Monad base, m ~ Caps.CapsT caps base, Caps.HasCap impl caps, Typeable impl) => ActionToCaps impl m (m a) where
  actionToCaps :: (impl m -> m a) -> m a
  actionToCaps action = do
    impl <- asks Caps.getCap
    action impl

instance ActionToCaps impl m r => ActionToCaps impl m (x -> r) where
  actionToCaps :: (impl m -> (x -> r)) -> (x -> r)
  actionToCaps action x = actionToCaps (flip action x)
