-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Unit tests for Lorentz.
module Lorentz.Test.Unit
    ( expectContractEntrypoints
    ) where

import Test.HUnit (Assertion, assertFailure)

import Lorentz hiding (contract)
import Michelson.Test.Unit (matchContractEntrypoints)
import Michelson.Typed (convertContract, flattenEntrypoints)

-- | Expect the given contract to have some specific entrypoints.
expectContractEntrypoints
  :: forall expectedEps contractEps st.
     ( NiceParameterFull expectedEps
     , NiceParameterFull contractEps
     , NiceStorage st
     )
  => Contract contractEps st -> Assertion
expectContractEntrypoints contract =
  withDict (niceParameterEvi @expectedEps) $
  withDict (niceParameterEvi @contractEps) $
  withDict (niceStorageEvi @st) $ do
    let entrypoints = flattenEntrypoints $ parameterEntrypointsToNotes @expectedEps
        contract' = convertContract . compileLorentzContract $ contract
    case matchContractEntrypoints contract' entrypoints of
      Left eps -> assertFailure $ "Some entrypoints were not found " <> show eps
      Right _ -> pass
