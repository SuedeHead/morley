-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Contract which remembers all parameters it has been called with.
--
-- Useful to save return values of @View@ entry points.
module Lorentz.Test.Consumer
  ( contractConsumer
  ) where

import Lorentz.Base
import Lorentz.Constraints
import Lorentz.Instr
import Lorentz.Macro
import Lorentz.Run

-- | Remembers parameters it was called with, last goes first.
contractConsumer :: NiceParameterFull cp => Contract cp [cp]
contractConsumer = defaultContract $
  unpair # cons # nil # pair
