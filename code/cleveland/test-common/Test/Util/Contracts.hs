-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Utility functions to read sample contracts (for testing).

module Test.Util.Contracts
  ( contractsDir
  , inContractsDir
  , (</>)

  , getIllTypedContracts
  , getWellTypedContracts
  , getUnparsableContracts
  , getWellTypedMichelsonContracts
  , getWellTypedMorleyContracts
  , getContractsWithReferences
  ) where

import Data.List (isSuffixOf)
import System.Directory (listDirectory)
import System.FilePath (addExtension, (</>))

-- | Directory with sample contracts.
contractsDir :: FilePath
contractsDir = "../../contracts/"

inContractsDir :: FilePath -> FilePath
inContractsDir = (contractsDir </>)

getIllTypedContracts :: IO [FilePath]
getIllTypedContracts = do
  illTyped <- concatMapM (\ext ->
                            concatMapM (getContractsWithExtension ext)
                            illTypedContractDirs
                         ) [".tz", ".mtz"]
  unparsable <- getUnparsableContracts
  return $ filter (not . (flip elem unparsable)) illTyped

getWellTypedContracts :: IO [FilePath]
getWellTypedContracts = getWellTypedMichelsonContracts <> getWellTypedMorleyContracts

getUnparsableContracts :: IO [FilePath]
getUnparsableContracts = do
  unparsable <- concatMapM (flip getContractsWithExtension (contractsDir </> "unparsable"))
    [".tz", ".mtz"]
  return $ unparsable ++ unparsableExample


getWellTypedMichelsonContracts :: IO [FilePath]
getWellTypedMichelsonContracts =
  concatMapM (getContractsWithExtension ".tz") wellTypedContractDirs

getWellTypedMorleyContracts :: IO [FilePath]
getWellTypedMorleyContracts = concatMapM (getContractsWithExtension ".mtz") wellTypedContractDirs

getContractsWithExtension :: String -> FilePath -> IO [FilePath]
getContractsWithExtension ext dir = mapMaybe convertPath <$> listDirectory dir
  where
    convertPath :: FilePath -> Maybe FilePath
    convertPath fileName
      | (ext `isSuffixOf` fileName) =
        Just (dir </> fileName)
      | otherwise = Nothing

wellTypedContractDirs :: [FilePath]
wellTypedContractDirs =
  contractsDir :
  map ((contractsDir </> "tezos_examples") </>)
    [ "attic"
    , "entrypoints"
    , "macros"
    , "mini_scenarios"
    , "non_regression"
    , "opcodes"
    ]

illTypedContractDirs :: [FilePath]
illTypedContractDirs =
  [ contractsDir </> "ill-typed"
  , contractsDir </> "tezos_examples" </> "ill_typed"
  ]

unparsableExample :: [FilePath]
unparsableExample =
  [ contractsDir </> "tezos_examples/ill_typed/big_map_arity.tz"
  ]

getContractsWithReferences :: String -> FilePath -> String -> IO [(FilePath, FilePath)]
getContractsWithReferences ext fp refExt =
  fmap attachPrettyPath <$> getContractsWithExtension ext fp
  where
    attachPrettyPath :: FilePath -> (FilePath, FilePath)
    attachPrettyPath src = (src, addExtension src  refExt)
