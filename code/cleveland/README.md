# Cleveland: A testing framework for Morley

This package provides an eDSL for testing contracts written in Michelson, Morley or Lorentz.

These tests can be run:
* on an emulated environment using the `Michelson.Test` or `Lorentz.Test` modules
  (this interface is documented in detail [here](https://gitlab.com/morley-framework/morley/-/blob/master/code/cleveland/testingEDSL.md))
* on a real network (e.g. testnet) using the `Morley.Nettest` module.
  In addition to testnet once can also use [local-chain](https://gitlab.com/morley-framework/local-chain)
  with short block periods in order to speed up testing process.

We also provide several Hedgehog generators for most of `morley`'s data types
in the `Hedgehog.Gen.*` modules.
