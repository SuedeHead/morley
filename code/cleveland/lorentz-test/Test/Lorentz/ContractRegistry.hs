-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- {-# LANGUAGE NoApplicativeDo, RebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

-- | Tests (and examples) on Lorentz' @if .. then .. else ..@.
module Test.Lorentz.ContractRegistry
  ( unit_contractRegistryCommands
  ) where

import Prelude hiding (drop, try)

import Control.Exception
import qualified Data.Map as Map
import Test.HUnit (Assertion)

import Lorentz
import Lorentz.ContractRegistry

instance Exception Text

sampleContract :: Contract () ()
sampleContract = defaultContract $
  drop # unit # nil # pair

sampleContractInfo :: ContractInfo
sampleContractInfo = ContractInfo
  { ciContract = sampleContract
  , ciIsDocumented = False
  , ciStorageParser = Just (pure ())
  , ciStorageNotes = Nothing
  }

singleContractReg :: ContractRegistry
singleContractReg = ContractRegistry $ Map.fromList
  [ "Sample" ?:: sampleContractInfo
  ]

multiContractReg :: ContractRegistry
multiContractReg = ContractRegistry $ Map.fromList
  [ "Sample" ?:: sampleContractInfo
  , "Sample2" ?:: sampleContractInfo
  ]

unit_contractRegistryCommands :: Assertion
unit_contractRegistryCommands = do

  runContractRegistry singleContractReg (Print Nothing Nothing False False)

  runContractRegistry multiContractReg (Print (Just "Sample2") Nothing False False)

  -- | Fail due to not specifying the name with a contract registry that contains multiple contracts.
  e <- try $ runContractRegistry multiContractReg (Print Nothing Nothing False False)
  case e of
    Right _ -> throwIO ("Expect the test to fail." :: Text)
    Left (_ :: SomeException) -> pure ()
