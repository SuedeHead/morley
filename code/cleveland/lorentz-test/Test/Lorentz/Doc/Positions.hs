-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on ordering of documentation items.
module Test.Lorentz.Doc.Positions
  ( test_Errors
  ) where

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Cleveland.Util (assertGoesBefore)
import Lorentz.Errors
import Lorentz.Errors.Numeric

test_Errors :: [TestTree]
test_Errors =
  [ testCase "Error tag mapping is described before errors" $
      -- This is required because when @Errors@ section is modified
      -- to mention numeric error tags, it needs some clarification
      -- provided by @About error tag mapping@ section.
      Proxy @DDescribeErrorTagMap `assertGoesBefore` Proxy @DError
  ]
