-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on Lorentz storage annotation parsing.

module Test.Lorentz.Storage
  ( test_FieldAnnotations
  , test_TypeAnnotations
  ) where

import Test.HUnit ((@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)
import Test.Util.Annotation

import Lorentz ((:!))
import qualified Lorentz as L
import Lorentz.Annotation
import Lorentz.Run hiding (Contract(..))
import Lorentz.Value
import Michelson.Typed (Contract(..))
import Michelson.Untyped.Annotation

----------------------------------------------------------------------------
-- Storage declarations
----------------------------------------------------------------------------

data MyParams = MyParams
  { p1 :: Natural
  , p2 :: Address
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data UnitStorage = UnitStorage
  { unit :: ()
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data MyStorage1 = MyStorage1
  { st1 :: ()
  , st2 :: ()
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data MyStorage2 = MyStorage2
  { st3 :: Maybe Address
  , st4 :: (Natural, Natural)
  , st5 :: MyParams
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data MyStorage2r = MyStorage2r
  { st3r :: Maybe Address
  , st4r :: (Natural, Natural)
  , st5r :: MyParams
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data MyStorage3 = MyStorage3
  { st6 :: ("tuplearg" :! ("TL" :! Address, "TR" :! Integer), "boolarg" :! Bool)
  , st7 :: ("integerarg" :! Natural, "boolarg" :! Bool)
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data MyStorage4 = MyStorage4
  { st8 :: ("bigmaparg" :! L.Lambda (BigMap Natural ("balance" :! Natural , "address" :! L.Address)) ())
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data MyStorage5 = MyStorage5
  { st9 :: ("maybearg" :! Maybe ("maybeinner" :! Natural))
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data MyStorage6 = MyStorage6
  { st10 :: ("lambdaarg" :! L.Lambda Natural Natural)
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

data MyStorage7 = MyStorage7
  { st11 :: ("listarg" :! [("balance" :! Natural , "address" :! L.Address)])
  } deriving stock Generic
    deriving anyclass (IsoValue, HasAnnotation)

dummyContract :: forall storage. L.Contract () storage
dummyContract = defaultContract L.fakeCoerce

----------------------------------------------------------------------------
-- Field annotations
----------------------------------------------------------------------------

test_FieldAnnotations :: [TestTree]
test_FieldAnnotations =
  [
    testCase "Simple storage" $ do
      extractAnnotation @MyStorage1
      @?=
      FANodePair
        (ann "st1") FALeaf
        (ann "st2") FALeaf

 , testCase "Complex storage" $ do
      extractAnnotation @MyStorage2
      @?=
        FANodePair
          (ann "st3")
            FALeaf
          noAnn
            (FANodePair
              (ann "st4") FALeaf
              (ann "st5")
                (FANodePair
                  (ann "p1") FALeaf
                  (ann "p2") FALeaf))

  , testCase "Complex parameter recursive" $ do
      extractAnnotation @MyStorage2r
      @?=
        FANodePair
          (ann "st3r")
            FALeaf
          noAnn
            (FANodePair
              (ann "st4r")
                FALeaf
              (ann "st5r")
                (FANodePair
                  (ann "p1") FALeaf
                  (ann "p2") FALeaf))
  ]
  where
    storageAnnTree :: Contract cp st -> FieldAnnTree st
    storageAnnTree = extractFieldAnnTree . cStoreNotes

    extractAnnotation :: forall st. L.NiceStorage st => FieldAnnTree (ToT st)
    extractAnnotation = storageAnnTree $ compileLorentzContract (dummyContract @st)

test_TypeAnnotations :: [TestTree]
test_TypeAnnotations =
  [ testCase "Address primitive storage with no annotations" $
      extractAnnotation @Address
      @?=
      TALeaf noAnn

  , testCase "Named type annotation" $
      extractAnnotation @MyStorage3
      @?=
      TANodePair noAnn
        (TANodePair noAnn
          (TANodePair (ann "tuplearg")
            (TALeaf (ann "TL"))
            (TALeaf (ann "TR")))
          (TALeaf (ann "boolarg")))
        (TANodePair noAnn
          (TALeaf (ann "integerarg"))
          (TALeaf (ann "boolarg")))

  , testCase "BigMap type annotation" $
      extractAnnotation @MyStorage4
      @?=
      TANodeLambda (ann "bigmaparg")
        (TANodeBigMap noAnn
          (TALeaf noAnn)
          (TANodePair noAnn
            (TALeaf (ann "balance"))
            (TALeaf (ann "address"))))
        (TALeaf noAnn)

  , testCase "Maybe type annotation" $
      extractAnnotation @MyStorage5
      @?=
      TANodeOption (ann "maybearg") (TALeaf (ann "maybeinner"))

  , testCase "Lambda type annotation" $
      extractAnnotation @MyStorage6
      @?=
      TANodeLambda (ann "lambdaarg") (TALeaf noAnn) (TALeaf noAnn)

  , testCase "List type annotation" $
      extractAnnotation @MyStorage7
      @?=
      TANodeList (ann "listarg")
        (TANodePair noAnn
          (TALeaf (ann "balance"))
          (TALeaf (ann "address")))

  ]
  where
    storageAnnTree :: Contract cp st -> TypeAnnTree st
    storageAnnTree = extractTypeAnnTree . cStoreNotes

    extractAnnotation :: forall st. L.NiceStorage st => TypeAnnTree (ToT st)
    extractAnnotation = storageAnnTree $ compileLorentzContract (dummyContract @st)
