-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for checking MorleyLogs processing.
module Test.Lorentz.MorleyLogs
  ( test_MorleyLogs
  ) where

import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Lorentz
import Lorentz.Test.Integrational
import Michelson.Test (originate, testTreesWithUntypedContract)
import Michelson.Untyped.Value (Value'(..))

test_MorleyLogs :: IO [TestTree]
test_MorleyLogs =
  testTreesWithUntypedContract "../../contracts/empties.tz" $ \withoutLogs ->
  testTreesWithUntypedContract "../../contracts/single_log.mtz" $ \withSingleLog ->
  testTreesWithUntypedContract "../../contracts/multiple_logs.mtz" $ \withMultiLogs ->
  pure
  [ testGroup "Checking MorleyLogs processing"
    [ testCase "Calling contract with single log" $
        integrationalTestExpectation $ do
          idAddr <- originateS withSingleLog
          lCallDef idAddr ()
          lCallDef idAddr ()
          lExpectScenarioLogs $ (==) [["log"], ["log"]]

    , testCase "Calling several contracts with and without logs" $
        integrationalTestExpectation $ do
          idAddrW <- originateW withoutLogs
          idAddrS <- originateS withSingleLog
          idAddrM <- originateM withMultiLogs
          lCallDef idAddrW ()
          lCallDef idAddrS ()
          lCallDef idAddrM ()
          lExpectAddressLogs idAddrW $ (==) [[]]
          lExpectScenarioLogs $ (==) [[], ["log"], ["log1", "log2", "log3"]]

    , testCase "Calling several contracts to check the logging order" $
        integrationalTestExpectation $ do
          idAddrS <- originateS withSingleLog
          idAddrM <- originateM withMultiLogs
          lCallDef idAddrS ()
          lCallDef idAddrM ()
          lCallDef idAddrS ()
          lExpectAddressLogs idAddrM (["log1", "log2", "log3"] `elem`)
          lExpectScenarioLogs $ (==) [["log"], ["log1", "log2", "log3"], ["log"]]

    , testCase "Calling contracts in parallel with branchout" $
        integrationalTestExpectation $ branchout
          [ ("1", do
            idAddr <- originateS withSingleLog
            lCallDef idAddr ()
            lExpectAddressLogs idAddr $ (["log"] `elem`))
          , ("2", do
            idAddr <- originateS withSingleLog
            lCallDef idAddr ()
            lExpectAddressLogs idAddr $ (==) [["log"]])
          ]
      ]
  ]
  where
    originateContract name c = TAddress @() <$> originate c name ValueUnit (toMutez 100)
    originateW = originateContract "without logs"
    originateS = originateContract "with single log"
    originateM = originateContract "with multiple logs"

