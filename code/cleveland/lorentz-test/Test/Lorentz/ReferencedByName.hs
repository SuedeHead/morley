-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for 'Lorentz.NameReferenced' module.
--
-- They test logic of macros in that module and type-level logic,
-- and serve as examples of their use.

module Test.Lorentz.ReferencedByName
  ( test_dupL
  ) where

import Prelude hiding (drop, swap)
import Test.HUnit (Assertion, (@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Lorentz
import Michelson.Typed

-- | Compare instructions on their untyped representation.
--
-- Necessary to exclude @Nop@s.
(@?~=) :: a :-> b -> a :-> b -> Assertion
(@?~=) = (@?=) `on` (instrToOps . iAnyCode)
infix 1 @?~=

test_dupL :: [TestTree]
test_dupL =
  [ testCase "Duplicating third element" $
      stackType @[(), "a" :! Integer, "b" :! Natural] # dupL #b
        @?~= duupX @3 # fromNamed #b
  ]
