-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for optimization of Lorentz-specific instruction sequences.
module Test.Lorentz.Optimizer
  ( unit_eq0
  , unit_non
  ) where

import Lorentz ((:->), ( # ))
import qualified Lorentz as L
import Michelson.Optimizer (optimize)
import Michelson.Typed.Util (linearizeLeft)
import Test.HUnit (Assertion, (@?=))

-- | Check whether code on left hand side optimizes to the code on
-- the right hand side.
infix 1 ->?=
(->?=) :: i :-> o -> i :-> o -> Assertion
(L.iAnyCode -> pre) ->?= (L.iAnyCode -> post) =
  linearizeLeft (optimize pre) @?= linearizeLeft post

unit_eq0 :: Assertion
unit_eq0 =
  L.push @Integer 0 # L.eq ->?= L.eq0

unit_non :: Assertion
unit_non =
  L.non @Integer 0 ->?= L.nonZero
