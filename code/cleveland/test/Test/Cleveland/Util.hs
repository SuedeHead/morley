-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Cleveland.Util
  ( test_Time
  ) where

import Data.Fixed (Deci, E0, E1, E12, E3, Milli, Pico, Uni)
import Data.Time (secondsToNominalDiffTime)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))
import Time (ms, sec)

import Cleveland.Util (ceilingUnit, timeToFixed, timeToNominalDiffTime)

test_Time :: TestTree
test_Time =
  testGroup "time-related functions"
    [ testGroup "timeToFixed converts to fixed precision" $
      [ testCase "E0"  $ timeToFixed @E0  (sec 1.234) @?= (1     :: Uni  )
      , testCase "E1"  $ timeToFixed @E1  (sec 1.234) @?= (1.2   :: Deci )
      , testCase "E3"  $ timeToFixed @E3  (sec 1.234) @?= (1.234 :: Milli)
      , testCase "E12" $ timeToFixed @E12 (sec 1.234) @?= (1.234 :: Pico )
      ]
    , testGroup "ceilingUnit rounds up" $
      [ testCase "sec 2.0" $ ceilingUnit (sec 2.0) @?= sec 2
      , testCase "sec 2.1" $ ceilingUnit (sec 2.1) @?= sec 3
      , testCase "sec 2.4" $ ceilingUnit (sec 2.4) @?= sec 3
      , testCase "sec 2.5" $ ceilingUnit (sec 2.5) @?= sec 3
      , testCase "sec 2.9" $ ceilingUnit (sec 2.9) @?= sec 3
      ]
    , testGroup "timeToNominalDiffTime converts to NominalDiffTime" $
      [ testCase "sec 1"   $ timeToNominalDiffTime (sec 1  ) @?= secondsToNominalDiffTime 1
      , testCase "sec 0.1" $ timeToNominalDiffTime (sec 0.1) @?= secondsToNominalDiffTime 0.1
      , testCase "sec 0.9" $ timeToNominalDiffTime (sec 0.9) @?= secondsToNominalDiffTime 0.9
      , testCase "ms 1.2"  $ timeToNominalDiffTime (ms  1.2) @?= secondsToNominalDiffTime 0.0012
      ]
    ]
