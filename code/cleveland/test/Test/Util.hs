-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- Utility functions used in the @cleveland:test:cleveland-test@ test suite.
module Test.Util
  ( runViaTasty
  , runViaTastyOnEmulator
  , runViaTastyOnNetwork
  , outcomeIsFailure
  ) where

import Test.Tasty (testGroup)
import Test.Tasty.HUnit (Assertion, assertFailure, testCase)
import Test.Tasty.Options (OptionSet, singleOption)
import Test.Tasty.Providers (IsTest(run), TestName)
import Test.Tasty.Runners
  (FailureReason(TestFailed), Outcome(Failure, Success), Result(resultOutcome),
  TestTree(AskOptions))

import Morley.Nettest (EmulatedT, MonadNettest, NettestT, uncapsNettest, uncapsNettestEmulated)
import Morley.Nettest.Pure (PureM)
import Morley.Nettest.Tasty
  (RunOnEmulator(RunOnEmulator), RunOnNetwork(RunOnNetwork), whenNetworkEnabled)
import Morley.Nettest.Tasty.Options (ContextLinesOpt(ContextLinesOpt))

-- | Run a Nettest via Tasty, and performs some checks on the Tasty result.
--
-- It uses the Tasty options taken from the environment (CLI and environment variables).
-- It also sets 'ContextLinesOpt' to 0, otherwise the test report may capture
-- source code lines not actually related to the test.
--
-- These options can be overriden by passing in an additional 'OptionSet'.
runViaTasty
  :: TestName
  -> OptionSet
  -> (forall caps base m. MonadNettest caps base m => m ())
  -> (Result -> Assertion)
  -> TestTree
runViaTasty testName options nettestTest checkResult =
  testGroup testName
    [ runViaTastyOnEmulator "On emulator" options nettestTest checkResult
    , runViaTastyOnNetwork "On network" options nettestTest checkResult
    ]

-- | Run a Nettest via Tasty, and performs some checks on the Tasty result.
--
-- It uses the Tasty options taken from the environment (CLI and environment variables).
-- It also sets 'ContextLinesOpt' to 0, otherwise the test report may capture
-- source code lines not actually related to the test.
--
-- These options can be overriden by passing in an additional 'OptionSet'.
runViaTastyOnEmulator :: TestName -> OptionSet -> EmulatedT PureM () -> (Result -> Assertion) -> TestTree
runViaTastyOnEmulator testName options nettestTest checkResult =
  AskOptions $ \envOptions ->
    testCase testName $ do
      let allOptions = envOptions <> singleOption (ContextLinesOpt 0) <> options
      let tastyTest = RunOnEmulator $ uncapsNettestEmulated (void nettestTest)
      tastyResult <- run allOptions tastyTest (\_ -> pure ())
      checkResult tastyResult

-- | Run a Nettest via Tasty, and performs some checks on the Tasty result.
--
-- It uses the Tasty options taken from the environment (CLI and environment variables).
-- It also sets 'ContextLinesOpt' to 0, otherwise the test report may capture
-- source code lines not actually related to the test.
--
-- These options can be overriden by passing in an additional 'OptionSet'.
runViaTastyOnNetwork :: TestName -> OptionSet -> NettestT IO () -> (Result -> Assertion) -> TestTree
runViaTastyOnNetwork testName options nettestTest checkResult =
  whenNetworkEnabled $ \_ ->
    AskOptions $ \envOptions ->
      testCase testName $ do
        let allOptions = envOptions <> singleOption (ContextLinesOpt 0) <> options
        let tastyTest = RunOnNetwork $ uncapsNettest (void nettestTest)
        tastyResult <- run allOptions tastyTest (\_ -> pure ())
        checkResult tastyResult

-- | Checks that a Tasty test failed because the nested Nettest test failed
-- (and not because, for example, the test framework threw an exception).
outcomeIsFailure :: HasCallStack => Result -> Assertion
outcomeIsFailure tastyResult =
  case resultOutcome tastyResult of
    Failure TestFailed -> pass
    Failure reason ->
      assertFailure $ toString $ unlines
        [ "Expected Tasty test to fail because the Nettest test failed, but failed because:"
        , show reason
        ]
    Success ->
      assertFailure "Expected Tasty test to fail, but it succeeded"
