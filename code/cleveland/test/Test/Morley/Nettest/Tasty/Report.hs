-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Morley.Nettest.Tasty.Report
  ( test_formatError
  ) where

import Data.Char (isSpace)
import qualified Data.List as List
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Options (singleOption)
import Test.Tasty.Runners (Result(resultDescription))
import qualified Unsafe

import Morley.Nettest
import Morley.Nettest.Pure (PureM)
import Morley.Nettest.Tasty.Options (ContextLinesOpt(ContextLinesOpt))

import Test.Morley.Nettest.Tasty.ReportExamples (reportExamples)
import Test.Util (runViaTastyOnEmulator)

test_formatError :: TestTree
test_formatError =
  testGroup "formatError" $
    fmap checkResultDescription reportExamples

checkResultDescription :: (TestName, EmulatedT PureM (), String) -> TestTree
checkResultDescription (testName, scenario, expectedErr) =
  runViaTastyOnEmulator testName (singleOption (ContextLinesOpt 1)) scenario \tastyResult -> do
    let
      -- Some error lines might have trailing whitespace, we can ignore it here.
      strippedErr =
        List.lines (resultDescription tastyResult)
        & fmap (List.dropWhileEnd isSpace)
        & List.unlines

      -- The strings quoted with [r||] in this module contain a leading and trailing empty lines,
      -- so we need to remove them here.
      strippedExpectedErr =
        List.lines expectedErr
        & Unsafe.tail
        & Unsafe.init
        & List.unlines

    strippedErr @?= strippedExpectedErr
