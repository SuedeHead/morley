-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Morley.Nettest.Tasty.ReportExamples
  ( reportExamples
  ) where

import Text.RawString.QQ (r)

import Morley.Client (Alias(..))
import Morley.Nettest
import Morley.Nettest.Pure



----------------------------------------------------------------------------
-- Examples
----------------------------------------------------------------------------

example1 :: EmulatedT PureM ()
example1 = void $ signBytes "" unknownAlias

example1ExpectedErr :: String
example1ExpectedErr =
  [r|
   ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
21 ┃ example1 :: EmulatedT PureM ()
22 ┃ example1 = void $ signBytes "" unknownAlias
   ┃                   ^^^^^^^^^^^^^^^^^^^^^^^^^
   ┃                   | Unknown address alias: UnknownAlias
23 ┃

CallStack (from HasCallStack):
  signBytes, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:22:19 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

----------------------------------------------------------------------------
-- Examples with helper functions
----------------------------------------------------------------------------

exampleWithHelperFunction1 :: EmulatedT PureM ()
exampleWithHelperFunction1 = exampleWithHelperFunction1Helper

exampleWithHelperFunction1Helper :: EmulatedT PureM ()
exampleWithHelperFunction1Helper = void $ signBytes "" unknownAlias

exampleWithHelperFunction1ExpectedErr :: String
exampleWithHelperFunction1ExpectedErr =
  [r|
   ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
45 ┃ exampleWithHelperFunction1Helper :: EmulatedT PureM ()
46 ┃ exampleWithHelperFunction1Helper = void $ signBytes "" unknownAlias
   ┃                                           ^^^^^^^^^^^^^^^^^^^^^^^^^
   ┃                                           | Unknown address alias: UnknownAlias
47 ┃

CallStack (from HasCallStack):
  signBytes, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:46:43 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

exampleWithHelperFunction2 :: EmulatedT PureM ()
exampleWithHelperFunction2 = exampleWithHelperFunction2Helper

exampleWithHelperFunction2Helper :: HasCallStack => EmulatedT PureM ()
exampleWithHelperFunction2Helper = withFrozenCallStack $ void $ signBytes "" unknownAlias

exampleWithHelperFunction2ExpectedErr :: String
exampleWithHelperFunction2ExpectedErr =
  [r|
   ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
62 ┃ exampleWithHelperFunction2 :: EmulatedT PureM ()
63 ┃ exampleWithHelperFunction2 = exampleWithHelperFunction2Helper
   ┃                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   ┃                              | Unknown address alias: UnknownAlias
64 ┃

CallStack (from HasCallStack):
  exampleWithHelperFunction2Helper, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:63:30 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

----------------------------------------------------------------------------
-- Example with no callstack frames
----------------------------------------------------------------------------

exampleNoCallStack1 :: EmulatedT PureM ()
exampleNoCallStack1 = withFrozenCallStack $ void $ signBytes "" unknownAlias

exampleNoCallStack1ExpectedErr :: String
exampleNoCallStack1ExpectedErr =
  [r|
Unknown address alias: UnknownAlias
  |]

----------------------------------------------------------------------------
-- Example with errors spanning across multiple lines
----------------------------------------------------------------------------

exampleMultipleLines1 :: EmulatedT PureM ()
exampleMultipleLines1 =
  void ( signBytes
           ""
           unknownAlias )

exampleMultipleLines1ExpectedErr :: String
exampleMultipleLines1ExpectedErr =
  [r|
    ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
100 ┃ exampleMultipleLines1 =
101 ┃   void ( signBytes
102 ┃            ""
103 ┃            unknownAlias )
    ┃          ^^^^^^^^^^^^^^
    ┃          | Unknown address alias: UnknownAlias
104 ┃

CallStack (from HasCallStack):
  signBytes, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:101:10 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

exampleMultipleLines2 :: EmulatedT PureM ()
exampleMultipleLines2 =
  void (   signBytes
         ""
           unknownAlias )

exampleMultipleLines2ExpectedErr :: String
exampleMultipleLines2ExpectedErr =
  [r|
    ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
122 ┃ exampleMultipleLines2 =
123 ┃   void (   signBytes
124 ┃          ""
125 ┃            unknownAlias )
    ┃          ^^^^^^^^^^^^^^
    ┃          | Unknown address alias: UnknownAlias
126 ┃

CallStack (from HasCallStack):
  signBytes, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:123:12 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

exampleMultipleLines3 :: EmulatedT PureM ()
exampleMultipleLines3 =
  void (   signBytes
           ""
         unknownAlias )

exampleMultipleLines3ExpectedErr :: String
exampleMultipleLines3ExpectedErr =
  [r|
    ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
144 ┃ exampleMultipleLines3 =
145 ┃   void (   signBytes
146 ┃            ""
147 ┃          unknownAlias )
    ┃          ^^^^^^^^^^^^
    ┃          | Unknown address alias: UnknownAlias
148 ┃

CallStack (from HasCallStack):
  signBytes, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:145:12 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

exampleMultipleLines4 :: EmulatedT PureM ()
exampleMultipleLines4 =
  void (      signBytes
                   ""
         unknownAlias   )

exampleMultipleLines4ExpectedErr :: String
exampleMultipleLines4ExpectedErr =
  [r|
    ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
166 ┃ exampleMultipleLines4 =
167 ┃   void (      signBytes
168 ┃                    ""
169 ┃          unknownAlias   )
    ┃          ^^^^^^^^^^^^^^
    ┃          | Unknown address alias: UnknownAlias
170 ┃

CallStack (from HasCallStack):
  signBytes, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:167:15 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

exampleMultipleLines5 :: EmulatedT PureM ()
exampleMultipleLines5 =
  void (    signBytes
                     ""
         unknownAlias   )

exampleMultipleLines5ExpectedErr :: String
exampleMultipleLines5ExpectedErr =
  [r|
    ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
188 ┃ exampleMultipleLines5 =
189 ┃   void (    signBytes
190 ┃                      ""
191 ┃          unknownAlias   )
    ┃          ^^^^^^^^^^^^^^
    ┃          | Unknown address alias: UnknownAlias
192 ┃

CallStack (from HasCallStack):
  signBytes, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:189:13 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

exampleMultipleLines6 :: EmulatedT PureM ()
exampleMultipleLines6 =
  void (    signBytes
                   ""
           unknownAlias   )

exampleMultipleLines6ExpectedErr :: String
exampleMultipleLines6ExpectedErr =
  [r|
    ┏━━ test/Test/Morley/Nettest/Tasty/ReportExamples.hs ━━━
210 ┃ exampleMultipleLines6 =
211 ┃   void (    signBytes
212 ┃                    ""
213 ┃            unknownAlias   )
    ┃            ^^^^^^^^^^^^
    ┃            | Unknown address alias: UnknownAlias
214 ┃

CallStack (from HasCallStack):
  signBytes, called at test/Test/Morley/Nettest/Tasty/ReportExamples.hs:211:13 in main:Test.Morley.Nettest.Tasty.ReportExamples
  |]

unknownAlias :: Alias
unknownAlias = Alias "UnknownAlias"

reportExamples :: [(String, EmulatedT PureM (), String)]
reportExamples =
  [ ("example1", example1, example1ExpectedErr)
  , ("exampleWithHelperFunction1", exampleWithHelperFunction1, exampleWithHelperFunction1ExpectedErr)
  , ("exampleWithHelperFunction2", exampleWithHelperFunction2, exampleWithHelperFunction2ExpectedErr)
  , ("exampleNoCallStack1", exampleNoCallStack1, exampleNoCallStack1ExpectedErr)
  , ("exampleMultipleLines1", exampleMultipleLines1, exampleMultipleLines1ExpectedErr)
  , ("exampleMultipleLines2", exampleMultipleLines2, exampleMultipleLines2ExpectedErr)
  , ("exampleMultipleLines3", exampleMultipleLines3, exampleMultipleLines3ExpectedErr)
  , ("exampleMultipleLines4", exampleMultipleLines4, exampleMultipleLines4ExpectedErr)
  , ("exampleMultipleLines5", exampleMultipleLines5, exampleMultipleLines5ExpectedErr)
  , ("exampleMultipleLines6", exampleMultipleLines6, exampleMultipleLines6ExpectedErr)
  ]
