-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Morley.Nettest.Tasty
  ( test_NetworkRelatedFlags
  , test_Memoize
  ) where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Options (singleOption)

import Morley.Nettest.Tasty (memoize, shouldRunNetwork)
import Morley.Nettest.Tasty.Options (NoRunNetworkOpt(..), RunNetworkOpt(..), Switch(..))

test_NetworkRelatedFlags :: TestTree
test_NetworkRelatedFlags =
  -- These tests check that the decision table in the haddock for the
  -- 'Morley.Nettest.Tasty' module is correctly implemented.
  testGroup "Network-related flags"
    [ testShouldRunNetwork Unset (RunNetworkOpt Unset) (NoRunNetworkOpt Unset) False
    , testShouldRunNetwork Set   (RunNetworkOpt Unset) (NoRunNetworkOpt Unset) True
    , testShouldRunNetwork Unset (RunNetworkOpt Set)   (NoRunNetworkOpt Unset) True
    , testShouldRunNetwork Set   (RunNetworkOpt Set)   (NoRunNetworkOpt Unset) True

    , testShouldRunNetwork Unset (RunNetworkOpt Unset) (NoRunNetworkOpt Set)   False
    , testShouldRunNetwork Set   (RunNetworkOpt Unset) (NoRunNetworkOpt Set)   False
    , testShouldRunNetwork Unset (RunNetworkOpt Set)   (NoRunNetworkOpt Set)   False
    , testShouldRunNetwork Set   (RunNetworkOpt Set)   (NoRunNetworkOpt Set)   False
    ]
  where
    testShouldRunNetwork :: Switch -> RunNetworkOpt -> NoRunNetworkOpt -> Bool -> TestTree
    testShouldRunNetwork ci runNetworkOpt noRunNetworkOpt expected =
      if expected
        then
          testCase ("Network tests are run when: " <> showArgs ci runNetworkOpt noRunNetworkOpt) $
            shouldRunNetwork ci (singleOption runNetworkOpt <> singleOption noRunNetworkOpt) @?= True
        else
          testCase ("Network tests are NOT run when: " <> showArgs ci runNetworkOpt noRunNetworkOpt) $
            shouldRunNetwork ci (singleOption runNetworkOpt <> singleOption noRunNetworkOpt) @?= False

    showArgs :: Switch -> RunNetworkOpt -> NoRunNetworkOpt -> String
    showArgs ci (RunNetworkOpt runNetwork) (NoRunNetworkOpt noRunNetwork) = mconcat
      [ "CI=", show ci
      , " / RunNetwork=", show runNetwork
      , " / NoRunNetwork=", show noRunNetwork
      ]

test_Memoize :: TestTree
test_Memoize =
  testGroup "memoize"
    [ testCase "is lazy" $ do
        ioref <- newIORef @_ @Int 1

        _action <- memoize (modifyIORef ioref succ)

        val <- readIORef ioref
        val @?= 1

    , testCase "writes only once to cache" $ do
        ioref <- newIORef @_ @Int 1

        action <- memoize (modifyIORef ioref succ)
        action
        action
        action

        val <- readIORef ioref
        val @?= 2
    ]
