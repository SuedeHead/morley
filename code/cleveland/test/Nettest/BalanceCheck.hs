-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.BalanceCheck
  ( test_BalanceCheck
  , test_EmptyBalanceCheck
  ) where

import Test.Tasty

import Michelson.Untyped.Entrypoints
import Tezos.Address (Address)
import Tezos.Core (toMutez)

import Morley.Nettest
import Morley.Nettest.Tasty

test_BalanceCheck :: TestTree
test_BalanceCheck =
  nettestScenarioCaps "An address's balance can be checked" $ do
    nettestAddr :: Address <- resolveNettestAddress
    testAddr :: Address <- newFreshAddress "test"
    let
      test :: AddressOrAlias
      test = AddressResolved testAddr

      nettest :: AddressOrAlias
      nettest = AddressResolved nettestAddr
      td = TransferData
        { tdFrom = nettest
        , tdTo = test
        , tdAmount = toMutez 100
        , tdEntrypoint = DefEpName
        , tdParameter = ()
        }
    comment "balance is updated after transfer"
    transfer td
    checkBalance test (toMutez 100)

test_EmptyBalanceCheck :: TestTree
test_EmptyBalanceCheck =
  nettestScenarioCaps "An empty address' balance can be checked" $ do
    nettestAddr :: Address <- resolveNettestAddress
    testAddr :: Address <- newFreshAddress "test"

    -- Doing something in order not to get "validating empty scenario" error
    transfer TransferData
      { tdFrom = AddressResolved nettestAddr
      , tdTo = AddressResolved nettestAddr
      , tdAmount = toMutez 1
      , tdEntrypoint = DefEpName
      , tdParameter = ()
      }

    checkBalance (AddressResolved testAddr) (toMutez 0)
