-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.StorageCheck
  ( test_StorageCheck
  , test_GetStorageExpr
  , test_StorageCheckEmulator
  ) where

import qualified Data.Map as Map
import Test.Hspec.Expectations (shouldContain)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Runners (resultDescription)

import Lorentz hiding (comment)

import Morley.Micheline (toExpression)
import Morley.Nettest
import Morley.Nettest.Tasty

import Test.Util (runViaTastyOnEmulator, runViaTastyOnNetwork)

test_StorageCheck :: TestTree
test_StorageCheck =
  testGroup "checkStorage"
    [ nettestScenarioCaps "passes when contract's storage matches the expected value" $ do
        addr <- addressResolved <$> originateSimple
          "save parameter in storage"
          1
          testContract

        comment "checking initial storage"
        checkStorage addr (toVal @Natural 1)

        transfer TransferData
          { tdFrom = nettestAddress
          , tdTo = addr
          , tdAmount = toMutez 100
          , tdEntrypoint = DefEpName
          , tdParameter = 2 :: Natural
          }

        comment "storage is updated after transfer"
        checkStorage addr (toVal @Natural 2)

    , testGroup "throws an exception when the storage value is unexpected" $
      [ runViaTastyOnEmulator "On emulator" mempty
          failingTest
          \tastyResult -> resultDescription tastyResult `shouldContain` "have storage 99, but 1"

      , runViaTastyOnNetwork "On network" mempty
          failingTest
          \tastyResult -> resultDescription tastyResult `shouldContain` "is expected to have storage 99 of type nat. But its actual storage is 1 of type nat."
      ]
    ]
  where
    failingTest :: MonadNettest caps base m => m ()
    failingTest = do
      addr <- addressResolved <$> originate OriginateData
        { odFrom = nettestAddress
        , odName = "save parameter in storage"
        , odStorage = 1
        , odBalance = toMutez 25
        , odContract = testContract
        }

      comment "checking initial storage"
      checkStorage addr (toVal @Natural 99)

test_GetStorageExpr :: TestTree
test_GetStorageExpr = nettestScenarioCaps "getStorageExpr returns serialized storage with expected expression" $ do
  addr <- addressResolved <$> originateSimple
    "save parameter in storage"
    42
    testContract

  expr1 <- getStorageExpr addr
  expr1 @== toExpression (toVal @Natural 42)

  transfer TransferData
    { tdFrom = nettestAddress
    , tdTo = addr
    , tdAmount = toMutez 100
    , tdEntrypoint = DefEpName
    , tdParameter = 1234 :: Natural
    }

  expr2 <- getStorageExpr addr
  expr2 @== toExpression (toVal @Natural 1234)

type Storage = Natural

-- | Simple contract that takes a parameter and saves it in its storage.
testContract :: Contract Natural Storage
testContract = defaultContract $
  car #
  nil @Operation #
  pair

test_StorageCheckEmulator :: TestTree
test_StorageCheckEmulator =
  nettestScenarioOnEmulatorCaps "checkStorage' retrieves storage with big maps" $ do
    let storage =
          Storage2
            { st2Field1 = 23
            , st2Field2 = BigMap $ Map.fromList
                [ (1, [mt|a|])
                , (2, [mt|b|])
                ]
            }
    addr <- originateSimple "checkStorage'" storage testContract2
    checkStorage' (addressResolved addr) (toVal storage)

data Storage2 = Storage2
  { st2Field1 :: Natural
  , st2Field2 :: BigMap Natural MText
  }
  deriving stock (Generic, Show, Eq)
  deriving anyclass (IsoValue, HasAnnotation)

-- | Simple contract that does nothing when called.
testContract2 :: Contract () Storage2
testContract2 = defaultContract $
  cdr #
  nil @Operation #
  pair
