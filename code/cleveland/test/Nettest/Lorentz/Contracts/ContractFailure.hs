-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
{-# OPTIONS_GHC -Wno-orphans #-}
module Nettest.Lorentz.Contracts.ContractFailure
  ( failingContract
  ) where

import Lorentz as L

failingContract :: Contract () Storage
failingContract = defaultContract failingContractLorentz

type Storage = ("storage" :! ())

type instance ErrorArg "testError" = Natural

instance CustomErrorHasDoc "testError" where
  customErrClass = ErrClassActionException
  customErrDocMdCause = "Error for testing custom error handling in nettest"

failingContractLorentz :: ContractCode () Storage
failingContractLorentz =
  L.drop # push @Natural 5 # failCustom #testError
