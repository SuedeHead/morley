-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.CustomErrors
  ( test_CustomErrors
  ) where

import Test.Tasty

import Lorentz (EntrypointRef(CallDefault), TAddress)
import Tezos.Address (Address)
import Tezos.Core (toMutez)
import Util.Named

import Morley.Nettest
import Morley.Nettest.Tasty
import Nettest.Lorentz.Contracts.ContractFailure

test_CustomErrors :: TestTree
test_CustomErrors =
  nettestScenarioCaps "Custom errors can be caught" $ do
    nettestAddr :: Address <- resolveNettestAddress
    let
      nettest :: AddressOrAlias
      nettest = AddressResolved nettestAddr

      od = OriginateData
        { odFrom = nettest
        , odName = "failing"
        , odStorage = #storage .! ()
        , odBalance = toMutez 0
        , odContract = failingContract
        }

    failingAddr :: TAddress () <- originate od

    expectCustomError #testError 5 $ call failingAddr CallDefault ()
