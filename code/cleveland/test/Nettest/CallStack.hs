-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
{-# OPTIONS_GHC -Wno-orphans #-}

module Nettest.CallStack
  ( test_callStack
  ) where

import Data.Char (isNumber, isSpace)
import qualified Data.List as List
import Servant.Client
  (BaseUrl(BaseUrl), ClientEnv(baseUrl), ClientError(ConnectionError), Scheme(Http))
import Test.Hspec.Expectations
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Runners (Result(resultDescription))
import Text.RawString.QQ (r)
import Time (sec)
import qualified Unsafe

import Lorentz hiding (assert, comment, not)
import Michelson.Typed (convertContract, untypeValue)
import Morley.Client (Alias(..), TezosClientError(EConnreset), mceClientEnvL, mceTezosClientL)
import Morley.Client.TezosClient (tceEndpointUrlL)
import Tezos.Address (Address(ContractAddress), ContractHash(ContractHash))

import Michelson.Test.Integrational
  (ScenarioBranchName(..), ScenarioError(ScenarioError), TestError(CustomTestError))
import Morley.Nettest
import Morley.Nettest.Parser (neMorleyClientEnvL)
import Morley.Nettest.Tasty

import Test.Util (outcomeIsFailure, runViaTastyOnEmulator, runViaTastyOnNetwork)

test_callStack :: TestTree
test_callStack =
  testGroup "Error messages include a helpful callstack" $
    [ testFailureIncludesCallStack "callstack points to runIO"
        [r|
          runIO (throwM DummyException)
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          runIO (throwM DummyException)

    , testFailureIncludesCallStack "callstack points to resolveAddress"
        [r|
          void $ resolveAddress (AddressAlias invalidAlias)
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by using an unknown alias
          void $ resolveAddress (AddressAlias invalidAlias)

    , testFailureIncludesCallStack "callstack points to getAlias"
        [r|
          void $ getAlias (AddressResolved invalidAddr)
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by using an unknown address
          void $ getAlias (AddressResolved invalidAddr)

    , testGroup "callstack points to newAddress"
        [ testFailureIncludesCallStackOnEmulator "On emulator"
            [r|
              void $ newAddress "b"
                     ^^^^^^^^^^^^^^
            |]
            do
              -- force a failure by first draining nettest funds first...
              addr <- newAddress "a"
              funds <- getBalance nettestAddress
              transfer TransferData
                { tdFrom = nettestAddress
                , tdTo = AddressResolved addr
                , tdAmount = funds
                , tdEntrypoint = DefEpName
                , tdParameter = ()
                }
              -- ... then trying to create a new address
              -- (which will implicitly try and fail to transfer some funds from nettest to the new address).
              void $ newAddress "b"

        , sabotageNettestEnv $
            testFailureIncludesCallStackOnNetwork "On network"
              [r|
                void $ newAddress ""
                       ^^^^^^^^^^^^^
              |]
              do
                void $ newAddress ""
        ]

    , testFailureIncludesCallStack "callstack points to signBytes"
        [r|
          void $ signBytes "" invalidAlias
                 ^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ signBytes "" invalidAlias

    , testFailureIncludesCallStack "callstack points to signBinary"
        [r|
          void $ signBinary @ByteString "" invalidAlias
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ signBinary @ByteString "" invalidAlias

    , testFailureIncludesCallStack "callstack points to originateUntyped"
        [r|
          void $ originateUntyped UntypedOriginateData
            { uodFrom = nettestAddress
            , uodName = ""
            , uodBalance = toMutez 0
            , uodStorage = untypeValue $ toVal @Natural 3
            , uodContract = convertContract $ compileLorentzContract testContract
            }
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by using a storage of the wrong type
          void $ originateUntyped UntypedOriginateData
            { uodFrom = nettestAddress
            , uodName = ""
            , uodBalance = toMutez 0
            , uodStorage = untypeValue $ toVal @Natural 3
            , uodContract = convertContract $ compileLorentzContract testContract
            }

    , testFailureIncludesCallStack "callstack points to originateUntypedSimple"
        [r|
          void $ originateUntypedSimple ""
            (untypeValue $ toVal @Natural 3)
            (convertContract $ compileLorentzContract testContract)
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by using a storage of the wrong type
          void $ originateUntypedSimple ""
            (untypeValue $ toVal @Natural 3)
            (convertContract $ compileLorentzContract testContract)

    , testFailureIncludesCallStack "callstack points to originate"
        [r|
          void $ originate OriginateData
            { odFrom = nettestAddress
            , odName = ""
            , odBalance = maxBound
            , odStorage = ()
            , odContract = testContract
            }
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering `maxBound` mutez
          void $ originate OriginateData
            { odFrom = nettestAddress
            , odName = ""
            , odBalance = maxBound
            , odStorage = ()
            , odContract = testContract
            }

    , testFailureIncludesCallStack "callstack points to transfer"
        [r|
          transfer TransferData
            { tdFrom = AddressAlias invalidAlias
            , tdTo = nettestAddress
            , tdAmount = toMutez 0
            , tdEntrypoint = DefEpName
            , tdParameter = ()
            }
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering from an unknown alias
          transfer TransferData
            { tdFrom = AddressAlias invalidAlias
            , tdTo = nettestAddress
            , tdAmount = toMutez 0
            , tdEntrypoint = DefEpName
            , tdParameter = ()
            }

    , testFailureIncludesCallStack "callstack points to transferBatch"
        [r|
          transferBatch (AddressAlias invalidAlias) $ one TransferData
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering from an unknown alias
          transferBatch (AddressAlias invalidAlias) $ one TransferData
            { tdFrom = AddressAlias invalidAlias
            , tdTo = nettestAddress
            , tdAmount = toMutez 0
            , tdEntrypoint = DefEpName
            , tdParameter = ()
            }

    , testFailureIncludesCallStack "callstack points to call"
        [r|
          call invalidTAddr CallDefault ()
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering to an unknown address
          call invalidTAddr CallDefault ()

    , testFailureIncludesCallStack "callstack points to callBatch"
        [r|
          callBatch $ one $ mkCallData (TAddress @() invalidAddr) CallDefault ()
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering to an unknown address
          callBatch $ one $ mkCallData (TAddress @() invalidAddr) CallDefault ()

    , testFailureIncludesCallStack "callstack points to callFrom"
        [r|
          callFrom nettestAddress invalidTAddr CallDefault ()
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering to an unknown address
          callFrom nettestAddress invalidTAddr CallDefault ()

    , testFailureIncludesCallStack "callstack points to callBatchFrom"
        [r|
          callBatchFrom nettestAddress $ one $ mkCallData (TAddress @() invalidAddr) CallDefault ()
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          -- force a failure by transfering to an unknown address
          callBatchFrom nettestAddress $ one $ mkCallData (TAddress @() invalidAddr) CallDefault ()

    , testFailureIncludesCallStack "callstack points to importUntypedContract"
        [r|
          void $ importUntypedContract "<invalid file path>"
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ importUntypedContract "<invalid file path>"

    , testGroup "expectFailure"
      [ testFailureIncludesCallStack "when action does not throw, callstack points to expectFailure"
          [r|
            pass
              `expectFailure`
                NettestBadParameter
            ^^^^^^^^^^^^^^^^^^^^^^^
          |]
          do
            pass
              `expectFailure`
                NettestBadParameter

      , testFailureIncludesCallStack
          "when action throws the expected exception type, but wrong constructor, callstack points to expectFailure"
          [r|
            call addr CallDefault ()
              `expectFailure`
                NettestBadParameter
            ^^^^^^^^^^^^^^^^^^^^^^^^
          |]
          do
            addr <- originateSimple "" () contractFailWith1
            call addr CallDefault ()
              `expectFailure`
                NettestBadParameter

      , testFailureIncludesCallStack
          "when action throws the expected exception constructor, but the args don't match, callstack points to expectFailure"
          [r|
            call addr CallDefault ()
              `expectFailure`
                NettestFailedWith @Natural addr 2
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          |]
          do
            addr <- originateSimple "" () contractFailWith1
            call addr CallDefault ()
              `expectFailure`
                NettestFailedWith @Natural addr 2

      , testGroup "when action throws an unexpected exception, callstack points to inner action" $
          let unexpectedExceptions =
                [ ( "DummyException"
                  , SomeException DummyException
                  )
                , ( "unexpected TestError constructor"
                  , SomeException $ ScenarioError (ScenarioBranchName []) (CustomTestError "err")
                  )
                , ( "Servant ClientError"
                  , SomeException $ ConnectionError (SomeException DummyException)
                  )
                , ( "TezosClientError"
                  , SomeException EConnreset
                  )
                ]
          in  flip fmap unexpectedExceptions $ \(testName, SomeException ex) ->
                testFailureIncludesCallStack
                  testName
                  [r|
                    runIO (throwM ex)
                    ^^^^^^^^^^^^^^^^^
                  |]
                  do
                    runIO (throwM ex)
                      `expectFailure`
                        NettestBadParameter
      ]

    , testFailureIncludesCallStack "callstack points to expectCustomError"
        [r|
          expectCustomError #unitError () pass
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          expectCustomError #unitError () pass

    , testFailureIncludesCallStack "callstack points to expectCustomError_"
        [r|
          expectCustomError_ #unitError pass
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          expectCustomError_ #unitError pass

    , testFailureIncludesCallStack "callstack points to getBalance"
        [r|
          void $ getBalance (AddressAlias invalidAlias)
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getBalance (AddressAlias invalidAlias)

    , testFailureIncludesCallStack "callstack points to checkBalance"
        [r|
          checkBalance (AddressAlias invalidAlias) (toMutez 0)
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          checkBalance (AddressAlias invalidAlias) (toMutez 0)

    , testFailureIncludesCallStack "callstack points to getStorage"
        [r|
          void $ getStorage @(ToT ()) (AddressAlias invalidAlias)
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getStorage @(ToT ()) (AddressAlias invalidAlias)

    , testFailureIncludesCallStack "callstack points to getStorageExpr"
        [r|
          void $ getStorageExpr (AddressAlias invalidAlias)
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getStorageExpr (AddressAlias invalidAlias)

    , testFailureIncludesCallStack "callstack points to checkStorage"
        [r|
          checkStorage (AddressAlias invalidAlias) (toVal ())
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          checkStorage (AddressAlias invalidAlias) (toVal ())

    , testFailureIncludesCallStackOnEmulator "callstack points to checkStorage' on emulator"
        [r|
          checkStorage' (AddressAlias invalidAlias) (toVal ())
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          checkStorage' (AddressAlias invalidAlias) (toVal ())

    , testFailureIncludesCallStack "callstack points to getPublicKey"
        [r|
          void $ getPublicKey (AddressAlias invalidAlias)
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          void $ getPublicKey (AddressAlias invalidAlias)

    , sabotageNettestEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to getChainId on network"
          [r|
            void getChainId
                 ^^^^^^^^^^
          |]
          do
            void getChainId

    , sabotageNettestEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to advanceTime on network"
          [r|
            advanceTime (sec 1)
            ^^^^^^^^^^^^^^^^^^^
          |]
          do
            advanceTime (sec 1)

    , sabotageNettestEnv $
        testFailureIncludesCallStackOnNetwork "callstack points to getNow on network"
          [r|
            void getNow
                 ^^^^^^
          |]
          do
            void getNow

    , testFailureIncludesCallStackOnEmulator
        "when a branchout branch throws, the callstack points to the function inside the branch"
        [r|
                  getBalance (AddressAlias invalidAlias)
                  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                  | In 'a' branch:
                  | Unknown address alias: UnknownAlias
        |]
        do
          branchout
            [ "a" ?-
                void $
                  getBalance (AddressAlias invalidAlias)
            ]

    , testFailureIncludesCallStackOnEmulator
        "when offshoot throws, the callstack points to the function inside offshoot"
        [r|
              getBalance (AddressAlias invalidAlias)
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
              | In 'a' branch:
              | Unknown address alias: UnknownAlias
        |]
        do
          offshoot "a" $
            void $
              getBalance (AddressAlias invalidAlias)

    , testFailureIncludesCallStack "callstack points to failure"
        [r|
          failure "a"
          ^^^^^^^^^^^
        |]
        do
          failure "a"

    , testFailureIncludesCallStack "callstack points to assert"
        [r|
          assert False "a"
          ^^^^^^^^^^^^^^^^
        |]
        do
          assert False "a"

    , testFailureIncludesCallStack "callstack points to @=="
        [r|
          1 @== (2 :: Int)
          ^^^^^^^^^^^^^^^^
        |]
        do
          1 @== (2 :: Int)

    , testFailureIncludesCallStack "callstack points to @/="
        [r|
          1 @/= (1 :: Int)
          ^^^^^^^^^^^^^^^^
        |]
        do
          1 @/= (1 :: Int)

    , testFailureIncludesCallStack "callstack points to checkCompares"
        [r|
          checkCompares @Int 1 (==) 2
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          checkCompares @Int 1 (==) 2

    , testFailureIncludesCallStack "callstack points to checkComparesWith"
        [r|
          checkComparesWith @Int show 1 (==) show 2
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        |]
        do
          checkComparesWith @Int show 1 (==) show 2
    ]

  where
    invalidAlias = Alias "UnknownAlias"
    invalidAddr = ContractAddress (ContractHash "0001") :: Address
    invalidTAddr = TAddress @() invalidAddr

-- | Check that exceptions thrown by the given function contain a callstack that points to that function.
--
-- The scenario is run on both the emulator and on a network.
testFailureIncludesCallStack
  :: TestName
  -> String
  -> (forall caps base m. MonadNettest caps base m => m ())
  -> TestTree
testFailureIncludesCallStack testName expectedErrorLines nettest =
  testGroup testName
    [ testFailureIncludesCallStackOnEmulator
        "On emulator"
        expectedErrorLines
        nettest
    , testFailureIncludesCallStackOnNetwork
        "On network"
        expectedErrorLines
        nettest
    ]

testFailureIncludesCallStackOnEmulator :: TestName -> String -> (forall m. Monad m => EmulatedT m ()) -> TestTree
testFailureIncludesCallStackOnEmulator testName expectedErrorLines nettest =
  runViaTastyOnEmulator testName mempty nettest $ \tastyResult -> do
    outcomeIsFailure tastyResult
    checkErrorMessage (resultDescription tastyResult) expectedErrorLines

testFailureIncludesCallStackOnNetwork :: TestName -> String -> (forall m. Monad m => NettestT m ()) -> TestTree
testFailureIncludesCallStackOnNetwork testName expectedErrorLines nettest =
  runViaTastyOnNetwork testName mempty nettest $ \tastyResult -> do
    outcomeIsFailure tastyResult
    checkErrorMessage (resultDescription tastyResult) expectedErrorLines

-- | If we can't force a function to fail on a network by, e.g.,
-- passing the wrong arguments or violating its pre-conditions,
-- we can use this function to mess with the 'NettestEnv' config and force
-- the test to crash.
--
-- For example, 'newAddress' and 'getChainId' don't normally fail,
-- but if we mess with the config, they will.
sabotageNettestEnv :: TestTree -> TestTree
sabotageNettestEnv =
  modifyNettestEnv f
  where
    faultyBaseUrl = BaseUrl Http "" 0 ""

    f :: NettestEnv -> NettestEnv
    f =
      (neMorleyClientEnvL.mceTezosClientL.tceEndpointUrlL .~ faultyBaseUrl) .
      (neMorleyClientEnvL.mceClientEnvL %~ \clientEnv -> clientEnv
        { baseUrl = faultyBaseUrl }
      )

-- | Checks that an error message includes a pretty-printed callstack,
-- and that it points to this file and contains the expected lines.
checkErrorMessage :: HasCallStack => String -> String -> Assertion
checkErrorMessage err expectedLines = do
  Unsafe.head (List.lines err) `shouldContain` "━━ test/Nettest/CallStack.hs ━━━"

  if strippedExpectedLines `List.isInfixOf` strippedErrorLines
    then pass
    else
      assertFailure $
        List.unlines $
          [ "Expected the error message to contain: " ]
          <> strippedExpectedLines
          <> [ "But it didn't. Actual error message was: "]
          <> strippedErrorLines

  where
    stripLineNumber line =
      line
      & List.dropWhile isSpace
      & List.dropWhile isNumber
      & List.dropWhile isSpace
      & List.dropWhile (== '┃')
      & List.drop 1

    -- Strip 1) the header, 2) the line numbers and 3) the vertical border
    -- from the error message, to make writing these tests easier.
    strippedErrorLines =
      fmap stripLineNumber $ Unsafe.tail $ List.lines err

    -- The strings quoted with [r||] in this module contain a leading and trailing empty lines,
    -- so we need to remove them here.
    strippedExpectedLines = Unsafe.init $ Unsafe.tail $ List.lines expectedLines

----------------------------------------------------------------------------
-- Test data
----------------------------------------------------------------------------

type instance ErrorArg "unitError" = ()

instance CustomErrorHasDoc "unitError" where
  customErrClass = ErrClassActionException
  customErrDocMdCause = "Error for testing custom error handling in nettest"

-- | Simple contract that does nothing when called.
testContract :: Contract () ()
testContract = defaultContract $
  cdr #
  nil @Operation #
  pair

contractFailWith1 :: Contract () ()
contractFailWith1 = defaultContract $
  push @Natural 1 # failWith

data DummyException = DummyException
  deriving stock (Eq, Show)

instance Exception DummyException where
