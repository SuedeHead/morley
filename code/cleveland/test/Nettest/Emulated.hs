-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.Emulated
  ( test_Emulated
  ) where

import Test.Hspec.Expectations (shouldContain)
import Test.Tasty
import Test.Tasty.Runners (Result(resultDescription))

import Lorentz (pattern DefEpName, EntrypointRef(CallDefault), ToTAddress(toTAddress), toMutez)
import Morley.Client (Alias(..))
import Tezos.Core (unsafeAddMutez)

import Morley.Nettest
import Morley.Nettest.Tasty

import Test.Util (runViaTastyOnEmulator)

test_Emulated :: [TestTree]
test_Emulated =
  [ testGroup "branchout"
    [ nettestScenarioOnEmulatorCaps "passes if all branches pass" $
        branchout
          [ "a" ?- pass
          , "b" ?- pass
          ]

    , nettestScenarioOnEmulatorCaps "fails if any branch fails" $ do
        nettest <- toTAddress @()<$> resolveNettestAddress
        let branchA = "a" ?- call nettest CallDefault ()
        let branchB = "b" ?- pass
        branchout [branchA, branchB] `expectFailure` NettestEmptyTransaction nettest
        branchout [branchB, branchA] `expectFailure` NettestEmptyTransaction nettest


    , nettestScenarioOnEmulatorCaps "a branch's effects do not leak into another branch" $ do
        testAddr <- AddressResolved <$> newAddress "test"
        initialBalance <- getBalance testAddr
        branchout
          [ "a" ?- do
              simpleTransfer testAddr (toMutez 2)
              checkBalance testAddr (initialBalance `unsafeAddMutez` toMutez 2)
          , "b" ?- do
              simpleTransfer testAddr (toMutez 3)
              checkBalance testAddr (initialBalance `unsafeAddMutez` toMutez 3)
          ]

    , nettestScenarioOnEmulatorCaps "branchout's effects are discarded" $ do
        testAddr <- AddressResolved <$> newAddress "test"
        initialBalance <- getBalance testAddr
        branchout
          [ "a" ?- simpleTransfer testAddr (toMutez 3) ]
        checkBalance testAddr initialBalance

    , runViaTastyOnEmulator "adds branch name to error" mempty
        do
          branchout [ "<branch name>" ?- void $ signBytes "" (Alias "InvalidAlias") ]
        \tastyResult ->
          resultDescription tastyResult `shouldContain` "<branch name>"
    ]
  , testGroup "offshoot"
    [ nettestScenarioOnEmulatorCaps "fails if inner scenario fails" $ do
        nettest <- toTAddress @()<$> resolveNettestAddress
        offshoot "a" (call nettest CallDefault ())
          `expectFailure` NettestEmptyTransaction nettest

    , nettestScenarioOnEmulatorCaps "offshoot's effects are discarded" $ do
        testAddr <- AddressResolved <$> newAddress "test"
        initialBalance <- getBalance testAddr
        offshoot "a" $ simpleTransfer testAddr (toMutez 1)
        checkBalance testAddr initialBalance
    ]
  ]
  where
    simpleTransfer addr tz =
      transfer TransferData
        { tdFrom = nettestAddress
        , tdTo = addr
        , tdAmount = tz
        , tdEntrypoint = DefEpName
        , tdParameter = ()
        }
