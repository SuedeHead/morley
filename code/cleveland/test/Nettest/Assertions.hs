-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE NoMonomorphismRestriction #-}

module Nettest.Assertions
  ( test_failure_fails
  , test_assert_succeeds
  , test_assert_fails
  , test_eq_succeeds
  , test_eq_fails
  , test_neq_succeeds
  , test_neq_fails
  , test_checkCompares_succeeds
  , test_checkCompares_fails
  , test_checkComparesWith_succeeds
  , test_checkComparesWith_fails
  ) where

import Fmt (pretty)
import Test.Hspec.Expectations (shouldContain)
import Test.Tasty (TestTree)
import Test.Tasty.Runners (Result(resultDescription))

import Morley.Nettest
import Morley.Nettest.Tasty (nettestScenarioCaps)

import Test.Util (outcomeIsFailure, runViaTasty)

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

test_failure_fails :: TestTree
test_failure_fails =
  runViaTasty "`failure` fails unconditionally" mempty
    (failure "<error msg>")
    \result -> do
      outcomeIsFailure result
      resultDescription result `shouldContain` "| <error msg>"

test_assert_succeeds :: TestTree
test_assert_succeeds =
  nettestScenarioCaps "`assert` succeeds when condition is true" $
    assert True "<error msg>"

test_assert_fails :: TestTree
test_assert_fails =
  runViaTasty "`assert` fails when condition is false" mempty
    (assert False "<error msg>")
    \result -> do
      outcomeIsFailure result
      resultDescription result `shouldContain` "| <error msg>"

test_eq_succeeds :: TestTree
test_eq_succeeds =
  nettestScenarioCaps "`@==` succeeds when the values are equal" $
    True @== True

test_eq_fails :: TestTree
test_eq_fails =
  runViaTasty "`@==` fails when the values are not equal" mempty
    (True @== False)
    \result -> do
      outcomeIsFailure result
      resultDescription result `shouldContain` "lhs"
      resultDescription result `shouldContain` "rhs"

test_neq_succeeds :: TestTree
test_neq_succeeds =
  nettestScenarioCaps "`@/=` succeeds when the values are not equal" $
    True @/= False

test_neq_fails :: TestTree
test_neq_fails =
  runViaTasty "`@/=` fails when the values are equal" mempty
    (True @/= True)
    \result -> do
      outcomeIsFailure result
      resultDescription result `shouldContain` "| The two values are equal:"

test_checkCompares_succeeds :: TestTree
test_checkCompares_succeeds =
  nettestScenarioCaps "`checkCompares` succeeds when the comparison succeeds" $
    checkCompares @Int 2 elem [1, 2, 3]

test_checkCompares_fails :: TestTree
test_checkCompares_fails =
  runViaTasty "`checkCompares` fails when the comparison fails" mempty
    (checkCompares @Int 1 elem [])
    \result -> do
      outcomeIsFailure result
      resultDescription result `shouldContain` "lhs"
      resultDescription result `shouldContain` "rhs"

test_checkComparesWith_succeeds :: TestTree
test_checkComparesWith_succeeds =
  nettestScenarioCaps "`checkComparesWith` succeeds when the comparison succeeds" $
    checkComparesWith @Int pretty 2 elem pretty [1, 2, 3]

test_checkComparesWith_fails :: TestTree
test_checkComparesWith_fails =
  runViaTasty "`checkComparesWith` fails when the comparison fails and prints values" mempty
    (checkComparesWith @Int toCardinal 1 elem (show . fmap toOrdinal) [2, 3])
    \result -> do
      outcomeIsFailure result
      resultDescription result `shouldContain` "| One"
      resultDescription result `shouldContain` "| [\"Second\",\"Third\"]"
  where
    toCardinal = \case
      1 -> "One"
      2 -> "Two"
      3 -> "Three"
      n -> show n
    toOrdinal = \case
      1 -> "First"
      2 -> "Second"
      3 -> "Third"
      n -> show @String n
