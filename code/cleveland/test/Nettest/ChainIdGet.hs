-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.ChainIdGet
  ( test_ChainIdGet
  ) where

import Test.Tasty

import Lorentz ((:!), EntrypointRef(CallDefault), TAddress, ( # ))
import qualified Lorentz as L
import Tezos.Core (ChainId, dummyChainId)
import Util.Named ((.!))

import Morley.Nettest
import Morley.Nettest.Tasty

type Storage = ("storage" :! ChainId)

checkChainIdContract :: L.Contract () Storage
checkChainIdContract = L.defaultContract $
  L.drop # L.chainId # L.toNamed #storage # L.nil @L.Operation # L.pair

test_ChainIdGet :: TestTree
test_ChainIdGet =
  nettestScenarioCaps "ChainId is retrievable" $ do
    helperAddr :: TAddress () <-
      originateSimple "helper" (#storage .! dummyChainId) checkChainIdContract

    chainId <- getChainId
    call helperAddr CallDefault ()
    checkStorage (AddressResolved $ L.toAddress helperAddr) $ L.toVal chainId
