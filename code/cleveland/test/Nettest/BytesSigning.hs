-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.BytesSigning
  ( test_BytesSigning
  ) where

import Test.Tasty

import Lorentz (EntrypointRef(CallDefault), ( # ))
import qualified Lorentz as L
import Tezos.Address (Address)
import Tezos.Crypto

import Morley.Nettest
import Morley.Nettest.Tasty

checkSignatureContract :: L.Contract (PublicKey, (L.TSignature ByteString, ByteString)) ()
checkSignatureContract = L.defaultContract $
  L.car #
  L.unpair # L.dip L.unpair #
  L.checkSignature # L.assert [L.mt|Invalid signature|] #
  L.unit # L.nil @L.Operation # L.pair

test_BytesSigning :: TestTree
test_BytesSigning =
  nettestScenarioCaps "Bytestrings can be signed" $ do
    nettestAddr :: Address <- resolveNettestAddress
    signerAddr :: Address <- newFreshAddress "signer"

    helperAddr <- originateSimple "helper" () checkSignatureContract

    let
      signer :: AddressOrAlias
      signer = AddressResolved signerAddr

      nettest :: AddressOrAlias
      nettest = AddressResolved nettestAddr

      bytes :: ByteString
      bytes = "some bytestring"

    signerAlias <- getAlias signer
    sig <- signBinary bytes signerAlias
    signerPK <- getPublicKey signer
    nettestPK <- getPublicKey nettest

    expectFailure (call helperAddr CallDefault (nettestPK, (sig, bytes))) $
      NettestFailedWith helperAddr [L.mt|Invalid signature|]
    call helperAddr CallDefault (signerPK, (sig, bytes))
