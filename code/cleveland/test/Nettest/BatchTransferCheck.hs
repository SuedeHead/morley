-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.BatchTransferCheck
  ( test_SimpleTransfer
  , test_BatchTransferLeapingCosts
  ) where

import Test.Tasty

import Lorentz ((:->), ( # ))
import qualified Lorentz as L
import Lorentz.Value
import Morley.Nettest
import Morley.Nettest.Tasty


test_SimpleTransfer :: TestTree
test_SimpleTransfer =
  nettestScenarioCaps "Check the batch transaction correctness" $ do
    nettestAddr :: Address <- resolveNettestAddress
    testAddr1 :: Address <- newFreshAddress "test1"
    testAddr2 :: Address <- newFreshAddress "test2"

    let
      nettest :: AddressOrAlias
      nettest = AddressResolved nettestAddr

      test1 :: AddressOrAlias
      test1 = AddressResolved testAddr1

      test2 :: AddressOrAlias
      test2 = AddressResolved testAddr2

      td =
        [ TransferData
          { tdFrom = nettest
          , tdTo = test1
          , tdAmount = toMutez 100
          , tdEntrypoint = DefEpName
          , tdParameter = ()
          }

        , TransferData
          { tdFrom = nettest
          , tdTo = test1
          , tdAmount = toMutez 200
          , tdEntrypoint = DefEpName
          , tdParameter = ()
          }

        , TransferData
          { tdFrom = nettest
          , tdTo = test2
          , tdAmount = toMutez 300
          , tdEntrypoint = DefEpName
          , tdParameter = ()
          }
        ]

    comment "balance is updated after batch transfer"
    transferBatch nettest td
    checkBalance test1 (toMutez 300)
    checkBalance test2 (toMutez 300)

-- | Add given element on stack once on first invocation, and 10k times on
-- subsequent invocations.
leapingContract :: forall a. L.NiceParameterFull a => L.Contract a [a]
leapingContract = L.defaultContract $
  L.unpair #
  L.duupX @2 # L.size # L.int # L.ifEq0 (L.push 1) (L.push 5000) #
  lIterate (L.dup @a # L.dip L.cons) #
  L.drop @a #
  L.nil # L.pair
  where
    lIterate :: s :-> s -> Natural : s :-> s
    lIterate f =
      decrease #
      L.loop (L.dip f # decrease) #
      L.drop @Natural

    decrease :: Natural : s :-> Bool : Natural : s
    decrease =
      L.push @Natural 1 # L.rsub # L.isNat #
      L.ifSome (L.push True) (L.push 999 # L.push False)

-- | Even in case when transactions in batch have very different costs,
-- and costs of subsequent transactions is affected by previous transactions,
-- everything works as expected.
test_BatchTransferLeapingCosts :: TestTree
test_BatchTransferLeapingCosts =
  nettestScenarioCaps "Check fees evaluation correctness for leaping transfer costs" $ do
    nettestAddr :: Address <- resolveNettestAddress

    let
      nettest :: AddressOrAlias
      nettest = AddressResolved nettestAddr

    contract <- originateSimple "contract" [] (leapingContract @L.MText)

    comment "Perform batch transfer, second transaction should have much \
            \higher cost with respect to the first one"
    transferBatch nettest $ do
      param <- [[mt|a|], [mt|b|]]
      return TransferData
        { tdFrom = nettest
        , tdTo = AddressResolved $ toAddress contract
        , tdAmount = toMutez 0
        , tdEntrypoint = DefEpName
        , tdParameter = param
        }

    comment "Sanity check on storage"
    checkStorage (AddressResolved $ toAddress contract) $
      toVal (replicate 5000 [mt|b|] ++ one [mt|a|])
