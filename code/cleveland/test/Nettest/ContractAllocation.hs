-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.ContractAllocation
  ( test_ContractAllocation
  ) where

import Test.Tasty

import Lorentz (EntrypointRef(CallDefault), TAddress)
import Tezos.Address (Address)
import Tezos.Core (toMutez)
import Util.Named

import Morley.Nettest
import Morley.Nettest.Tasty
import Nettest.Lorentz.Contracts.ContractAllocator

test_ContractAllocation :: TestTree
test_ContractAllocation =
  nettestScenarioCaps "Contracts can be allocated and called" $ do
    nettestAddr :: Address <- resolveNettestAddress
    addresses :: [Address] <- mapM newFreshAddress $ aliasesSeries 10 "addr"
    let
      nettest :: AddressOrAlias
      nettest = AddressResolved nettestAddr

      od = OriginateData
        { odFrom = nettest
        , odName = "allocator"
        , odStorage = #storage .! (take 570 $ cycle addresses)
        , odBalance = toMutez 25
        , odContract = allocatorContract
        }

    allocatorAddr :: TAddress [Address] <- originate od

    call allocatorAddr CallDefault (take 25 $ cycle addresses)
