-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.NewAddressCheck
  ( test_AddressesReuse
  , test_NewAddressCheck
  ) where

import Test.Tasty

import Michelson.Untyped.Entrypoints
import Tezos.Address (Address)
import Tezos.Core

import Morley.Nettest
import Morley.Nettest.Tasty

test_AddressesReuse :: TestTree
test_AddressesReuse =
  -- If we start hitting this check, then we have to revise the fact that
  -- 'newAddress' does not always put money on the generated address.
  -- If probability of generating the same address twice is low, then
  -- we better always blindly transfer some money to new addresses.
  nettestScenarioCaps "Address generation is deterministic" $ do
    addr1 <- newAddress "test"
    addr2 <- newAddress "test"

    addr1 @== addr2

test_NewAddressCheck :: [TestTree]
test_NewAddressCheck =
  [ nettestScenarioCaps "Newly created address gets money if it had low balance" $ do
      nettestAddr :: Address <- resolveNettestAddress
      testAddr :: Address <- newAddress "test"
      let
        test :: AddressOrAlias
        test = AddressResolved testAddr

        nettest :: AddressOrAlias
        nettest = AddressResolved nettestAddr

      balance <- getBalance test
      let
        -- Prepare to spend most of the money (about 4/5 of the current balance)
        (toRemain, _) = balance `divModMutezInt` (5 :: Int) ?: error "Bad div"
        toSpend = balance `unsafeSubMutez` toRemain

      let
        td = TransferData
          { tdFrom = test
          , tdTo = nettest
          , tdAmount = toSpend
          , tdEntrypoint = DefEpName
          , tdParameter = ()
          }
      transfer td

      balance2 <- getBalance test
      assert (balance2 <= toRemain) $
        "Sanity check failed. Something went wrong, is test broken?"

      -- creating the same address to check how balance replenishes
      _ <- newAddress "test"

      balance3 <- getBalance test
      checkCompares balance3 (>) toRemain

  , nettestScenarioCaps "Newly created address does not get money if it has enough" $ do
      testAddr :: Address <- newAddress "test"
      let
        test :: AddressOrAlias
        test = AddressResolved testAddr

      balance1 <- getBalance test

      -- creating the same address to check that no extra money are put on it
      _ <- newAddress "test"

      balance2 <- getBalance test
      balance1 @== balance2
  ]
