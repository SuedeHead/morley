-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.TransferCheck
  ( test_TransferFromContract
  ) where

import Test.Hspec.Expectations (shouldContain)
import Test.Tasty
import Test.Tasty.Runners (resultDescription)

import Lorentz (( # ))
import qualified Lorentz as L
import Lorentz.Value
import Morley.Nettest

import Test.Util (outcomeIsFailure, runViaTastyOnEmulator, runViaTastyOnNetwork)

test_TransferFromContract :: [TestTree]
test_TransferFromContract =
  [ runViaTastyOnEmulator "Disallow transferring tz when releaving an implicit account (#440)" mempty testRevealImplicitAccount
    \tastyResult -> do
      outcomeIsFailure tastyResult
      resultDescription tastyResult `shouldContain` morleyMessage
  , runViaTastyOnNetwork  "Disallow transferring tz when releaving an implicit account (#440)" mempty testRevealImplicitAccount
    \tastyResult -> do
      outcomeIsFailure tastyResult
      resultDescription tastyResult `shouldContain` "Implicit accounts (rx) cannot be revealed"

  , runViaTastyOnEmulator "Disallow transferring tz from an empty implicit account (#440)" mempty testEmptyImplicitAccount
    \tastyResult -> do
      outcomeIsFailure tastyResult
      resultDescription tastyResult `shouldContain` morleyMessage
  , runViaTastyOnNetwork  "Disallow transferring tz from an empty implicit account (#440)" mempty testEmptyImplicitAccount
    \tastyResult -> do
      outcomeIsFailure tastyResult
      resultDescription tastyResult `shouldContain` "Empty implicit contract"
  ]
  where
    td :: TAddress () -> TransferData
    td contractAddr = TransferData
      { tdFrom = addressResolved contractAddr
      , tdTo = nettestAddress
      , tdAmount = toMutez 200
      , tdEntrypoint = DefEpName
      , tdParameter = ()
      }

    morleyMessage :: String
    morleyMessage = "Global transaction of funds (200 μꜩ) from an originated contract"

    testRevealImplicitAccount :: MonadNettest caps base m => m ()
    testRevealImplicitAccount = do
      contractAddr :: TAddress a <- originateSimple "rx" () testContract

      comment "give some funds to the originated contract"
      transfer TransferData
        { tdFrom = nettestAddress
        , tdTo = addressResolved contractAddr
        , tdAmount = toMutez 200
        , tdEntrypoint = DefEpName
        , tdParameter = ()
        }

      comment "fail when transferring from contract"
      transfer $ td contractAddr

    testEmptyImplicitAccount :: MonadNettest caps base m => m ()
    testEmptyImplicitAccount = do
      testAddr :: Address <- newFreshAddress "test"
      contractAddr :: TAddress a <- originate OriginateData
        { odFrom = AddressResolved testAddr
        , odName = "rx"
        , odStorage = ()
        , odBalance = toMutez 500
        , odContract = testContract
        }

      comment "fail when transferring from contract"
      transfer $ td contractAddr

testContract :: L.Contract () ()
testContract = L.defaultContract $ L.car # L.nil @L.Operation # L.pair
