-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.AddressRevealing
  ( test_AddressRevealing
  ) where

import Test.Tasty

import Michelson.Untyped.Entrypoints
import Tezos.Address (Address)
import Tezos.Core (toMutez)

import Morley.Nettest
import Morley.Nettest.Tasty

test_AddressRevealing :: TestTree
test_AddressRevealing =
  nettestScenarioCaps "New address key is revealed" $ do
    nettestAddr :: Address <- resolveNettestAddress
    testAddr :: Address <- newFreshAddress "test"
    let
      test :: AddressOrAlias
      test = AddressResolved testAddr

      nettest :: AddressOrAlias
      nettest = AddressResolved nettestAddr
      td = TransferData
        { tdFrom = nettest
        , tdTo = test
        , tdAmount = toMutez 1000
        , tdEntrypoint = DefEpName
        , tdParameter = ()
        }
      tdReverse = td { tdFrom = test
                     , tdTo = nettest
                     , tdAmount = toMutez 1
                     }
    transfer td
    comment "new key address is revealed"
    transfer tdReverse
