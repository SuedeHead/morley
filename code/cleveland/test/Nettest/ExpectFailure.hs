-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.ExpectFailure
  ( test_CorrectlyHandlesExceptions
  , test_FailsIfTheGivenActionDoesNotThrow
  ) where

import Fmt (pretty)
import Test.Hspec.Expectations (shouldContain)
import Test.Tasty
import Test.Tasty.Runners (resultDescription)

import Lorentz as L hiding (comment)

import Michelson.Test.Integrational (TestError(ExpectingInterpreterToFail))
import Morley.Nettest
import Morley.Nettest.Client (TestError(UnexpectedClientSuccess))
import Morley.Nettest.Tasty

import Test.Util (outcomeIsFailure, runViaTastyOnEmulator, runViaTastyOnNetwork)

type Storage = ()

test_CorrectlyHandlesExceptions :: TestTree
test_CorrectlyHandlesExceptions =
  nettestScenarioCaps "correctly handles exceptions" $ do
    nettestAddr <- resolveNettestAddress

    contractAddr :: TAddress Parameter <- originate OriginateData
      { odFrom = nettestAddress
      , odName = "test expectFailure"
      , odStorage = ()
      , odBalance = toMutez 1
      , odContract = defaultContract testContractCode
      }

    let
      transfer'
        :: (MonadNettest caps base m, ToAddress addr)
        => EpName -> NiceParameter v => v -> addr -> m ()
      transfer' epName param to = transfer TransferData
        { tdFrom = nettestAddress
        , tdTo = addressResolved to
        , tdAmount = toMutez 0
        , tdEntrypoint = epName
        , tdParameter = param
        }

      validAddr = nettestAddr

    comment "test triggering error scenarios"
    transfer' (ep "") TriggerFailWith contractAddr `expectFailure`
      NettestFailedWith contractAddr [mt|oops|]
    transfer' (ep "") TriggerShiftOverflow contractAddr `expectFailure`
      NettestShiftOverflow contractAddr
    transfer' (ep "") (ExpectAddress validAddr) nettestAddr `expectFailure`
      NettestEmptyTransaction nettestAddr
    transfer' (ep "") (VoidEP (mkVoid True)) contractAddr `expectFailure`
      NettestFailedWithError (VoidResult False)

    comment "test passing invalid parameters"
    transfer' (ep "expectAddress") validAddr contractAddr
    transfer' (ep "expectAddress") [mt|aa|] contractAddr `expectFailure` NettestBadParameter
    transfer' (ep "expectAddress") (1 :: Natural) contractAddr `expectFailure` NettestBadParameter

    comment "test numeric errors"
    let (testContractWithNumericErrors, errorTagMap) = useNumericErrors testContractCode
    contractNumericAddr :: TAddress Parameter <- originate OriginateData
      { odFrom = nettestAddress
      , odName = "test expectFailure numeric errors"
      , odStorage = ()
      , odBalance = toMutez 1
      , odContract = defaultContract testContractWithNumericErrors
      }

    transfer' (ep "") (VoidEP (mkVoid True)) contractNumericAddr `expectFailure`
      NettestFailedWithNumericError errorTagMap (VoidResult False)

test_FailsIfTheGivenActionDoesNotThrow :: TestTree
test_FailsIfTheGivenActionDoesNotThrow =
  testGroup "fails if the given action does not throw"
    [ runViaTastyOnEmulator "On emulator" mempty
        do
          pass `expectFailure` NettestBadParameter
        \tastyResult -> do
          outcomeIsFailure tastyResult
          resultDescription tastyResult `shouldContain` pretty ExpectingInterpreterToFail

    , runViaTastyOnNetwork "On network" mempty
        do
          pass `expectFailure` NettestBadParameter
        \tastyResult -> do
          outcomeIsFailure tastyResult
          resultDescription tastyResult `shouldContain` pretty UnexpectedClientSuccess
    ]

data Parameter
  = ExpectAddress Address
    -- ^ Entrypoint that expects an Address as an argument
  | TriggerFailWith
    -- ^ Entrypoint that always fails with the string "oops"
  | TriggerShiftOverflow
    -- ^ Entrypoint that always fails with a shift overflow error.
  | VoidEP (Void_ Bool Bool)
    -- ^ A Void entrypoint
  deriving stock Generic
  deriving anyclass IsoValue

instance ParameterHasEntrypoints Parameter where
  type ParameterEntrypointsDerivation Parameter = EpdPlain

-- | A contract that triggers a variety of error scenarios
-- depending on the argument it's given.
testContractCode :: ContractCode Parameter Storage
testContractCode =
  car #
  entryCaseSimple @Parameter
    ( #cExpectAddress /-> L.drop # push () # nil # pair
    , #cTriggerFailWith /-> push [mt|oops|] # failWith
    , #cTriggerShiftOverflow /->
        push @Natural 257 #
        push @Natural 1 #
        lsl #
        L.drop # push () # nil @Operation # pair
    , #cVoidEP /-> void_ L.not
    )
