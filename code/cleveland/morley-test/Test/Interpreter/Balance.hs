-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Interpreter.Balance
  ( test_balanceIncludesAmount
  , test_balanceIncludesAmountComplexCase
  ) where

import Hedgehog (Gen, forAll, property, withTests)
import qualified Hedgehog.Gen as Gen
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Michelson.Test (testTreesWithUntypedContract)
import Michelson.Typed
import qualified Michelson.Untyped as U
import Morley.Nettest
import Morley.Nettest.Tasty
import Tezos.Core

import Test.Util.Contracts

data Fixture =
  Fixture
    { fStartingBalance :: Mutez
    , fAmount :: Mutez
    } deriving stock (Show)

genFixture :: Gen Fixture
genFixture = do
  fStartingBalance <- unsafeMkMutez <$> Gen.enum 1000 5000
  fAmount <- unsafeMkMutez <$> Gen.enum 0 1000
  return Fixture{..}

test_balanceIncludesAmount :: IO [TestTree]
test_balanceIncludesAmount = do
  testTreesWithUntypedContract
    (inContractsDir "check_if_balance_includes_incoming_amount.tz") $
      \checker ->
        pure
          [ testProperty "BALANCE includes AMOUNT" $ withTests 50 $ property $ do
              fixture <- forAll genFixture
              nettestTestProp $ nettestBalanceTestScenario
                checker fixture
          ]

nettestBalanceTestScenario :: U.Contract -> Fixture -> NettestScenario m
nettestBalanceTestScenario checker Fixture{..} = uncapsNettest $ do
  let result = unsafeAddMutez fStartingBalance fAmount

  let
    uoData = UntypedOriginateData
      { uodFrom = nettestAddress
      , uodName = "checkIfBalanceIncludeAmount"
      , uodBalance = fStartingBalance
      , uodStorage = (untypeValue $ toVal ())
      , uodContract = checker
      }

  address <- addressResolved <$> originateUntyped uoData
  let
    transferData = TransferData
      { tdFrom = nettestAddress
      , tdTo = address
      , tdAmount = fAmount
      , tdEntrypoint = DefEpName
      , tdParameter = result
      }

  transfer transferData
  checkBalance address result

test_balanceIncludesAmountComplexCase :: IO [TestTree]
test_balanceIncludesAmountComplexCase = do
  testTreesWithUntypedContract (inContractsDir "balance_test_case_a.tz") $ \contractA ->
    testTreesWithUntypedContract (inContractsDir "balance_test_case_b.tz") $ \contractB ->
      pure
        [ nettestScenarioCaps "BALANCE returns expected value in nested calls" $ do
            let
              origDataA = UntypedOriginateData
                { uodFrom = nettestAddress
                , uodName = "balance_test_case_a"
                , uodBalance = (unsafeMkMutez 0)
                , uodStorage = (untypeValue $ toVal @[Mutez] [])
                , uodContract = contractA
                }
            let
              origDataB = UntypedOriginateData
                { uodFrom = nettestAddress
                , uodName = "balance_test_case_b"
                , uodBalance = (unsafeMkMutez 0)
                , uodStorage = (untypeValue $ toVal ())
                , uodContract = contractB
                }
            addressA <- addressResolved <$> originateUntyped origDataA
            addressB <-  originateUntyped origDataB
            let
              transferData = TransferData
                { tdFrom = nettestAddress
                , tdTo = addressA
                , tdAmount = unsafeMkMutez 100
                , tdEntrypoint = DefEpName
                , tdParameter = addressB
                }

            transfer transferData

            -- A sends 30 to B, then B sends 5 back to A. A records call to BALANCE at each entry.
            -- We expect that 5 mutez sent back are included in the second call to BALANCE.
            let expectedStorage = toVal [unsafeMkMutez 75, unsafeMkMutez 100]
            checkStorage addressA expectedStorage
        ]
