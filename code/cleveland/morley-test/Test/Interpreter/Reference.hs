-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
module Test.Interpreter.Reference
  ( test_InterpreterWithReferenceImplementation
  ) where

import Fmt (pretty)
import Hedgehog
  (MonadTest, annotate, evalEither, evalIO, forAll, property, withTests, (===))
import System.IO.Silently (silence)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Michelson.Typed (genValue)
import Michelson.Interpret (InterpretError(..), MichelsonFailed(..))
import Michelson.Runtime as Morley
import Michelson.Runtime.GState (genesisAddress)
import Michelson.Test.Import
import Michelson.TypeCheck (SomeContract(..), getWTP, typeVerifyStorage)
import Michelson.Typed (Contract(..), Dict(..), EpAddress(..), Value, Value'(..), dfsModifyValue)
import Michelson.Typed.Arith (ArithError(..), MutezArithErrorType(..))
import Michelson.Typed.Convert
import Michelson.Typed.Scope
import qualified Michelson.Untyped as U
import Morley.Client (RunError(..), runMorleyClientM)
import Morley.Client.RPC.Error (RunCodeErrors(..))
import Morley.Client.Util as Reference (runContract)
import Test.Util.Contracts (getWellTypedMichelsonContracts)
import Tezos.Core (Timestamp(..), dummyChainId, getCurrentTime, unsafeMkMutez)
import Util.Named

import Cleveland.Util (ShowWith(ShowWith), failedTest)
import Morley.Nettest (NettestEnv(neMorleyClientEnv))
import Morley.Nettest.Tasty (whenNetworkEnabled)

test_InterpreterWithReferenceImplementation :: IO TestTree
test_InterpreterWithReferenceImplementation = do
  files <- filter (`notElem` excludedContracts) <$> getWellTypedMichelsonContracts

  filesAndContracts :: [(FilePath, SomeContract)] <-
    forM files $ \file -> do
      someContract <- importSomeContract file
      pure (file, someContract)

  pure $
    whenNetworkEnabled $ \withEnv ->
      testGroup "compare interpreter with reference implementation" $
        mapMaybe (withFrozenCallStack $ testContract withEnv) filesAndContracts
  where
    -- These constracts are currently excluded from the tests since
    -- our or reference implementations doesn't act in an expected way.
    --
    -- Ideally, we should remove the usage of this list one day :coolstory:
    excludedContracts :: [FilePath]
    excludedContracts =
      [ "../../contracts/tezos_examples/mini_scenarios/replay.tz"
      -- ↑ See [#320] and https://gitlab.com/tezos/tezos/-/issues/897
      , "../../contracts/call_self_several_times.tz"
      , "../../contracts/tezos_examples/attic/cps_fact.tz"
      -- ↑ Due to that fact that 'run_code' doesn't perform internal operations in contrary to morley interpreter
      , "../../contracts/tezos_examples/mini_scenarios/multiple_entrypoints_counter.tz"
      , "../../contracts/tezos_examples/mini_scenarios/multiple_en2.tz"
      -- ↑ 'SELF' and 'SENDER' instruction return same addresses in 'run_code'
      , "../../contracts/tezos_examples/mini_scenarios/lockup.tz"
      , "../../contracts/tezos_examples/attic/forward.tz"
      -- ↑ Reference 'run_code' implementation does something weird to allow transfers with arbitrary
      -- amounts even when BALANCE + AMOUNT < required amount of money that needs to be transfered.
      -- This probably will be resolved in https://gitlab.com/tezos/tezos/-/issues/897, when 'run_code'
      -- will actually try to apply operation instead of running it inside weird environment.
      -- Or in [#406] if the issue turns to be on our side.
     ]

testContract :: HasCallStack => (forall a. (NettestEnv -> IO a) -> IO a) -> (FilePath, SomeContract) -> Maybe TestTree
testContract withEnv (file, someContract) =
  case someContract of
    (SomeContract (contract@Contract {} :: Contract cp st)) ->
      case checkBigMapPresence (sing @st) of
        -- Bigmaps are handled differently in RPC, they're stored as a natural id in the storage
        -- and their updates are handled separately. We don't support such functionality in morley
        -- yet, so we don't check contracts with bigmaps to simplify the testing.
        BigMapPresent -> Nothing
        -- We run each contract 4 times to check that it behaves the same way on different
        -- inputs.
        BigMapAbsent ->
          Just $ testProperty ("compare result with morley interpreter for " <> file) $
            withTests 4 $ property $ do
              -- TODO [#392]: this check have to become unnecessary
              Dict <- evalEither $ first (ShowWith pretty) $ getWTP @cp
              Dict <- evalEither $ first (ShowWith pretty) $ getWTP @st

              parameter <- forAll $ genValue @cp
              storage <- forAll $ genValue @st

              (resReference, resMorley) <-
                evalIO $
                  withEnv $ \env -> do
                    resReference <- try @_ @RunCodeErrors $
                      runMorleyClientM (neMorleyClientEnv env) $ Reference.runContract @cp @st contract parameter storage
                    currentTimestamp <- getCurrentTime
                    -- Reference implementation sends 0,05 tz to implicit contract for this contract
                    let amount = if file == "../../contracts/tezos_examples/opcodes/proxy.tz"
                                then unsafeMkMutez 50000 else minBound
                    resMorley <- try @_ @ExecutorError $ silence $
                      Morley.runContract (Just currentTimestamp) 100500 (unsafeMkMutez 4000000000000) ""
                      (untypeValue storage) (convertContract contract)
                      (TxData genesisAddress (TxUntypedParam $ untypeValue parameter) U.DefEpName amount)
                      (#verbose .! False)
                      (#dryRun .! True)

                    pure (resReference, resMorley)

              compareResults resReference resMorley parameter storage

compareResults
      :: (HasCallStack, ParameterScope cp, StorageScope st, MonadTest m)
      => Either RunCodeErrors (Value st)
      -> Either ExecutorError U.Value -> Value cp -> Value st -> m ()
compareResults (Left rpcErr) (Left interpreterErr) _ _ = compareErrors rpcErr interpreterErr
compareResults (Left err) (Right _) parameter storage = do
  failedTest . fromString $
    "Morley interpreter unexpectedly didn't fail.\n Passed parameter: " <> pretty parameter <>
    ".\n Passed storage: " <> pretty storage <> ".\n Reference implementation failed with: " <>
    pretty err
compareResults (Right _) (Left err) parameter storage = do
  failedTest . fromString $
    "Morley interpreter unexpectedly failed.\n " <> "Passed parameter: " <> pretty parameter <>
    ".\n Passed storage: " <> pretty storage <> ".\n Morley interpreter failed with: " <>
    displayException err
compareResults (Right (st1 :: Value st)) (Right uSt2) parameter storage = do
  case typeVerifyStorage @st uSt2 of
    Left _ -> do
      failedTest . fromString $ "Resulted storages have different types.\n" <>
        "Reference implementation returned storage: " <> pretty st1 <> ".\n" <>
        "Morley returned storage: " <> pretty uSt2
    Right st2 -> do
      annotate $
        ("Both contracts succeeded, but new storages are different.\n Passed parameter: " <>
          pretty parameter <> ".\n Passed storage: " <> pretty storage <> ".\n" <>
          "Reference implementation returned storage: " <> pretty st1 <> ".\n" <>
          "Morley returned storage: " <> pretty st2
        )
      compareValues st1 st2

-- | Compare values with weaken equality requirements, e.g. we don't
-- check that @VAddress@, @VBytes@, @VChainId@ and @VTimestamp@ have same constructor arguments,
-- since they can be different even when both interpreters were successfully run
compareValues :: (HasCallStack, MonadTest m) => Value st -> Value st -> m ()
compareValues st1 st2 = preprocessValue st1 === preprocessValue st2
  where
    preprocessValue :: Value t -> Value t
    preprocessValue = dfsModifyValue placeStubs

    placeStubs :: Value t -> Value t
    placeStubs = \case
      VAddress _ -> VAddress $ EpAddress genesisAddress U.DefEpName
      VBytes _ -> VBytes "kek"
      VChainId _ -> VChainId dummyChainId
      VTimestamp _ -> VTimestamp $ Timestamp 100
      v -> v


assertRpcErrs :: (HasCallStack, MonadTest m) => [RunError] -> (RunError -> Bool) -> String -> m ()
assertRpcErrs errs predicate msg =
  if any predicate errs then pass else do failedTest $ fromString msg

-- Note that error comparison can be extended when the new contracts will be added.
compareErrors :: (HasCallStack, MonadTest m) => RunCodeErrors -> ExecutorError -> m ()
compareErrors rpcErr@(RunCodeErrors errs) interpreterErr = case interpreterErr of
  EEInterpreterFailed _ (InterpretError (runtimeErr, _)) -> case runtimeErr of
    MichelsonFailedWith _ ->
      assertRpcErrs errs
      (\case
          ScriptRejected {} -> True
          _ -> False
      ) $ "Morley interpreter failed with FAILWITH, \
          \however reference interpreter failed with:\n" <> displayException rpcErr
    MichelsonArithError arithErr -> case arithErr of
      ShiftArithError {}->
        assertRpcErrs errs
        (\case
            ScriptOverflow -> True
            _ -> False
        ) $ "Morley interpreter failed with shift overflow, \
            \however reference interpreter failed with:\n" <> displayException rpcErr
      MutezArithError AddOverflow _ _ ->
        assertRpcErrs errs
        (\case
            MutezAdditionOverflow {} -> True
            _ -> False
        ) $ "Morley interpreter failed with mutez addition overflow, \
            \however reference interpreter failed with:\n" <> displayException rpcErr
      MutezArithError MulOverflow _ _ ->
        assertRpcErrs errs
        (\case
            MutezMultiplicationOverflow {} -> True
            ScriptOverflow -> True
            _ -> False
        ) $ "Morley interpreter failed with mutez multiplication overflow, \
            \however reference interpreter failed with:\n" <> displayException rpcErr
      MutezArithError SubUnderflow _ _ ->
        assertRpcErrs errs
        (\case
            MutezSubtractionUnderflow {} -> True
            _ -> False
        ) $ "Morley interpreter failed with mutez subtraction underflow, \
            \however reference interpreter failed with:\n" <> displayException rpcErr
    MichelsonGasExhaustion ->
      assertRpcErrs errs
      (\case
          GasExhaustedOperation -> True
          _ -> False
      ) $ "Morley interpreter failed due to gas exhaustion, \
          \however reference interpreter failed with:\n" <> displayException rpcErr
    _ -> do
      failedTest . fromString $ "Unexpected morley runtime failure:\n" <> pretty runtimeErr <>
          "\nReference interpreter failed with:\n" <> displayException rpcErr
  EEIllTypedParameter _ ->
    assertRpcErrs errs
    (\case
        InconsistentTypes {} -> True
        BadContractParameter {} -> True
        _ -> False
    ) $ "Morley interpreter failed during parmeter typechecking, \
        \however reference interpreter failed with:\n" <> displayException rpcErr
  _ -> do
    failedTest . fromString $ "Unexpected morley interpreter failure:\n" <> displayException interpreterErr <>
      "\nReference interpreter failed with:\n" <> displayException rpcErr
