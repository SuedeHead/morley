-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Interpreter tests involving 'StackRef'.

module Test.Interpreter.StackRef
  ( test_mkStackRef
  ) where

import Data.Default (def)
import Test.Hspec.Expectations (shouldSatisfy)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Michelson.Test (contractProp)
import Michelson.Test.Dummy (dummyContractEnv)
import Michelson.Typed

test_mkStackRef :: TestTree
test_mkStackRef =
  testCase "does not segfault" $
    contractProp contract (flip shouldSatisfy isRight . fst)
    dummyContractEnv () ()
  where
    stackRef = PrintComment . one . Right $ mkStackRef @1

    contract :: Contract 'TUnit 'TUnit
    contract = Contract
      { cCode = contractCode
      , cStoreNotes = starNotes
      , cParamNotes = starParamNotes
      , cEntriesOrder = def
      }

    contractCode :: ContractCode 'TUnit 'TUnit
    contractCode =
      CAR `Seq` DUP `Seq` Ext (PRINT stackRef) `Seq`
      DROP `Seq` NIL `Seq` PAIR
