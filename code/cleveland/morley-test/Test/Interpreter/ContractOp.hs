-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module, containing spec to test contract_op.tz contract.
module Test.Interpreter.ContractOp
  ( test_contract_op
  ) where

import qualified Data.Map as M
import Fmt (pretty)
import Hedgehog (MonadTest, property, (===))
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Michelson.Interpret (ContractEnv(..), ContractReturn)
import Michelson.Test (contractProp, dummyContractEnv, failedTest, testTreesWithTypedContract)
import Michelson.Typed (Contract, ToT, fromVal)
import Michelson.Untyped (ParameterType(..), T(..), Type(..), noAnn)
import Michelson.TypeCheck (SomeParamType(..), mkSomeParamTypeUnsafe)
import Tezos.Address

import Test.Util.Contracts

-- | Spec to test @contract_op.tz@ contract.
--
-- Test results are confirmed by the reference implementation.
test_contract_op :: IO [TestTree]
test_contract_op =
  testTreesWithTypedContract (inContractsDir "contract_op.tz") $ \contract -> pure $
  [ testProperty "contract not found" $ property $
      contractProp' False [] contract
  ] <>
  map (\(res, paramType) ->
          testProperty (msg res paramType) $ property $
          contractProp' res [(addr, paramType)] contract
      )
  [ (True, mkSomeParamTypeUnsafe $ ParameterType intQ "root")
  , (True, mkSomeParamTypeUnsafe $ ParameterType int "root")
  , (False, mkSomeParamTypeUnsafe $ ParameterType intQ noAnn)
  , (False, mkSomeParamTypeUnsafe $ ParameterType int noAnn)
  , (False, mkSomeParamTypeUnsafe $ ParameterType intP noAnn)
  , (False, mkSomeParamTypeUnsafe $ ParameterType string noAnn)
  , (False, mkSomeParamTypeUnsafe $ ParameterType intP "root")
  , (False, mkSomeParamTypeUnsafe $ ParameterType intQ "another_root")
  ]
  where
    msg isGood paramType =
      "parameter in environment is '" <> pretty paramType <> "', " <>
      bool "" "but "  isGood <> "contract expects '%root int :q'"

    intQ = Type TInt "q"
    int = Type TInt noAnn
    intP = Type TInt "p"
    string = Type TString noAnn

    addr = unsafeParseContractHash "KT1WsLzQ61xtMNJHfwgCHh2RnALGgFAzeSx9"

    validate
      :: MonadTest m
      => Bool
      -> ContractReturn (ToT Bool)
      -> m ()
    validate ex (Right ([], fromVal -> l), _) = l === ex
    validate _ (Left _, _) = failedTest "Unexpected fail in interepreter"
    validate _ _ = failedTest "Unexpected result of script execution"

    contractProp'
      :: MonadTest m
      => Bool -> [(ContractHash, SomeParamType)] -> Contract (ToT Address) (ToT Bool)
      -> m ()
    contractProp' res ctrs contract =
      contractProp
        contract
        (validate res)
        dummyContractEnv {ceContracts = M.fromList ctrs}
        (ContractAddress addr)
        False
