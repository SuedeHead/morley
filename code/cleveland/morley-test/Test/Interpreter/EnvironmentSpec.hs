-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for the 'environment.tz' contract

module Test.Interpreter.EnvironmentSpec
  ( test_environment
  ) where

import Hedgehog (Gen, forAll, property, withTests)
import qualified Hedgehog.Gen as Gen
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Michelson.Runtime.GState
import Michelson.Test (testTreesWithContract)
import Michelson.Typed
import qualified Michelson.Typed as T
import qualified Michelson.Untyped as U
import Morley.Nettest
import Morley.Nettest.Tasty
import Tezos.Core

import Test.Util.Contracts

test_environment :: IO [TestTree]
test_environment =
  testTreesWithContract (inContractsDir "environment.tz") $
  \contract -> pure
    [ nettestImpl contract
    , nettestScenarioCaps "Default balance" $ do
        addr <- newFreshAddress "random address testDefaultBalance"
        let
          transferData = TransferData
            { tdFrom = nettestAddress
            , tdTo = AddressResolved addr
            , tdAmount = unsafeMkMutez 1
            , tdEntrypoint = DefEpName
            , tdParameter = ()
            }

        transfer transferData
        checkBalance (AddressResolved addr) (unsafeMkMutez 1)
    ]

data Fixture = Fixture
  { fPassOriginatedAddress :: Bool
  , fBalance :: Mutez
  , fAmount :: Mutez
  } deriving stock (Show)

genFixture :: Gen Fixture
genFixture = do
  fPassOriginatedAddress <- Gen.bool
  fBalance <- unsafeMkMutez <$> Gen.enum 1 1234
  fAmount <- unsafeMkMutez <$> Gen.enum 1 42
  return Fixture {..}

nettestImpl
  :: (U.Contract, T.Contract 'TAddress 'TUnit)
  -> TestTree
nettestImpl (uEnvironment, _environment)  = do
  -- The conditions under which this contract fails are described in a comment
  -- at the beginning of the contract.
  testProperty "contract fails under certain conditions" $
    withTests 50 $ property $ do
      fixture <- forAll genFixture
      nettestTestProp $ uncapsNettest $ do
        -- Let's originate the 'environment.tz' contract
        let
          uoData = UntypedOriginateData
            { uodFrom = nettestAddress
            , uodName = "environment"
            , uodBalance = fBalance fixture
            , uodStorage = U.ValueUnit
            , uodContract = uEnvironment
            }

        environmentAddress <- originateUntyped uoData

        -- And transfer tokens to it
        let
          param
            | fPassOriginatedAddress fixture = environmentAddress
            | otherwise = genesisAddress
          transferData = TransferData
            { tdFrom = nettestAddress
            , tdTo =  AddressResolved environmentAddress
            , tdAmount = fAmount fixture
            , tdEntrypoint = DefEpName
            , tdParameter = param
            }

        -- Execute operations and check that interpreter fails when one of
        -- failure conditions is met or updates environment's storage
        -- approriately
        let
          balanceAfterTransfer = fBalance fixture `unsafeAddMutez` fAmount fixture
        if
          | balanceAfterTransfer > unsafeMkMutez 1000 ->
              transfer transferData `expectFailure` NettestFailedWith environmentAddress balanceAfterTransfer
          | fPassOriginatedAddress fixture ->
              transfer transferData `expectFailure` NettestFailedWith environmentAddress environmentAddress
          | fAmount fixture < unsafeMkMutez 15 ->
              transfer transferData `expectFailure` NettestFailedWith environmentAddress (fAmount fixture)
          | otherwise -> transfer transferData
