-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for the 'stringCaller.tz' contract and its interaction with
-- the 'failOrStoreAndTransfer.tz' contract. Both of them have comments describing
-- their behavior.

module Test.Interpreter.StringCaller
  ( test_stringCaller
  ) where

import Hedgehog (forAll, property, withTests)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Michelson (genMText)
import Michelson.Test (testTreesWithContract)
import Michelson.Text
import Michelson.Typed
import qualified Michelson.Typed as T
import qualified Michelson.Untyped as U
import Morley.Nettest
import Morley.Nettest.Tasty
import Tezos.Address
import Tezos.Core

import Test.Util.Contracts

test_stringCaller :: IO [TestTree]
test_stringCaller =
  testTreesWithContract (inContractsDir "string_caller.tz") $ \stringCaller ->
  testTreesWithContract (inContractsDir "fail_or_store_and_transfer.tz") $
    \failOrStoreAndTransfer ->
      pure $ [testImpl stringCaller failOrStoreAndTransfer]

testImpl ::
     (U.Contract, T.Contract 'TString 'TAddress)
  -> (U.Contract, T.Contract 'TString 'TString)
  -> TestTree
testImpl (uStringCaller, _stringCaller) (uFailOrStore, _failOrStoreAndTransfer) =
  -- failOrStoreAndTransfer's pre-conditions:
  -- balance is >= 1300
  testGroup "calls failOrStoreAndTransfer, updates storage & balance, checks pre-conditions"
    [ nettestScenarioCaps "when parameter is a constant" $
        scenario uStringCaller uFailOrStore constStr

    -- The test is trivial, so it's kinda useless to run it many times
    , testProperty "when parameter is an arbitrary value" $
      withTests 2 $ property $ do
        mtext <- forAll genMText
        nettestTestProp $ uncapsNettest (scenario uStringCaller uFailOrStore mtext)
    ]
  where
    constStr = [mt|caller|]


scenario :: MonadNettest caps base m => U.Contract -> U.Contract -> MText -> m ()
scenario stringCaller failOrStoreAndTransfer str = do
  let
    initFailOrStoreBalance = unsafeMkMutez 900
    initStringCallerBalance = unsafeMkMutez 500

    failOrStoreData = UntypedOriginateData
      { uodFrom = nettestAddress
      , uodName = "failOrStoreAndTransfer"
      , uodBalance = initFailOrStoreBalance
      , uodStorage = U.ValueString [mt|hello|]
      , uodContract = failOrStoreAndTransfer
      }

  failOrStoreAddress <- originateUntyped failOrStoreData

  let
    stringCallerData = UntypedOriginateData
      { uodFrom = nettestAddress
      , uodName = "stringCaller"
      , uodBalance = initStringCallerBalance
      , uodStorage = U.ValueString $ mformatAddress failOrStoreAddress
      , uodContract = stringCaller
      }

  stringCallerAddress <- addressResolved <$> originateUntyped stringCallerData

  initialConstAddrBalance <- getBalance (AddressResolved constAddr)

  -- Transfer 100 tokens to stringCaller, it should transfer 300 tokens
  -- to failOrStoreAndTransfer
  let
    transferData = TransferData
      { tdFrom = nettestAddress
      , tdTo = stringCallerAddress
      , tdAmount = unsafeMkMutez 100
      , tdEntrypoint = DefEpName
      , tdParameter = str
      }

    transferToStringCaller = transfer transferData
  transferToStringCaller

  -- Execute operations and check balances and storage of 'failOrStore'
  do
    let
      -- `stringCaller.tz` transfers 300 mutez.
      -- 'failOrStoreAndTransfer.tz' transfers 5 tokens.
      -- Also 100 tokens are transferred from the nettest address.
      expectedStringCallerBalance = unsafeMkMutez (500 - 300 + 100)
      expectedFailOrStoreBalance = unsafeMkMutez (900 + 300 - 5)
      expectedConstAddrBalance = initialConstAddrBalance `unsafeAddMutez` unsafeMkMutez 5

    checkStorage (AddressResolved failOrStoreAddress) $ toVal @MText str
    checkBalance (AddressResolved failOrStoreAddress) expectedFailOrStoreBalance
    checkBalance stringCallerAddress expectedStringCallerBalance
    checkBalance (AddressResolved constAddr) expectedConstAddrBalance

  -- Now let's transfer 100 tokens to stringCaller again.
  -- This time execution should fail, because failOrStoreAndTransfer should fail
  -- because its balance is greater than 1300.
  transferToStringCaller `expectFailure` NettestFailedWith failOrStoreAddress ()

-- Address hardcoded in 'failOrStoreAndTransfer.tz'.
constAddr :: Address
constAddr = unsafeParseAddress "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU"
