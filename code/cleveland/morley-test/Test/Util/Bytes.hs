-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Utility functions for parsing bytestring used for test purposes.

module Test.Util.Bytes
  ( fromHex
  , fromHexUnsafe
  , stripOptional0x
  ) where

import qualified Data.Text as T
import Text.Hex (decodeHex)

stripOptional0x :: Text -> Text
stripOptional0x h = T.stripPrefix "0x" h ?: h

fromHex :: Text -> Maybe ByteString
fromHex = decodeHex . stripOptional0x

fromHexUnsafe :: HasCallStack => Text -> ByteString
fromHexUnsafe hex = fromHex hex
  ?: error ("Invalid hex: " <> show hex)
