-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Helpers for HUnit.

module Test.Util.HUnit
  ( assertEqualBuild
  ) where

import Test.HUnit (Assertion, assertEqual)
import Test.Util.Hedgehog (ShowThroughBuild(..))

assertEqualBuild
  :: (HasCallStack, Eq a, Show (ShowThroughBuild a))
  => String -> a -> a -> Assertion
assertEqualBuild desc a b =
  assertEqual desc (STB a) (STB b)
