-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on numeric utils.
module Test.Util.Num
  ( test_fromIntegralChecked
  ) where

import Test.HUnit ((@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Util.Num

test_fromIntegralChecked :: [TestTree]
test_fromIntegralChecked =
  [ testGroup "Conversion ok"
    [ testCase "Simple case" $
        fromIntegralChecked (5 :: Int) @?= Right (5 :: Word)
    ]
  , testGroup "Failure cases"
    [ testCase "Simple overflow" $
        fromIntegralChecked @Word64 @Word8 300 @?= Left "Numeric overflow"
    , testCase "Overflow to negative" $
        fromIntegralChecked @Word64 @Int8 200 @?= Left "Numeric overflow"
    , testCase "Underflow" $
        fromIntegralChecked @Int @Word (-200) @?= Left "Numeric underflow"
    ]

  ]
