-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for 'Morley.Micheline.Expression'.

module Test.Morley.Expression
  ( test_Roundtrip_binary
  , test_Roundtrip_expression
  , test_Roundtrip_JSON
  , test_ToJSON_omits_empty_lists
  , test_toExpression
  ) where

import Data.Aeson (FromJSON, ToJSON(toJSON), Value(String), eitherDecode, encode, object, (.=))
import qualified Data.Sequence as Seq
import Hedgehog (Gen)
import Test.Hspec.Expectations (shouldBe)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase, (@?=))

import Hedgehog.Gen.Michelson.Typed (genSimpleInstr, genValueInt, genValueMutez, genValueUnit)
import Hedgehog.Gen.Michelson.Untyped (genValidType)
import qualified Hedgehog.Gen.Morley.Micheline as M
import qualified Michelson.Typed as T
import qualified Michelson.Untyped as U
import qualified Morley.Micheline as M
import Test.Util.Hedgehog (roundtripTree)
import Tezos.Core (Mutez)


testJSON
  :: forall a. (Eq a, Show a, ToJSON a, FromJSON a, Typeable a)
  => Gen a -> TestTree
testJSON genA = roundtripTree @a genA encode eitherDecode

-- | Test that 'M.fromExpression' is inverse of 'M.toExpression'
roundtripExpression
  :: forall a. (Eq a, Show a, M.ToExpression a, M.FromExpression a, Typeable a)
  => Gen a -> TestTree
roundtripExpression genA = roundtripTree @a genA M.toExpression (M.fromExpression)

-- | Test that converting something to 'M.Expression', then to JSON,
-- then decoding this JSON and converting back to the original type
-- returns the original value.
roundtripExpressionJSON
  :: forall a. (Eq a, Show a, M.ToExpression a, M.FromExpression a, Typeable a)
  => Gen a -> TestTree
roundtripExpressionJSON genA = roundtripTree @a genA
  (encode . M.toExpression)
  (first displayException . M.fromExpression <=< eitherDecode)

testBinary :: Gen M.Expression -> TestTree
testBinary genA = roundtripTree genA M.encodeExpression M.eitherDecodeExpression

test_Roundtrip_JSON :: [TestTree]
test_Roundtrip_JSON =
  [ testJSON M.genExprAnnotation
  , testJSON M.genExpression
  , testJSON M.genMichelinePrimAp
  , roundtripExpressionJSON @(T.Value $ T.ToT Integer) genValueInt
  , roundtripExpressionJSON @(T.Value $ T.ToT Mutez) genValueMutez
  , roundtripExpressionJSON @(T.Value $ T.ToT ()) genValueUnit
  , roundtripExpressionJSON @(T.Instr '[T.ToT Mutez] '[T.ToT Mutez]) genSimpleInstr
  , roundtripExpressionJSON (T.fromUType <$> genValidType)
  , roundtripExpressionJSON genValidType
  ]

test_ToJSON_omits_empty_lists :: TestTree
test_ToJSON_omits_empty_lists =
  testCase "ToJSON omits empty lists" $ do
    let actual = toJSON . M.toExpression $ T.toVal (Nothing :: Maybe Integer)
    let expected = object ["prim" .= String "None"]

    actual @?= expected

test_Roundtrip_binary :: [TestTree]
test_Roundtrip_binary =
  [ testBinary M.genExpression
  ]

test_Roundtrip_expression :: [TestTree]
test_Roundtrip_expression =
  [ roundtripExpression @(T.Value $ T.ToT Integer) genValueInt
  , roundtripExpression @(T.Value $ T.ToT Mutez) genValueMutez
  , roundtripExpression @(T.Value $ T.ToT ()) genValueUnit
  , roundtripExpression @(T.Instr '[T.ToT Mutez] '[T.ToT Mutez]) genSimpleInstr
  , roundtripExpression @T.T (T.fromUType <$> genValidType)
  , roundtripExpression genValidType
  ]

test_toExpression :: [TestTree]
test_toExpression =
  [ testCase "Converting TInt to Expression" $
      M.toExpression T.TInt `shouldBe`
        M.ExpressionPrim (M.MichelinePrimAp (M.MichelinePrimitive "int") mempty mempty)

  , testCase "Converting (TOption TString) to Expression" $
      M.toExpression (T.TOption T.TString) `shouldBe`
        M.ExpressionPrim
          ( M.MichelinePrimAp
            { mpaPrim = M.MichelinePrimitive "option"
            , mpaArgs = Seq.fromList [M.ExpressionPrim
                ( M.MichelinePrimAp {mpaPrim = M.MichelinePrimitive "string"
                , mpaArgs = Seq.fromList [], mpaAnnots = Seq.fromList [] }
                )]
            , mpaAnnots = Seq.fromList []
            }
          )

  , testCase "Converting Instr (DIP SIZE) to Expression" $
      M.toExpression (T.DIP (T.SIZE @'T.TString)) `shouldBe`
        M.ExpressionSeq (Seq.fromList
          [ M.ExpressionPrim (M.MichelinePrimAp
              { mpaPrim = M.MichelinePrimitive "DIP"
              , mpaArgs = Seq.fromList [M.ExpressionSeq (Seq.fromList [M.ExpressionPrim (M.MichelinePrimAp
                  { mpaPrim = M.MichelinePrimitive "SIZE"
                  , mpaArgs = Seq.fromList []
                  , mpaAnnots = Seq.fromList []
                  })])]
              , mpaAnnots = Seq.fromList []
              })]
          )
  , testCase "Converting VInt to Expression" $
      M.toExpression @(T.Value $ T.ToT Integer) (T.VInt 12) `shouldBe` M.ExpressionInt 12

  , testCase "Converting NTPair to Expression" $
      M.toExpression (T.NTPair (U.ann "pair") (U.ann "a") (U.ann "b") (T.NTString U.noAnn) (T.NTInt $ U.ann "val")) `shouldBe`
        M.ExpressionPrim
          (M.MichelinePrimAp
              { mpaPrim = M.MichelinePrimitive "pair"
              , mpaArgs = Seq.fromList
                [ M.ExpressionPrim (M.MichelinePrimAp
                  { mpaPrim = M.MichelinePrimitive "string"
                  , mpaArgs = Seq.fromList []
                  , mpaAnnots = Seq.fromList [M.AnnotationField (U.ann "a") ]})
                , M.ExpressionPrim (M.MichelinePrimAp
                  { mpaPrim = M.MichelinePrimitive "int"
                  , mpaArgs = Seq.fromList []
                  , mpaAnnots = Seq.fromList [ M.AnnotationType (U.ann "val"), M.AnnotationField (U.ann "b")]})
                ]
              , mpaAnnots = Seq.fromList [ M.AnnotationType (U.ann "pair") ]
              }
          )

  , testCase "Converting \"CAR @bar %foo\" to Expression" $
      M.toExpression (T.InstrWithVarNotes (one (U.ann "bar")) (T.AnnCAR (U.ann "foo"))) `shouldBe`
        M.ExpressionSeq (Seq.fromList
          [ M.ExpressionPrim $ M.MichelinePrimAp
            { mpaPrim = M.MichelinePrimitive "CAR"
            , mpaArgs = Seq.Empty
            , mpaAnnots = Seq.fromList
              [ M.AnnotationField $ U.ann "foo"
              , M.AnnotationVariable $ U.ann "bar"
              ]
            }
          ])

  , testCase "Converting sample contract to Expression" $
      M.toExpression sampleContract `shouldBe`
        M.ExpressionSeq (Seq.fromList
          [ M.ExpressionPrim (M.MichelinePrimAp
              { mpaPrim = M.MichelinePrimitive "storage"
              , mpaArgs = Seq.fromList [M.ExpressionPrim (M.MichelinePrimAp {mpaPrim = M.MichelinePrimitive "chain_id", mpaArgs = Seq.fromList [], mpaAnnots = Seq.fromList []})]
              , mpaAnnots = Seq.fromList []
              })
          , M.ExpressionPrim (M.MichelinePrimAp
              { mpaPrim = M.MichelinePrimitive "parameter"
              , mpaArgs = Seq.fromList [M.ExpressionPrim (M.MichelinePrimAp {mpaPrim = M.MichelinePrimitive "unit", mpaArgs = Seq.fromList [], mpaAnnots = Seq.fromList []})]
              , mpaAnnots = Seq.fromList []
              })
          , M.ExpressionPrim (M.MichelinePrimAp
              { mpaPrim = M.MichelinePrimitive "code"
              , mpaArgs = Seq.fromList [M.ExpressionSeq (Seq.fromList
                [ M.ExpressionPrim (M.MichelinePrimAp {mpaPrim = M.MichelinePrimitive "DROP", mpaArgs = Seq.fromList [], mpaAnnots = Seq.fromList []})
                , M.ExpressionPrim (M.MichelinePrimAp {mpaPrim = M.MichelinePrimitive "CHAIN_ID", mpaArgs = Seq.fromList [], mpaAnnots = Seq.fromList []})
                , M.ExpressionPrim (M.MichelinePrimAp
                    { mpaPrim = M.MichelinePrimitive "NIL"
                    , mpaArgs = Seq.fromList
                      [ M.ExpressionPrim (M.MichelinePrimAp
                        { mpaPrim = M.MichelinePrimitive "operation"
                        , mpaArgs = Seq.fromList []
                        , mpaAnnots = Seq.fromList []
                        })
                      ]
                    , mpaAnnots = Seq.fromList []
                    })
                , M.ExpressionPrim (M.MichelinePrimAp {mpaPrim = M.MichelinePrimitive "PAIR", mpaArgs = Seq.fromList [], mpaAnnots = Seq.fromList []})
                ])]
              , mpaAnnots = Seq.fromList []
              })
          ])
  ]
  where
    sampleContract :: T.Contract 'T.TUnit 'T.TChainId
    sampleContract =
      let code = T.DROP `T.Seq` T.CHAIN_ID `T.Seq` T.NIL `T.Seq` T.PAIR
      in T.Contract
            { cCode = code
            , cParamNotes = T.starParamNotes @'T.TUnit
            , cStoreNotes = T.starNotes
            , cEntriesOrder = U.SPC
            }
