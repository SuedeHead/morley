-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on 'Value'.
module Test.Michelson.Typed.Value
  ( test_eqValueExt
  ) where

import Test.HUnit (assertBool)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Michelson.Text
import Michelson.Typed.Haskell.Value
import Michelson.Typed.Value

test_eqValueExt :: [TestTree]
test_eqValueExt =
  [ testCase "Simple" $
      assertBool "Occured to be not reflexive" $
        toVal @Integer 5 `eqValueExt` toVal @Integer 5

  , testCase "Complex" $
      assertBool "Occured to be not reflexive" $
        toVal ([mt|a|], 5 :: Integer)
          `eqValueExt`
        toVal ([mt|a|], 5 :: Integer)

  , testCase "Types do not matter if content is the same" $
      -- Not sure this property is desired, leaving a test just for
      -- illustration purposes
      assertBool "Unexpectedly equal" . not $
        toVal ([] @Integer) `eqValueExt` toVal ([] @MText)
  ]
