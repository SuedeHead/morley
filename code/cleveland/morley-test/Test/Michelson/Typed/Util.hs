-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for 'Michelson.Typed.Util'.

module Test.Michelson.Typed.Util
  ( unit_linearizeLeft_sample
  , hprop_linearizeLeft_performance
  , unit_dfsInstr_counter
  ) where

import Hedgehog (Property)
import Test.HUnit (Assertion, (@?), (@?=))

import Michelson.Test (meanTimeUpperBoundPropNF, sec)
import Michelson.Typed

unit_linearizeLeft_sample :: Assertion
unit_linearizeLeft_sample =
  isLeftLinear (linearizeLeft (rightLinear 3)) @? "Failed to linearize"

isLeftLinear :: Instr inp out -> Bool
isLeftLinear (Seq _ (Seq {})) = False
isLeftLinear (Seq a _) = isLeftLinear a
isLeftLinear _ = True

rightLinear :: Word -> Instr '[ 'TUnit, 'TUnit ] '[ 'TUnit, 'TUnit]
rightLinear = go
  where
    go 0 = SWAP
    go n = Seq SWAP (go (n - 1))

hprop_linearizeLeft_performance :: Property
hprop_linearizeLeft_performance =
  meanTimeUpperBoundPropNF (sec 10) (linearizeLeft . rightLinear) 300000

-- Use 'dfsFoldInstr' to count the number of the @UNIT@ instructions.
unit_dfsInstr_counter :: Assertion
unit_dfsInstr_counter = do
  dfsFoldInstr (settings True) step instr @?= Sum 3
  dfsFoldInstr (settings False) step instr @?= Sum 2
  where
    settings goToValues =
      DfsSettings
      { dsGoToValues = goToValues
      , dsCtorEffectsApp = ceaBottomToTop
      }

    step :: Instr a b -> Sum Word
    step = \case
      UNIT -> Sum 1
      _ -> mempty

    instr = UNIT `Seq` DUP `Seq` DIP UNIT `Seq` DROP `Seq` PUSH v

    v :: Value $ 'TPair 'TUnit $ 'TLambda 'TBool 'TBool
    v = VPair (VUnit, (VLam $ RfNormal $ UNIT `Seq` DROP))
