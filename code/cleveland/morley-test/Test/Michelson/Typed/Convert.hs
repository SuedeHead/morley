-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on 'Convert'.
module Test.Michelson.Typed.Convert
  ( test_sub
  , test_sha
  , test_arith
  ) where

import Test.Hspec ()
import Test.HUnit (assertEqual)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Michelson.Typed.Convert (instrToOps)
import Michelson.Typed.Instr (Instr(..))
import qualified Michelson.Typed.Instr as T (Instr (EQ, NEQ, LT, GT, LE, GE))
import Michelson.Typed.Value (Value' (..))
import qualified Michelson.Untyped as U

annA, annB :: U.Annotation a
annA = U.ann "a"
annB = U.ann "b"

test_sub :: TestTree
test_sub =
  testCase "SUB accepts one annotation" $
    assertEqual "SUB accepts one annotation" (instrToOps sub) subExpected
  where
    sub = PUSH (VInt 9) `Seq` PUSH (VInt 8) `Seq` InstrWithVarNotes (annA :| []) SUB
    subExpected =
      [ U.PrimEx $ U.PUSH U.noAnn (U.Type U.TInt U.noAnn) (U.ValueInt 9)
      , U.PrimEx $ U.PUSH U.noAnn (U.Type U.TInt U.noAnn) (U.ValueInt 8)
      , U.PrimEx $ U.SUB annA
      ]

test_sha :: TestTree
test_sha =
  testCase "SHA256 and SHA512 accept one annotation" $ do
    assertEqual "SHA256 accepts one annotation" (instrToOps sha256) sha256Expected
    assertEqual "SHA512 accepts one annotation" (instrToOps sha512) sha512Expected
  where
    push = U.PrimEx $ U.PUSH U.noAnn (U.Type U.TBytes U.noAnn) (U.ValueBytes $ U.InternalByteString $ "foo")
    sha256 = PUSH (VBytes "foo") `Seq` InstrWithVarNotes (annA :| []) SHA256
    sha256Expected = [push, U.PrimEx $ U.SHA256 annA]
    sha512 = PUSH (VBytes "foo") `Seq` InstrWithVarNotes (annA :| []) SHA512
    sha512Expected = [push, U.PrimEx $ U.SHA512 annA]

test_arith :: TestTree
test_arith = testCase "arithmetic operators have proper annotations" $
  zipWithM_ (assertEqual "single annotation in arithmetic works")
    (instrToOps <$> instrCmp)
    instrCmpExpected
  where
    instr =
      InstrWithVarNotes (annA :| []) (PUSH (VInt 2))
      `Seq` InstrWithVarNotes (annB :| []) (PUSH (VInt 1))
    instrExpected =
      [ U.PrimEx $ U.PUSH annA (U.Type U.TInt U.noAnn) (U.ValueInt 2)
      , U.PrimEx $ U.PUSH annB (U.Type U.TInt U.noAnn) (U.ValueInt 1)
      ]
    instrCmp = mkTArith <$> arithTOps
    instrCmpExpected = mkUArith <$> arithUOps

    -- Signature omitted due to being too cumbersome to read.
    mkTArith op = instr `Seq` InstrWithVarNotes (annA :| []) op

    mkUArith :: (U.VarAnn -> U.ExpandedInstr) -> [U.ExpandedOp]
    mkUArith op = instrExpected <> [U.PrimEx (op annA)]

    arithTOps = [T.EQ, T.NEQ, T.LT, T.GT, T.LE, T.GE]
    arithUOps = [U.EQ, U.NEQ, U.LT, U.GT, U.LE, U.GE]
