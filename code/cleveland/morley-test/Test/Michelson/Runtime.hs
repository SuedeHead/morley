-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for Michelson.Runtime.

module Test.Michelson.Runtime
  ( test_executorPure
  ) where

import Control.Lens (at)
import Data.Default (def)
import Fmt (pretty, (+|), (|+))
import Test.Hspec.Expectations (Expectation, expectationFailure)
import Test.HUnit (Assertion, assertFailure, (@?), (@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Michelson.Interpret (ContractEnv(..), InterpretResult(..), handleContractReturn, interpret)
import Michelson.Runtime hiding (transfer)
import Michelson.Runtime.GState (GState(..), genesisAddress, initGState)
import Michelson.Test.Dummy (dummyContractEnv, dummyMaxSteps, dummyNow, dummyOrigination)
import Michelson.Test.Integrational
  (IntegrationalScenario, TestError(CustomTestError), catchExpectedError, integrationalFail,
  integrationalTestExpectation, tOriginate, transfer, unexpectedInterpreterError)
import Michelson.Text (mt)
import Michelson.Typed
import qualified Michelson.Typed as T
import Michelson.Typed.Origination (OriginationOperation(..))
import Tezos.Address
import Tezos.Core (unsafeMkMutez)

test_executorPure :: IO [TestTree]
test_executorPure = do
  pure
    [ testGroup "Updates storage value of executed contract" $
      [ testCase "contract1" $ updatesStorageValue contractAux1
      , testCase "contract2" $ updatesStorageValue contractAux2
      ]
    , testCase "Succeeds to originate the same contract twice, with different addresses"
        succeedsToOriginateTwice
    , testCase "Fails transfering 0tz to plain account"
        $ integrationalTestExpectation testZeroTransactionFails
    , testCase "Success transfering 0tz to a contract"
        $ integrationalTestExpectation testZeroTransactionSuccess
    , testCase "Transfer of 0tz from unknown address is allowed" transferFromUnknown
    ]

----------------------------------------------------------------------------
-- Test code
----------------------------------------------------------------------------

-- | Data type, that containts contract and its auxiliary data.
data ContractAux cp st = ContractAux
  { caContract :: T.Contract cp st
  , caEnv :: ContractEnv
  , caStorage :: T.Value st
  , caParameter :: T.Value cp
  }

updatesStorageValue
  :: (ParameterScope cp, StorageScope st)
  => ContractAux cp st -> Assertion
updatesStorageValue ca = either (assertFailure . pretty) handleResult $ do
  let
    ce = caEnv ca
    origination = contractAuxToOrigination ca
    txData = TxData
      { tdSenderAddress = ceSender ce
      , tdParameter = TxTypedParam $ caParameter ca
      , tdEntrypoint = DefEpName
      , tdAmount = unsafeMkMutez 100
      }

  runExecutorM dummyNow dummyMaxSteps initGState $ do
    addr <- executeGlobalOrigination origination
    executeGlobalOperations [TransferOp addr txData]
    return addr
  where
    toNewStorage :: InterpretResult -> SomeValue
    toNewStorage InterpretResult {..} = SomeValue $ iurNewStorage

    handleResult :: (ExecutorRes, Address) -> Assertion
    handleResult (ir, addr) = do
      expectedValue <-
        either (assertFailure . pretty) (pure . toNewStorage) $
        handleContractReturn $
        interpret
          (caContract ca) epcCallRootUnsafe (caParameter ca) (caStorage ca) (caEnv ca)
      case gsAddresses (_erGState ir) ^. at addr of
        Nothing -> expectationFailure $ "Address not found: " <> pretty addr
        Just (ASContract ContractState{..}) -> SomeValue csStorage @?= expectedValue
        Just _ -> expectationFailure $ "Address has unexpected state " <> pretty addr

succeedsToOriginateTwice :: Expectation
succeedsToOriginateTwice = either (assertFailure . pretty) handleResult $ do
  runExecutorM dummyNow dummyMaxSteps initGState $ do
    addr1 <- executeGlobalOrigination origination
    addr2 <- executeGlobalOrigination origination
    return (addr1, addr2)
  where
    contract = caContract contractAux1
    origination = dummyOrigination (caStorage contractAux1) contract

    handleResult :: (ExecutorRes, (Address, Address)) -> Assertion
    handleResult (_, (addr1, addr2)) =
      addr1 /= addr2 @? "Two originated addresses are not different"

testZeroTransactionFails :: IntegrationalScenario
testZeroTransactionFails = do
  let
    txData = TxData
        { tdSenderAddress = genesisAddress
        , tdParameter = TxTypedParam $ toVal @[Integer] []
        , tdEntrypoint = DefEpName
        , tdAmount = unsafeMkMutez 0 }

  transfer txData genesisAddress `catchExpectedError`
    \case
      EEZeroTransaction addr
        | addr == genesisAddress -> pass
        | otherwise -> integrationalFail $ CustomTestError $
            "Expected " +| genesisAddress |+ ", but got " +| addr |+ ""
      err -> unexpectedInterpreterError err "expected attempt to send 0tz"

testZeroTransactionSuccess :: IntegrationalScenario
testZeroTransactionSuccess = do
  let
    contract = caContract contractAux1
    storage = caStorage contractAux1
    balance = ceBalance . caEnv $ contractAux1
    txData = TxData
        { tdSenderAddress = genesisAddress
        , tdParameter = TxTypedParam $ caParameter contractAux1
        , tdEntrypoint = DefEpName
        , tdAmount = unsafeMkMutez 0 }
  address <- tOriginate contract "test0tzContract" storage balance
  transfer txData address

transferFromUnknown :: Assertion
transferFromUnknown = do
  let
    res = runExecutorM dummyNow dummyMaxSteps initGState $ do
      addr <- executeGlobalOrigination origination
      executeGlobalOperations [TransferOp addr txData]

  whenLeft res $
    assertFailure . pretty
  where
    ca = contractAux1
    origination = contractAuxToOrigination ca
    txData =
      TxData
        { tdSenderAddress = detGenKeyAddress "transferFromUnknown"
        , tdParameter = TxTypedParam $ caParameter ca
        , tdEntrypoint = DefEpName
        , tdAmount = unsafeMkMutez 0
        }

----------------------------------------------------------------------------
-- Data
----------------------------------------------------------------------------

contractAux1 :: ContractAux 'TString 'TBool
contractAux1 = ContractAux
  { caContract = contract
  , caEnv = dummyContractEnv
  , caStorage = toVal True
  , caParameter = toVal [mt|aaa|]
  }
  where
    contract :: Contract 'TString 'TBool
    contract = Contract
      { cParamNotes = starParamNotes
      , cStoreNotes = starNotes
      , cCode =
          CDR `Seq` NIL `Seq` PAIR
      , cEntriesOrder = def
      }

contractAux2 :: ContractAux 'TString 'TBool
contractAux2 = contractAux1
  { caContract = (caContract contractAux1)
    { cCode =
        CDR `Seq` NOT `Seq` NIL `Seq` PAIR
    }
  }

contractAuxToOrigination
  :: (ParameterScope cp , StorageScope st)
  => ContractAux cp st -> OriginationOperation
contractAuxToOrigination ca =
  let contract = caContract ca
      ce = caEnv ca
      originationOp = dummyOrigination (caStorage ca) contract
   in originationOp {ooBalance = ceBalance ce}
