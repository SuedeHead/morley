-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for analyzer.

module Test.Analyzer
  ( unit_Sample_analyze
  ) where

import Prelude hiding (EQ)

import qualified Data.HashMap.Strict as HM
import Test.HUnit (Assertion, (@?=))

import Michelson.Analyzer
import Michelson.Text
import qualified Michelson.Typed as T
import Michelson.Typed.Instr

unit_Sample_analyze :: Assertion
unit_Sample_analyze = analyze sample @?= expectedRes
  where
    expectedRes = AnalyzerRes
      { arConstStrings = HM.fromList [(str1, 3), (str3, 4)]
      , arConstBytes = mempty
      , arErrorTags = HM.fromList [(str1, 2), (str3, 1)]
      }

str1 :: MText
str1 = [mt|aa|]

str3 :: MText
str3 = [mt|bb|]

sample :: T.ContractCode 'T.TString 'T.TString
sample =
  CAR `Seq` DUP `Seq`
  pushStr str3 `Seq`
  CONCAT `Seq`
  SIZE `Seq` INT `Seq` EQ `Seq`
  IF (LAMBDA (T.VLam . T.RfNormal $ pushStr str3 `Seq` CONCAT) `Seq` DROP)
     (DIP (pushStr str1) `Seq` DROP) `Seq`
  DIP (pushStr str3) `Seq` DROP `Seq`
  pushTrue `Seq`
  IF (pushStr str1 `Seq` FAILWITH)
     pushTrue `Seq`
  IF (Seq NOW $ Seq DROP $ pushStr str1 `Seq` (PAIR `Seq` FAILWITH))
     Nop `Seq`
  PUSH (T.VPair (T.VString str3, T.VUnit)) `Seq` FAILWITH
  where
    pushStr :: forall s. MText -> Instr s ('T.TString ': s)
    pushStr str = PUSH (T.VString str)

    pushTrue :: forall s. Instr s ('T.TBool ': s)
    pushTrue = PUSH (T.VBool True)
