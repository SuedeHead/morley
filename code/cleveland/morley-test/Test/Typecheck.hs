-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Typecheck
  ( unit_Good_contracts
  , unit_Bad_contracts
  , test_srcPosition
  , unit_Unreachable_code
  , test_Roundtrip
  , test_StackRef
  , test_TCTypeError_display
  , hprop_ValueSeq_as_list
  , test_SELF
  , test_Value_contract
  , test_variableAnnotations
  ) where

import Data.Default (def)
import qualified Data.Kind as Kind
import qualified Data.Map as M
import qualified Data.Text.IO.Utf8 as Utf8 (readFile)
import Data.Typeable (typeRep)
import Fmt (build, pretty)
import Hedgehog (Gen, Property, evalNF, forAll, property, (===))
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Hspec (expectationFailure)
import Test.Hspec.Expectations (Expectation)
import Test.HUnit (Assertion, assertFailure, (@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)
import Test.Tasty.HUnit (testCase)

import Hedgehog.Gen.Michelson.Typed (genValueInt, genValueTimestamp)
import Michelson.ErrorPos (InstrCallStack(..), LetName(..), Pos(..), SrcPos(..), srcPos)
import Michelson.Parser (utypeQ)
import Michelson.Runtime (prepareContract)
import Michelson.Test (failedTest, meanTimeUpperBoundPropNF, sec)
import Michelson.Test.Import (ImportContractError(..), readContract)
import Michelson.TypeCheck
import qualified Michelson.Typed as T
import Michelson.Untyped (ParameterType(..), noAnn, unsafeBuildEpName)
import qualified Michelson.Untyped as Un
import Tezos.Address (Address(..), mkContractHashHack)
import Tezos.Core (Timestamp)

import Test.Util.Contracts (getIllTypedContracts, getWellTypedContracts, inContractsDir)

unit_Good_contracts :: Assertion
unit_Good_contracts
  = mapM_ (\f -> do checkFile True f def{ tcVerbose = True } (const pass)
                    checkFile True f def (const pass))
    =<< getWellTypedContracts

unit_Bad_contracts :: Assertion
unit_Bad_contracts
  = mapM_ (\f -> do checkFile False f def{ tcVerbose = True } (const pass)
                    checkFile False f def (const pass))
    =<< getIllTypedContracts

pattern IsSrcPos :: Word -> Word -> InstrCallStack
pattern IsSrcPos l c <- InstrCallStack [] (SrcPos (Pos l) (Pos c))

test_srcPosition :: [TestTree]
test_srcPosition =
  [ testCase "Verify instruction position in a typecheck error" $ do
      checkIllFile (inContractsDir "ill-typed/basic3.tz") $ \case
          TCFailedOnInstr (Un.CONS _) _ (IsSrcPos 8 6) _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/testassert_invalid_stack3.mtz") $ \case
          TCFailedOnInstr Un.DROP _ (IsSrcPos 10 17) _ (Just NotEnoughItemsOnStack) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/testassert_invalid_stack2.mtz") $ \case
          TCExtError _ (IsSrcPos 9 2) (TestAssertError _) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/macro_in_let_fail.mtz") $ \case
          TCFailedOnInstr (Un.COMPARE _) _ (InstrCallStack [LetName "cmpLet"] (SrcPos (Pos 7) (Pos 6))) _
                                              (Just (TypeEqError _ _)) -> True
          _ -> False
      checkIllFile (inContractsDir "ill-typed/compare_annotation_mismatch.tz") $ \case
          TCFailedOnInstr (Un.COMPARE _) _ _ _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/annotation_mismatch_map_update.tz") $ \case
          TCFailedOnInstr (Un.UPDATE _) (SomeHST (_ ::& _ ::& (T.NTMap{}, _, _) ::& SNil)) _ _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/annotation_mismatch_map_get.tz") $ \case
          TCFailedOnInstr (Un.GET _) (SomeHST (_ ::& (T.NTMap{}, _, _) ::& SNil)) _ _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/annotation_mismatch_map_mem.tz") $ \case
          TCFailedOnInstr (Un.MEM _) (SomeHST (_ ::& (T.NTMap{}, _, _) ::& SNil)) _ _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/annotation_mismatch_big_map_update.tz") $ \case
          TCFailedOnInstr (Un.UPDATE _) (SomeHST (_ ::& _ ::& (T.NTBigMap{}, _, _) ::& SNil)) _ _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/annotation_mismatch_big_map_get.tz") $ \case
          TCFailedOnInstr (Un.GET _) (SomeHST (_ ::& (T.NTBigMap{}, _, _) ::& SNil)) _ _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/annotation_mismatch_big_map_mem.tz") $ \case
          TCFailedOnInstr (Un.MEM _) (SomeHST (_ ::& (T.NTBigMap{}, _, _) ::& SNil)) _ _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/annotation_mismatch_set_update.tz") $ \case
          TCFailedOnInstr (Un.UPDATE _) (SomeHST (_ ::& _ ::& (T.NTSet{}, _, _) ::& SNil)) _ _ (Just (AnnError _)) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/annotation_mismatch_set_mem.tz") $ \case
          TCFailedOnInstr (Un.MEM _) (SomeHST (_ ::& (T.NTSet{}, _, _) ::& SNil)) _ _ (Just (AnnError _)) -> True
          _ -> False
  ]
  where
    unexpected f e =
      expectationFailure $ "Unexpected typecheck error: " <> displayException e <> " in file: " <> f
    checkIllFile file check = checkFile False file def $
      \e -> if check e then pass else unexpected file e

checkFile
  :: HasCallStack
  => Bool
  -> FilePath
  -> TypeCheckOptions
  -> (TCError -> Expectation)
  -> Expectation
checkFile wellTyped file options onError = do
  c <- prepareContract (Just file)
  case typeCheckContract c options of
    Left err
      | wellTyped ->
        expectationFailure $
        "Typechecker unexpectedly failed on " <> show file <>
        ": " <> displayException err
      | otherwise -> onError err
    Right _
      | not wellTyped ->
        assertFailure $
        "Typechecker unexpectedly considered " <> show file <> " well-typed."
      | otherwise -> pass

unit_Unreachable_code :: Assertion
unit_Unreachable_code = do
  let file = inContractsDir "ill-typed/fail_before_nop.tz"
  let ics = InstrCallStack [] (srcPos 7 13)
  econtract <- readContract @'T.TUnit @'T.TUnit file <$> Utf8.readFile file
  econtract @?= Left (ICETypeCheck $ TCUnreachableCode ics (one $ Un.WithSrcEx ics $ Un.SeqEx []))

test_Roundtrip :: [TestTree]
test_Roundtrip =
  [ testGroup "Value"
    [ roundtripValue @Integer genValueInt
    , roundtripValue @Timestamp genValueTimestamp
    ]
  ]
  where
  roundtripValue
    :: forall (a :: Kind.Type).
        ( Each [T.KnownT, T.HasNoOp] '[T.ToT a]
        , Typeable a
        )
    => Gen (T.Value $ T.ToT a)
    -> TestTree
  roundtripValue genValue =
    testProperty (show $ typeRep (Proxy @a)) $ property $ do
      val :: T.Value (T.ToT a) <- forAll $ genValue
      let uval = T.untypeValue val
          runTC = runTypeCheckIsolated . usingReaderT (def @InstrCallStack)
      case runTC $ typeCheckValue uval of
        Right got -> got === val
        Left err -> failedTest $
                    "Type check unexpectedly failed: " <> pretty err

test_StackRef :: [TestTree]
test_StackRef =
  [ testProperty "Typecheck fails when ref is out of bounds" $ property $ do
      let instr = printStRef 2
          hst = stackEl ::& stackEl ::& SNil
      case
        runTypeCheckIsolated $
        typeCheckList [Un.WithSrcEx def $ Un.PrimEx instr] hst
        of
          Left err -> void $ evalNF err
          Right _ -> failedTest "Typecheck unexpectedly succeded"
  , testProperty "Typecheck time is reasonably bounded" $
      let hst = stackEl ::& SNil
          run i =
            case
              runTypeCheckIsolated $
              typeCheckList [Un.WithSrcEx def $ Un.PrimEx (printStRef i)] hst
            of
              Left err -> err
              Right _ -> error "Typecheck unexpectedly succeded"
      -- Making code processing performance scale with code size looks like a
      -- good property, so we'd like to avoid scenario when user tries to
      -- access 100500-th element of stack and typecheck hangs as a result
      in  meanTimeUpperBoundPropNF (sec 1) run 100000000000
  ]
  where
    printStRef i = Un.EXT . Un.UPRINT $ Un.PrintComment [Right (Un.StackRef i)]
    stackEl = (T.starNotes @'T.TUnit, T.Dict, noAnn)

test_TCTypeError_display :: [TestTree]
test_TCTypeError_display =
  -- One may say that it's madness to write tests on 'Buildable' instances,
  -- but IMO (martoon) it's worth resulting duplication because tests allow
  -- avoiding silly errors like lost spaces and ensuring general sanity
  -- of used way to display content.
  [ testCase "TypeEqError" $
      build (TypeEqError T.TUnit T.TKey)
      @?= "Types not equal: unit /= key"

  , testCase "StackEqError" $
      build (StackEqError [T.TUnit, T.TBytes] [])
      @?= "Stacks not equal: [unit, bytes] /= []"

  , testCase "UnsupportedTypes" $
      build (UnsupportedTypeForScope (T.TBigMap T.TInt T.TInt) T.BtHasBigMap)
      @?= "Type 'big_map int int' is unsupported here because it has 'big_map'"

  , testCase "InvalidValueType" $
      build (InvalidValueType T.TUnit)
      @?= "Value type is never a valid `unit`"
  ]

hprop_ValueSeq_as_list :: Property
hprop_ValueSeq_as_list = property $ do
  l <- forAll $ Gen.nonEmpty (Range.linear 0 100) (Gen.integral (Range.linearFrom 0 -1000 1000))
  let
    untypedValue = Un.ValueSeq $ Un.ValueInt <$> l
    typedValue = T.VList $ T.VInt <$> toList l
    runTypeCheckInstr = runTypeCheckIsolated . usingReaderT def
  runTypeCheckInstr (typeCheckValue untypedValue) === Right typedValue

test_SELF :: [TestTree]
test_SELF =
  [ testCase "Entrypoint not present" $
      checkFile False (inContractsDir "ill-typed/self-bad-entrypoint.mtz") def $
      \case
        TCFailedOnInstr Un.SELF{} _ _ _ (Just EntrypointNotFound{}) -> pass
        other -> assertFailure $ "Unexpected error: " <> pretty other

  , testCase "Entrypoint type mismatch" $
      checkFile False (inContractsDir "ill-typed/self-entrypoint-type-mismatch.mtz")
        def (const pass)

  , testCase "Entrypoint can be found" $
      checkFile True (inContractsDir "entrypoints/self1.mtz") def
        (const pass)
  ]

test_Value_contract :: [TestTree]
test_Value_contract =
  [ testCase "No contract exists" $
      case typeVerifyParameter @('T.TContract 'T.TUnit) mempty addrUVal1 of
        Left (TCFailedOnValue _ _ _ _ (Just (UnknownContract _))) -> pass
        res -> assertFailure $ "Unexpected result: " <> show res

  , testCase "Entrypoint does not exist" $
      case typeVerifyParameter @('T.TContract 'T.TKey) env1 addrUVal1 of
        Left (TCFailedOnValue _ _ _ _ (Just (EntrypointNotFound _))) -> pass
        res -> assertFailure $ "Unexpected result: " <> show res

  , testCase "Correct contract value" $
      case typeVerifyParameter @('T.TContract 'T.TInt) env1 addrUVal2 of
        Right _ -> pass
        res -> assertFailure $ "Unexpected result: " <> show res
  ]
  where
    addr1' = mkContractHashHack "123"
    addr1 = T.EpAddress (ContractAddress addr1') (unsafeBuildEpName "a")
    addr2 = T.EpAddress (ContractAddress addr1') (unsafeBuildEpName "q")
    addrUVal1 = Un.ValueString $ T.mformatEpAddress addr1
    addrUVal2 = Un.ValueString $ T.mformatEpAddress addr2

    env1 = M.fromList
      [ ( addr1', mkSomeParamTypeUnsafe $ ParameterType [utypeQ| ( nat %s | int %q ) |] noAnn)
      ]

test_variableAnnotations :: [TestTree]
test_variableAnnotations =
  [ testCase "Annotations are preserved in cadr_annotation.tz" $ do
      let file = inContractsDir "tezos_examples/attic/cadr_annotation.tz"
          param = Un.ann "param"
          no_name = Un.ann "no_name"
          p1 = Un.ann "p1"
          name = Un.ann "name"
      contracts <-
        readContract @('T.TPair ('T.TPair 'T.TUnit 'T.TString) 'T.TBool) @'T.TUnit file <$> Utf8.readFile file
      case contracts of
        Left err -> assertFailure $ displayException err
        Right (_uContract, tContract) -> T.cCode tContract @?=
          loc 2 7 (withNotes (T.NTPair Un.noAnn p1 Un.noAnn (T.NTPair Un.noAnn Un.noAnn no_name T.starNotes T.starNotes) T.starNotes) (T.InstrWithVarNotes (one param) (T.AnnCAR Un.noAnn)))
          `T.Seq` T.Nested (
            withNotes (T.NTPair Un.noAnn Un.noAnn no_name T.starNotes T.starNotes) (T.AnnCAR Un.noAnn)
            `T.Seq` T.InstrWithVarNotes (one name) (T.AnnCDR no_name))
          `T.Seq` loc 2 40 T.DROP
          `T.Seq` loc 2 46 T.UNIT
          `T.Seq` loc 2 52 T.NIL
          `T.Seq` loc 2 67 (T.AnnPAIR Un.noAnn Un.noAnn Un.noAnn)

  , testCase "Annotations are preserved in pexec_2.tz" $ do
      let file = inContractsDir "tezos_examples/opcodes/pexec_2.tz"
          p = Un.ann "p"
          s = Un.ann "s"
      contracts <-
        readContract @'T.TInt @('T.TList 'T.TInt) file <$> Utf8.readFile file
      case contracts of
        Left err -> assertFailure $ displayException err
        Right (_uContract, tContract) -> T.cCode tContract @?=
          T.Nested (
            T.DUP
            `T.Seq` T.InstrWithVarNotes (one p) (T.AnnCAR Un.noAnn)
            `T.Seq` T.DIP (T.InstrWithVarNotes (one s) (T.AnnCDR Un.noAnn)))
          `T.Seq` loc 4 6 (T.LAMBDA (T.VLam (T.RfNormal (
            T.Nested (
              T.DUP
              `T.Seq` T.AnnCAR Un.noAnn
              `T.Seq` T.DIP (T.AnnCDR Un.noAnn))
            `T.Seq` loc 5 24 (T.DIP (T.Nested (T.Seq T.DUP (T.Seq (T.AnnCAR Un.noAnn) (T.DIP (T.AnnCDR Un.noAnn))))))
            `T.Seq` loc 5 41 T.ADD
            `T.Seq` loc 5 47 T.MUL))))
          `T.Seq` loc 6 6 T.SWAP
          `T.Seq` loc 6 13 T.APPLY
          `T.Seq` loc 7 6 (T.PUSH (T.VInt 3))
          `T.Seq` loc 7 19 T.APPLY
          `T.Seq` loc 8 6 T.SWAP
          `T.Seq` loc 8 13 (T.MAP (loc 8 19 (T.DIP (loc 8 25 T.DUP))
            `T.Seq` loc 8 33 T.EXEC))
          `T.Seq` loc 9 6 (T.DIP (loc 9 12 T.DROP))
          `T.Seq` loc 10 6 T.NIL
          `T.Seq` loc 10 21 (T.AnnPAIR Un.noAnn Un.noAnn Un.noAnn)

  -- Regression test:
  , testCase "Annotations are preserved with UNPAIR macro with different number of field and variable annotations" $ do
      let file = inContractsDir "unpair_macro_simple.tz"
          u1 = Un.ann "u1"
          u2 = Un.ann "u2"
          u3 = Un.ann "u3"
          y1 = Un.ann "y1"
          y2 = Un.ann "y2"
      contracts <- readContract @'T.TUnit @'T.TUnit file <$> Utf8.readFile file
      case contracts of
        Left err -> assertFailure $ displayException err
        Right (_uContract, tContract) -> T.cCode tContract @?=
          loc 2 7 T.DROP
          `T.Seq` loc 3 7  (withNotes (T.NTUnit u3) (withVN "a3" T.UNIT))
          `T.Seq` loc 3 21 (withNotes (T.NTUnit u2) (withVN "a2" T.UNIT))
          `T.Seq` loc 3 35 (withNotes (T.NTUnit u1) (withVN "a1" T.UNIT))
          `T.Seq` withNotes (T.NTPair Un.noAnn Un.noAnn y1 (T.NTUnit u1) (T.NTPair Un.noAnn Un.noAnn y2 (T.NTUnit u2) (T.NTUnit u3))) (T.Nested (
            withNotes (T.NTUnit u1) (T.DIP (
              withNotes (T.NTPair Un.noAnn Un.noAnn y2 (T.NTUnit u2) (T.NTUnit u3)) (T.AnnPAIR Un.noAnn y2 Un.noAnn)))
            `T.Seq` withNotes (T.NTPair Un.noAnn Un.noAnn y1 (T.NTUnit u1) (T.NTPair Un.noAnn Un.noAnn y2 (T.NTUnit u2) (T.NTUnit u3))) (withVN "q" (T.AnnPAIR Un.noAnn y1 Un.noAnn))))
          `T.Seq` withNotes (T.NTUnit u1) (T.Nested (
            withNotes (T.NTPair Un.noAnn Un.noAnn y1 (T.NTUnit u1) (T.NTPair Un.noAnn Un.noAnn y2 (T.NTUnit u2) (T.NTUnit u3))) T.DUP
            `T.Seq` withNotes (T.NTUnit u1) (withVN "one" (T.AnnCAR y1))
            `T.Seq` withNotes (T.NTUnit u1) (T.DIP (
              withNotes (T.NTPair Un.noAnn Un.noAnn y2 (T.NTUnit u2) (T.NTUnit u3)) (T.AnnCDR Un.noAnn)
              `T.Seq` withNotes (T.NTPair Un.noAnn Un.noAnn y2 (T.NTUnit u2) (T.NTUnit u3)) T.DUP
              `T.Seq` withNotes (T.NTUnit u2) (withVN "two" (T.AnnCAR Un.noAnn))
              `T.Seq` withNotes (T.NTUnit u2) (T.DIP (withNotes (T.NTUnit u3) (T.AnnCDR Un.noAnn)))))))
          `T.Seq` loc 5 7 (withNotes (T.NTUnit u1) (T.DIP (
            loc 5 13 (withNotes (T.NTUnit u3) T.DROP)
            `T.Seq` (loc 5 19 T.DROP))))
          `T.Seq` loc 5 27 T.NIL
          `T.Seq` loc 5 42 (withNotes (T.NTPair Un.noAnn Un.noAnn Un.noAnn (T.NTList Un.noAnn (T.NTOperation Un.noAnn)) (T.NTUnit u1)) (T.AnnPAIR Un.noAnn Un.noAnn Un.noAnn))
  ]
  where
    loc :: Word -> Word -> T.Instr a b -> T.Instr a b
    loc row col = T.WithLoc (InstrCallStack [] (SrcPos (Pos row) (Pos col)))

    withNotes n = T.InstrWithNotes (T.PackedNotes n)

    withVN vn = T.InstrWithVarNotes (one (Un.ann vn))
