-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Tezos.Core
  ( test_ParseTimestamp
  , test_TimestampQuote
  , test_ChainId_roundtrip
  , test_Parse_ChainId
  ) where

import Test.Hspec (shouldBe, shouldSatisfy)
import Test.HUnit ((@?=))
import Test.Tasty (TestTree)
import Test.Tasty (testGroup)
import Test.Tasty.HUnit (testCase)
import Test.Util.Hedgehog (ShowThroughBuild(..), aesonRoundtrip, roundtripTreeSTB)

import Hedgehog.Gen.Tezos.Core (genChainId)
import Tezos.Core

----------------------------------------------------------------------------
-- Timestamp
----------------------------------------------------------------------------

test_ParseTimestamp :: [TestTree]
test_ParseTimestamp =
  [ testCase "Correctly parse valid inputs" $ do
      parseTimestamp "2018-08-08T00:00:00Z" `shouldBe` Just (Timestamp 1533686400)
      parseTimestamp "2018-08-08 00:00:00Z" `shouldBe` Just (Timestamp 1533686400)

  , testCase "Does not parse leading and trailing spaces" $ do
      parseTimestamp " 2018-08-08T00:00:00Z" `shouldSatisfy` isNothing
      parseTimestamp "2018-08-08T00:00:00Z " `shouldSatisfy` isNothing
      parseTimestamp " 2018-08-08T00:00:00Z " `shouldSatisfy` isNothing

  , testCase "Does not parse with multiple separating spaces" $ do
      parseTimestamp "2018-08-08  00:00:00Z" `shouldSatisfy` isNothing
      parseTimestamp "2018-08-08   00:00:00Z" `shouldSatisfy` isNothing
  ]

test_TimestampQuote :: [TestTree]
test_TimestampQuote =
  [ testCase "Quoter works in simple case" $
      [timestampQuote|2019-07-26T12:09:12Z|]
        @?= timestampFromSeconds 1564142952

  , testCase "Quoter works with surrounding spaces" $
      [timestampQuote| 2019-07-26T12:09:12Z |]
        @?= timestampFromSeconds 1564142952
  ]

----------------------------------------------------------------------------
-- ChainId
----------------------------------------------------------------------------

test_ChainId_roundtrip :: [TestTree]
test_ChainId_roundtrip =
    [ testGroup "parse . format ≡ pure"
      [ roundtripTreeSTB genChainId formatChainId parseChainId ]
    , testGroup "JSON encoding/deconding"
      [ aesonRoundtrip genChainId ]
    ]

test_Parse_ChainId :: [TestTree]
test_Parse_ChainId =
  [ testCase "Successfully parses valid sample data" $
    forM_ sampleChainIds (\a -> bimap STB STB (parseChainId a) `shouldSatisfy` isRight)
  , testCase "Fails to parse invalid data" $ do
    forM_ invalidChainIds (\a -> bimap STB STB (parseChainId a) `shouldSatisfy` isLeft)
  ]
  where
    sampleChainIds =
      [ "NetXUdfLh6Gm88t"
      ]
    invalidChainIds =
      [ ""
      , "1"
      , "NetXUdfLh6Gm88r"  -- last char changed
      , "5uYAAtMU9Bi2Qe"  -- valid base58, wrong size
      ]
