-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for 'Tezos.Crypto.Hash'.

module Test.Tezos.Crypto.Hash
  ( test_Bytes_Hashing
  ) where

import Fmt (fmt, hexF)
import Test.HUnit ((@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Tezos.Crypto.Hash

test_Bytes_Hashing :: [TestTree]
test_Bytes_Hashing =
  [ testGroup "blake2b" $ hashingTest blake2b blake2bHashes
  , testGroup "sha256" $ hashingTest sha256 sha256Hashes
  , testGroup "sha512" $ hashingTest sha512 sha512Hashes
  , testGroup "sha3" $ hashingTest sha3 sha3Hashes
  , testGroup "keccak" $ hashingTest keccak keccakHashes
  ]

-- These values have been computed using the following contract:
{-
parameter string;
storage bytes;
code { CDR; SHA512; # replace with desired function
       NIL operation; PAIR;};

-}

blake2bHashes, sha256Hashes, sha512Hashes, sha3Hashes, keccakHashes :: [(ByteString, Text)]
blake2bHashes =
  [ ("\0", "03170a2e7597b7b7e3d84c05391d139a62b157e78786d8c082f29dcf4c111314")  -- 0x00
  , ("\0\0", "9ee6dfb61a2fb903df487c401663825643bb825d41695e63df8af6162ab145a6")  -- 0x0000
  ]
sha256Hashes =
  [ ("\0", "6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d")  -- 0x00
  , ("\0\0", "96a296d224f285c67bee93c30f8a309157f0daa35dc5b87e410b78630a09cfc7")  -- 0x0000
  ]
sha512Hashes =
  [ ("\0", "b8244d028981d693af7b456af8efa4cad63d282e19ff14942c246e50d9351d22704a802a71c3580b6370de4ceb293c324a8423342557d4e5c38438f0e36910ee")  -- 0x00
  , ("#", "d369286ac86b60fa920f6464d26becacd9f4c8bd885b783407cdcaa74fafd45a8b56b364b63f6256c3ceef26278a1c7799d4243a8149b5ede5ce1d890b5c7236")  -- 0x23
  ]
sha3Hashes =
  [ ("\0", "5d53469f20fef4f8eab52b88044ede69c77a6a68a60728609fc4a65ff531e7d0")  -- 0x00
  , ("#",  "415733a38975da5f451d27a3c42aa6ecb9b49e55466875b6c794606b7435083f")  -- 0x23
  ]
keccakHashes =
  [ ("\0", "bc36789e7a1e281436464229828f817d6612f7b477d66591ff96a9e064bcc98a")  -- 0x00
  , ("#",  "ace738c68088218d015fbdce138f062893d86818ac98932f7ce2907c5976fbde")  -- 0x23
  ]

hashingTest :: (ByteString -> ByteString) -> [(ByteString, Text)] -> [TestTree]
hashingTest hashFunc pairs = do
  flip map pairs $ \(bs, bsHashHex) -> do
    testCase ("correctly computes hash of 0x" <> fmt (hexF bs)) $
      fmt (hexF (hashFunc bs)) @?= bsHashHex
