-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Doc.Position
  ( test_DifferentPosition
  , test_SamePosition
  ) where

import Test.Tasty (TestTree)

import Cleveland.Util (goesBefore)
import Michelson.Doc
import Michelson.Typed.Haskell (DStorageType, DType)

-- | Tests 'docItemPosition'.
test_DifferentPosition :: [TestTree]
test_DifferentPosition =
  [ Proxy @DComment `goesBefore` Proxy @DGeneralInfoSection
  , Proxy @DGeneralInfoSection `goesBefore` Proxy @DGitRevision
  , Proxy @DGitRevision `goesBefore` Proxy @DStorageType
  , Proxy @DStorageType `goesBefore` Proxy @DType
  ]

test_SamePosition :: [TestTree]
test_SamePosition =
  [ Proxy @DTypeA `goesBefore` Proxy @DTypeB
  ]

-- | Custom 'DocItem' for testing purpose.
data DTypeA = DTypeA
instance DocItem DTypeA where
  docItemPos = 1
  docItemSectionName = Nothing
  docItemToMarkdown _ DTypeA = ""

data DTypeB = DTypeB
instance DocItem DTypeB where
  docItemPos = 1
  docItemSectionName = Nothing
  docItemToMarkdown _ DTypeB = ""
