-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | 'TxData' type and associated functionality.

module Michelson.Runtime.TxData
       ( TxData (..)
       , TxParam(..)
       , tdSenderAddressL
       , tdParameterL
       , tdEntrypointL
       , tdAmountL
       ) where

import Control.Lens (makeLensesWith)

import Michelson.Typed (ParameterScope)
import qualified Michelson.Typed as T
import Michelson.Untyped (EpName)
import qualified Michelson.Untyped as U
import Tezos.Address (Address)
import Tezos.Core (Mutez)
import Util.Lens (postfixLFields)

-- | A parameter associated with a particular transaction.
data TxParam where
  TxTypedParam :: forall t. ParameterScope t => T.Value t -> TxParam
  TxUntypedParam :: U.Value -> TxParam

deriving stock instance Show TxParam

-- | Data associated with a particular transaction.
data TxData = TxData
  { tdSenderAddress :: Address
  , tdParameter :: TxParam
  , tdEntrypoint :: EpName
  , tdAmount :: Mutez
  } deriving stock Show

makeLensesWith postfixLFields ''TxData
