-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Michelson.TypeCheck
  ( typeCheckContract
  , typeCheckContractAndStorage
  , typeCheckExt
  , typeCheckInstr
  , typeCheckList
  , typeCheckListNoExcept
  , typeCheckParameter
  , typeCheckStorage
  , typeCheckValue
  , typeVerifyParameter
  , typeVerifyStorage
  , module E
  , module M
  , module T
  , eqType
  , matchTypes
  ) where

import Michelson.TypeCheck.Error as E
import Michelson.TypeCheck.Ext
import Michelson.TypeCheck.Instr
import Michelson.TypeCheck.TypeCheck as T
import Michelson.TypeCheck.Types as M

import Michelson.TypeCheck.Helpers (eqType, matchTypes)
