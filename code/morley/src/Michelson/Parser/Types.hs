-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Core parser types

module Michelson.Parser.Types
  ( Parser
  , LetEnv (..)
  , noLetEnv
  ) where

import Data.Default (Default(..))
import qualified Data.Map as Map
import Text.Megaparsec (Parsec)

import Michelson.Let (LetType, LetValue)
import Michelson.Macro (LetMacro)
import Michelson.Parser.Error

type Parser = ReaderT LetEnv (Parsec CustomParserException Text)

instance Default a => Default (Parser a) where
  def = pure def

-- | The environment containing lets from the let-block
data LetEnv = LetEnv
  { letMacros :: Map Text LetMacro
  , letValues :: Map Text LetValue
  , letTypes  :: Map Text LetType
  } deriving stock (Show, Eq)

noLetEnv :: LetEnv
noLetEnv = LetEnv Map.empty Map.empty Map.empty
