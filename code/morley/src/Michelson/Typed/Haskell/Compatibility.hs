-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Compatibility of Haskell values representation in Michelson.
module Michelson.Typed.Haskell.Compatibility
  ( ligoLayout
  , ligoCombLayout
  ) where

import Util.CustomGeneric

-- | Default layout in LIGO.
--
-- To be used with 'customGeneric', see this method for more info.
--
-- This is similar to 'leftBalanced', but
--
-- * fields are sorted alphabetically;
-- * always puts as large complete binary subtrees as possible at left.
ligoLayout :: GenericStrategy
ligoLayout =
  reorderingData forbidUnnamedFields alphabetically $
    fromDepthsStrategy ligoDepths
  where
    ligoDepths n =
      case fmap last . nonEmpty $ takeWhile (\(_, p) -> p <= n) powersOfTwo of
        Nothing -> []
        Just (depth, power) ->
          let leftSub = replicate power depth
              rightSub = ligoDepths (n - power)
          in if null rightSub then leftSub else map succ $ leftSub ++ rightSub

    powersOfTwo = [0..] <&> \i -> (i, 2 ^ i)

-- | Comb layout in LIGO (@ [\@layout:comb] @).
--
-- To be used with 'customGeneric'.
ligoCombLayout :: GenericStrategy
ligoCombLayout = rightComb
