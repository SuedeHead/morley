-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Measuring operation size of typed stuff.
module Michelson.Typed.OpSize
  ( OpSize (..)
  , U.opSizeHardLimit
  , U.smallTransferOpSize

  , instrOpSize
  , contractOpSize
  , valueOpSize
  ) where

import Michelson.Typed.Aliases
import Michelson.Typed.Convert
import Michelson.Typed.Instr
import Michelson.Typed.Scope
import Michelson.Untyped (OpSize)
import qualified Michelson.Untyped as U

-- | Estimate instruction operation size.
instrOpSize :: Instr inp out -> OpSize
instrOpSize = U.expandedInstrsOpSize . instrToOps

-- | Estimate contract code operation size.
contractOpSize :: Contract cp st -> OpSize
contractOpSize = instrOpSize . cCode

-- | Estimate value operation size.
-- TODO: [#428]: do not use 'PrintedValScope' here.
valueOpSize :: (PrintedValScope t) => Value t -> OpSize
valueOpSize = U.valueOpSize . untypeValue
