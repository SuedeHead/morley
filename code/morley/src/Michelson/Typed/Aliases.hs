-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Michelson.Typed.Aliases
  ( Value
  , SomeValue
  , SomeConstrainedValue
  , Operation
  ) where

import Michelson.Typed.Instr
import Michelson.Typed.Value

type Value = Value' Instr
type SomeValue = SomeValue' Instr
type SomeConstrainedValue = SomeConstrainedValue' Instr
type Operation = Operation' Instr
