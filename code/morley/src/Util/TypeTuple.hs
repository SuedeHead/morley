-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Conversions between tuples and list-like types.
module Util.TypeTuple
  ( RecFromTuple (..)
  ) where

import Util.TypeTuple.Class
import Util.TypeTuple.Instances ()
