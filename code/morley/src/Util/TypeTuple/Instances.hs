-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

module Util.TypeTuple.Instances () where

import Util.TypeTuple.TH

concatMapM deriveRecFromTuple [0..25]
