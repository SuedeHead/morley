-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Functions supposed to be used in the @Main@ module.

module Util.Main
  ( wrapMain
  ) where

import Control.Exception.Uncaught (withDisplayExceptionHandler)
import Main.Utf8 (withUtf8)

-- | Some defaults in Haskell are debatable and we typically want
-- to customize them in our applications.
-- Some customizations are done at the level of @main@.
-- Currently we have two of them:
--
-- 1. 'withUtf8' from the @with-utf8@ package.
-- 2. 'withDisplayExceptionHandler' from the @uncaught-exception@ package.
--
-- This function is supposed to apply all @main@ customizations that we
-- typcally want to do.
wrapMain :: IO () -> IO ()
wrapMain = withUtf8 . withDisplayExceptionHandler
