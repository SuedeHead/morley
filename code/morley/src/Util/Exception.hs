-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Util.Exception
  ( -- * General simple helpers
    throwLeft
  ) where

-- | If monadic action returns a 'Left' value, it will be
-- thrown. Otherwise the returned value will be returned as is.
throwLeft :: (MonadThrow m, Exception e) => m (Either e a) -> m a
throwLeft =
  (>>= \case
      Left e -> throwM e
      Right x -> return x)
