-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Utilities related to the @aeson@ package.

module Util.Aeson
  ( morleyAesonOptions
  ) where

import Data.Aeson.Casing (aesonPrefix, camelCase)
import Data.Aeson.TH (Options)

-- | Options that we use in @morley@-based packages (including
-- @morley@) by default.
morleyAesonOptions :: Options
morleyAesonOptions = aesonPrefix camelCase
