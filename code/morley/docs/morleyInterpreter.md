<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Morley Interpreter

Morley interpreter takes a well-typed Morley contract (in [typed representation](./michelsonTypes.md#typed-types)), interprets all its instructions according to the Michelson specification and then performs side effects.
There can be the following side effects:
1. Update storage value of a contract.
2. Update balance of an address.
3. An operation returned by a contract (`operation` type from Michelson).

Morley interpreter is implemented in two layers:
* An interpreter for the core Michelson language which doesn't perform any side effects.
It means that it always interprets only one contract.
It's located in `Michelson.Interpret`.
* High-level Morley interpreter (we call it **executor**) that runs smart contracts using Michelson interpreter and _executes_ side effects.
It's located in `Morley.Runtime`.

## Michelson interpreter

Michelson interpreter simply implements the specification of the Michelson language.
It's implemented as a pure function which takes all necessary data as pure Haskell values stored inside a Haskell data type called `ContractEnv`.
This design allows the interpreter to work in any environment: it can work with real blockchain data, with data supplied by the user, with randomly generated data, etc.

## High-level executor

High-level executor works with [global blockchain state](./morleyRuntime.md#blockchain-state).
This executor has the following goals:
1. Execute operations returned by contracts.
An operation can originate a new contract or call another contract by sending a transaction to its address.
2. Perform other side effects: update storages of all the executed contracts and their balances.
3. Write an updated blockchain state on disk.

An end user can apply one of the following commands to run the executor.
* The `transfer` command sends a transaction from one address to another address.
If the destination address is a smart contract, its code will be executed.
Its storage will be updated, and it may return some operations which will also be executed.
The destination contract must be originated first.
* The `run` command originates a smart contract and transfers tokens to it.
By default, it doesn't update the global state.
It's intended to be used when one just wants to quickly interpret one contract and doesn't care about a global state file.
The command still needs to read the DB (JSON file) to know about all originated contracts and their parameters.
It's needed for the `CONTRACT` instruction.
Other environment data can be passed from CLI, but it's not mandatory, and default values are used if they are not passed explicitly.

Examples:
* `morley transfer --to KT1L39q6uCg1wQPB796q5oQQgDW673uo1s5y --parameter 'Left 10'` (`KT1L39q6uCg1wQPB796q5oQQgDW673uo1s5y` must be originated first).
* `morley run --contract foo.tz --parameter '"aaa"' --storage Unit` (in this case the contract will be originated automatically).

By default, each operation uses _genesis address_ as a sender.
This address initially has a lot of tokens.
We do not require signatures for transactions, anyone can spend tokens without any private keys because it's irrelevant to Michelson.
