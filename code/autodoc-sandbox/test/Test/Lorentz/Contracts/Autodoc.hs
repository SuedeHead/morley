-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.Contracts.Autodoc
  ( test_documentation
  ) where

import Test.Tasty (TestTree)

import Lorentz.Test

import Lorentz.Contracts.Autodoc

test_documentation :: [TestTree]
test_documentation =
  runDocTests testLorentzDoc autodocSandboxContract
