Unreleased
==========

* Change the license to MIT.

0.3.0
=====

* Hide `Nat` export.

0.2.0.1
=======

* Update maintainer.

0.2.0
======

* Hide `readFile` and `writeFile`.
* Add `Unsafe` module which re-exports `Universum.Unsafe`.

0.1.0.4
=====

Initial release.
Re-exports `universum`.
