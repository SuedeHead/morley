-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Parser
  ( test_bakerFeeParser
  ) where

import Test.Hspec.Expectations (shouldBe, shouldSatisfy)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Morley.Client.TezosClient.Parser
import Morley.Micheline
import Tezos.Core

test_bakerFeeParser :: TestTree
test_bakerFeeParser = testGroup "tezos-client baker fee parser"
  [ testCase "resulted fee is multiplied by 1e6" $
    parseBakerFeeFromOutput "Fee to the baker: ꜩ0.056008\n" `shouldBe`
    Right (TezosMutez $ toMutez 56008)
  , testCase "no valid baker fee in the output" $
    parseBakerFeeFromOutput "Notfee to the baker: ꜩ0.056008\n" `shouldSatisfy`
    isLeft
  ]
