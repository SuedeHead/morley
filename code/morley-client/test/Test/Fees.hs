-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Fees
  ( test_Fees_comp_iterations
  ) where

import Data.Map (fromList)
import Test.HUnit ((@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import qualified Lorentz as L
import Michelson.Runtime.GState (genesisAddress1, genesisAddress2)
import Michelson.Untyped.Entrypoints
import Morley.Client.Action.Origination
import Morley.Client.Action.Transaction
import Morley.Client.RPC.Types
import Morley.Client.TezosClient
import Test.Util
import TestM
import Tezos.Core (toMutez)

mockState
  :: MockState
mockState = defaultMockState
  { msContracts = fromList $
    [ (genesisAddress1, dumbImplicitContractState)
    , (genesisAddress2, dumbContractState)
    ]
  }

countForgesHandlers :: Handlers $ TestT (State Word)
countForgesHandlers = chainOperationHandlers
  { hForgeOperation = \op -> do
      liftToMockTest $ modify (+1)
      hForgeOperation chainOperationHandlers op

  , hRunOperation = \RunOperation{..} -> do
      originatedContracts <- handleRunOperationInternal roOperation
      return RunOperationResult
        { rrOperationContents =
          [ OperationContent $ RunMetadata
            { rmOperationResult = OperationApplied $
              -- Real-life numbers
              AppliedResult
                { arConsumedGas = 10100
                , arStorageSize = 250
                , arPaidStorageDiff = 250
                , arOriginatedContracts = originatedContracts
                , arAllocatedDestinationContracts = 0
                }
            , rmInternalOperationResults = []
            }
          ]
        }
  }

runForgesCountingTest :: HasCallStack => TestT (State Word) a -> Word
runForgesCountingTest action = do
  let (res, count) =
        usingState 0 $ runMockTestT countForgesHandlers mockState action
  case res of
    Left e -> error . toText $ "Test action failed: " <> displayException e
    Right _ -> count

averageContract :: L.Contract () ()
averageContract = mkContract $
  L.unpair L.# L.drop L.#
  foldl1 (L.#) (replicate 100 (L.push () L.# L.drop)) L.#
  L.nil L.# L.pair
  where
    mkContract c =
      (L.defaultContract c){ L.cCompilationOptions = L.intactCompilationOptions }

-- | For small contracts we would like to find proper fees in
-- one hop since fees evaluation requires RPC calls
-- (though lightweight ones like forgeOperation).
--
-- In case this test fails, it would be nice to adjust initial fees
-- so that we again need only one iteration. If that is impossible,
-- update/remove this test.
test_Fees_comp_iterations :: [TestTree]
test_Fees_comp_iterations =
  [ testCase "One transaction" $
      let forgeCalls =
            runForgesCountingTest $
              lTransfer genesisAddress1 genesisAddress2
                (toMutez 10) DefEpName () Nothing
      in forgeCalls @?= sum
          [ 2  -- for fees adjustment
          , 1  -- check on fees being on par
          , 0  -- forging the entire batch - reusing the previously forged op
          ]

  , testCase "One origination"
      let forgeCalls =
            runForgesCountingTest $
              lOriginateContract True "c"
                (AddressResolved genesisAddress1) (toMutez 10)
                averageContract () Nothing
      in forgeCalls @?= sum
          [ 2  -- for fees adjustment
          , 1  -- check on fees being on par
          , 0  -- forging the entire batch - reusing the previously forged op
          ]

  ]
