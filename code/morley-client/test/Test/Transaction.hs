-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Transaction
  ( test_lRunTransactionsUnit
  ) where

import Data.Map (fromList)
import Test.Hspec.Expectations (shouldThrow)
import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Michelson.Runtime.GState (genesisAddress1, genesisAddress2, genesisAddress3)
import Michelson.Untyped.Entrypoints
import Morley.Client.Action.Transaction
import Test.Util
import TestM
import Tezos.Core (unsafeMkMutez)

mockState
  :: MockState
mockState = defaultMockState
  { msContracts = fromList $
    [ (genesisAddress1, dumbImplicitContractState)
    , (genesisAddress2, dumbContractState)
    ]
  }

test_lRunTransactionsUnit :: TestTree
test_lRunTransactionsUnit = testGroup "Mock test transaction sending"
  [ testCase "Successful transaction" $ handleSuccess $
    runMockTest chainOperationHandlers mockState $
      lTransfer genesisAddress1 genesisAddress2
        (unsafeMkMutez 10) DefEpName () Nothing
  , testCase "Sender doesn't exist" $ handleUnknownContract $
    runMockTest chainOperationHandlers mockState $
      lTransfer genesisAddress3 genesisAddress2
        (unsafeMkMutez 10) DefEpName () Nothing
  , testCase "Destination doesn't exist" $ handleUnknownContract $
    runMockTest chainOperationHandlers mockState $
      lTransfer genesisAddress1 genesisAddress3
        (unsafeMkMutez 10) DefEpName () Nothing
  ]
  where
    handleSuccess :: Either SomeException a -> Assertion
    handleSuccess (Right _) = pass
    handleSuccess (Left e) = assertFailure $ displayException e

    handleUnknownContract :: Either SomeException a -> Assertion
    handleUnknownContract (Right _) =
      assertFailure "Transaction sending unexpectedly didn't fail."
    handleUnknownContract (Left e) =
      shouldThrow (throwM e) $ \case
        UnknownContract _ -> True
        _ -> False
