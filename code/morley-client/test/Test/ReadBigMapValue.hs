-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.ReadBigMapValue
  ( test_ReadBigMapValueUnit
  , test_ReadBigMapValueMaybeUnit
  ) where

import Data.Map (fromList)
import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Michelson.Runtime.GState (genesisAddress1)
import Morley.Client.RPC.Getters
import Test.Util
import TestM

bigMapGetHandlers :: Handlers TestM
bigMapGetHandlers = defaultHandlers
  { hGetBigMapValue = handleGetBigMapValue
  }

mockStateWithBigMapContract
  :: MockState
mockStateWithBigMapContract = defaultMockState
  { msContracts = fromList $
    [ (genesisAddress1, dumbContractState
        { csMbExplicit = mapSnd (csMbExplicit dumbContractState) $ \_ ->
            Just $ mapToContractStateBigMap @Integer @Integer validBigMapId $ fromList [(2, 3), (3, 5)]
        })
    ]
  }
  where
    mapSnd :: Maybe (a, b) -> (b -> c) -> Maybe (a, c)
    mapSnd (Just (a, b)) f = Just (a, f b)
    mapSnd Nothing _ = Nothing

validBigMapId :: BigMapId Integer Integer
validBigMapId = BigMapId 123

invalidBigMapId :: BigMapId Integer Integer
invalidBigMapId = BigMapId 456

resultShouldBe :: (HasCallStack, Eq a, Show a) => a -> Either SomeException a -> Assertion
resultShouldBe expected = \case
  Left e -> assertFailure $ displayException e
  Right actual -> actual @?= expected

test_ReadBigMapValueUnit :: TestTree
test_ReadBigMapValueUnit =
  testGroup "readBigMapValue"
    [ testCase "Retrieves existing value" $
        resultShouldBe 5 $
          runMockTest bigMapGetHandlers mockStateWithBigMapContract $
            readBigMapValue validBigMapId (3 :: Integer)
    ]

test_ReadBigMapValueMaybeUnit :: TestTree
test_ReadBigMapValueMaybeUnit =
  testGroup "readBigMapValueMaybe"
    [ testCase "Retrieves existing value" $
        resultShouldBe (Just 5) $
          runMockTest bigMapGetHandlers mockStateWithBigMapContract $
            readBigMapValueMaybe validBigMapId (3 :: Integer)
    , testCase "Returns Nothing when contract does not exist" $
        resultShouldBe Nothing $
          runMockTest bigMapGetHandlers mockStateWithBigMapContract $
            readBigMapValueMaybe invalidBigMapId (3 :: Integer)
    , testCase "Returns Nothing when key does not exist" $
        resultShouldBe Nothing $
          runMockTest bigMapGetHandlers mockStateWithBigMapContract $
            readBigMapValueMaybe validBigMapId (9 :: Integer)
    ]
