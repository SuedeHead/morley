-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.ParameterTypeGet
  ( test_parameterTypeGetUnit
  ) where

import Data.Map as Map (empty, fromList)
import Test.HUnit (Assertion, assertEqual, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import qualified Lorentz as L

import Michelson.Runtime.GState (genesisAddress1)
import Michelson.TypeCheck.TypeCheck (SomeParamType, mkSomeParamTypeUnsafe)
import Michelson.Typed
import qualified Michelson.Untyped as U
import Morley.Client
import Morley.Client.RPC.Getters
import Morley.Client.RPC.Types
import Morley.Micheline
import Test.Util
import TestM
import Tezos.Address

testContract :: L.NiceParameterFull param => L.Contract param ()
testContract = L.defaultContract $
  L.cdr L.# L.nil L.# L.pair

buildSmartContractState
  :: L.NiceParameterFull param => Alias -> L.Contract param () -> ContractState
buildSmartContractState alias contract = ContractState
  { csCounter = 100500
  , csAlias = alias
  , csMbExplicit = Just
    ( OriginationScript
      { osCode = toExpression $ L.compileLorentzContract contract
      , osStorage = toExpression $ toVal ()
      }
    , Nothing
    )
  }

contractHash1, contractHash2, contractHash3 :: ContractHash
contractHash1 = ContractHash "lol"
contractHash2 = ContractHash "kek"
contractHash3 = ContractHash "mda"

smartContractAddr1, smartContractAddr2, smartContractAddr3 :: Address
smartContractAddr1 = ContractAddress contractHash1
smartContractAddr2 = ContractAddress contractHash2
smartContractAddr3 = ContractAddress contractHash3

mockStateWithSmartContracts
  :: MockState
mockStateWithSmartContracts = defaultMockState
  { msContracts = fromList $
    [ (smartContractAddr1, buildSmartContractState "lol" (testContract @Natural))
    , (smartContractAddr2, buildSmartContractState "kek" (testContract @Bool))
    , (genesisAddress1, dumbImplicitContractState)
    ]
  }

test_parameterTypeGetUnit :: TestTree
test_parameterTypeGetUnit = testGroup "Mock test big map getter"
  [ testCase "Only parameters for smart contracts are extracted" $ expectContractMap
    (runMockTest chainOperationHandlers mockStateWithSmartContracts $
      getContractsParameterTypes
      [genesisAddress1, smartContractAddr1, smartContractAddr2]
    ) $ fromList
    [ (contractHash1, mkSomeParamTypeUnsafe $ U.ParameterType (U.Type U.TNat U.noAnn) U.noAnn)
    , (contractHash2, mkSomeParamTypeUnsafe $ U.ParameterType (U.Type U.TBool U.noAnn) U.noAnn)
    ]
  , testCase "Parameter type for nonexistent smart contract is not extracted" $
    expectContractMap
    (runMockTest chainOperationHandlers mockStateWithSmartContracts $
      getContractsParameterTypes
      [smartContractAddr3]
    ) $ Map.empty
  ]
  where
    expectContractMap
      :: Either SomeException (Map ContractHash SomeParamType)
      -> Map ContractHash SomeParamType
      -> Assertion
    expectContractMap (Right found) expected =
      assertEqual "unexpected smart contract map" found expected
    expectContractMap (Left e) _ = assertFailure $ displayException e
