-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-star-is-type #-}

module Test.AsRPC
  ( unit_Renames_constructors_fields_and_generic_metadata
  , unit_Can_derive_many_instances_at_once
  , unit_Supports_higher_kinded_types
  ) where

import Data.Typeable ((:~:)(Refl))
import GHC.Generics
import qualified Language.Haskell.TH.Syntax as TH
import Test.Tasty.HUnit (Assertion)

import Lorentz (BigMap, IsoValue(ToT), MText, customGeneric, leftBalanced, ligoLayout)
import Morley.Client.RPC (BigMapId)
import Morley.Client.RPC.AsRPC

import Test.Util (shouldCompileIgnoringInstance, shouldCompileTo)

data ExampleStorage a b = ExampleStorage
  { _esField1 :: Integer
  , _esField2 :: [BigMap Integer MText]
  , _esField3 :: a
  }
  deriving stock Generic
  deriving anyclass IsoValue
deriveRPC "ExampleStorage"

unit_Renames_constructors_fields_and_generic_metadata :: Assertion
unit_Renames_constructors_fields_and_generic_metadata = do
  $(deriveRPC "ExampleStorage" >>= TH.lift) `shouldCompileTo`
    [d|
      data ExampleStorageRPC a (b :: k) = ExampleStorageRPC
        { _esField1RPC :: AsRPC Integer
        , _esField2RPC :: AsRPC [BigMap Integer MText]
        , _esField3RPC :: AsRPC a
        }

      type instance AsRPC (ExampleStorage a (b :: k)) = ExampleStorageRPC a (b :: k)
      deriving anyclass instance IsoValue (AsRPC a) => IsoValue (ExampleStorageRPC a (b :: k))

      instance Generic (ExampleStorageRPC a (b :: k))
          where type Rep (ExampleStorageRPC a
                                      (b :: k)) = D1 ('MetaData "ExampleStorageRPC" "Test.AsRPC" "main" 'False)
                                                      (C1 ('MetaCons "ExampleStorageRPC" 'PrefixI 'True)
                                                          ((:*:) (S1 ('MetaSel ('Just "_esField1RPC")
                                                                              'NoSourceUnpackedness
                                                                              'NoSourceStrictness
                                                                              'DecidedStrict)
                                                                    (Rec0 (AsRPC Integer)))
                                                                ((:*:) (S1 ('MetaSel ('Just "_esField2RPC")
                                                                                      'NoSourceUnpackedness
                                                                                      'NoSourceStrictness
                                                                                      'DecidedStrict)
                                                                            (Rec0 (AsRPC ([BigMap Integer
                                                                                                  MText]))))
                                                                        (S1 ('MetaSel ('Just "_esField3RPC")
                                                                                      'NoSourceUnpackedness
                                                                                      'NoSourceStrictness
                                                                                      'DecidedStrict)
                                                                            (Rec0 (AsRPC a))))))
                from (ExampleStorageRPC v0
                                  v1
                                  v2) = M1 (M1 ((:*:) (M1 (K1 v0)) ((:*:) (M1 (K1 v1)) (M1 (K1 v2)))))
                to (M1 (M1 ((:*:) (M1 (K1 v0))
                                  ((:*:) (M1 (K1 v1)) (M1 (K1 v2)))))) = ExampleStorageRPC v0 v1 v2
    |]

data Ex1 = Ex1 Integer Ex1Inner
  deriving stock Generic
  deriving anyclass IsoValue
data Ex1Inner = Ex1Inner Integer
  deriving stock Generic
  deriving anyclass IsoValue

data Ex2 = Ex2 Integer
  deriving stock Generic
  deriving anyclass IsoValue
deriveRPC "Ex2"

data Ex3 = Ex3 Integer
  deriving stock (Generic, Eq, Ord)
  deriving anyclass IsoValue

data Ex4 a = Ex4 a
  deriving stock Generic
  deriving anyclass IsoValue

data ExampleMany = ExampleMany
  { _emField1 :: Integer
  , _emField2 :: Ex1
  , _emField3 :: Ex2
  , _emField4 :: [BigMap Ex3 (Ex4 MText)]
  }
  deriving stock Generic
  deriving anyclass IsoValue

-- This empty slice is a hack, so that all the declarations above and their instances
-- may be in the type environment when we reify them in the test below.
$(pure [])

unit_Can_derive_many_instances_at_once :: Assertion
unit_Can_derive_many_instances_at_once = do
  shouldCompileIgnoringInstance ''Generic
    $(deriveManyRPC "ExampleMany" ["Ex3"] >>= TH.lift)
    [d|
      data ExampleManyRPC = ExampleManyRPC
        {_emField1RPC :: AsRPC Integer
        , _emField2RPC :: AsRPC Ex1
        , _emField3RPC :: AsRPC Ex2
        , _emField4RPC :: AsRPC [BigMap Ex3 (Ex4 MText)]
        }
      type instance AsRPC ExampleMany = ExampleManyRPC
      deriving anyclass instance IsoValue ExampleManyRPC

      -- An instance is generated for Ex1
      data Ex1RPC = Ex1RPC (AsRPC Integer) (AsRPC Ex1Inner)
      type instance AsRPC Ex1 = Ex1RPC
      deriving anyclass instance IsoValue Ex1RPC

      -- An instance is generated for Ex1's fields' types
      data Ex1InnerRPC = Ex1InnerRPC (AsRPC Integer)
      type instance AsRPC Ex1Inner = Ex1InnerRPC
      deriving anyclass instance IsoValue Ex1InnerRPC

      -- No instance is generated for Ex2, because one already exists

      -- No instance is generated for Ex3, because we explicitly said we don't want one

      -- An instance is generated for BigMap's concrete type arguments
      data Ex4RPC a = Ex4RPC (AsRPC a)
      type instance AsRPC (Ex4 a) = Ex4RPC a
      deriving anyclass instance IsoValue (AsRPC a) => IsoValue (Ex4RPC a)
    |]

-- Check that the declarations generated by `deriveManyRPC` actually compile.
deriveManyRPC "ExampleMany" []

----------------------------------------------------------------------------
-- AsRPC type family
----------------------------------------------------------------------------

checkLaws :: ToT (AsRPC t) :~: AsRPC (ToT t) -> ()
checkLaws Refl = ()

----------------------------------------------------------------------------
-- Examples data types:
--
-- Simple data type
----------------------------------------------------------------------------

data Simple = Simple Integer Integer [Integer]
  deriving stock Generic
  deriving anyclass IsoValue
deriveRPC "Simple"

data ExpectedSimpleRPC = ExpectedSimpleRPC Integer Integer [Integer]
  deriving stock Generic
  deriving anyclass IsoValue

_checkSimple :: ToT SimpleRPC :~: ToT ExpectedSimpleRPC
_checkSimple = Refl

_checkSimpleLaws :: ()
_checkSimpleLaws = checkLaws @Simple Refl

----------------------------------------------------------------------------
-- Simple record data type
----------------------------------------------------------------------------

data SimpleRecord = SimpleRecord
  { _simpleRecordField1 :: Integer
  , _simpleRecordField2 :: Integer
  , _simpleRecordField3 :: [Integer]
  }
  deriving stock Generic
  deriving anyclass IsoValue
deriveRPC "SimpleRecord"

data ExpectedSimpleRecordRPC = ExpectedSimpleRecordRPC
  { eSimpleRecordField1 :: Integer
  , eSimpleRecordField2 :: Integer
  , eSimpleRecordField3 :: [Integer]
  }
  deriving stock Generic
  deriving anyclass IsoValue

_checkSimpleRecord :: ToT SimpleRecordRPC :~: ToT ExpectedSimpleRecordRPC
_checkSimpleRecord = Refl

_checkSimpleRecordLaws :: ()
_checkSimpleRecordLaws = checkLaws @SimpleRecord Refl

----------------------------------------------------------------------------
-- Data type with bigmap fields
----------------------------------------------------------------------------

data WithBigMap = WithBigMap
  { _wbmField1 :: Integer
  , _wbmField2 :: BigMap Integer Integer
  , _wbmField3 :: [BigMap Integer Integer]
  }
  deriving stock Generic
  deriving anyclass IsoValue
deriveRPC "WithBigMap"

data ExpectedWithBigMapRPC = ExpectedWithBigMapRPC
  { expectedWbmField1 :: Integer
  , expectedWbmField2 :: BigMapId Integer Integer
  , expectedWbmField3 :: [BigMapId Integer Integer]
  }
  deriving stock Generic
  deriving anyclass IsoValue

_checkWithBigMap :: ToT WithBigMapRPC :~: ToT ExpectedWithBigMapRPC
_checkWithBigMap = Refl

_checkWithBigMapLaws :: ()
_checkWithBigMapLaws = checkLaws @WithBigMap Refl

----------------------------------------------------------------------------
-- Data type with nested bigmap fields
----------------------------------------------------------------------------

data WithNestedBigMap = WithNestedBigMap
  { _wnbmField1 :: BigMap Integer (BigMap Integer Integer)
  }
  deriving stock Generic
  deriving anyclass IsoValue
deriveRPC "WithNestedBigMap"

data ExpectedWithNestedBigMapRPC = ExpectedWithNestedBigMapRPC
  { expectedWnbmField1 :: BigMapId Integer (BigMap Integer Integer)
  }
  deriving stock Generic
  deriving anyclass IsoValue

_checkWithNestedBigMap :: ToT WithNestedBigMapRPC :~: ToT ExpectedWithNestedBigMapRPC
_checkWithNestedBigMap = Refl

_checkWithNestedBigMapLaws :: ()
_checkWithNestedBigMapLaws = checkLaws @WithNestedBigMap Refl

----------------------------------------------------------------------------
-- Data type with custom generic strategy
----------------------------------------------------------------------------

data WithGenericStrategy
  = WithGenericStrategy_1 Integer Integer Integer
  | WithGenericStrategy_2 Integer Integer Integer
  | WithGenericStrategy_3 Integer Integer Integer
  | WithGenericStrategy_4 Integer Integer Integer

deriving anyclass instance IsoValue WithGenericStrategy
customGeneric "WithGenericStrategy" leftBalanced
deriveRPCWithStrategy "WithGenericStrategy" leftBalanced

_checkWithGenericStrategy :: ToT WithGenericStrategyRPC :~: ToT WithGenericStrategy
_checkWithGenericStrategy = Refl

_checkWithGenericStrategyLaws :: ()
_checkWithGenericStrategyLaws = checkLaws @WithGenericStrategy Refl

----------------------------------------------------------------------------
-- Data type with reordered fields
----------------------------------------------------------------------------

data WithReordered = WithRedordered
  { _wrField1 :: Integer
  , _wrField3 :: MText
  , _wrField2 :: [Integer]
  }

deriving anyclass instance IsoValue WithReordered
customGeneric "WithReordered" ligoLayout
deriveRPCWithStrategy "WithReordered" ligoLayout

_checkWithReordered :: ToT WithReorderedRPC :~: ToT WithReordered
_checkWithReordered = Refl

_checkWithReorderedLaws :: ()
_checkWithReorderedLaws = checkLaws @WithReordered Refl

----------------------------------------------------------------------------
-- Data type with type variables
----------------------------------------------------------------------------

data WithTypeVariables a = WithTypeVariables
  { _wtvField1 :: a
  }
  deriving stock Generic
  deriving anyclass IsoValue
deriveRPC "WithTypeVariables"

data ExpectedWithTypeVariablesRPC = ExpectedWithTypeVariablesRPC
  { expectedWtvField1 :: BigMapId Integer MText
  }
  deriving stock Generic
  deriving anyclass IsoValue

_checkWithTypeVariables :: ToT (WithTypeVariablesRPC (BigMap Integer MText)) :~: ToT ExpectedWithTypeVariablesRPC
_checkWithTypeVariables = Refl

_checkWithTypeVariablesLaws :: ()
_checkWithTypeVariablesLaws = checkLaws @(WithTypeVariables (BigMap Integer MText)) Refl

----------------------------------------------------------------------------
-- Data type with nested data types and type variables
----------------------------------------------------------------------------

data WithNested a = WithNested
  { _wnField1 :: WithNested2 a
  , _wnField2 :: [WithNested2 a]
  , _wnField3 :: WithNested2 [a]
  }
  deriving stock Generic
deriving anyclass instance IsoValue a => IsoValue (WithNested a)

data WithNested2 a = WithNested2
  { _wn2Field1 :: a
  , _wn2Field2 :: [a]
  }
  deriving stock Generic
  deriving anyclass IsoValue

deriveManyRPC "WithNested" []

data ExpectedWithNestedRPC = ExpectedWithNestedRPC
  { expectedWnField1 :: ExpectedWithNested2RPC
  , expectedWnField2 :: [ExpectedWithNested2RPC]
  , expectedWnField3 :: ExpectedWithNested2RPC'
  }
  deriving stock Generic
  deriving anyclass IsoValue

data ExpectedWithNested2RPC = ExpectedWithNested2RPC
  { expectedWn2Field1 :: BigMapId Integer MText
  , expectedWn2Field2 :: [BigMapId Integer MText]
  }
  deriving stock Generic
  deriving anyclass IsoValue

data ExpectedWithNested2RPC' = ExpectedWithNested2RPC'
  { expectedWn2Field1' :: [BigMapId Integer MText]
  , expectedWn2Field2' :: [[BigMapId Integer MText]]
  }
  deriving stock Generic
  deriving anyclass IsoValue

_checkWithNested2 :: ToT (WithNested2RPC (BigMap Integer MText)) :~: ToT ExpectedWithNested2RPC
_checkWithNested2 = Refl

_checkWithNested :: ToT (WithNestedRPC (BigMap Integer MText)) :~: ToT ExpectedWithNestedRPC
_checkWithNested = Refl

_checkWithNestedLaws :: ()
_checkWithNestedLaws = checkLaws @(WithNested (BigMap Integer MText)) Refl

_checkWithNested2Laws :: ()
_checkWithNested2Laws = checkLaws @(WithNested2 (BigMap Integer MText)) Refl

----------------------------------------------------------------------------
-- Data type with phantom type variables
----------------------------------------------------------------------------

data WithPhantom a b c = WithPhantom
  { _wpField1 :: Integer
  , _wpField2 :: b
  }
  deriving stock Generic
  deriving anyclass IsoValue
deriveRPC "WithPhantom"

data ExpectedWithPhantomRPC = ExpectedWithPhantomRPC
  { expectedWpField1 :: Integer
  , expectedWpField2 :: MText
  }
  deriving stock Generic
  deriving anyclass IsoValue

_checkWithPhantom :: ToT (WithPhantomRPC Integer MText (BigMap Integer Integer)) :~: ToT ExpectedWithPhantomRPC
_checkWithPhantom = Refl

_checkWithPhantomLaws :: ()
_checkWithPhantomLaws = checkLaws @(WithPhantom Integer MText (BigMap Integer Integer)) Refl

----------------------------------------------------------------------------
-- Data type with higher-kinded type variables
----------------------------------------------------------------------------

data WithHigherKind f = WithHigherKind
  { _whkField1 :: WithHigherKindNested f
  }
  deriving stock Generic
deriving anyclass instance (IsoValue (f Integer MText)) => IsoValue (WithHigherKind f)

data WithHigherKindNested f = WithHigherKindNested
  { _whknField1 :: f Integer MText
  }
  deriving stock Generic
deriving anyclass instance (IsoValue (f Integer MText)) => IsoValue (WithHigherKindNested f)

$(pure [])

unit_Supports_higher_kinded_types :: Assertion
unit_Supports_higher_kinded_types = do
  shouldCompileIgnoringInstance ''Generic
    $(deriveManyRPC "WithHigherKind" [] >>= TH.lift)
    [d|
      data WithHigherKindRPC (f :: * -> * -> *) = WithHigherKindRPC
        { _whkField1RPC :: AsRPC (WithHigherKindNested f)
        }
      type instance AsRPC (WithHigherKind (f :: * -> * -> *))
        = WithHigherKindRPC (f :: * -> * -> *)
      deriving anyclass instance IsoValue (AsRPC (WithHigherKindNested f))
        => IsoValue (WithHigherKindRPC (f :: * -> * -> *))

      data WithHigherKindNestedRPC (f :: * -> * -> *) = WithHigherKindNestedRPC
        { _whknField1RPC :: AsRPC (f Integer MText)
        }
      type instance AsRPC (WithHigherKindNested (f :: * -> * -> *))
        = WithHigherKindNestedRPC (f :: * -> * -> *)
      deriving anyclass instance IsoValue (AsRPC (f Integer MText))
        => IsoValue (WithHigherKindNestedRPC (f :: * -> * -> *))
    |]

deriveManyRPC "WithHigherKind" []

data ExpectedWithHigherKindRPC = ExpectedWithHigherKindRPC
  { expectedWhkField1 :: ExpectedWithHigherKindNestedRPC
  }
  deriving stock Generic
  deriving anyclass IsoValue

data ExpectedWithHigherKindNestedRPC = ExpectedWithHigherKindNestedRPC
  { expectedWhknField1 :: Map Integer MText
  }
  deriving stock Generic
  deriving anyclass IsoValue

_checkWithHigherKindNested :: ToT (WithHigherKindNestedRPC Map) :~: ToT ExpectedWithHigherKindNestedRPC
_checkWithHigherKindNested = Refl

_checkWithHigherKind :: ToT (WithHigherKindRPC Map) :~: ToT ExpectedWithHigherKindRPC
_checkWithHigherKind = Refl

_checkWithHigherKindLaws :: ()
_checkWithHigherKindLaws = checkLaws @(WithHigherKind Map) Refl

_checkWithHigherKindNestedLaws :: ()
_checkWithHigherKindNestedLaws = checkLaws @(WithHigherKindNested Map) Refl
