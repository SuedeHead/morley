-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Module that defines some basic infrastructure
-- for mocking tezos-node RPC interaction.
module TestM
  ( ContractState (..)
  , ContractStateBigMap (..)
  , Handlers (..)
  , MockState (..)
  , TestError (..)
  , TestM
  , TestT
  , defaultHandlers
  , defaultMockState
  , runMockTest
  , runMockTestT
  , liftToMockTest
  ) where

import Colog.Core.Class (HasLog(..))
import Colog.Message (Message)
import Control.Monad.Catch.Pure (CatchT(..))
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)

import Michelson.Typed.Scope (PrintedValScope)
import Morley.Client
import Morley.Client.Logging (ClientLogAction)
import Morley.Client.RPC
import Morley.Client.TezosClient (CalcOriginationFeeData, CalcTransferFeeData, TezosClientConfig)
import Morley.Micheline
import Tezos.Address
import Tezos.Core
import Tezos.Crypto (PublicKey, SecretKey, Signature)
import Util.ByteString

-- | A test-specific orphan.
deriving newtype instance IsString Alias

-- | Reader environment to interact with the mock state.
data Handlers m = Handlers
  { -- HasTezosRpc
    hGetHeadBlock :: m Text
  , hGetCounter :: Address -> m TezosInt64
  , hGetBlockConstants :: Text -> m BlockConstants
  , hGetProtocolParameters :: m ProtocolParameters
  , hRunOperation :: RunOperation -> m RunOperationResult
  , hPreApplyOperations :: [PreApplyOperation] -> m [RunOperationResult]
  , hForgeOperation :: ForgeOperation -> m HexJSONByteString
  , hInjectOperation :: HexJSONByteString -> m Text
  , hGetContractScript :: Address -> m OriginationScript
  , hGetContractBigMap :: Address -> GetBigMap -> m GetBigMapResult
  , hGetBigMapValue :: Natural -> Text -> m Expression
  , hGetBalance :: Address -> m Mutez
  , hRunCode :: RunCode -> m RunCodeResult
  , hGetChainId :: m ChainId

  -- HasTezosClient
  , hSignBytes :: AddressOrAlias -> ByteString -> m Signature
  , hGenKey :: AliasHint -> m Address
  , hGenFreshKey :: AliasHint -> m Address
  , hRevealKey :: Alias -> m ()
  , hWaitForOperation :: Text -> m ()
  , hRememberContract :: Bool -> Address -> AliasHint -> m ()
  , hImportKey :: Bool -> AliasHint -> SecretKey -> m ()
  , hResolveAddressMaybe :: AddressOrAlias -> m (Maybe Address)
  , hGetAlias :: AddressOrAlias -> m Alias
  , hGetPublicKey :: AddressOrAlias -> m PublicKey
  , hGetTezosClientConfig :: m TezosClientConfig
  , hCalcTransferFee
    :: forall t. PrintedValScope t => CalcTransferFeeData t -> m TezosMutez
  , hCalcOriginationFee
    :: forall cp st. PrintedValScope st => CalcOriginationFeeData cp st -> m TezosMutez

  -- HasLog
  , hLogAction :: ClientLogAction m
  }

defaultHandlers :: Monad m => Handlers (TestT m)
defaultHandlers = Handlers
  { hGetHeadBlock = throwM $ UnexpectedRpcCall "getHeadBlock"
  , hGetCounter = \_ -> throwM $ UnexpectedRpcCall "getCounter"
  , hGetBlockConstants = \_ -> throwM $ UnexpectedRpcCall "getBlockConstants"
  , hGetProtocolParameters = throwM $ UnexpectedRpcCall "getProtocolParameters"
  , hRunOperation = \_ -> throwM $ UnexpectedRpcCall "runOperation"
  , hPreApplyOperations = \_ -> throwM $ UnexpectedRpcCall "preApplyOperations"
  , hForgeOperation = \_ -> throwM $ UnexpectedRpcCall "forgeOperation"
  , hInjectOperation = \_ -> throwM $ UnexpectedRpcCall "injectOperation"
  , hGetContractScript = \_ -> throwM $ UnexpectedRpcCall "getContractScript"
  , hGetContractBigMap = \_ _ -> throwM $ UnexpectedRpcCall "getContractBigMap"
  , hGetBigMapValue = \_ _ -> throwM $ UnexpectedRpcCall "getBigMapValue"
  , hGetBalance = \_ -> throwM $ UnexpectedRpcCall "getBalance"
  , hRunCode = \_ -> throwM $ UnexpectedRpcCall "runCode"
  , hGetChainId = throwM $ UnexpectedRpcCall "getChainId"

  , hSignBytes = \_ _ -> throwM $ UnexpectedClientCall "signBytes"
  , hGenKey = \_ -> throwM $ UnexpectedClientCall "genKey"
  , hGenFreshKey = \_ -> throwM $ UnexpectedRpcCall "genFreshKey"
  , hRevealKey = \_ -> throwM $ UnexpectedClientCall "revealKey"
  , hWaitForOperation = \_ -> throwM $ UnexpectedRpcCall "waitForOperation"
  , hRememberContract = \_ _ _ -> throwM $ UnexpectedClientCall "hRememberContract"
  , hImportKey = \_ _ _ -> throwM $ UnexpectedClientCall "importKey"
  , hResolveAddressMaybe = \_ -> throwM $ UnexpectedRpcCall "resolveAddressMaybe"
  , hGetAlias = \_ -> throwM $ UnexpectedRpcCall "getAlias"
  , hGetPublicKey = \_ -> throwM $ UnexpectedRpcCall "getPublicKey"
  , hGetTezosClientConfig = throwM $ UnexpectedClientCall "getTezosClientConfig"
  , hCalcTransferFee = \_ -> throwM $ UnexpectedClientCall "calcTransferFee"
  , hCalcOriginationFee = \_ -> throwM $ UnexpectedClientCall "calcOriginationFee"

  , hLogAction = mempty
  }

-- | Type to represent contract state in the @MockState@.
-- This type can represent both implicit and explicit contracts.
-- For implicit contracts (i.e. addresses beginning with tz1, tz2, or tz3),
-- @csMbExplicit@ field should be Nothing.
data ContractState = ContractState
  { csCounter :: TezosInt64
  , csAlias :: Alias
  , csMbExplicit :: Maybe (OriginationScript, Maybe ContractStateBigMap)
  }

-- | Type to represent big_map in @ContractState@.
data ContractStateBigMap = ContractStateBigMap
  { csbmKeyType :: Expression
  , csbmValueType :: Expression
  , csbmMap :: Map Text ByteString
  -- ^ Real tezos bigmap also has deserialized keys and values
  , csbmId :: Natural
  -- ^ The big_map's ID
  }

newtype TestHandlers m = TestHandlers {unTestHandlers :: Handlers (TestT m)}

-- | Type to represent chain state in mock tests.
data MockState = MockState
  { msContracts :: Map Address ContractState
  , msHeadBlock :: Text
  , msBlockConstants :: BlockConstants
  , msProtocolParameters :: ProtocolParameters
  }

defaultMockState :: MockState
defaultMockState = MockState
  { msContracts = mempty
  , msHeadBlock = "HEAD"
  , msBlockConstants = BlockConstants
    { bcProtocol = "PROTOCOL"
    , bcChainId = "CHAIN_ID"
    , bcHeader = BlockHeader
      { bhTimestamp = posixSecondsToUTCTime 0
      }
    }
  , msProtocolParameters = ProtocolParameters 257 1040000 60000 [30, 40]
  }

type TestT m = StateT MockState (ReaderT (TestHandlers m) (CatchT m))

type TestM = TestT Identity

runMockTestT
  :: forall a m. Monad m =>
     Handlers (TestT m) -> MockState -> TestT m a -> m (Either SomeException a)
runMockTestT handlers mockState action =
  runCatchT $ runReaderT (evalStateT action mockState) (TestHandlers handlers)

runMockTest
  :: forall a. Handlers TestM -> MockState -> TestM a -> Either SomeException a
runMockTest =
  runIdentity ... runMockTestT

getHandler :: Monad m => (Handlers (TestT m) -> fn) -> TestT m fn
getHandler fn = fn . unTestHandlers <$> ask

liftToMockTest :: Monad m => m a -> TestT m a
liftToMockTest = lift . lift . lift

-- | Various mock test errors.
data TestError
  = UnexpectedRpcCall Text
  | UnexpectedClientCall Text
  | UnknownContract Address
  | UnexpectedImplicitContract Address
  | ContractDoesntHaveBigMap Address
  | InvalidChainId
  | InvalidProtocol
  | CounterMismatch
  deriving stock Show

instance Exception TestError

instance HasLog (TestHandlers m) Message (TestT m) where
  getLogAction = hLogAction . unTestHandlers
  setLogAction action (TestHandlers handlers) =
    TestHandlers $ handlers { hLogAction = action }

instance Monad m => HasTezosClient (TestT m) where
  signBytes alias op = do
    h <- getHandler hSignBytes
    h alias op
  genKey alias = do
    h <- getHandler hGenKey
    h alias
  genFreshKey alias = do
    h <- getHandler hGenFreshKey
    h alias
  revealKey alias = do
    h <- getHandler hRevealKey
    h alias
  waitForOperation op = do
    h <- getHandler hWaitForOperation
    h op
  rememberContract replaceExisting addr alias = do
    h <- getHandler hRememberContract
    h replaceExisting addr alias
  importKey replaceExisting alias key = do
    h <- getHandler hImportKey
    h replaceExisting alias key
  resolveAddressMaybe addr = do
    h <- getHandler hResolveAddressMaybe
    h addr
  getAlias originator = do
    h <- getHandler hGetAlias
    h originator
  getPublicKey alias = do
    h <- getHandler hGetPublicKey
    h alias
  getTezosClientConfig =
    join $ getHandler hGetTezosClientConfig
  calcTransferFee transferData = do
    h <- getHandler hCalcTransferFee
    h transferData
  calcOriginationFee origData = do
    h <- getHandler hCalcOriginationFee
    h origData

instance Monad m => HasTezosRpc (TestT m) where
  getHeadBlock =
    join $ getHandler hGetHeadBlock
  getCounter addr = do
    h <- getHandler hGetCounter
    h addr
  getBlockConstants block = do
    h <- getHandler hGetBlockConstants
    h block
  getProtocolParameters =
    join $ getHandler hGetProtocolParameters
  runOperation op = do
    h <- getHandler hRunOperation
    h op
  preApplyOperations ops = do
    h <- getHandler hPreApplyOperations
    h ops
  forgeOperation op = do
    h <- getHandler hForgeOperation
    h op
  injectOperation op = do
    h <- getHandler hInjectOperation
    h op
  getContractScript addr = do
    h <- getHandler hGetContractScript
    h addr
  getContractStorage addr = do
    h <- getHandler hGetContractScript
    osStorage <$> h addr
  getContractBigMap addr getBigMap = do
    h <- getHandler hGetContractBigMap
    h addr getBigMap
  getBigMapValue bigMapId scriptExpr = do
    h <- getHandler hGetBigMapValue
    h bigMapId scriptExpr
  getBalance addr = do
    h <- getHandler hGetBalance
    h addr
  runCode r = do
    h <- getHandler hRunCode
    h r
  getChainId = join (getHandler hGetChainId)
