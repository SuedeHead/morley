# Morley-client

This package contains client that can be used for interaction with Tezos blockchain.

`morley-client` uses [Tezos RPC API](https://tezos.gitlab.io/developer/rpc.html) for
interaction with remote (or local) tezos-node and `tezos-client` binary for some operations
such as signing and local contract manipulations.

For now `morley-client` is capable in contract origination and transfer sending.

Since all comands require tezos-node you should provide info about it
in the arguments, e.g.:
```sh
morley-client -E carthage.testnet.tezos.serokell.team:8732 COMMAND
```

Note that the `-E` flag can be omitted and in this case the defaults from `tezos-client`
configuration file will be used or the hardcoded ones if configuration was not found.

Note also that flags `-A`/`--node-addr`, `-P`/`--node-port` and `-S`/`--tls` are deprecated
in `tezos-client` (see [documentation](https://tezos.gitlab.io/008/cli-commands.html?highlight=endpoint#client-manual)), and are supported here only provisionally.

!!! Disclaimer !!!
Even though we sign operations using `tezos-client` we also have the ability to
sign them by using explicitly passed unencrypted secret keys, so it cannot be safely used
with the accounts which have real money, main purpose of this executable is testing.

Also, for now we accept in `--secret-key` option keys that only support Ed25519 curve.

## Contract origination

In order to originate Michelson contract you will need to provide sender (originator)
alias from which the contract will be originated, filepath to file with contract code (or provide this
code from stdin), initial balance and initial storage.

Here is sample command that will originate [`add1.tz`](https://gitlab.com/morley-framework/morley/-/blob/master/contracts/tezos_examples/attic/add1.tz)
contract:
``` sh
morley-client originate --from tz1LbpS4mB1ZWbux55qvQzaRZZ6XJTTSDC6x --initial-storage 0 --initial-balance 0 --contract ./contracts/tezos_examples/attic/add1.tz
```

NOTE: If you pass secret key explicitly, then we won't use tezos-client for operation signing.

## Transfer sending

In order to perform transfer to a smart contract you will need to provide sender
address and secret key (as well as in contract origination), receiver address,
transfer amount and argument.

Here is sample command that will perform transfer to [`add1.tz`](https://gitlab.com/morley-framework/morley/-/blob/master/contracts/tezos_examples/attic/add1.tz)
contract:
```sh
morley-client transfer \
--from tz1akcPmG1Kyz2jXpS4RvVJ8uWr7tsiT9i6A \
--to KT1USbmjj6P2oJ54UM6HxBZgpoPtdiRSVABW --amount 1 --parameter 0
```

## Sender address and secret key

Note that sender addresses and secret keys used in `morley-client` command should be
known for the network. In order to get them you can use [faucet](https://faucet.tzalpha.net/)
and `tezos-client activate account`, thus `tezos-client show address <alias> -S`
will show both acount address and secret key.
