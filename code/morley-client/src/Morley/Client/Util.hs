-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Client.Util
  ( epNameToTezosEp
  , extractAddressesFromValue
  , disableAlphanetWarning
  , runContract
  ) where

import Generics.SYB (everything, mkQ)
import System.Environment (setEnv)

import Michelson.Text
import qualified Michelson.Typed as T
  (Contract, HasNoBigMap, ParameterScope, StorageScope, Value)
import Michelson.Typed.Entrypoints (EpAddress(..), parseEpAddress)
import Michelson.Untyped (InternalByteString(..), Value, Value'(..))
import Michelson.Untyped.Entrypoints (pattern DefEpName, EpName(..))
import Morley.Client.RPC.Class
import Morley.Client.RPC.Types
import Morley.Micheline
import Tezos.Address
import Util.Exception as E (throwLeft)

-- | Sets the environment variable for disabling tezos-client
-- "not a mainnet" warning
disableAlphanetWarning :: IO ()
disableAlphanetWarning = setEnv "TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER" "YES"

-- | Convert 'EpName' to the textual representation used by RPC and tezos-client.
epNameToTezosEp :: EpName -> Text
epNameToTezosEp = \case
  DefEpName -> "default"
  epName -> unEpName epName

-- | Extract all addresses value from given untyped 'Value'.
--
-- Note that it returns all values that can be used as an address.
-- However, some of fetched values can never be used as an address.
extractAddressesFromValue :: Value -> [Address]
extractAddressesFromValue val =
  everything (<>) (mkQ [] fetchAddress) val
  where
    fetchAddress :: Value -> [Address]
    fetchAddress = \case
      ValueString s -> case parseEpAddress (unMText s) of
        Right addr -> [eaAddress addr]
        Left _ -> []
      ValueBytes (InternalByteString b) -> case parseAddressRaw b of
        Right addr -> [addr]
        Left _ -> []
      _ -> []


-- | Run contract with given parameter and storage and get new storage without
-- injecting anything to the chain.
--
-- Storage type is limited to not have any bigmaps because their updates are treated differently
-- in node RPC and its quite nontrivial to properly support storage update when storage type
-- containts bigmaps.
runContract
  :: forall cp st m. (HasTezosRpc m, T.ParameterScope cp, T.StorageScope st, T.HasNoBigMap st)
  => T.Contract cp st -> T.Value cp -> T.Value st -> m (T.Value st)
runContract contract parameter storage = do
  head' <- getHeadBlock
  headConstants <- getBlockConstants head'
  res <- runCode $ RunCode
    { rcScript = toExpression contract
    , rcStorage = toExpression storage
    , rcInput = toExpression parameter
    , rcAmount = 0
    , rcChainId = bcChainId headConstants
    }
  throwLeft $ pure $ fromExpression (rcrStorage res)
