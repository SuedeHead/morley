-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Parsers that are used in 'Morley.Client.TezosClient.Impl'
module Morley.Client.TezosClient.Parser
  ( parseBakerFeeFromOutput
  ) where

import Data.Scientific (Scientific)
import qualified Text.Megaparsec as P (Parsec, parse, skipManyTill)
import Text.Megaparsec.Char (newline, printChar, space)
import Text.Megaparsec.Char.Lexer (lexeme, scientific, symbol)
import Text.Megaparsec.Error (ParseErrorBundle, errorBundlePretty)
import qualified Text.Show (show)

import Morley.Micheline
import Tezos.Core

type Parser = P.Parsec Void Text

data FeeParserException = FeeParserException (ParseErrorBundle Text Void)
  deriving stock Eq

instance Show FeeParserException where
  show (FeeParserException bundle) = errorBundlePretty bundle

instance Exception FeeParserException where
  displayException = show

-- | Function to parse baker fee from given @tezos-client@ output.
parseBakerFeeFromOutput
  :: Text -> Either FeeParserException TezosMutez
parseBakerFeeFromOutput output = first FeeParserException $
  P.parse bakerFeeParser "" output
  where
    bakerFeeParser :: Parser TezosMutez
    bakerFeeParser = do
      num <- P.skipManyTill (printChar <|> newline) $ do
        void $ symbol space "Fee to the baker: "
        P.skipManyTill printChar $ lexeme (newline >> pass) scientific
      maybe (fail "Mutez overflow") pure $
        scientificToMutez num
    scientificToMutez :: Scientific -> Maybe TezosMutez
    scientificToMutez x = fmap TezosMutez . mkMutez $ floor $ x * 1e6
