-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE InstanceSigs #-}

module Morley.Client.App
  ( MorleyClientEnv
  , MorleyClientM
  , runMorleyClientM
  ) where

import Colog (HasLog(..), Message)
import Control.Concurrent (threadDelay)
import qualified Data.Aeson as Aeson
import qualified Data.Binary.Builder as Binary
import Data.List ((!!))
import Data.Text (isInfixOf)
import Fmt (Buildable(..), Builder, build, pretty, (+|), (|+))
import Network.HTTP.Types (Status(..), renderQuery)
import Servant.Client (runClientM)
import Servant.Client.Core
  (ClientError(..), Request, RequestBody(..), RequestF(..), Response, ResponseF(..), RunClient(..))
import System.Random (randomRIO)
import UnliftIO (MonadUnliftIO)
import UnliftIO.Timeout (timeout)

import Morley.Client.Env (MorleyClientEnv'(..))
import Morley.Client.Logging (logDebug)
import qualified Morley.Client.RPC.API as API
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types (InternalError(..), ProtocolParameters(..), RunError)
import Morley.Client.TezosClient.Class
import Morley.Client.TezosClient.Impl (TezosClientError(..))
import qualified Morley.Client.TezosClient.Impl as TezosClient
import Morley.Client.TezosClient.Types
import Morley.Micheline.Json (TezosMutez(..))
import Tezos.Core (parseChainId)
import Tezos.Crypto (Signature(..))
import qualified Tezos.Crypto.Ed25519 as Ed25519
import Util.Exception (throwLeft)

----------------------------------------------------------------------------
-- App
----------------------------------------------------------------------------

type MorleyClientEnv = MorleyClientEnv' MorleyClientM

newtype MorleyClientM a = MorleyClientM
  { unMorleyClientM :: ReaderT MorleyClientEnv IO a }
  deriving newtype
    ( Functor, Applicative, Monad, MonadReader MorleyClientEnv
    , MonadIO, MonadThrow, MonadCatch, MonadMask, MonadUnliftIO
    )

-- | Run 'MorleyClientM' action within given 'MorleyClientEnv'. Retry action
-- in case of invalid counter error.
runMorleyClientM :: MorleyClientEnv -> MorleyClientM a -> IO a
runMorleyClientM env client =
  runReaderT (unMorleyClientM (retryInvalidCounter client)) env

instance HasLog MorleyClientEnv Message MorleyClientM where
  getLogAction = mceLogAction
  setLogAction action mce = mce { mceLogAction = action }

instance HasTezosClient MorleyClientM where
  signBytes senderAlias opHash = retryOnceOnTimeout $ do
    env <- ask
    case mceSecretKey env of
      Just sk -> pure . SignatureEd25519 $ Ed25519.sign sk opHash
      Nothing -> TezosClient.signBytes senderAlias opHash
  waitForOperation = retryOnceOnTimeout ... TezosClient.waitForOperationInclusion
  rememberContract = failOnTimeout ... TezosClient.rememberContract
  importKey = failOnTimeout ... TezosClient.importKey
  resolveAddressMaybe = retryOnceOnTimeout ... TezosClient.resolveAddressMaybe
  getAlias = retryOnceOnTimeout ... TezosClient.getAlias
  getPublicKey = retryOnceOnTimeout ... TezosClient.getPublicKey
  -- This function doesn't perform any chain related operations with tezos-client,
  -- so `ECONNRESET` cannot appear here
  getTezosClientConfig = do
    path <- tceTezosClientPath <$> view tezosClientEnvL
    mbDataDir <- tceMbTezosClientDataDir <$> view tezosClientEnvL
    liftIO $ TezosClient.getTezosClientConfig path mbDataDir
  genFreshKey = retryOnceOnTimeout ... TezosClient.genFreshKey
  genKey = failOnTimeout ... TezosClient.genKey
  -- Key revealing cannot be safely retried, so we're not trying to recover it
  -- from `ECONNRESET`.
  revealKey = failOnTimeout ... TezosClient.revealKey
  calcTransferFee = retryOnceOnTimeout ... TezosClient.calcTransferFee
  calcOriginationFee = retryOnceOnTimeout ... TezosClient.calcOriginationFee

instance RunClient MorleyClientM where
  runRequest :: Request -> MorleyClientM Response
  runRequest req = do
    logRequest req
    env <- mceClientEnv <$> ask
    response <- either throwClientError pure =<<
      liftIO (runClientM (runRequest req) env)
    response <$ logResponse response
  throwClientError err = case err of
    FailureResponse _ resp
      | 500 <- statusCode (responseStatusCode resp) ->
        handleInternalError (responseBody resp)
    _ -> throwM err
    where
      -- In some cases RPC returns important useful errors as internal ones.
      -- We try to parse the response to a list of 'InternalError'.
      -- If we receive one 'InternalError', we throw it wrapped into
      -- 'ClientInternalError', that's what we observed in most obvious cases.
      -- If we receive more than one, we wrap them into 'UnexpectedInternalErrors'.
      handleInternalError :: LByteString -> MorleyClientM a
      handleInternalError body = case Aeson.decode @[InternalError] body of
        Nothing -> case Aeson.decode @[RunError] body of
          Nothing -> throwM err
          Just runErrs -> throwM $ RunCodeErrors runErrs
        Just [knownErr] -> throwM $ ClientInternalError knownErr
        Just errs -> throwM $ UnexpectedInternalErrors errs

instance HasTezosRpc MorleyClientM where
  getHeadBlock = retryOnceOnTimeout $ API.getLastBlock API.nodeMethods
  getCounter= retryOnceOnTimeout ... API.getCounter API.nodeMethods
  getBlockConstants = retryOnceOnTimeout ... API.getBlockConstants API.nodeMethods
  getProtocolParameters = retryOnceOnTimeout $ API.getProtocolParameters API.nodeMethods
  runOperation = retryOnceOnTimeout ... API.runOperation API.nodeMethods
  preApplyOperations = retryOnceOnTimeout ... API.preApplyOperations API.nodeMethods
  forgeOperation = retryOnceOnTimeout ... API.forgeOperation API.nodeMethods
  injectOperation = failOnTimeout ... API.injectOperation API.nodeMethods (Just "main")
  getContractScript = retryOnceOnTimeout ... API.getScript API.nodeMethods
  getContractStorage = retryOnceOnTimeout ... API.getStorage API.nodeMethods
  getContractBigMap = retryOnceOnTimeout ... API.getBigMap API.nodeMethods
  getBigMapValue = retryOnceOnTimeout ... API.getBigMapValue API.nodeMethods
  getBalance = retryOnceOnTimeout ... fmap unTezosMutez . API.getBalance API.nodeMethods
  runCode = API.runCode API.nodeMethods
  getChainId = throwLeft $ parseChainId <$> API.getChainId API.nodeMethods

----------------------------------------------------------------------------
-- Servant client helpers
----------------------------------------------------------------------------

-- | Convert a bytestring to a string assuming this bytestring stores
-- something readable in UTF-8 encoding.
fromBS :: ConvertUtf8 Text bs => bs -> Text
fromBS = decodeUtf8

ppRequestBody :: RequestBody -> Builder
ppRequestBody = build .
  \case
    RequestBodyLBS lbs -> fromBS lbs
    RequestBodyBS bs -> fromBS bs
    RequestBodySource {} -> "<body is not in memory>"

-- | Pretty print a @servant@'s request.
-- Note that we print only some part of 'Request': method, request
-- path and query, request body. We don't print other things that are
-- subjectively less interesting such as HTTP version or media type.
-- But feel free to add them if you want.
ppRequest :: Request -> Builder
ppRequest Request {..} =
  fromBS requestMethod |+ " " +| fromBS (Binary.toLazyByteString requestPath)
  |+ fromBS (renderQuery True $ toList requestQueryString) |+
  maybe mempty (mappend "\n" . ppRequestBody . fst) requestBody

logRequest :: HasCallStack => Request -> MorleyClientM ()
logRequest req = logDebug $ "RPC request: " +| ppRequest req |+ ""

-- | Pretty print a @servant@'s response.
-- Note that we print only status and body,
-- the rest looks not so interesting in our case.
--
-- If response is not human-readable text in UTF-8 encoding it will
-- print some garbage.  Apparently we don't make such requests for now.
ppResponse :: Response -> Builder
ppResponse Response {..} =
  statusCode responseStatusCode |+ " " +|
  fromBS (statusMessage responseStatusCode) |+ "\n" +|
  fromBS responseBody  |+ ""

logResponse :: HasCallStack => Response -> MorleyClientM ()
logResponse resp = logDebug $ "RPC response: " +| ppResponse resp |+ ""

-- | Helper function that retries 'MorleyClientM' action in case of counter error.
retryInvalidCounter :: MorleyClientM a -> MorleyClientM a
retryInvalidCounter action = do
  action `catch` \(rpcErr :: ClientRpcError) -> handleRpcError action rpcErr
         `catch` \(tezosClientErr :: TezosClientError) -> handleTezosClientError action tezosClientErr
  where
    handleRpcError :: MorleyClientM a -> ClientRpcError -> MorleyClientM a
    handleRpcError a = \case
      ClientInternalError (CounterInThePast {}) -> retryAction a
      ClientInternalError (Failure msg)
        | "Counter" `isInfixOf` msg && "already used for contract" `isInfixOf` msg ->
          retryAction a
      anotherErr -> throwM anotherErr
    handleTezosClientError :: MorleyClientM a ->TezosClientError -> MorleyClientM a
    handleTezosClientError a = \case
      CounterIsAlreadyUsed _ _ -> retryAction a
      anotherErr -> throwM anotherErr
    retryAction :: MorleyClientM a -> MorleyClientM a
    retryAction a = do
      i <- liftIO $ (blockAwaitAmounts !!) <$> randomRIO (0, length blockAwaitAmounts - 1)
      logDebug $ "Invalid counter error occured, retrying the request after " <> show i <> " blocks"
      ProtocolParameters {..} <- getProtocolParameters
      let
        blockInterval = ppTimeBetweenBlocks !! 0
      -- Invalid counter error may occur in case we try to perform multiple operations
      -- from the same address. We should try to wait different amount of times before retry
      -- in case there are multiple actions failed with invalid counter error.
      liftIO $ threadDelay $ i * (fromIntegral blockInterval) * 1e6
      retryInvalidCounter a
    blockAwaitAmounts :: [Int]
    blockAwaitAmounts = [1..5]

data TimeoutError = TimeoutError
  deriving stock Show

instance Buildable TimeoutError where
  build TimeoutError =
    "Timeout for action call was reached. Probably, something is wrong with \
    \testing environment."

instance Exception TimeoutError where
  displayException = pretty

-- | Helper function that retries 'MorleyClientM' action in case action hasn't succeed
-- in 'timeout'. In case retry didn't help, error that indicates
-- timeout is thrown.
retryOnTimeout :: Bool -> MorleyClientM a -> MorleyClientM a
retryOnTimeout wasRetried action = do
  res <- timeout timeoutInterval action
  maybe (if wasRetried then throwM TimeoutError else retryOnTimeout True action)
    pure res

-- | Helper function that consider action failed in case of timeout,
-- because it's unsafe to perform some of the actions twice. E.g. performing two 'injectOperation'
-- action can lead to a situation when operation is injected twice.
failOnTimeout :: MorleyClientM a -> MorleyClientM a
failOnTimeout = retryOnTimeout True

-- | Helper function that retries action once in case of timeout. If retry ended up with timeout
-- as well, action is considered failed. It's safe to retry read-only actions that don't update chain state
-- or @tezos-client@ config/environment.
retryOnceOnTimeout :: MorleyClientM a -> MorleyClientM a
retryOnceOnTimeout = retryOnTimeout False

-- | Timeout for 'retryTimeout' and 'failTimeout' helpers in microseconds.
timeoutInterval :: Int
timeoutInterval = 120 * 1e6
