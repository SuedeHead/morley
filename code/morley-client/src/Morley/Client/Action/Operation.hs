-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Implementation of generic operations submission.
module Morley.Client.Action.Operation
  ( runOperations
  ) where

import Data.List (zipWith4)
import qualified Data.Text as T
import Fmt (Buildable(..), blockListF', listF, (+|), (|+))

import Morley.Client.Action.Common
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types
import Morley.Client.TezosClient
import Morley.Micheline (TezosInt64, TezosMutez(..))
import Tezos.Address
import Tezos.Crypto
import Util.ByteString

logOperations
  :: WithClientLog env m
  => AddressOrAlias
  -> [Either TransactionData OriginationData]
  -> m ()
logOperations sender ops = do
  let opName =
        if | all isLeft ops -> "transactions"
           | all isRight ops -> "originations"
           | otherwise -> "operations"

      buildOp = \case
        Left tx -> build tx
        Right orig -> build $ odName orig

  logInfo $ T.strip $ -- strip trailing newline
    "Running " +| opName +| " by " +| sender |+ ":\n" +|
    blockListF' "-" buildOp ops

-- | Perform sequence of operations.
--
-- Returns operation hash and result of each operation
-- (nothing for transactions and an address for originated contracts).
runOperations
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => AddressOrAlias
  -> [Either TransactionData OriginationData]
  -> m (Text, [Either () Address])
runOperations sender operations = do
  logOperations sender operations
  senderAddress <- resolveAddress sender

  pp@ProtocolParameters{..} <- getProtocolParameters
  OperationConstants{..} <- preProcessOperation senderAddress

  let convertOps i = \case
        Left (TransactionData TD {..}) ->
          Left TransactionOperation
          { toDestination = tdReceiver
          , toCommonData = commonData
          , toAmount = TezosMutez tdAmount
          , toParameters = toParametersInternals tdEpName tdParam
          }
        Right OriginationData{..} ->
          Right OriginationOperation
          { ooCommonData = commonData
          , ooBalance = TezosMutez odBalance
          , ooScript = mkOriginationScript odContract odStorage
          }
        where
          commonData = mkCommonOperationData senderAddress (ocCounter + i) pp

  let opsToRun = zipWith convertOps [(1 :: TezosInt64)..] operations
      mbFees = map (either (\(TransactionData TD {..}) -> tdMbFee) odMbFee) operations

  -- Perform run_operation with dumb signature in order
  -- to estimate gas cost, storage size and paid storage diff
  let runOp = RunOperation
        { roOperation = RunOperationInternal
          { roiBranch = ocLastBlockHash
          , roiContents = opsToRun
          , roiSignature = stubSignature
          }
        , roChainId = bcChainId ocBlockConstants
        }
  results <- getAppliedResults (Left runOp)

  let -- Learn how to forge given operations
      forgeOp :: [Either TransactionOperation OriginationOperation] -> m ByteString
      forgeOp ops =
        fmap unHexJSONByteString . forgeOperation $ ForgeOperation
          { foBranch = ocLastBlockHash
          , foContents = ops
          }

  let -- Attach a signature to forged operation + return the signature itself
      signForgedOp :: ByteString -> m (Signature, ByteString)
      signForgedOp op = do
        signature' <- signBytes sender (addOperationPrefix op)
        return (signature', prepareOpForInjection op signature')

  -- Fill in fees
  let
    updateOp opToRun mbFee AppliedResult{..} isFirst = do
      let storageLimit = sum
            [ arPaidStorageDiff
            , arAllocatedDestinationContracts * fromIntegral ppOriginationSize
            , fromIntegral (length arOriginatedContracts)
                * fromIntegral ppOriginationSize
            , 20  -- similarly to tezos-client, we add 20 for safety
            ]
      let gasLimit = arConsumedGas + 100  -- adding extra for safety
          updateCommonDataForFee fee =
            updateCommonData gasLimit storageLimit (TezosMutez fee)

      (_fee, op, mReadySignedOp) <- convergingFee
        @(Either TransactionOperation OriginationOperation)
        @(Maybe (Signature, ByteString))  -- ready operation and its signature
        (\fee ->
          return $ bimap
            (toCommonDataL %~ updateCommonDataForFee fee)
            (ooCommonDataL %~ updateCommonDataForFee fee)
            opToRun
          )
        (\op -> do
          forgedOp <- forgeOp [op]
          -- In the Tezos implementation the first transaction
          -- in the series pays for signature.
          -- Signature of hash should be constant in size,
          -- so we can pass any signature, not necessarily the final one
          (fullForgedOp, mExtra) <-
            if isFirst
              then do
                res@(_signature, signedOp) <- signForgedOp forgedOp
                return (signedOp, Just res)
              else
                pure (forgedOp, Nothing)
          return
            ( maybe (computeFee ocFeeConstants (length fullForgedOp) gasLimit) id mbFee
            , mExtra
            )
          )

      return (op, mReadySignedOp)

  -- These two lists must have the same length here.
  -- @opsToRun@ is constructed directly from @params@.
  -- The length of @results@ is checked in @getAppliedResults@.
  (updOps, readySignedOps) <- fmap unzip . sequenceA $
    zipWith4 updateOp opsToRun mbFees results (True : repeat False)

  -- Forge operation with given limits and get its hexadecimal representation
  (signature', signedOp) <- case readySignedOps of
    -- Save one forge + sign call pair in case of one operation
    [Just readyOp] -> pure readyOp
    -- In case of batch we have to reforge the full operation
    _ -> forgeOp updOps >>= signForgedOp

  -- Operation still can fail due to insufficient gas or storage limit, so it's required
  -- to preapply it before injecting
  let preApplyOp = PreApplyOperation
        { paoProtocol = bcProtocol ocBlockConstants
        , paoBranch = ocLastBlockHash
        , paoContents = updOps
        , paoSignature = signature'
        }
  ars2 <- getAppliedResults (Right preApplyOp)

  operationHash <- injectOperation (HexJSONByteString signedOp)
  waitForOperation operationHash

  let contractAddrs = arOriginatedContracts <$> ars2
  opsRes <- forM (zip operations contractAddrs) $ \case
    (Left _, []) ->
      return $ Left ()
    (Left _, addrs) -> do
      logInfo . T.strip $
        "The following contracts were originated during transactions: " +|
        listF addrs |+ ""
      return $ Left ()
    (Right _, []) ->
      throwM RpcOriginatedNoContracts
    (Right OriginationData{..}, [addr]) -> do
      logDebug $ "Saving " +| addr |+ " for " +| odName |+ "\n"
      rememberContract odReplaceExisting addr odName
      return $ Right addr
    (Right _, addrs@(_ : _ : _)) ->
      throwM $ RpcOriginatedMoreContracts addrs

  return (operationHash, opsRes)
