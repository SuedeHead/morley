-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module with functions that used in both transaction sending and contract
-- origination.
module Morley.Client.Action.Common
  ( OperationConstants(..)
  , TD (..)
  , TransactionData(..)
  , OriginationData(..)
  , addOperationPrefix
  , getAppliedResults
  , computeFee
  , convergingFee
  , preProcessOperation
  , stubSignature
  , prepareOpForInjection
  , updateCommonData
  , toParametersInternals
  , mkOriginationScript
  ) where

import Control.Lens (Prism')
import Data.ByteString (cons)
import Data.Default (def)
import qualified Data.Kind as Kind
import Fmt (Buildable(..), pretty, (+|), (|+))

import qualified Michelson.Typed as T
import Michelson.Typed.Scope
import Michelson.Untyped.Entrypoints
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types
import Morley.Client.TezosClient
import Morley.Client.Util
import Morley.Micheline (TezosInt64, TezosMutez(..), toExpression)
import Morley.Micheline.Expression (Expression(ExpressionString))
import Tezos.Address
import Tezos.Core
import Tezos.Crypto

-- | Datatype that contains various values required for
-- chain operations.
data OperationConstants = OperationConstants
  { ocLastBlockHash :: Text
  -- ^ Block in which operations is going to be injected
  , ocBlockConstants :: BlockConstants
  -- ^ Information about block: chain_id and protocol
  , ocFeeConstants :: FeeConstants
  -- ^ Information about fees
  , ocCounter :: TezosInt64
  -- ^ Sender counter
  }

-- | Helper for 'TransactionData' and 'LTransactionData'.
data TD (t :: Kind.Type) = TD
  { tdReceiver :: Address
  , tdAmount :: Mutez
  , tdEpName :: EpName
  , tdParam :: t
  , tdMbFee :: Maybe Mutez
  }

-- | Data for a single transaction in a batch.
data TransactionData where
  TransactionData ::
    forall (t :: T.T). ParameterScope t =>
    TD (T.Value t) -> TransactionData

instance Buildable TransactionData where
  build (TransactionData TD {..}) =
    "To: " +| tdReceiver |+ ". EP: " +| tdEpName |+ ". Parameter: " +| tdParam |+
    ". Amount: " +| tdAmount |+ ""

-- | Data for a single origination in a batch
data OriginationData = forall cp st. StorageScope st => OriginationData
  { odReplaceExisting :: Bool
  , odName :: AliasHint
  , odBalance :: Mutez
  , odContract :: T.Contract cp st
  , odStorage :: T.Value st
  , odMbFee :: Maybe Mutez
  }

toParametersInternals
  :: ParameterScope t
  => EpName
  -> T.Value t
  -> ParametersInternal
toParametersInternals epName epParam = ParametersInternal
  { piEntrypoint = epNameToTezosEp epName
  , piValue = toExpression epParam
  }

mkOriginationScript
  :: T.Contract cp st -> T.Value st -> OriginationScript
mkOriginationScript contract@T.Contract{} initialStorage = OriginationScript
  { osCode = toExpression contract
  , osStorage = toExpression initialStorage
  }

-- | Preprocess chain operation in order to get required constants.
preProcessOperation
  :: (HasTezosRpc m) => Address -> m OperationConstants
preProcessOperation sourceAddr = do
  ocLastBlockHash <- getHeadBlock
  ocBlockConstants <- getBlockConstants ocLastBlockHash
  let ocFeeConstants = def
  ocCounter <- getCounter sourceAddr
  pure OperationConstants{..}

-- | Perform runOperation or preApplyOperations and combine the results.
--
-- If an error occurs, this function tries to turn errors returned by RPC
-- into 'ClientRpcError'. If it can't do the conversion, 'UnexpectedErrors'
-- will be thrown.
getAppliedResults
  :: (HasTezosRpc m)
  => Either RunOperation PreApplyOperation -> m [AppliedResult]
getAppliedResults op = do
  (runResult, expectedContentsSize) <- case op of
    Left runOp ->
      (, length $ roiContents $ roOperation runOp) <$> runOperation runOp
    Right preApplyOp -> do
      results <- preApplyOperations [preApplyOp]
      -- There must be exactly one result because we pass a list
      -- consisting of 1 item.
      case results of
        [result] -> pure (result, length $ paoContents preApplyOp)
        _ -> throwM $ RpcUnexpectedSize 1 (length results)

  handleOperationResult runResult expectedContentsSize
  where
    handleOperationResult ::
      MonadThrow m => RunOperationResult -> Int -> m [AppliedResult]
    handleOperationResult RunOperationResult{..} expectedContentsSize = do
      when (length rrOperationContents /= expectedContentsSize) $
        throwM $ RpcUnexpectedSize expectedContentsSize (length rrOperationContents)

      mapM (\(OperationContent (RunMetadata res internalOps)) ->
              let internalResults = map unInternalOperation internalOps in
                case foldr combineResults res internalResults of
                  OperationApplied appliedRes -> pure appliedRes
                  OperationFailed errors -> handleErrors errors
           ) rrOperationContents

    -- When an error happens, we will get a list of 'RunError' in response.
    -- This list often contains more than one item.
    -- We tested which errors are returned in certain scenarios and added
    -- handling of such scenarios here.
    -- We don't rely on any specific order of errors and on the number of errors.
    -- For example, in case of bad parameter this number can be different.
    handleErrors :: MonadThrow m => [RunError] -> m a
    handleErrors errs
      | Just address <- findError _RuntimeError
      , Just expr <- findError _ScriptRejected
      = throwM $ ContractFailed address expr
      -- This case should be removed once 006 is finally deprecated
      | Just address <- findError _BadContractParameter
      , Just (_, expr) <- findError _InvalidSyntacticConstantError
      = throwM $ BadParameter address expr
      | Just address <- findError _BadContractParameter
      , Just (_, expr) <- findError _InvalidConstant
      = throwM $ BadParameter address expr
      | Just address <- findError _BadContractParameter
      , Just notation <- findError _InvalidContractNotation
      = throwM $ BadParameter address (ExpressionString notation)
      | Just address <- findError _REEmptyTransaction
      = throwM $ EmptyTransaction address
      | Just address <- findError _RuntimeError
      , Just _ <- findError _ScriptOverflow
      = throwM $ ShiftOverflow address
      | otherwise = throwM $ UnexpectedRunErrors errs
      where
        findError :: Prism' RunError a -> Maybe a
        findError prism = fmap head . nonEmpty . mapMaybe (preview prism) $ errs

-- | Compute fee for operation.
computeFee :: FeeConstants -> Int -> TezosInt64 -> Mutez
computeFee FeeConstants{..} opSize gasLimit =
  -- Here and further we mostly follow the Tezos implementation:
  -- https://gitlab.com/tezos/tezos/-/blob/14d6dafd23eeafe30d931a41d43c99b1ebed5373/src/proto_alpha/lib_client/injection.ml#L584

  unsafeMkMutez . ceiling . sum $
    [ toRational $ unMutez fcBase
    , toRational fcMutezPerOpByte * toRational opSize
    , toRational fcMutezPerGas * toRational gasLimit
    ]

-- | @convergingFee mkOperation countFee@ tries to find the most minimal fee
-- @F@ and the respective operation @Op@ so that @mkOperation F = Op@ and
-- @countFee Op <= F@.
convergingFee
  :: forall op extra m. Monad m
  => (Mutez -> m op)
  -> (op -> m (Mutez, extra))
  -> m (Mutez, op, extra)
convergingFee mkOperation countFee = iterateFee 5 assessedMinimalFee
  where
    assessedMinimalFee = toMutez 0
    -- ↑ In real life we can encounter small fees like ~300 mutez
    -- (for small transfers to implicit addresses), but even if we set this
    -- as a starting fee, we won't win any number of iteration steps.
    -- So setting just zero.

    {- We have to use iterative algorithm because fees are included into
       operation, and higher fees increase operation size and thus fee may
       grow again. Fortunatelly, fees strictly grow with operation size and
       operation size strictly grows with fees, so the implementation is simple.
    -}
    iterateFee :: Word -> Mutez -> m (Mutez, op, extra)
    iterateFee 0 _ = error "Failed to converge at some fee"
    iterateFee countdown curFee = do
      op <- mkOperation curFee
      (requiredFee, extra) <- countFee op
      if requiredFee <= curFee
        then pure (curFee, op, extra)
        else iterateFee (countdown - 1) requiredFee

-- | Update common operation data based on preliminary run which estimates storage and
-- gas limits and fee.
--
-- Reference implementation adds 100 gas and 20 bytes to the limits for safety.
updateCommonData
  :: TezosInt64 -> TezosInt64 -> TezosMutez
  -> CommonOperationData -> CommonOperationData
updateCommonData gasLimit storageLimit fee commonData =
  commonData
  { codGasLimit = gasLimit
  , codStorageLimit = storageLimit
  , codFee = fee
  }

stubSignature :: Signature
stubSignature = unsafeParseSignature
  "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"
  where
    unsafeParseSignature :: HasCallStack => Text -> Signature
    unsafeParseSignature = either (error . pretty) id . parseSignature

addOperationPrefix :: ByteString -> ByteString
addOperationPrefix = cons 0x03

prepareOpForInjection :: ByteString -> Signature -> ByteString
prepareOpForInjection operationHex signature' =
  operationHex <> signatureToBytes signature'
