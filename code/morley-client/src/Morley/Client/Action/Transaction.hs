-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
-- | Functions to submit transactions via @tezos-client@ and node RPC.

module Morley.Client.Action.Transaction
  ( runTransactions
  , lRunTransactions

  -- * Transfer
  , transfer
  , lTransfer

  -- Datatypes for batch transactions
  , TD (..)
  , LTransactionData (..)
  , TransactionData (..)
  ) where

import qualified Data.Kind as Kind

import Lorentz.Constraints
import qualified Michelson.Typed as T
import Michelson.Typed.Scope
import Michelson.Untyped.Entrypoints
import Morley.Client.Action.Common
import Morley.Client.Action.Operation
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.TezosClient.Class
import Morley.Client.TezosClient.Types
import Tezos.Address
import Tezos.Core (Mutez)

-- | Perform sequence of transactions to the contract, each transactions should be
-- defined via @EntrypointParam t@. Returns operation hash and block in which
-- this operation appears.
runTransactions
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => Address -> [TransactionData]
  -> m Text
runTransactions sender transactions = do
  (opHash, _) <- runOperations (AddressResolved sender) (Left <$> transactions)
  return opHash

-- | Lorentz version of 'TransactionData'.
data LTransactionData where
  LTransactionData ::
    forall (t :: Kind.Type). NiceParameter t =>
    TD t -> LTransactionData

-- | Lorentz version of 'runTransactions'
lRunTransactions
  :: forall m env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    )
  => Address
  -> [LTransactionData]
  -> m Text
lRunTransactions sender transactions =
  runTransactions sender $ map convertLTransactionData transactions
  where
    convertLTransactionData :: LTransactionData -> TransactionData
    convertLTransactionData (LTransactionData (TD {..} :: TD t))=
      withDict (niceParameterEvi @t) $
      TransactionData TD
      { tdReceiver = tdReceiver
      , tdEpName = tdEpName
      , tdAmount = tdAmount
      , tdParam = T.toVal tdParam
      , tdMbFee = tdMbFee
      }

transfer
  :: forall m t env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    , ParameterScope t
    )
  => Address
  -> Address
  -> Mutez
  -> EpName
  -> T.Value t
  -> Maybe Mutez
  -> m Text
transfer from to amount epName param mbFee =
  runTransactions from $
  [TransactionData TD
    { tdReceiver = to
    , tdAmount = amount
    , tdEpName = epName
    , tdParam = param
    , tdMbFee = mbFee
    }
  ]

lTransfer
  :: forall m t env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    , NiceParameter t
    )
  => Address
  -> Address
  -> Mutez
  -> EpName
  -> t
  -> Maybe Mutez
  -> m Text
lTransfer from to amount epName param mbFee =
  withDict (niceParameterEvi @t) $
    transfer from to amount epName (T.toVal param) mbFee
