-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Functions to originate smart contracts via @tezos-client@ and node RPC.
module Morley.Client.Action.Origination
  ( originateContract
  , originateContracts
  , originateUntypedContract
  -- Lorentz version
  , lOriginateContract
  , lOriginateContracts

  -- Datatypes for batch originations
  , LOriginationData (..)
  , OriginationData (..)
  ) where

import Data.Default (def)

import qualified Lorentz as L
import Lorentz.Constraints
import Michelson.TypeCheck (SomeContract(..), typeCheckContract, typeVerifyStorage)
import Michelson.Typed (Contract(..), IsoValue(..), Value)
import Michelson.Typed.Scope
import qualified Michelson.Untyped as U
import Morley.Client.Action.Common
import Morley.Client.Action.Operation
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.TezosClient
import Tezos.Address
import Tezos.Core
import Util.Exception

-- | Originate given contracts with given initial storages. Returns
-- operation hash and originated contracts' addresses.
originateContracts
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => AddressOrAlias
  -> [OriginationData]
  -> m (Text, [Address])
originateContracts sender originations = do
  (opHash, res) <- runOperations sender (Right <$> originations)
  return (opHash, fromOrigination <$> res)
  where
    fromOrigination = either (error "Unexpected transaction") id

-- | Originate single contract
originateContract
  :: forall m cp st env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     , StorageScope st
     )
  => Bool
  -> AliasHint
  -> AddressOrAlias
  -> Mutez
  -> Contract cp st
  -> Value st
  -> Maybe Mutez
  -> m (Text, Address)
originateContract replaceExisting name sender' balance contract initialStorage mbFee = do
  (hash, contracts) <- originateContracts sender' $
    [OriginationData replaceExisting name balance contract initialStorage mbFee]
  case contracts of
    [addr] -> pure (hash, addr)
    _ -> throwM $ RpcOriginatedMoreContracts contracts

-- | Originate a single untyped contract
originateUntypedContract
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => Bool
  -> AliasHint
  -> AddressOrAlias
  -> Mutez
  -> U.Contract
  -> U.Value
  -> Maybe Mutez
  -> m (Text, Address)
originateUntypedContract replaceExisting name sender' balance contract initialStorage mbFee = do
  SomeContract (fullContract@Contract{} :: Contract cp st) <-
    throwLeft . pure $ typeCheckContract contract def
  storage <- throwLeft . pure $ typeVerifyStorage @st initialStorage
  originateContract replaceExisting name sender' balance fullContract storage mbFee

-- | Lorentz version of 'OriginationData'
data LOriginationData = forall cp st. (NiceParameterFull cp, NiceStorage st)
  => LOriginationData
  { lodReplaceExisting :: Bool
  , lodName :: AliasHint
  , lodBalance :: Mutez
  , lodContract :: L.Contract cp st
  , lodStorage :: st
  , lodMbFee :: Maybe Mutez
  }

-- | Lorentz version of 'originateContracts'
lOriginateContracts
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => AddressOrAlias
  -> [LOriginationData]
  -> m (Text, [Address])
lOriginateContracts sender' originations =
  originateContracts sender' $ map convertLOriginationData originations
  where
    convertLOriginationData :: LOriginationData -> OriginationData
    convertLOriginationData LOriginationData {..} = case lodStorage of
      (_ :: st) -> withDict (niceStorageEvi @st) $ OriginationData
        { odReplaceExisting = lodReplaceExisting
        , odName = lodName
        , odBalance = lodBalance
        , odContract = L.compileLorentzContract lodContract
        , odStorage = toVal lodStorage
        , odMbFee = lodMbFee
        }

-- | Originate single Lorentz contract
lOriginateContract
  :: forall m cp st env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     , NiceStorage st
     , NiceParameterFull cp
     )
  => Bool
  -> AliasHint
  -> AddressOrAlias
  -> Mutez
  -> L.Contract cp st
  -> st
  -> Maybe Mutez
  -> m (Text, Address)
lOriginateContract replaceExisting name sender' balance lContract lStorage mbFee = do
  (hash, contracts) <- lOriginateContracts sender' $
    [LOriginationData replaceExisting name balance lContract lStorage mbFee]
  case contracts of
    [addr] -> return (hash, addr)
    _ ->  throwM $ RpcOriginatedMoreContracts contracts
