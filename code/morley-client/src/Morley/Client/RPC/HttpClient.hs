-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Client.RPC.HttpClient
  ( newClientEnv
  ) where

import Network.HTTP.Client (ManagerSettings(..), Request(..), defaultManagerSettings, newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant.Client (BaseUrl(..), ClientEnv, Scheme(..), mkClientEnv)

-- | Make servant client environment from morley client config.
--
-- Note: Creating a new servant manager is a relatively expensive
-- operation, so this function is not supposed to be called often.
newClientEnv :: BaseUrl -> IO ClientEnv
newClientEnv endpointUrl = do
  manager' <- newManager $ case baseUrlScheme endpointUrl of
    Http  -> defaultManagerSettings{ managerModifyRequest = fixRequest }
    Https -> tlsManagerSettings{ managerModifyRequest = fixRequest }
  return $ mkClientEnv manager' endpointUrl

-- | Add header, required by the Tezos RPC interface
fixRequest :: Request -> IO Request
fixRequest req = return $
  req { requestHeaders = ("Content-Type", "application/json") :
        filter (("Content-Type" /=) . fst) (requestHeaders req)
      }
