-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | An abstraction layer over RPC implementation.
-- The primary reason it exists is to make it possible to mock
-- RPC in tests.

module Morley.Client.RPC.Class
  ( HasTezosRpc (..)
  ) where

import Tezos.Address
import Tezos.Core (ChainId, Mutez)
import Util.ByteString

import Morley.Client.RPC.Types
import Morley.Micheline (Expression, TezosInt64)

-- | Type class that provides interaction with tezos node via RPC
class (Monad m, MonadCatch m) => HasTezosRpc m where
  getHeadBlock :: m Text
  -- ^ Get hash of the current head block, this head hash is used in other
  -- RPC calls.
  getCounter :: Address -> m TezosInt64
  -- ^ Get address counter, which is required for both transaction sending
  -- and contract origination.
  getBlockConstants :: Text -> m BlockConstants
  -- ^ Get block constants that required by other RPC calls.
  getProtocolParameters :: m ProtocolParameters
  -- ^ Get protocol parameters that are for limits calculations.
  runOperation :: RunOperation -> m RunOperationResult
  -- ^ Perform operation run, this operation doesn't require proper signing.
  -- As a result it returns burned gas and storage diff (also list of originated
  -- contracts but their addresses are incorrect due to the fact that operation
  -- could be not signed properly) or indicates about operation failure.
  preApplyOperations :: [PreApplyOperation] -> m [RunOperationResult]
  -- ^ Preapply list of operations, each operation has to be signed with sender
  -- secret key. As a result it returns list of results each of which has information
  -- about burned gas, storage diff size and originated contracts.
  forgeOperation :: ForgeOperation -> m HexJSONByteString
  -- ^ Forge operation in order to receive its hexadecimal representation.
  injectOperation :: HexJSONByteString -> m Text
  -- ^ Inject operation, note that this operation has to be signed before
  -- injection. As a result it returns operation hash.
  getContractScript :: Address -> m OriginationScript
  -- ^ Get code and storage of the desired contract. Note that both code and storage
  -- are presented in low-level Micheline representation.
  -- If the storage contains a @big_map@, then the expression will contain the @big_map@'s ID,
  -- not its contents.
  getContractStorage :: Address -> m Expression
  -- ^ Get storage of the desired contract. Note that storage
  -- are presented in low-level Micheline representation.
  -- If the storage contains a @big_map@, then the expression will contain the @big_map@'s ID,
  -- not its contents.
  getContractBigMap :: Address -> GetBigMap -> m GetBigMapResult
  -- ^ Get big map value by contract address.
  getBigMapValue :: Natural -> Text -> m Expression
  -- ^ Get big map value by the big map's ID and the hashed entry key.
  getBalance :: Address -> m Mutez
  -- ^ Get balance for given address.
  runCode :: RunCode -> m RunCodeResult
  -- ^ Emulate contract call. This RPC endpoint does the same as
  -- @tezos-client run script@ command does.
  getChainId :: m ChainId
  -- ^ Get current @ChainId@
