-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | This module contains servant types for tezos-node
-- RPC API.
module Morley.Client.RPC.API
  ( NodeMethods (..)
  , nodeMethods
  ) where

import Servant.API
  ((:<|>)(..), (:>), Capture, Get, JSON, Post, QueryParam, ReqBody, ToHttpApiData(..))
import Servant.Client.Core (RunClient, clientIn)

import Morley.Client.RPC.Types
import Morley.Micheline (Expression, TezosInt64, TezosMutez)
import Tezos.Address (Address, formatAddress)
import Util.ByteString

-- We put an empty line after each endpoint to make it easier to
-- visually distinguish them.
type NodeAPI =
  "chains" :> "main" :> "blocks" :> (
    -- GET
    "head" :> "hash" :> Get '[JSON] Text :<|>

    "head" :> "context" :> "contracts" :> Capture "contract" Address' :>
    "counter" :> Get '[JSON] TezosInt64 :<|>

    "head" :> "context" :> "contracts" :> Capture "contract" Address' :>
    "script" :> Get '[JSON] OriginationScript :<|>

    "head" :> "context" :> "contracts" :> Capture "contract" Address' :>
    "storage" :> Get '[JSON] Expression :<|>

    Capture "block_id" Text :> Get '[JSON] BlockConstants :<|>

    "head" :> "context" :> "constants" :> Get '[JSON] ProtocolParameters :<|>

    Capture "block_id" Text :> "operations" :> Get '[JSON] [[BlockOperation]] :<|>

    -- Despite this RPC is deprecated, it is said to be implemented quite sanely,
    -- and also we were said that it is not going to be removed soon.
    -- In babylonnet this entrypoint finds big_map with relevant key type and
    -- seeks for key in it; if there are multiple big_maps with the same key type,
    -- only one of them is considered (which one - it seems better not to rely on
    -- this info).
    "head" :> "context" :> "contracts" :> Capture "contract" Address' :>
    "big_map_get" :> ReqBody '[JSON] GetBigMap :> Post '[JSON] GetBigMapResult :<|>

    -- This endpoint supersedes the endpoint above.
    -- It takes a big_map ID instead of a contract ID, so it naturally supports
    -- contracts with multiple big maps.
    -- The 'script_expr' is the `Script-expression-ID-Hash` obtained by either:
    -- 1) calling `tezos-client hash data '123' of type int`.
    -- 2) or using 'Lorentz.Pack.valueToScriptExpr' and then base58 encode it.
    "head" :> "context" :> "big_maps"
      :> Capture "big_map_id" Natural
      :> Capture "script_expr" Text
      :> Get '[JSON] Expression :<|>

    "head" :> "context" :> "contracts"
    :> Capture "contract" Address' :> "balance" :> Get '[JSON] TezosMutez :<|>

    -- POST

    -- Turn a structured representation of an operation into a
    -- bytestring that can be submitted to the blockchain.
    "head" :> "helpers" :> "forge" :> "operations"
    :> ReqBody '[JSON] ForgeOperation :> Post '[JSON] HexJSONByteString :<|>

    -- Run an operation without signature checks.
    "head" :> "helpers" :> "scripts" :> "run_operation"
    :> ReqBody '[JSON] RunOperation :> Post '[JSON] RunOperationResult :<|>

    -- Simulate the validation of operations.
    "head" :> "helpers" :> "preapply" :> "operations"
    :> ReqBody '[JSON] [PreApplyOperation] :> Post '[JSON] [RunOperationResult] :<|>

    -- Run contract with given parameter and storage.
    "head" :> "helpers" :> "scripts" :> "run_code"
    :> ReqBody '[JSON] RunCode :> Post '[JSON] RunCodeResult
    ) :<|>

  "chains" :> "main" :> "chain_id" :> Get '[JSON] Text :<|>

  -- Inject a previously forged and signed operation into a node and broadcast it.
  "injection" :> "operation" :> QueryParam "chain" Text
  :> ReqBody '[JSON] HexJSONByteString
  :> Post '[JSON] Text

nodeAPI :: Proxy NodeAPI
nodeAPI = Proxy

data NodeMethods m = NodeMethods
  { getLastBlock :: m Text
  , getCounter :: Address -> m TezosInt64
  , getScript :: Address -> m OriginationScript
  , getStorage :: Address -> m Expression
  , getBlockConstants :: Text -> m BlockConstants
  , getProtocolParameters :: m ProtocolParameters
  , getHeadBlockOperations :: Text -> m [[BlockOperation]]
  , getBigMap :: Address -> GetBigMap -> m GetBigMapResult
  , getBigMapValue :: Natural -> Text -> m Expression
  , getBalance :: Address -> m TezosMutez
  , forgeOperation :: ForgeOperation -> m HexJSONByteString
  , runOperation :: RunOperation -> m RunOperationResult
  , preApplyOperations :: [PreApplyOperation] -> m [RunOperationResult]
  , runCode :: RunCode -> m RunCodeResult
  , getChainId :: m Text
  , injectOperation :: Maybe Text -> HexJSONByteString -> m Text
  }

nodeMethods :: forall m. RunClient m => NodeMethods m
nodeMethods = NodeMethods
  { getCounter = getCounter' . Address'
  , getScript = getScript' . Address'
  , getStorage = getStorage' . Address'
  , getBigMap = getBigMap' . Address'
  , getBalance = getBalance' . Address'
  , ..
  }
  where
    (getLastBlock :<|> getCounter' :<|> getScript' :<|> getStorage' :<|> getBlockConstants :<|>
      getProtocolParameters :<|> getHeadBlockOperations :<|> getBigMap' :<|> getBigMapValue :<|> getBalance' :<|>
      forgeOperation :<|> runOperation :<|> preApplyOperations :<|> runCode) :<|> getChainId :<|> injectOperation =
      nodeAPI `clientIn` (Proxy @m)

----------------------------------------------------------------------------
-- Instances and helpers
----------------------------------------------------------------------------

-- | We use this wrapper to avoid orphan instances.
newtype Address' = Address'
  { unAddress' :: Address }

instance ToHttpApiData Address' where
  toUrlPiece = formatAddress . unAddress'
