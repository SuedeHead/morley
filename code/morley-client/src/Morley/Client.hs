-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Morley client that connects with real Tezos network through RPC and tezos-client binary.
-- For more information please refer to README.
module Morley.Client
  ( -- * Command line parser
    parserInfo
  , clientConfigParser

  -- * Client monad and environment
  , MorleyClientM
  , MorleyClientConfig (..)
  , MorleyClientEnv' (..)
  , MorleyClientEnv
  , runMorleyClientM
  , mkMorleyClientEnv
  -- ** Lens
  , mceTezosClientL
  , mceLogActionL
  , mceSecretKeyL
  , mceClientEnvL

  -- * High-level actions
  , module Morley.Client.Action

  -- * RPC
  , HasTezosRpc (..)
  , getContract

  -- ** Errors
  , ClientRpcError (..)
  , UnexpectedErrors (..)
  , IncorrectRpcResponse (..)
  , RunError (..)
  -- ** Getters
  , ValueDecodeFailure (..)
  , ValueNotFound (..)
  , BigMapId(..)
  , readContractBigMapValue
  , readBigMapValueMaybe
  , readBigMapValue
  -- ** AsRPC
  , AsRPC
  , deriveRPC
  , deriveRPCWithStrategy
  , deriveManyRPC
  , deriveManyRPCWithStrategy

  -- * @tezos-client@
  , Alias (..)
  , AliasHint (..)
  , aliasesSeries
  , AddressOrAlias (..)
  , addressResolved
  , HasTezosClient (..)
  , resolveAddress
  , TezosClientError (..)

  -- * Util
  , disableAlphanetWarning

  -- * Reexports
  , Opt.ParserInfo -- Needed for tests
  ) where

import qualified Options.Applicative as Opt

import Morley.Client.Action
import Morley.Client.App
import Morley.Client.Env
import Morley.Client.Init
import Morley.Client.Parser
import Morley.Client.RPC
import Morley.Client.RPC.AsRPC
import Morley.Client.TezosClient
import Morley.Client.Util
