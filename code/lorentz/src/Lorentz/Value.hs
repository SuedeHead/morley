-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Re-exports typed Value, CValue, some core types, some helpers and
-- defines aliases for constructors of typed values.
--
module Lorentz.Value
  ( Value
  , IsoValue (..)
  , WellTypedIsoValue

    -- * Primitive types
  , Integer
  , Natural
  , MText
  , Bool (..)
  , ByteString
  , Address
  , EpAddress (..)
  , Mutez
  , Timestamp
  , ChainId
  , KeyHash
  , PublicKey
  , Signature
  , Set
  , Map
  , M.BigMap (..)
  , M.Operation
  , Maybe (..)
  , List
  , ContractRef (..)
  , TAddress (..)
  , FutureContract (..)

  , M.EpName
  , pattern M.DefEpName
  , EntrypointCall
  , SomeEntrypointCall

    -- * Constructors
  , toMutez
  , zeroMutez
  , oneMutez
  , mt
  , timestampFromSeconds
  , timestampFromUTCTime
  , timestampQuote

    -- * Conversions
  , M.coerceContractRef
  , callingTAddress
  , callingDefTAddress
  , ToAddress (..)
  , ToTAddress (..)
  , ToContractRef (..)
  , FromContractRef (..)
  , convertContractRef

    -- * Misc
  , Show
  , Default (..)
  , Label (..)
  , PrintAsValue (..)

  -- * Re-exports
  , module ReExports
  ) where

import Data.Constraint ((\\))
import Data.Default (Default(..))

import Fmt (Buildable(..))
import Lorentz.Address
import Lorentz.Constraints.Scopes
import Michelson.Text
import Michelson.Typed (EntrypointCall, IsoValue(..), SomeEntrypointCall, Value, WellTypedIsoValue)
import qualified Michelson.Typed as M
import Michelson.Typed.Haskell.Compatibility as ReExports
import Tezos.Core
  (ChainId, Mutez, Timestamp, oneMutez, timestampFromSeconds, timestampFromUTCTime, timestampQuote,
  toMutez, zeroMutez)
import Tezos.Crypto (KeyHash, PublicKey, Signature)
import Util.CustomGeneric as ReExports
import Util.Label (Label(..))

type List = []

-- | Provides 'Buildable' instance that prints Lorentz value via Michelson's
-- 'Value'.
--
-- Result won't be very pretty, but this avoids requiring 'Show' or
-- 'Buildable' instances.
newtype PrintAsValue a = PrintAsValue a

instance NicePrintedValue a => Buildable (PrintAsValue a) where
  build (PrintAsValue a) = build (toVal a) \\ nicePrintedValueEvi @a
