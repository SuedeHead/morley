-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Commonly used parts of regular Prelude.
module Lorentz.Prelude
  ( ($)
  , (.)
  , type ($)
  , Eq
  , Ord
  , Bounded (..)
  , Semigroup (..)
  , Monoid (..)
  , Generic
  , Text
  , Either (..)
  , Maybe (..)
  , Proxy (..)
  , fromString
  , undefined
  , error
  ) where
