-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Common primitives.
module Lorentz.Common
  ( Entrypoint
  , Entrypoint_
  ) where

import Lorentz.Base

-- | Single entrypoint of a contract.
--
-- Note that we cannot make it return @[[Operation], store]@ because
-- such entrypoint should've been followed by @pair@, and this is not
-- possible if entrypoint implementation ends with 'failWith'.
type Entrypoint param store = '[ param, store ] :-> ContractOut store

-- | Version of 'Entrypoint' which accepts no argument.
type Entrypoint_ store = '[ store ] :-> ContractOut store
