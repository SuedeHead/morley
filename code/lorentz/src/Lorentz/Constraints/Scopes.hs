-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE UndecidableSuperClasses #-}

-- | Scope-related constraints used in Lorentz.
--
-- This contains constraints from 'Michelson.Typed.Scope' modified for use
-- in Lorentz.
module Lorentz.Constraints.Scopes
  ( -- * Grouped constraints
    NiceComparable
  , NiceConstant
  , NiceFullPackedValue
  , NicePackedValue
  , NiceParameter
  , NicePrintedValue
  , NiceStorage
  , NiceUnpackedValue

  , niceParameterEvi
  , niceStorageEvi
  , niceConstantEvi
  , nicePackedValueEvi
  , niceUnpackedValueEvi
  , nicePrintedValueEvi

    -- * Individual constraints (internals)
  , CanHaveBigMap
  , KnownValue
  , NoOperation
  , NoContractType
  , NoBigMap

  , -- * Re-exports
    withDict
  ) where

import Data.Constraint (evidence, trans, weaken2)

import Lorentz.Annotation (HasAnnotation)
import Michelson.Typed

-- We write these constraints as class + instance, rather than
-- type aliases, in order to allow their partial application.

-- | Gathers constraints, commonly required for values.
class (IsoValue a, Typeable a) => KnownValue a
instance (IsoValue a, Typeable a) => KnownValue a

-- | Ensure given type does not contain "operation".
class (IsoValue a, ForbidOp (ToT a)) => NoOperation a
instance (IsoValue a, ForbidOp (ToT a)) => NoOperation a

class (IsoValue a, ForbidContract (ToT a)) => NoContractType a
instance (IsoValue a, ForbidContract (ToT a)) => NoContractType a

class (IsoValue a, ForbidBigMap (ToT a)) => NoBigMap a
instance (IsoValue a, ForbidBigMap (ToT a)) => NoBigMap a

class (IsoValue a, HasNoNestedBigMaps (ToT a)) => CanHaveBigMap a
instance (IsoValue a, HasNoNestedBigMaps (ToT a)) => CanHaveBigMap a

-- | Constraint applied to any part of parameter type.
--
-- Note that you don't usually apply this constraint to the whole parameter,
-- consider using 'Lorentz.Constraints.Derivative.NiceParameterFull' in such case.
--
-- Using this type is justified e.g. when calling another contract, there
-- you usually supply an entrypoint argument, not the whole parameter.
type NiceParameter a = (KnownValue a, ProperParameterBetterErrors (ToT a))

type NiceStorage a =
  (HasAnnotation a, KnownValue a, ProperStorageBetterErrors (ToT a))

type NiceConstant a = (KnownValue a, ProperConstantBetterErrors (ToT a))

type NicePackedValue a = (KnownValue a, ProperPackedValBetterErrors (ToT a))

type NiceUnpackedValue a = (KnownValue a, ProperUnpackedValBetterErrors (ToT a))

type NiceFullPackedValue a = (NicePackedValue a, NiceUnpackedValue a)

type NicePrintedValue a = (KnownValue a, ProperPrintedValBetterErrors (ToT a))

type NiceComparable n = (KnownValue n, Comparable (ToT n))

niceParameterEvi :: forall a. NiceParameter a :- ParameterScope (ToT a)
niceParameterEvi =
  properParameterEvi @(ToT a) `trans` weaken2

niceStorageEvi :: forall a. NiceStorage a :- StorageScope (ToT a)
niceStorageEvi =
  Sub (evidence $ properStorageEvi @(ToT a))

niceConstantEvi :: forall a. NiceConstant a :- ConstantScope (ToT a)
niceConstantEvi =
  properConstantEvi @(ToT a) `trans` weaken2

nicePackedValueEvi :: forall a. NicePackedValue a :- PackedValScope (ToT a)
nicePackedValueEvi =
  properPackedValEvi @(ToT a) `trans` weaken2

niceUnpackedValueEvi :: forall a. NiceUnpackedValue a :- UnpackedValScope (ToT a)
niceUnpackedValueEvi =
  properUnpackedValEvi @(ToT a) `trans` weaken2

nicePrintedValueEvi :: forall a. NicePrintedValue a :- PrintedValScope (ToT a)
nicePrintedValueEvi =
  properPrintedValEvi @(ToT a) `trans` weaken2
