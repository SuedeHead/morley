-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Some derivative constraints.
--
-- They are moved to separate module because they need to lie quite high in
-- modules dependencies graph (unlike "Lorentz.Constraints.Scopes").
module Lorentz.Constraints.Derivative
  ( NiceParameterFull
  ) where

import Lorentz.Entrypoints.Core

-- | Constraint applied to a whole parameter type.
type NiceParameterFull cp = (Typeable cp, ParameterDeclaresEntrypoints cp)
