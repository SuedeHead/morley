#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

sender="tz1NpbW6KL2B9ELL2CEbUj1jZKHNpoC2hYdS"
receiver="tz1hHGTh6Yk4k7d2PiTcBUeMvw6fJCFikedv"
tempdir="$(mktemp -d --tmpdir="$PWD")"
tezos_client_args=(-A "$TASTY_NETTEST_NODE_ADDR" -P "$TASTY_NETTEST_NODE_PORT" -d "$tempdir")
tezos-client "${tezos_client_args[@]}" import secret key moneybag "$TASTY_NETTEST_IMPORT_SECRET_KEY" --force

@test "Morley test implicit transfer logic with 'Unit' parameter" {
    morley-client "${tezos_client_args[@]}" transfer \
                  --from "$sender" \
                  --to "$receiver" --amount 1 --parameter 'Unit'
}

@test "Morley test implicit transfer logic with non-'Unit' parameter" {
    run morley-client "${tezos_client_args[@]}" transfer \
        --from "$sender" \
        --to "$receiver" --amount 1 --parameter 0
    [ "$status" -ne 0 ]
}

@test "Morley test originated contract transfer logic" {
    contract_alias="fresh-contract"
    morley-client "${tezos_client_args[@]}" originate \
                  --contract ./contracts/tezos_examples/attic/add1.tz \
                  --contract-name "$contract_alias" \
                  --initial-storage '0' \
                  --from "$sender"

    morley-client "${tezos_client_args[@]}" transfer \
                  --from "$sender" \
                  --to "$contract_alias" \
                  --amount 1 \
                  --parameter 1
}

@test "Morley test manual fee provision" {
    run morley-client "${tezos_client_args[@]}" transfer \
        --from "$sender" \
        --to "$receiver" --amount 1 --parameter 0 --fee 1000000
    [ "$status" -ne 0 ]
}

@test "Morley-client should report the same balance as tezos-client" {
    # A balance >= 4294.967296 ꜩ is required to trigger #302.
    morley_client_balance=$(morley-client "${tezos_client_args[@]}" get-balance --addr "$sender")
    tezos_client_balance=$(tezos-client "${tezos_client_args[@]}" get balance for "$sender")
    echo "morley_client_balance=${morley_client_balance}" > test-morley-client.log
    echo "tezos_client_balance=${tezos_client_balance}" >> test-morley-client.log
    [ "$morley_client_balance" = "$tezos_client_balance" ]
}
