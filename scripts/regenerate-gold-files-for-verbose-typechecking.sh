#!/usr/bin/env sh

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

tests_dir="contracts/verbose-typecheck"
for test_file in "$tests_dir"/*.tz "$tests_dir"/*.mtz; do
    base_file="${test_file%.*}"
    gold_file="${base_file}.gold"
    cat > "$gold_file" <<EOF
# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

EOF
    morley typecheck --contract "$test_file" --verbose 2>&1 \
        | sed 's/[[:space:]]\+$//' >> "$gold_file"  \
        || true
done

