#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

setup () {
  bin_dir="tmp"
  contract="contracts/tezos_examples/attic/add1.tz"
  contract_with_ep="contracts/entrypoints/contract1.mtz"
  db="bats_db.json"
  if [[ $USE_LATEST == "True" ]]; then
    morley="./scripts/morley.sh --latest"
  else
    morley="./scripts/morley.sh"
  fi
  genesisAddress="tz1f1S7V2hZJ3mhj47djb5j1saek8c2yB2Cx"
  HOME="$(mktemp -d --tmpdir="$PWD")"
}

@test "invoking ./morley.sh without arguments" {
  $morley
}

@test "invoking ./morley.sh --help" {
  $morley --help
}

@test "invoking ./morley.sh --version" {
  $morley --version
}

@test "invoking ./morley.sh typecheck with correct arguments" {
  $morley typecheck --contract "$contract" --verbose
}

@test "invoking ./morley.sh run with correct arguments" {
  $morley run --contract "$contract" \
    --db "$db" --storage 1 --parameter 1 --amount 1 --verbose \
    --now 0 --max-steps 1000 --balance 100 --write
  $morley run --contract "$contract_with_ep" \
    --db "$db" --storage 1 --parameter 1 --amount 1 --entrypoint a \
    --verbose --now 0 --max-steps 1000 --balance 100 --write
  rm "$db"
}


@test "invoking ./morley.sh originate with correct arguments" {
  $morley originate --contract "$contract" \
    --db "$db" \
    --originator $genesisAddress \
    --delegate $genesisAddress \
    --storage 1 --balance 1 --verbose
  rm "$db"
}

@test "invoking ./morley.sh originate without db argument" {
  $morley originate --contract "$contract" \
    --originator $genesisAddress \
    --delegate $genesisAddress \
    --storage 1 --balance 1 --verbose
}

@test "invoking ./morley.sh transfer with correct arguments" {
  $morley transfer --db "$db" \
    --to $genesisAddress \
    --sender $genesisAddress \
    --parameter 1 --amount 1 --now 0 --max-steps 1000 \
    --verbose --dry-run
}

@test "invoking ./morley.sh print with correct arguments" {
  $morley print --contract "$contract"
}

@test "run contract with big_map passed in storage" {
  $morley run --contract \
    contracts/big_map_in_storage.tz  --storage 'Pair {} 0' --parameter 1
  $morley run --contract \
    contracts/big_map_in_storage.tz  --storage 'Pair {Elt 3 5; Elt 4 6} 0' --parameter 1
  run $morley run --contract \
    contracts/big_map_in_storage.tz  --storage 'Pair {Elt 4 5; Elt 3 6} 0' --parameter 1
  [ "$status" -ne 0 ]
}

@test "invoking morley to typecheck contract with cyrillic comments from stdin" {
  cat contracts/add1_with_cyrillic_comments.tz | $morley typecheck
}

@test "invoking morley to print contract with cyrillic comments" {
  $morley print --contract contracts/add1_with_cyrillic_comments.tz
}

@test "invoking morley expecting it to fail to parse an invalid contract with non-ascii characters in error message" {
  $morley typecheck --contract contracts/unparsable/non_ascii_error.mtz 2>&1 | grep 'unknown type'
}

@test "invoking ./morley.sh print -o" {
  $morley print --contract contracts/tezos_examples/opcodes/first.tz -o output.mtz
  rm output.mtz
}

@test "invoking ./morley.sh optimize -o" {
  $morley print --contract contracts/tezos_examples/opcodes/first.tz -o output.mtz
  rm output.mtz
}

@test "invoking morley to analyze tezos_examples/attic/forward.tz" {
  $morley analyze --contract contracts/tezos_examples/attic/forward.tz | grep '"buyer": 2'
}

@test "invoking morley repl" {
  $morley repl --map-dir stacks --latest <<EOF
push nat 5
:dumpstack stacks/stack.json
:help
:stack
:loadstack stacks/stack.json
:clear
:quit
EOF
  grep "nat" stacks/stack.json
}
