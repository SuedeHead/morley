#!/usr/bin/env python3.7

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# Run a transaction and measure operation size of provided value.
#
# This script requires a deployed instance of "contracts/measure_op_size.tz"
# contract (on local runs operation size limit cannot fire).
# At the moment of writting "KT1QBd4jp5GrfsuXMDrebEQ7gxddQGNvLn6C" contract
# can be used (pass it as '--destination' parameter).

import argparse
import subprocess
import re
import sys

from templates.common import (babylonnet_client, transfer)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Deploy our contract')
    parser.add_argument('--originator', required=True,
        metavar='ADDRESS', help="Some address with money")
    parser.add_argument('--destination', required=True,
        metavar='ADDRESS', help="Test contract")
    parser.add_argument('--param', required=True,
        metavar='MICHELSON CODE', help="Code which we measure operation size of")

    args = parser.parse_args()

    # Passing very long string allows reaching operation size limit without
    # hiting gas limit
    param='PUSH string "' + "a" * 20000 + '"; DROP; ' + args.param

    # Operation size if args.param is empty
    base_cost = 20173

    cmd = babylonnet_client + [
        "transfer", "0", "from", args.originator, "to", args.destination,
        "--burn-cap", "30", "--arg", '{ ' + param + ' }'
        ]

    output = None
    try:
        process = subprocess.run(cmd, check=True, capture_output=True)
    except subprocess.CalledProcessError as e:
        output = e.output.decode("utf-8")

    op_size_or_err = re.search("Oversized operation \(size: (\d+)", output)
    if op_size_or_err is None:
       print("Unexpected output:")
       print(output)
       exit(1)

    op_size = int(op_size_or_err.group(1))
    print (op_size - base_cost)
