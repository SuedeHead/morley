# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# Source: https://gitlab.com/morley-framework/morley/-/merge_requests/302#note_308133530
# Now I wonder about a more complex case. Let's originate two contracts with 0 balance
# and call them A and B. Someone transfers 100 XTZ to A. Inside A BALANCE should return 100.
# A returns an operation that transfers 30 XTZ to B, so B is called. Inside B BALANCE should return 30.
# Then B returns an operation that transfers 5 XTZ to A.
# I guess inside A BALANCE now should return 75. And let's say that then A returns no operations,
# so execution stops here.
# I suppose A should have list mutez storage and put results of two BALANCE calls there.

parameter address;
storage (list mutez);
code {UNPAIR;

      PUSH mutez 90;
      BALANCE;
      IFCMPGT { CONTRACT unit;
                IF_NONE { NIL operation; }
                        { DIP { NIL operation; };
                          PUSH mutez 30;
                          UNIT;
                          TRANSFER_TOKENS;
                          CONS;
                        };
              }
              { DROP;
                NIL operation
              };

      DIP { BALANCE;
            CONS;
          };

      PAIR;}
