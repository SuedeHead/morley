# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

let {

  stuff :: '[unit] -> '[unit] = {
    # Remove nesting
    { PUSH string "nested"; }

    # DIP { DROP } → SWAP; DROP
    DIP DROP;
    STACKTYPE '[string];

    # IF {} {} → DROP
    # DUP is needed to prevent `IF TRUE` optimization
    PUSH bool True; DUP; IF {} {};
    STACKTYPE '[bool, string];

    # Doing nothing:
    SWAP; SWAP;
    PUSH int 1; DROP;
    DUP; DROP;
    UNIT; DROP;
    { SENDER; DROP; }
    # … there are many cases

    # DUP; SWAP → DUP
    DUP; SWAP;
    STACKTYPE '[bool, bool, string];

    # Useless DIP
    NOW; DIP { SWAP };
    STACKTYPE '[timestamp, bool, bool, string];

    # Branch shortcut
    LEFT key; IF_LEFT { PACK; } { PACK; PACK; };
    RIGHT key; IF_RIGHT { PACK; } { PACK; PACK; };
    STACKTYPE '[bytes, bool, bool, string];

    # Compare with 0
    PUSH int 5; PUSH int 0; COMPARE; EQ;
    STACKTYPE '[bool, bytes, bool, bool, string];

    # Simple drop
    DROP 0;
    DROP 1; # not optimized yet
    STACKTYPE '[bytes, bool, bool, string];

    # Simple DIPs
    DIP { };
    DIP 0 { DROP };
    DIP 1 { DROP }; # not optimized yet
    STACKTYPE '[bool, string];

    # Specific PUSHes
    PUSH unit Unit;
    PUSH (map int int) { };
    PUSH (set nat) { };
    STACKTYPE '[set nat, map int int, unit, bool, string];

    # Adjacent DIPs
    DIP { SWAP; };
    DIP { SWAP; };

    # PAIR; UNPAIR → nothing
    PAIR; UNPAIR;

    # Redundant SWAP due to commutativity
    PUSH nat 5; PUSH int 7; SWAP; ADD;
    PUSH nat 5; PUSH int 7; SWAP; MUL;
    CMPEQ; PUSH bool True; SWAP; XOR;

    # Cleanup the stack
    # Ideally it should be optimized to `DROP 6`, see #299
    DROP; DROP; DROP; DROP; DROP; DROP 1; UNIT;
  };

};

storage unit;
parameter unit;
code {
  UNPAIR;
  STACKTYPE '[unit, unit];

  # stuff inside DIP
  DIP { stuff; DROP };
  STACKTYPE '[unit];

  # Inline stuff
  stuff;
  STACKTYPE '[unit];

  # End with FAILWITH for simplicity
  FAILWITH;
}
