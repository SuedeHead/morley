# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

parameter unit;
storage (pair (list int) (int:count));
code {CDR;
      UNPAIR;

      PUSH (int:aux) 0; # push aux integer to make stack deeper

      EMPTY_MAP string int;
      PUSH int 42;
      SOME;
      PUSH string "hello";
      UPDATE;
      PUSH int 256;
      SOME;
      PUSH string "lala";
      UPDATE;

      # Count the number of elements in the map.
      # pair string (int:val) : (int:aux) : (list int) : (int:count) : 'A ->
      #   (int:val + 1) : (int:aux) : (list int) : (int:count + 1) : 'A
      MAP {CDR;
           PUSH int 1;
           ADD;
           # modify something deep in the stack
           DIP 3 { PUSH int 1; ADD; };};

      # Collect map elements to the list in the second pass, to check that interpreter
      # traverses map in the correct order.
      # pair string (int:val) : (int:aux) : (list int) : (int:count) : 'A ->
      #   Unit : (int:aux) : (val : list int) : (int:count) : 'A
      MAP {CDR;
           SWAP;
           DIP { CONS };
           UNIT};

      DROP; # drop the map
      DROP; # and drop the aux integer

      PAIR;

      NIL operation;
      PAIR;};
