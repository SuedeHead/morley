# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

.PHONY: all test-all stylish lint clean

# Build target from the common utility Makefile
MAKEU = $(MAKE) -f code/make/Makefile

ifeq (${MORLEY_USE_CABAL},1)
	build_all = cabal new-build --enable-tests --enable-benchmarks -O0 all
	test_all = cabal new-test -O0 all
	clean_all = cabal new-clean
	bench_all = cabal new-bench
else
	test_all = $(MAKEU) test PACKAGE=""
	build_all = $(MAKEU) PACKAGE=""
	clean_all = stack clean
	bench_all = stack bench
endif

all:
	$(call build_all,)
test-all:
	$(call test_all,)
stylish:
	find code/ examples/ -name '.stack-work' -prune -o -name '*.hs' -exec stylish-haskell -i {} \;
lint:
	scripts/lint.sh
clean:
	$(call clean_all,)
bench:
	$(call bench_all,)
