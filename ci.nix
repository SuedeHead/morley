# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

rec {
  sources = import ./nix/sources.nix;
  # pkgs for builtins.currentSystem
  pkgs = import ./nix/nixpkgs-with-haskell-nix.nix {};
  xrefcheck = import sources.xrefcheck;
  weeder-hacks = import sources.haskell-nix-weeder { inherit pkgs; };
  tezos-client = (import "${sources.tezos-packaging}/nix/build/pkgs.nix" {}).ocamlPackages.tezos-client;

  # all local packages and their subdirectories
  # we need to know subdirectories for weeder and for cabal check
  local-packages = [
    { name = "lorentz"; subdirectory = "code/lorentz"; }
    { name = "morley"; subdirectory = "code/morley"; }
    { name = "morley-client"; subdirectory = "code/morley-client"; }
    { name = "cleveland"; subdirectory = "code/cleveland"; }
    { name = "morley-prelude"; subdirectory = "code/morley-prelude"; }
    { name = "autodoc-sandbox"; subdirectory = "code/autodoc-sandbox"; }
  ];

  # names of all local packages
  local-packages-names = map (p: p.name) local-packages;

  # source with gitignored files filtered out
  projectSrc = pkgs: pkgs.haskell-nix.haskellLib.cleanGit {
    name = "morley";
    src = ./.;
  };

  # haskell.nix package set
  # parameters:
  # - release -- 'true' for master and producion branches builds, 'false' for all other builds.
  #   This flag basically disables weeder related files production, haddock and enables stripping
  # - commitSha, commitDate -- git revision info used during compilation of packages with autodoc
  # - optimize -- 'true' to enable '-O1' ghc flag, we intend to use it only in production branch
  hs-pkgs = hs-pkgs-template pkgs;
  hs-pkgs-template = pkgs: { release, optimize ? false, commitSha ? null, commitDate ? null}:
    let
      haskell-nix = pkgs.haskell-nix;
    in
      haskell-nix.stackProject {
      src = projectSrc pkgs;

      # use .cabal files for building because:
      # 1. haskell.nix fails to work for package.yaml with includes from the parent directory
      # 2. with .cabal files haskell.nix filters out source files for each component, so only the changed components will rebuild
      ignorePackageYaml = true;

      modules = [
        # common options for all local packages:
        {
          packages = pkgs.lib.genAttrs local-packages-names (packageName: {
            package.ghcOptions = with pkgs.lib; concatStringsSep " " (
              # we use O1 for production binaries in order to improve their performance
              # for end-users
              [ (if optimize then "-O1" else "-O0") "-Werror"]
              # produce *.dump-hi files, required for weeder:
              ++ optionals (!release) ["-ddump-to-file" "-ddump-hi"]
              # do not erase any 'assert' calls
              ++ optionals (!release) ["-fno-ignore-asserts"]
            );
            dontStrip = !release;  # strip in release mode, reduces closure size
            doHaddock = !release;  # don't haddock in release mode

            # in non-release mode collect all *.dump-hi files (required for weeder)
            postInstall = if release then null else weeder-hacks.collect-dump-hi-files;
          });
        }

        {
          # don't haddock dependencies
          doHaddock = false;

          packages.autodoc-sandbox = {
            preBuild = ''
              export MORLEY_DOC_GIT_COMMIT_SHA=${if release then pkgs.lib.escapeShellArg commitSha else "UNSPECIFIED"}
              export MORLEY_DOC_GIT_COMMIT_DATE=${if release then pkgs.lib.escapeShellArg commitDate else "UNSPECIFIED"}
            '';
          };
        }
      ];
  };

  hs-pkgs-development = hs-pkgs { release = false; };

  # component set for all local packages like this:
  # { morley = { library = ...; exes = {...}; tests = {...}; ... };
  #   morley-prelude = { ... };
  #   ...
  # }
  packages = pkgs.lib.genAttrs local-packages-names (packageName: hs-pkgs-development."${packageName}".components);

  # returns a list of all components (library + exes + tests + benchmarks) for a package
  get-package-components = pkg: with pkgs.lib;
    optional (pkg ? library) pkg.library
    ++ attrValues pkg.exes
    ++ attrValues pkg.tests
    ++ attrValues pkg.benchmarks;

  # per-package list of components
  components = pkgs.lib.mapAttrs (pkgName: pkg: get-package-components pkg) packages;

  # a list of all components from all packages in the project
  all-components = with pkgs.lib; flatten (attrValues components);

  haddock = with pkgs.lib; flatten (attrValues
    (mapAttrs (pkgName: pkg: optional (pkg ? library) pkg.library.haddock) packages));

  # run some executables to produce contract documents
  contracts-doc = { release, commitSha ? null, commitDate ? null }@releaseArgs: pkgs.runCommand "contracts-doc" {
    buildInputs = [
      (hs-pkgs releaseArgs).autodoc-sandbox.components.exes.autodoc-sandbox-registry
    ];
  } ''
    mkdir $out
    cd $out
    mkdir autodoc
    autodoc-sandbox-registry document --name AutodocSandbox --output \
      autodoc/AutodocSandboxContract.md
  '';

  # release version of morley executable
  morley-release = { production ? false }:
    (hs-pkgs { release = true; optimize = production; }).morley.components.exes.morley;

  # docker image for morley executable
  # 'creationDate' -- creation date to put into image metadata
  morleyDockerImage = { creationDate, tag ? null, production ? false }:
    pkgs.dockerTools.buildImage {
    name = "morley";
    contents = morley-release { inherit production; };
    created = creationDate;
    inherit tag;
  };

  # nixpkgs has weeder 2, but we use weeder 1
  weeder-legacy = pkgs.haskellPackages.callHackageDirect {
    pkg = "weeder";
    ver = "1.0.9";
    sha256 = "0gfvhw7n8g2274k74g8gnv1y19alr1yig618capiyaix6i9wnmpa";
  } {};

  # a derivation which generates a script for running weeder
  weeder-script = weeder-hacks.weeder-script {
    hs-pkgs = hs-pkgs-development;
    local-packages = local-packages;
    weeder = weeder-legacy;
  };

  # checks if all packages are appropriate for uploading to hackage
  run-cabal-check = pkgs.runCommand "cabal-check" { buildInputs = [ pkgs.cabal-install ]; } ''
    ${pkgs.lib.concatMapStringsSep "\n" ({ name, subdirectory }: ''
      echo 'Running `cabal check` for ${name}'
      cd ${projectSrc pkgs}/${subdirectory}
      cabal check
    '') local-packages}

    touch $out
  '';

  # nixpkgs has an older version of stack2cabal which doesn't build
  # with new libraries, use a newer version
  stack2cabal = pkgs.haskellPackages.callHackageDirect {
    pkg = "stack2cabal";
    ver = "1.0.11";
    sha256 = "00vn1sjrsgagqhdzswh9jg0cgzdgwadnh02i2fcif9kr5h0khfw9";
  } { };
}
